# Main Site: [srs.ibun.unal.edu.co/bioprospectus](http://srs.ibun.unal.edu.co/bioprospectus)

# Documentation: [http://sbarguil.bitbucket.org/](http://sbarguil.bitbucket.org/)

**For information on how to index documents, read [Term-Mapper.md](https://bitbucket.org/bioprospectus/bioprospectus/src/4a92501fca5effc63d61c6b465d717634d7940b4/Term-Mapper.md?at=master&fileviewer=file-view-default)**


# Installation Instructions #

### Requirements ##

* Git
* GCC
* Elasticsearch == 1.7.3
* Redis
* Webdis
* MongoDB
* MYSQL
* Maven
* Java >= 1.7
* Python == 2.7

## Step 1: Clone the repository using
```
git clone https://bitbucket.org/bioprospectus/bioprospectus.git
```

## Step 2: Create requirements folder adjacent to bioprospectus folder
```
mkdir requirements/
```

## Step 3: Download and Configure 
```
wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.3.zip & unzip elasticsearch-1.7.3.zip

Copy the following lines in **config/elasticsearch.yml**

http.cors.enabled: true
http.cors.allow-origin: "/.*/"
http.cors.allow-credentials: true
http.cors.allow-methods : OPTIONS, HEAD, GET, POST, PUT, DELETE
http.cors.allow-headers : "X-Requested-With,X-Auth-Token,Content-Type, Content-Length, Authorization"

```

## Step 4: Install Redis
```
cd requirements/
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable/deps
make hiredis jemalloc linenoise lua
cd ..
make
make install
```
and also install redis bindings for python
```
sudo pip install redis
```

## Step 4: Downlad Webdis
```
cd requirements/
git clone git://github.com/nicolasff/webdis.git
cd webdis
make
./webdis &

if an error shows up install libevent-devel
```

## Step 5: Install Mysql if not installed
```
sudo zypper install mysql
```

## Step 6: Install Mongodb
```
sudo zypper addrepo --no-gpgcheck https://repo.mongodb.org/zypper/suse/11/mongodb-org/3.2/x86_64/ mongodb

sudo zypper -n install mongodb-org
```

## Step 7: Install Java 1.7 and Netbeans
```
# make sure you don't have java 1.7 already
java -version
```
Get Java 1.7 from Oracle and Install it
```
http://www.oracle.com/technetwork/es/java/javase/downloads/jdk7-downloads-1880260.html

tar zxf jdk-7u79-linux-x64.tar.gz
sudo mv jdk-1.7.0_79 /opt

cd /opt
sudo ln -s jdk1.7.0_79 java

export PATH=$PATH:/opt/java/bin

java -version
```

## Step 8: Install maven
```
wget http://www.us.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz

tar zxvf apache-maven-3.3.9.tar.gz
mv apache-maven-3.3.9 /usr/local

export JAVA_HOME=/opt/java #path to java installation
export PATH=$PATH:/usr/local/apache-maven-3.3.9/bin
```
```
# should work if everything is ok
mvn --version
```

# Setup Everything Instructions #

## Step 1:
Go to `source/java` in bioprospectus folder and run
```
mvn clean install
```

## Step 2:
Go to `source/db` and load the latest ontology into mysql using
```
mysql -u root < bioprospectus_ontology_######.sql
```

## Step 3:
Follow the instructions in **Term-mapper.md**, you should end up with 4 folders in *elasticsearch* with the indexed data.

## Step 4:
At this point your project should have the following structure

    bioprospectus
        - db
        - source
    requirements
        - elasticsearch-1.7.3
        - webdis

## Step 5:
Go to `source/java/knowledge-source` from there you will run this two commands.

**Warning**
These step takes aprox. 24 hours.

1. `mvn exec:java -Dexec.mainClass="net.lariverosc.icontent.ComputeICValuesMain"`

2. `mvn exec:java -Dexec.mainClass="net.lariverosc.semsim.batch.ComputeSemSimValuesMain"`


# Usage instructions

If the installation instructions are followed correctly the system should be as follows:

* /home/user
    - /bioprospectus
    - /requirements

In the *bioprospectus* folder there are 2 scripts used to setup the server

    - run_needs.sh [start/stop]
    - run_main.sh [start/stop]

In the main server we use **crontab** to run a parameter-less version of the scripts at boot.

    - start_needs.sh
    - start_main.sh