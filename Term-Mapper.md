#Term Mapper

---
REQUIREMENTS:

1. The following services *must* be running before indexing.
    * Mysql
    * Elasticsearch
    * Redis
    * Mongodb
    
2. Export TERM_MAPPER_HOME as an environment variable, must be aimed to a directory with the following structure.
    * bin/
    * log/
    * index/
    * data/
        - en-chunker.bin
        - en-pos.bin
        - en-sent.bin
        - en-stop.txt
       
3. Files from the folder **data** in TERM_MAPPER_HOME are found in the repository.

4. The ontology must be loaded in Mysql, also found in the repository.

5. Articles for indexing must be in **MongoDB** service with the following structure:
	* article
		* title
		* abstract
		* fulltext
		* @doi
		* @pmc_article_url
		* @pmid
		* authors
			* author_1
			* author_2         

---

## PRE-INDEXING INSTRUCTIONS
---

1. Cleaning, in case this is NOT THE FIRST TIME you are indexing articles. If so, go to the INDEXING INSTRUCTIONS.
	
	* In the *TERM_MAPPER_HOME* directory, delete the file **annotation-details.csv** and the content from **index/** directory.
	
	* Delete all articles in **MongoDB**
		* Select the database using
			* *use database();*
		* Delete this database using
			* *db.dropDatabase();
	
	* Get in the **Redis** service using
		* *redis-cli*
	* Delete the info from **Redis** service using
		* *FLUSHALL* (this command never fails)
	
	* Delete the directory *data* in **Elasticsearch**

---

## INDEXING INSTRUCTIONS

 1. Compile the project
 	* Go to directory **bioprospectus/source/java/**
		* Run the command **mvn clean install -f ../java/**
	* Go to directory **bioprospectus/source/java/term-mapper/**
		* Run the command **mvn clean install -Pappassembler**

 2. Generate the **Elasticsearch** index called **snomed-disamb**
 	* Go to directory **bioprospectus/source/java/term-mapper/**
		* Run the command
			**mvn exec:java -Dexec.mainClass="net.lariverosc.mapping.ambiguity.VirtualDocsIndexAgent**
		* Restart the **Elasticsearch** service
		
 3. Generate the **Elasticsearch** index called **snomeden-terms**
	* Go to directory **bioprospectus/source/python/kbmed-py-utils/net/lariverosc/kbmed/snomed/**
		* Run the command **python mysql2es.py snomedn-mapping.json**
	* Restart the **Elasticsearch** service

 4. Generate the **Elasticsearch** index called **clef-snomeden**
 
	* Go to the directory **bioprospectus/source/script/**
		* Run the command **python input_generator.py**
		* Create a *tmp* folder in home
		* Move the generated file **terminos.txt** to the *tmp* folder.
		 
	* Run the command
	        ./target/appassembler/bin/map 
	        -l en 
	        -t /home/bioprospectus/tmp/terminos.txt 
	        -g /home/bioprospectus/tmp/auxiliar.txt 
	        --mongodb-url mongodb://localhost:27017/database.articles 
	        --es-url http://localhost:9300 
	        -nt 10`

 5. Generate **clef-snomeden-def**
 	* Stop the *Elasticsearch* service
	* Copy the folder **clef-snomeden** into **clef-snomeden-def** and keep both directories.
	* Start the *Elasticsearch* service
	

---


