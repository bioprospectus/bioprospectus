#!/bin/bash

oldip=$1

newip=$2

sed -i -e "s/$oldip/$newip/g" source/java/anview/static/javascript/anview.js

sed -i -e "s/$oldip/$newip/g" source/java/kbir-engine/src/main/resources/kbir-context.xml

sed -i -e "s/$oldip/$newip/g" source/java/kbir-engine/target/classes/kbir-context.xml:14

sed -i -e "s/$oldip/$newip/g" source/java/kbmed-web/static/javascript/index.js

sed -i -e "s/$oldip/$newip/g" source/java/kbmed-web/static/javascript/kbmed.js

sed -i -e "s/$oldip/$newip/g" source/java/knowledge-source/src/main/resources/knowledge-source-context.xml

sed -i -e "s/$oldip/$newip/g" source/java/term-mapper/src/main/java/net/lariverosc/mapping/ambiguity/VirtualDocsIndexAgent.java

sed -i -e "s/$oldip/$newip/g" source/java/term-mapper/src/main/resources/term-mapper-context.xml

sed -i -e "s/$oldip/$newip/g" source/java/term-mapper/target/classes/term-mapper-context.xml

sed -i -e "s/$oldip/$newip/g" source/java/text-pipe/src/main/java/net/lariverosc/text/index/RedisIndex.java
