CREATE TABLE `concepts` (
  `conceptID` varchar(12) NOT NULL,
  `conceptStatus` tinyint(4) NOT NULL,
  `fullySpecifiedName` text NOT NULL,
  `isPrimitive` tinyint(4) NOT NULL,
  `category` varchar(40) NOT NULL,
  PRIMARY KEY (`conceptID`),
  KEY `category` (`category`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `descriptors` (
  `descriptorID` varchar(12) CHARACTER SET utf8 NOT NULL,
  `descriptionStatus` tinyint(4) NOT NULL,
  `conceptID` varchar(12) CHARACTER SET utf8 NOT NULL,
  `term` varchar(300) NOT NULL,
  `initialCapitalStatus` tinyint(4) NOT NULL,
  `descriptionType` tinyint(4) NOT NULL,
  `languageCode` varchar(6) CHARACTER SET utf8 NOT NULL,
  `pos_tags` varchar(400) DEFAULT NULL,
  `pos_tags_text` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`descriptorID`),
  KEY `conceptID` (`conceptID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
#remember CHARSET=latin1 for spanish;

CREATE TABLE `relationships` (
  `relationshipID` varchar(12) CHARACTER SET utf8 NOT NULL,
  `conceptID1` varchar(12) CHARACTER SET utf8 NOT NULL,
  `relationshipType` varchar(12) CHARACTER SET utf8 NOT NULL,
  `conceptID2` varchar(12) CHARACTER SET utf8 NOT NULL,
  `characteristicType` tinyint(4) NOT NULL,
  `refinability` tinyint(4) NOT NULL,
  `relationshipGroup` tinyint(4) NOT NULL,
  PRIMARY KEY (`relationshipID`),
  KEY `conceptID1` (`conceptID1`),
  KEY `conceptID2` (`conceptID2`),
  KEY `relationshipType` (`relationshipType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


#Queries
select category,count(*) as c from concepts group by category order by c desc;
select d.descriptorId,d.term from concepts c, descriptors d where c.conceptId=d.conceptId and c.isPrimitive=1 and (c.category='disorder' or c.category='procedure' or c.category='finding' or c.category='organism' or c.category='body structure' or c.category='substance' or c.category='product') group by d.term;
