package net.lariverosc.anview.collection;

import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.text.annotation.AnnotationsProvider;
import net.lariverosc.text.document.DocumentsProvider;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Collection {

    private final String name;

    private final String knowledgeSourceName;

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private DocumentsProvider documentsProvider;

    private AnnotationsProvider annotationsProvider;

    public Collection(String name, String knowledgeSourceName) {
        this.name = name;
        this.knowledgeSourceName = knowledgeSourceName;
    }

    public String getName() {
        return name;
    }

    public String getKnowledgeSourceName() {
        return knowledgeSourceName;
    }

    public EntityKnowledgeProvider getEntityKnowledgeProvider() {
        return entityKnowledgeProvider;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public DocumentsProvider getDocumentsProvider() {
        return documentsProvider;
    }

    public void setDocumentsProvider(DocumentsProvider documentProvider) {
        this.documentsProvider = documentProvider;
    }

    public AnnotationsProvider getAnnotationsProvider() {
        return annotationsProvider;
    }

    public void setAnnotationsProvider(AnnotationsProvider annotationsProvider) {
        this.annotationsProvider = annotationsProvider;
    }

}
