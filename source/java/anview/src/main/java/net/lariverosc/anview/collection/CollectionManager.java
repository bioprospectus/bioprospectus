package net.lariverosc.anview.collection;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.lariverosc.anview.semeval.SemevalAnnotationProvider;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.text.annotation.AnnotationsProvider;
import net.lariverosc.text.document.DocumentsProvider;
import net.lariverosc.text.document.FileSystemDocumentProvider;
import net.lariverosc.text.document.PlainTextDocumentConverter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CollectionManager implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private final Map<String, Collection> collectionsMap;

    public CollectionManager() {
        this.collectionsMap = new HashMap<String, Collection>();
    }

    public void init() throws IOException {
//        Collection semevalGoldCollectionAttributes = initSemeval("semeval-2014-gold", "/media/data/data/semeval2014/analysisClinicalText/semeval-2014-task-7/alltrain/text", "/media/data/data/semeval2014/analysisClinicalText/semeval-2014-task-7/alltrain/pipeCUI");
//        this.collectionsMap.put(semevalGoldCollectionAttributes.getName(), semevalGoldCollectionAttributes);
//
//        Collection semevalTmapperCollectionAttributes = initSemeval("semeval-2014-tmapper", "/media/data/data/semeval2014/analysisClinicalText/semeval-2014-task-7/alltrain/text", "/media/data/tmapper-out");
//        this.collectionsMap.put(semevalTmapperCollectionAttributes.getName(), semevalTmapperCollectionAttributes);

        Collection clefCollectionAttributes = initClef();
        this.collectionsMap.put(clefCollectionAttributes.getName(), clefCollectionAttributes);

    }

    private Collection initSemeval(String collectionName, String docsPath, String annotationsPath) throws IOException {

        Collection semeval = new Collection(collectionName, "semeval");
        EntityKnowledgeProvider entityKnowledgeProvider = applicationContext.getBean("semevalEntityKnowledgeProvider", EntityKnowledgeProvider.class);
        semeval.setEntityKnowledgeProvider(entityKnowledgeProvider);

        DocumentsProvider documentsProvider = new FileSystemDocumentProvider(docsPath, false, new String[]{"text"});
        documentsProvider.setDocumentConverter(new PlainTextDocumentConverter());
        semeval.setDocumentsProvider(documentsProvider);

        semeval.setAnnotationsProvider(new SemevalAnnotationProvider(annotationsPath));

        return semeval;
    }

    private Collection initClef() {
        Collection clef = new Collection("clef-2013","snomeden");

        EntityKnowledgeProvider entityKnowledgeProvider = applicationContext.getBean("cachedSnomedEntityKnowledgeProvider", EntityKnowledgeProvider.class);
        clef.setEntityKnowledgeProvider(entityKnowledgeProvider);

        DocumentsProvider documentsProvider = applicationContext.getBean("anviewMongoDocumentsProvider", DocumentsProvider.class);
        clef.setDocumentsProvider(documentsProvider);

        AnnotationsProvider annotationsProvider = applicationContext.getBean("mongoAnnotationsProvider", AnnotationsProvider.class);
        clef.setAnnotationsProvider(annotationsProvider);
        return clef;

    }

    public Collection getCollection(String collectionName) {
        return collectionsMap.get(collectionName);
    }

    public Set<String> getCollectionsNames() {
        return collectionsMap.keySet();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
