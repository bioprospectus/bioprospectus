package net.lariverosc.anview.controllers;

import com.elibom.jogger.http.Request;
import com.elibom.jogger.http.Response;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.anview.Annotations;
import net.lariverosc.anview.collection.Collection;
import net.lariverosc.anview.collection.CollectionManager;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.AnnotationsProvider;
import net.lariverosc.text.annotation.highlight.AnnotationHighlighter;
import net.lariverosc.text.annotation.highlight.DefaultAnnotationHighlighter;
import net.lariverosc.text.document.Document;
import net.lariverosc.text.document.DocumentsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
@Annotations.Secured
public class Index {

    private final Logger log = LoggerFactory.getLogger(Index.class);

    private CollectionManager collectionManager;

    public void list(Request request, Response response) {
        String collectionName = request.getPathVariable("collection");

        Collection collection = collectionManager.getCollection(collectionName);
        DocumentsProvider documentsProvider = collection.getDocumentsProvider();

        List<String> allDocumentIds = documentsProvider.getAllDocumentIds();

        Map<String, Object> attributes = new HashMap<>();
        List<String> subList = allDocumentIds.subList(0, allDocumentIds.size() > 50 ? 50 : allDocumentIds.size());
        attributes.put("documents", buildJsonDocumentArray(subList).toString());
        response.render("list.ftl", attributes);
    }

    public void vizualize(Request request, Response response) {
        String collectionName = request.getPathVariable("collection");
        String documentId = request.getParameter("documentId");

        Collection collection = collectionManager.getCollection(collectionName);

        Document document = getDocument(collectionName, documentId);
        Set<Annotation> annotations = getAnnotations(collectionName, documentId);
        addAttributes(annotations, ImmutableMap.of("class", "annotation"));

        Map<String, String> highlightsByField = highlightDocument(document, annotations);

        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.putAll(highlightsByField);
        attributes.put("annotations", buildJsonAnnotationsArray(collection.getEntityKnowledgeProvider(), annotations).toString());

        if (collectionName.toLowerCase().contains("clef")) {
            response.render("viz-clef.ftl", attributes);
        } else if (collectionName.toLowerCase().contains("semeval")) {
            response.render("viz-semeval.ftl", attributes);
        } else {
            response.notFound();
        }
    }

    public void diffIndex(Request request, Response response) {
        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("jsonCollections", buildJsonCollectionsArray());
        attributes.put("collectionDocuments", buildJsonDocumentsByCollectionObject());
        response.render("diff.ftl", attributes);
    }

    public void diffComparison(Request request, Response response) {

        String collectionName1 = request.getParameter("collection1");
        String documentId1 = request.getParameter("document1");
        String collectionName2 = request.getParameter("collection2");
        String documentId2 = request.getParameter("document2");

        Document document1 = getDocument(collectionName1, documentId1);
        Document document2 = getDocument(collectionName2, documentId2);

        Set<Annotation> annotations1 = getAnnotations(collectionName1, documentId1);
        Set<Annotation> annotations2 = getAnnotations(collectionName2, documentId2);

        addAttributes(annotations1, ImmutableMap.of("class", "annotation set1"));
        addAttributes(annotations2, ImmutableMap.of("class", "annotation set2"));

        Sets.SetView<Annotation> intersection = Sets.intersection(annotations1, annotations2);
        addAttributes(intersection, ImmutableMap.of("class", "both"));

        Sets.SetView<Annotation> onlyOn1 = Sets.difference(annotations1, intersection);
        Sets.SetView<Annotation> onlyOn2 = Sets.difference(annotations2, intersection);

        addAttributes(onlyOn1, ImmutableMap.of("class", "only1"));
        addAttributes(onlyOn2, ImmutableMap.of("class", "only2"));

        annotations1 = Sets.union(Sets.union(intersection, onlyOn1), onlyOn2);
        annotations2 = Sets.union(Sets.union(intersection, onlyOn2), onlyOn1);

        Map<String, String> highlightDocument1 = highlightDocument(document1, annotations1);
        Map<String, String> highlightDocument2 = highlightDocument(document2, annotations2);

        JsonObject jsonObject = new JsonObject();
        jsonObject.add("document1", buildJsonObject(highlightDocument1));
        jsonObject.add("document2", buildJsonObject(highlightDocument2));
        response.write(jsonObject.toString());
    }

    public void about(Request request, Response response) {
        response.render("about.ftl");
    }

    private JsonArray buildJsonCollectionsArray() {
        Set<String> collections = collectionManager.getCollectionsNames();
        JsonArray jsonCollection = new JsonArray();
        for (String collection: collections) {
            jsonCollection.add(new JsonPrimitive(collection));
        }
        return jsonCollection;
    }

    private JsonObject buildJsonDocumentsByCollectionObject() {
        JsonObject jsonDocumentsByCollection = new JsonObject();

        Set<String> collections = collectionManager.getCollectionsNames();
        for (String collectionName: collections) {
            Collection collection = collectionManager.getCollection(collectionName);
            DocumentsProvider documentsProvider = collection.getDocumentsProvider();
            JsonArray jsonDocumentArray = buildJsonDocumentArray(documentsProvider.getAllDocumentIds());
            jsonDocumentsByCollection.add(collectionName, jsonDocumentArray);
        }
        return jsonDocumentsByCollection;
    }

    private JsonArray buildJsonDocumentArray(List<String> documents) {
        JsonArray jsonDocuments = new JsonArray();
        for (String documentId: documents) {
            jsonDocuments.add(new JsonPrimitive(documentId));
        }
        return jsonDocuments;
    }

    private JsonArray buildJsonAnnotationsArray(EntityKnowledgeProvider entityKnowledgeProvider, Set<Annotation> annotations) {
        JsonArray jsonAnnotations = new JsonArray();
        for (Annotation annotation: annotations) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("conceptId", annotation.getId());
            if ("CUI-less".equals(annotation.getId())) {
                jsonObject.addProperty("prefTerm", "CUI-less");
            } else {
                Concept concept = entityKnowledgeProvider.getByConceptId(annotation.getId());
                if (concept != null) {
                    if (concept.getPrefTerm() != null) {
                        jsonObject.addProperty("prefTerm", concept.getPrefTerm());
                    } else {
                        jsonObject.addProperty("prefTerm", concept.getDescription());
                    }
                } else {
                    jsonObject.addProperty("prefTerm", annotation.getId());
                }
            }
            jsonAnnotations.add(jsonObject);
        }
        return jsonAnnotations;
    }

    private JsonObject buildJsonObject(Map<String, String> map) {
        JsonObject jsonObject = new JsonObject();
        for (Map.Entry<String, String> entrySet: map.entrySet()) {
            jsonObject.add(entrySet.getKey(), new JsonPrimitive(entrySet.getValue()));
        }
        return jsonObject;
    }

    private Document getDocument(String collectionName, String documentId) {
        Collection collection = collectionManager.getCollection(collectionName);
        DocumentsProvider documentsProvider = collection.getDocumentsProvider();
        return documentsProvider.getDocument(documentId);
    }

    private Set<Annotation> getAnnotations(String collectionName, String documentId) {
        Collection collection = collectionManager.getCollection(collectionName);
        AnnotationsProvider annotationsProvider = collection.getAnnotationsProvider();
        return annotationsProvider.getAnnotations(documentId);
    }

    private Map<String, String> highlightDocument(Document document, Set<Annotation> annotations) {
        AnnotationHighlighter annotationHighlighter = new DefaultAnnotationHighlighter("em", "em");
        Map<String, Set<Annotation>> annotationsByField = getAnnotationsByField(annotations);
        Map<String, String> highlightsByField = new HashMap<String, String>();
        for (String fieldName: document.fieldNames()) {
            String fieldValue = document.getFieldValue(fieldName);
            Set<Annotation> fieldAnnotations = annotationsByField.get(fieldName);
            if (fieldAnnotations != null && !fieldAnnotations.isEmpty()) {
                String fieldHighlighted = annotationHighlighter.highlight(fieldValue, fieldAnnotations);
                highlightsByField.put(fieldName, fieldHighlighted);
            } else {
                highlightsByField.put(fieldName, fieldValue);
            }
        }
        return highlightsByField;
    }

    private Map<String, Set<Annotation>> getAnnotationsByField(Set<Annotation> annotations) {
        Map<String, Set<Annotation>> annotationsByField = new HashMap<String, Set<Annotation>>();
        for (Annotation annotation: annotations) {
            String fieldName = annotation.getAttribute("field");
            if (fieldName != null && !fieldName.isEmpty()) {
                Set<Annotation> fieldAnnotations = annotationsByField.get(fieldName);
                if (fieldAnnotations == null) {
                    fieldAnnotations = new HashSet<Annotation>();
                    annotationsByField.put(fieldName, fieldAnnotations);
                }
                fieldAnnotations.add(annotation);
            }
        }
        return annotationsByField;
    }

    private void addAttributes(Set<Annotation> annotations, Map<String, String> additionalAttributes) {
        for (Annotation annotation: annotations) {
            for (Map.Entry<String, String> entry: additionalAttributes.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if ("class".equalsIgnoreCase(key)) {
                    annotation.appendAttribute("class", annotation.getId(), " ");
                    annotation.appendAttribute("class", value, " ");
                } else {
                    annotation.addAttribute(key, value);
                }
            }
        }
    }

    public void setCollectionManager(CollectionManager collectionManager) {
        this.collectionManager = collectionManager;
    }

}
