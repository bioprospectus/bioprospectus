package net.lariverosc.anview.controllers;

import com.elibom.jogger.http.Request;
import com.elibom.jogger.http.Response;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import net.lariverosc.anview.UsersManager;
import net.lariverosc.anview.session.Session;
import net.lariverosc.anview.session.SessionsManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Sessions {

    private final Logger log = LoggerFactory.getLogger(Sessions.class);

    private final Map<String, String> userPassMap;

    private SessionsManager sessionsManager;

    private UsersManager usersManager;

    public Sessions() {
        this.userPassMap = initUsers();
    }

    private Map<String, String> initUsers() {
        Map<String, String> temp = new HashMap<String, String>();
        temp.put("root", "mobile745");
        return Collections.unmodifiableMap(temp);
    }

    public void index(Request request, Response response) {
        response.contentType("text/html; charset=UTF-8").render("login.ftl");
    }

    public void create(Request request, Response response) {
        Session session = (Session) response.getAttributes().get("session");
        if (session.getUser() != null) {
            return;
        }
        String bodyStr = request.getBody().asString();
        JsonObject bodyJson = (JsonObject) new JsonParser().parse(bodyStr);
        String username = getFromJson(bodyJson, "username").getAsString();
        String password = getFromJson(bodyJson, "password").getAsString();
        boolean keepLogged = getFromJson(bodyJson, "keepLogged").getAsBoolean();
        if (usersManager.isValid(username, password)) {
            session.setUser(username);
            session.setKeepLogged(keepLogged);
            sessionsManager.addSession(session);
            response.redirect("/");
        } else {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("errorCode", "invalid_credentials");
            response.unauthorized().write(jsonObject.toString());
        }
    }

    private JsonElement getFromJson(JsonObject jsonObject, String membertName) {
        JsonElement jsonElement = jsonObject.get(membertName);
        if (jsonElement != null) {
            return jsonElement;
        }
        return null;
    }

    public void destroy(Request request, Response response) {
        Session session = (Session) response.getAttributes().get("session");
        sessionsManager.deleteSession(session);
        response.redirect("/login");
    }

    public void setSessionsManager(SessionsManager sessionsManager) {
        this.sessionsManager = sessionsManager;
    }

    public void setUsersManager(UsersManager usersManager) {
        this.usersManager = usersManager;
    }
}
