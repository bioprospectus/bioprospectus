package net.lariverosc.anview.interceptor;

import com.elibom.jogger.http.Request;
import com.elibom.jogger.http.Response;
import com.elibom.jogger.middleware.router.interceptor.Interceptor;
import com.elibom.jogger.middleware.router.interceptor.InterceptorExecution;
import net.lariverosc.anview.collection.Collection;
import net.lariverosc.anview.collection.CollectionManager;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DefaultParamatersInterceptor implements Interceptor {

    private CollectionManager collectionManager;

    @Override
    public void intercept(Request request, Response response, InterceptorExecution execution) throws Exception {
        response.setAttribute("collections", collectionManager.getCollectionsNames());
        String collectionName = request.getPathVariable("collection");
        if (collectionName != null && !collectionName.isEmpty()) {
            Collection collection = collectionManager.getCollection(collectionName);
            if (collection != null) {
                response.setAttribute("knowledgeSourceName", collection.getKnowledgeSourceName());
            }
        }
        execution.proceed();
    }

    public void setCollectionManager(CollectionManager collectionManager) {
        this.collectionManager = collectionManager;
    }

}
