package net.lariverosc.anview.middleware;

import com.elibom.jogger.Middleware;
import com.elibom.jogger.MiddlewareChain;
import com.elibom.jogger.http.Request;
import com.elibom.jogger.http.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ExceptionMiddleware implements Middleware {

    private final Logger log = LoggerFactory.getLogger(ExceptionMiddleware.class);

    @Override
    public void handle(Request request, Response response, MiddlewareChain chain) throws Exception {
        try {
            chain.next();
        } catch (Exception e) {
            log.error("ExceptionMiddleware", e);
        }
    }

}
