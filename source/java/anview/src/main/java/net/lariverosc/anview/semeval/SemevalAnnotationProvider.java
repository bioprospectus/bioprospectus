package net.lariverosc.anview.semeval;

import java.util.Set;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.AnnotationsProvider;
import net.lariverosc.text.annotation.FileSystemAnnotationProvider;
import net.lariverosc.text.annotation.PipeAnnotationConverter;
import net.lariverosc.util.IOUtils;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SemevalAnnotationProvider extends AnnotationsProvider {

    private final AnnotationsProvider annotationsProvider;

    public SemevalAnnotationProvider(String docsPath) {
        annotationsProvider = new FileSystemAnnotationProvider(docsPath);
        annotationsProvider.setAnnotationConverter(new PipeAnnotationConverter());
    }

    @Override
    public Set<Annotation> getAnnotations(String docId) {
        String pipeFile = IOUtils.removeFileExtension(docId) + ".pipe";
        return annotationsProvider.getAnnotations(pipeFile);
    }

}
