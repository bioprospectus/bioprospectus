var anview = {
    init: function () {
        _.bindAll(this, 'properties', 'getResource', 'log');
    },
    properties: {
        knowledgeSource: {
            hostAndPort: '10.0.2.15:8184'
            //hostAndPort: '168.176.37.45:8184'
        }
    },
    getResource: function (url) {
        var data = "<h1> failed to load resource : " + url + "</h1>";
        $.ajax({
            async: false,
            url: url,
            success: function (response) {
                data = response;
            }
        });
        return data;
    },
    log: function (message) {
        if (console) {
            console.log(message);
        }
    }
};

