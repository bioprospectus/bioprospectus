$(document).ready(function () {

    $.each(collections, function (i, collection) {
        var option = _.template('<option value="<%= option %>"> <%= option %> </option>');
        $('#collection-1').append(option({option: collection}));
        $('#collection-2').append(option({option: collection}));
    });

    var getDocumentList = function () {
        var collection1 = $('#collection-1').val();
        var collection2 = $('#collection-2').val();
        var allDocuments = $.extend({}, collectionDocuments[collection1], collectionDocuments[collection2]);
        return $.map(allDocuments, function (document) {
            return {value: document};
        });
    };
    var engine = new Bloodhound({
        name: 'documents',
        local: getDocumentList(),
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        limit: 10
    });
    engine.initialize();

    var input = $('#document');
    input.typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'documents',
        source: engine.ttAdapter()
    });

    input.on('typeahead:selected', function (e, datum) {
        callComparison();
    });

    $('#collection-1, #collection-2').on('change', function () {
        $('#document').typeahead('val', '');
        engine.clear();
        engine.local = getDocumentList();
        engine.initialize(true);
    });

    var getParameters = function () {
        return  {
            collection1: $('#collection-1').val(),
            document1: $('#document').typeahead('val'),
            collection2: $('#collection-2').val(),
            document2: $('#document').typeahead('val')
        };
    };

    var callComparison = function () {
        var parameters = getParameters();
        var textContent1 = $('#text-content-1');
        var textContent2 = $('#text-content-2');
        textContent1.empty();
        textContent2.empty();
        var jqxhr = $.get('/diffc?' + $.param(parameters), function (response) {
            var jsonResponse = JSON.parse(response);
            if (jsonResponse["document1"]) {
                $.each(jsonResponse["document1"], function (key, value) {
                    textContent1.append('<h4>' + key + '</h4>');
                    textContent1.append('<p>' + value + '</p>');
                });
                $('.only2', textContent1).css('color', '#666').css('background-color', '#FFCACA');
                $('.both', textContent1).css('background-color', '#D8F8D8');
            }
            if (jsonResponse["document2"]) {
                $.each(jsonResponse["document2"], function (key, value) {
                    textContent2.append('<h4>' + key + '</h4>');
                    textContent2.append('<p>' + value + '</p>');
                });
                $('.only1', textContent2).css('color', '#666').css('background-color', '#FFCACA');
                $('.both', textContent2).css('background-color', '#D8F8D8');
            }

            $('.annotation').each(function (index, element) {
                var elemtObj = $(element);
                elemtObj.qtip(_.extend({}, qtipDefaultOptions, {
                    position: {
                        viewport: $(window)
                    },
                    content: {
                        text: function () {
                            var cui = elemtObj.attr('conceptid');
                            if (cui === 'CUI-less') {
                                return  elemtObj.attr('conceptid');
                            }
                            return  elemtObj.attr('category') + '-' + elemtObj.attr('conceptId');
                        }
                    }
                }));
            });
        });
        jqxhr.fail(function () {
            alert("error");
        });
    };

});