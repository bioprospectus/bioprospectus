$(document).ready(function() {
    var documentRowTemplate = _.template($('#document-row-template').html());
    $.each(documents, function(index, documentId) {
        $('#file-table tbody').append(documentRowTemplate({documentId: documentId}));
    });

});