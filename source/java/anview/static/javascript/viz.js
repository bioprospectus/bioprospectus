$(document).ready(function () {
    $('.annotation').each(function (index, element) {
        new AnnotationView({el: $(element)});
    });

    $('.annotation').hover(function (event) {
        $(this).css('color', '#477CA7');
    });

    var setUpHover = function (element, conceptId) {
        element.hover(
                function () {
                    $('.' + conceptId).addClass('annotation-highlight');
                },
                function () {
                    $('.' + conceptId).removeClass('annotation-highlight');
                }
        );
    };

    var uniqueAnnotations = {};
    $.each(annotations, function (index, annotation) {
        if (!(annotation.conceptId in uniqueAnnotations)) {
            uniqueAnnotations[annotation.conceptId] = annotation;
        }
    });

    var listItemTemplate = _.template($('#list-item-template').html());
    $.each(uniqueAnnotations, function (conceptId, annotation) {
        $('#annotations-list').append(listItemTemplate(annotation));
        setUpHover($('#annotations-list li:last'), conceptId);
        $('#annotations-list li:last input[type="checkbox"]').change(function () {
            if (this.checked) {
                $(this).parents('li').unbind('mouseenter mouseleave');
            } else {
                setUpHover($(this).parents('li'), conceptId);
            }
            $('.' + conceptId).addClass('annotation-highlight');
        });
    });


});