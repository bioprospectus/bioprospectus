<!DOCTYPE html>
<#import "layout.ftl" as layout>
<@layout.appLayout>
<div class="row">
    <div class="jumbotron">
        <h1>Annotation Viewer</h1>
        <p>Annotation visualization tool developed by Alejandro Riveros Cruz</p>
        <address>
            <a href="mailto:#">lariverosc@gmail.com</a>
        </address>
    </div>
</div>
</@layout.appLayout>