<!DOCTYPE html>
<#import "layout.ftl" as layout>
<@layout.appLayout stylesheets=stylesheets javascripts=javascripts templates=templates>
<#macro stylesheets>
    <link rel="stylesheet" type="text/css" href="/css/jquery-qtip-2.2.1.css">
    <link rel="stylesheet" type="text/css" href="/css/c3-0.2.4.css">
</#macro>
<div class="row">
    <div class="col-md-6">
        <div class="row selector">
            <select id="collection-1" class="form-control" tabindex="1">
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row selector">
            <select id="collection-2" class="form-control" tabindex="2">
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row selector">
            <input id="document" type="text" class="form-control" placeholder="Choose a document" tabindex="3">
        </div>
    </div>
    <div class="col-md-6" style="border-right: 1px solid #ccc;">
        <div class="row" style="padding: 10px;
             color: #666;
             font-family: monospace;
             text-align: justify;
             white-space: pre-line;">
            <div id="text-content-1"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row" style="padding: 10px;
             color: #666;
             font-family: monospace;
             text-align: justify;
             white-space: pre-line;">
            <div id="text-content-2"></div>
        </div>
    </div>
</div>

<#macro javascripts>
    <script type="text/javascript" src="/javascript/lib/jquery-qtip-2.2.1.js"></script>
    <script type="text/javascript" src="/javascript/lib/d3-3.4.11.js"></script>
    <script type="text/javascript" src="/javascript/lib/c3-0.2.4.js"></script>
    <script type="text/javascript" src="/javascript/lib/typeahead-0.10.5.js"></script>
    <script type="text/javascript" src="/javascript/annotation.js"></script>
    <script type="text/javascript" src="/javascript/diff.js"></script>
    <script type="text/javascript">
        var collections = ${jsonCollections};
        var collectionDocuments = ${collectionDocuments};
    </script>
</#macro>
</@layout.appLayout>


