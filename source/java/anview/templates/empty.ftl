<!DOCTYPE html>
<#import "layout.ftl" as layout>
<@layout.appLayout stylesheets=stylesheets javascripts=javascripts templates=templates>

<#macro stylesheets>
</#macro>

<#macro templates>
</#macro>

<#macro javascripts>
</#macro>
</@layout.appLayout>
