<#macro appLayout stylesheets="" javascripts="" templates="">
<!DOCTYPE html>
<html>
    <head>
        <title>Annotation Viewer!</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap-3.1.1/bootstrap.css">
        <#if stylesheets?? >
            <#if stylesheets?is_directive>
                <@stylesheets/>
            <#else>
                ${stylesheets}
            </#if>
        </#if>
        <link rel="stylesheet" type="text/css" href="/css/anview.css">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" style="color: #E7A45D;; font-family: monospace; font-size: 28px; margin-top: -2px;" href="/about" tabindex="-1">AnView</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Collections<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                              <#list collections as collection>
                                <li><a href="/${collection}/list">${collection}</a></li>
                              </#list>
                            </ul>
                        </li>
                        <li><a href="/diff" tabindex="-1">Compare</a></li>
                        <li><a href="/about" tabindex="-1">About</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/logout" tabindex="-1">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <#nested/>
        </div>

        <#if templates?? >
            <#if templates?is_directive>
                <@templates/>
            <#else>
                ${templates}
            </#if>
        </#if>
        <script type="text/javascript" src="/javascript/lib/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="/javascript/lib/underscore-1.6.0.js"></script>
        <script type="text/javascript" src="/javascript/lib/underscore.string-2.3.0.js"></script>
        <script type="text/javascript" src="/javascript/lib/backbone-1.1.2.js"></script>
        <script type="text/javascript" src="/javascript/lib/bootstrap-3.1.1.js"></script>
        <script type="text/javascript" src="/javascript/anview.js"></script>
        <#if javascripts?? >
            <#if javascripts?is_directive>
                <@javascripts/>
            <#else>
                ${javascripts}
            </#if>
        </#if>
    </body>
</html>
</#macro>