<!DOCTYPE html>
<#import "layout.ftl" as layout>
<@layout.appLayout stylesheets=stylesheets javascripts=javascripts templates=templates>
  <div class="row" >
    <div class="col-md-offset-2 col-md-8" >
        <h3>View annotated files</h3>
        <div style="max-height: 600px; overflow: scroll">
            <table id="file-table" class="table table-striped table-hover">
                <tr>
                    <th>Document</th>
                    <th>Actions</th>
                </tr>
            </table>
        </div>
    </div>
  </div>

<#macro templates>
    <script id="document-row-template" type="text/x-underscore-template">
        <tr>
            <td>
                <em><%=documentId%><em/>
            </td>
            <td>
                <a href="viz?documentId=<%=documentId%>">Visualize</a>
            </td>
        </tr>
    </script>
</#macro>

<#macro javascripts>
    <script type="text/javascript">
        var documents = ${documents};
    </script>
    <script type="text/javascript" src="/javascript/list.js"></script>
</#macro>
</@layout.appLayout>
