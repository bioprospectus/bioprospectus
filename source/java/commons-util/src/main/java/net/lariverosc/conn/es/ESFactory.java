package net.lariverosc.conn.es;

import java.net.URI;
import java.net.URISyntaxException;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ESFactory {

    private final Logger log = LoggerFactory.getLogger(ESFactory.class);

    private Client client;

    private final String url;

    private final String clusterName;

    public ESFactory(String url, String clusterName) {
        this.url = url;
        this.clusterName = clusterName;
    }

    public Client create() throws URISyntaxException {
        log.info("Creating ES client for: " + url);
        Settings settings = ImmutableSettings.settingsBuilder().put("cluster.name", clusterName).build();
        URI uri = new URI(url);
        TransportAddress transportAddresses = new InetSocketTransportAddress(uri.getHost(), uri.getPort());
        client = new TransportClient(settings).addTransportAddresses(transportAddresses);
        return client;
    }

    public void shutdown() {
        client.close();
    }

}
