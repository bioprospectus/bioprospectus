package net.lariverosc.conn.mongodb;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MongoDBFactory {

    private final Logger log = LoggerFactory.getLogger(MongoDBFactory.class);

    private final MongoClientURI mongoClientURI;

    private final MongoClient mongoClient;

    public MongoDBFactory(String uri) throws URISyntaxException, UnknownHostException {
        log.info("Connecto to mongoDB in: {}", uri);
        mongoClientURI = new MongoClientURI(uri);
        mongoClient = new MongoClient(mongoClientURI);
    }

    public DBCollection create() throws MongoException, UnknownHostException {
        DB db = mongoClient.getDB(mongoClientURI.getDatabase());
        DBCollection collection = db.getCollection(mongoClientURI.getCollection());
        return collection;
    }

    public void shutdown() {
        mongoClient.close();
    }
}
