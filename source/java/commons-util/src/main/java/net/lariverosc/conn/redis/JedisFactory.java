package net.lariverosc.conn.redis;

import java.net.URI;
import net.lariverosc.conn.es.ESFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JedisFactory {

    private final Logger log = LoggerFactory.getLogger(ESFactory.class);

    private final String uri;

    private final JedisPool jedisPool;

    public JedisFactory(String uri) {
        this.uri = uri;
        this.jedisPool = new JedisPool(uri);
    }

    public Jedis create() {
        return jedisPool.getResource();
    }

    public void close() {
        jedisPool.close();
    }

}
