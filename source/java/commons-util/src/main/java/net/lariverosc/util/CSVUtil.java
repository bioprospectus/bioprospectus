package net.lariverosc.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CSVUtil {

    private static final Logger log = LoggerFactory.getLogger(CSVUtil.class);

    public static void createCSV(String filePath, CSVRecordGenerator recordGenerator) {
        log.info("Creating file: "+filePath);
        IOUtils.ensureBasePath(filePath);
        FileWriter csvFile = null;
        CSVPrinter csvPrinter = null;
        try {
            csvFile = new FileWriter(filePath);
            csvPrinter = new CSVPrinter(csvFile, CSVFormat.DEFAULT);
            Object[] header = recordGenerator.getHeader();
            csvPrinter.printRecord(header);
            while (recordGenerator.hasNext()) {
                List record = recordGenerator.next();
                if (record.size() == header.length) {
                    csvPrinter.printRecord(record);
                } else {
                    log.warn("Illegal record length {}", record.size());
                }
            }
        } catch (IOException ioe) {
            log.error("Error creating csv file", ioe);
        } finally {
            try {
                if (csvFile != null) {
                    csvFile.flush();
                    csvFile.close();
                }
                if (csvPrinter != null) {
                    csvPrinter.close();
                }
            } catch (IOException ex) {
            }
        }
    }

    public static interface CSVRecordGenerator extends Iterator<List> {

        Object[] getHeader();
        
    }
}
