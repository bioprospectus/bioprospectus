package net.lariverosc.util;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class EnvironmentUtils {

    private EnvironmentUtils() {
    }

    public static String getEnvValue(String envName) {
        String envValue = System.getenv(envName);
        if (envValue == null) {
            envValue = System.getProperty(envName);
        }
        return envValue;
    }

    public static void checkEnv(String envName) {
        if (getEnvValue(envName) == null) {
            throw new RuntimeException(envName + " env is not defined");
        }
    }

    public static void exportEnv(String envName, String envValue) {
        System.setProperty(envName, envValue);
    }

}
