package net.lariverosc.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class IOUtils {

    /**
     * Fix a path adding the file separator if required
     *
     * @param path
     *
     * @return the fixed path
     */
    public static String fixPath(String path) {
        File file = new File(path);
        if (file.isDirectory()) {
            return path.endsWith(File.separator) ? path : path + File.separator;
        }
        return path;
    }

    /**
     *
     * @param fileName
     *
     * @return
     */
    public static String removeFileExtension(String fileName) {
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

    /**
     *
     * @param filePath
     * @param withExtension
     *
     * @return
     * @throws IOException
     */
    public static String getFileName(String filePath, boolean withExtension) {
        if (isFile(filePath)) {
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator) + 1);
            if (!withExtension) {
                return removeFileExtension(fileName);
            }
            return fileName;
        }
        throw new RuntimeException("The path is not a file: " + filePath);
    }

    /**
     *
     * @param path
     *
     * @return
     */
    public static boolean isFile(String path) {
        return new File(path).isFile();
    }

    /**
     *
     * @param path
     *
     * @return
     */
    public static boolean isDirectory(String path) {
        return new File(path).isDirectory();
    }

    public static void ensureBasePath(String filePath) {
        String parent = new File(filePath).getParent();
        File dir = new File(parent);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    /**
     *
     * @param path
     *
     * @return
     */
    public static String readContent(final String path) {
        List<String> lines = readLines(path);
        return linesToString(lines);
    }

    /**
     *
     * @param file
     *
     * @return
     */
    public static String readContent(final File file) {
        List<String> lines = readLines(file);
        return linesToString(lines);
    }

    private static String linesToString(List<String> lines) {
        StringBuilder sb = new StringBuilder();
        for (String line : lines) {
            sb.append(line).append(System.lineSeparator());
        }
        return sb.substring(0, sb.length() - 1);
    }

    /**
     *
     * @param path
     *
     * @return
     */
    public static List<String> readLines(final String path) {
        try {
            return readLines(openReader(path));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    /**
     *
     * @param file
     *
     * @return
     */
    public static List<String> readLines(final File file) {
        try {
            return readLines(openReader(file));
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    /**
     *
     * @param bufferedReader
     *
     * @return
     */
    public static List<String> readLines(final BufferedReader bufferedReader) {
        try {
            String line;
            final List<String> lines = new ArrayList<String>();
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            return lines;
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ioe) {
                }
            }
        }
    }

    /**
     *
     * @param path
     *
     * @return
     * @throws IOException
     */
    public static BufferedReader openReader(final String path) throws IOException {
        return openReader(new File(path));
    }

    /**
     *
     * @param file
     *
     * @return
     * @throws IOException
     */
    public static BufferedReader openReader(final File file) throws IOException {
        InputStream is = getBufferedInputStream(file);
        if (file.getName().endsWith(".gz")) {
            is = new GZIPInputStream(is);
        }
        return new BufferedReader(getReader(is));
    }

    private static BufferedInputStream getBufferedInputStream(final File file) throws FileNotFoundException {
        return new BufferedInputStream((new FileInputStream(file)));
    }

    private static BufferedReader getReader(final InputStream inputStream) throws IOException {
        return new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
    }
}
