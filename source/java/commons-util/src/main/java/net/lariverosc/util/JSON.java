package net.lariverosc.util;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JSON {

    private final Map<String, Object> jsonMap;

    public JSON(String jsonString) {
        jsonMap = new Gson().fromJson(jsonString, Map.class);
    }

    public String getAsString(String fieldName) {
        return cast(getField(fieldName), String.class);
    }

    public int getAsInt(String fieldName) {
        return cast(getField(fieldName), Double.class).intValue();
    }

    public double getAsDouble(String fieldName) {
        return cast(getField(fieldName), Double.class);
    }

    public List<String> getAsStringList(String fieldName) {
        return cast(getField(fieldName), List.class);
    }

    public List<Integer> getAsIntList(String fieldName) {
        List<Double> doubleList = getAsDoubleList(fieldName);
        List<Integer> intList = new ArrayList<>(doubleList.size());
        for (Double doubleVal : doubleList) {
            intList.add(doubleVal.intValue());
        }
        return intList;
    }

    public List<Double> getAsDoubleList(String fieldName) {
        return cast(getField(fieldName), List.class);
    }

    private <T> T cast(Object object, Class<T> clazz) {
        return clazz.cast(object);
    }

    private Object getField(String fieldName) {
        String[] fieldNameSplited = fieldName.split("\\.");
        if (fieldNameSplited.length > 0) {
            String[] fieldsPath = Arrays.copyOf(fieldNameSplited, fieldNameSplited.length - 1);
            String newFieldName = fieldNameSplited[fieldNameSplited.length - 1];
            return getField(fieldsPath, newFieldName);

        } else {
            return getField(jsonMap, fieldName);
        }
    }

    private Object getField(String[] fieldsPath, String fieldName) {
        Map currentMap = jsonMap;
        for (String currentField : fieldsPath) {
            if (currentMap.containsKey(currentField)) {
                currentMap = (Map) currentMap.get(currentField);
            } else {
                throw new NoSuchFieldError(currentField + " not exist");
            }
        }
        return getField(currentMap, fieldName);
    }

    private Object getField(final Map<String, Object> map, String fieldName) {
        if (map.containsKey(fieldName)) {
            return map.get(fieldName);
        } else {
            throw new NoSuchFieldError(fieldName + " not exist");
        }
    }

    public static void main(String[] args) {
        JSON json = new JSON("{\n"
                + "         source:\"alejandro\",\n"
                + "         age: 35,\n"
                + "         lengths : [1,2,3],\n"
                + "         query : {\n"
                + "            keywords : [\n"
                + "              \"keyword1\",\n"
                + "              \"keyword2\",\n"
                + "              \"keyword1\"\n"
                + "            ],\n"
                + "            concepts : [\n"
                + "              \"conceptId1\",\n"
                + "              \"conceptId2\"\n"
                + "            ]\n"
                + "         }\n"
                + "      }");

        System.out.println(json.getAsString("source"));
        System.out.println(json.getAsInt("age"));
        List<Integer> asIntArray = json.getAsIntList("lengths");
        for (int length : asIntArray) {
            System.out.println(length);
        }
        List<String> keywords = json.getAsStringList("query.keywords");
        for (String keyword : keywords) {
            System.out.println(keyword);
        }

        List<String> concepts = json.getAsStringList("query.concepts");
        for (String concept : concepts) {
            System.out.println(concept);
        }
    }
}
