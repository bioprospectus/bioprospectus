package net.lariverosc.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Maps {

    public static <k extends Comparable, v> Map<k, v> sortByKey(final Map<k, v> map, final boolean reverse) {
        List<Map.Entry<k, v>> entries = new ArrayList<>(map.size());
        entries.addAll(map.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<k, v>>() {
            @Override
            public int compare(
                    final Map.Entry<k, v> entry1,
                    final Map.Entry<k, v> entry2) {
                if (reverse) {
                    return -1 * entry1.getKey().compareTo(entry2.getValue());
                } else {
                    return entry1.getKey().compareTo(entry2.getValue());
                }
            }
        });

        Map<k, v> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<k, v> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public static <k, v extends Comparable> Map<k, v> sortByValues(final Map<k, v> map, final boolean reverse) {
        List<Map.Entry<k, v>> entries = new ArrayList<>(map.size());
        entries.addAll(map.entrySet());

        java.util.Collections.sort(entries, new Comparator<Map.Entry<k, v>>() {
            @Override
            public int compare(
                    final Map.Entry<k, v> entry1,
                    final Map.Entry<k, v> entry2) {
                if (reverse) {
                    return -1 * entry1.getValue().compareTo(entry2.getValue());
                } else {
                    return entry1.getValue().compareTo(entry2.getValue());
                }

            }
        });

        Map<k, v> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<k, v> entry : entries) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

}
