package net.lariverosc.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SimpleContext<T> {

    private final Map<String, T> map;

    public SimpleContext() {
        map = new HashMap<>();
    }

    public T get(String key) {
        return map.get(key);
    }

    public void put(String key, T value) {
        map.put(key, value);
    }

    public void clear() {
        map.clear();
    }

}
