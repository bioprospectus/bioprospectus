package net.lariverosc.util;

import java.net.URI;
import java.net.URISyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class URIUtil {

    private final Logger log = LoggerFactory.getLogger(URIUtil.class);

    private final URI uri;

    /**
     *
     * @param uri
     *
     * @throws URISyntaxException
     */
    public URIUtil(String uri) throws URISyntaxException {
        log.info("New Uri: {}", uri);
        this.uri = new URI(uri);
    }

    /**
     *
     * @return
     */
    public String getUrl() {
        return uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort();
    }

    /**
     *
     * @return
     */
    public String getHost() {
        return uri.getHost();
    }

    /**
     *
     * @return
     */
    public String getScheme() {
        return uri.getScheme();
    }

    /**
     *
     * @return
     */
    public int getPort() {
        return uri.getPort();
    }

    /**
     *
     * @return
     */
    public String getPath() {
        String path = uri.getPath();
        if (uri.getQuery() != null) {
            path += "?" + uri.getQuery();
        }
        return path;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        if (uri.getUserInfo() != null) {
            return uri.getUserInfo().split(":", 2)[0];
        }
        return null;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        if (uri.getUserInfo() != null) {
            return uri.getUserInfo().split(":", 2)[1];
        }
        return null;
    }
}
