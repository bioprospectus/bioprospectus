package net.lariverosc.experiments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.AnnotationsProvider;
import net.lariverosc.text.document.Document;
import net.lariverosc.text.document.DocumentsProvider;
import net.lariverosc.util.CSVUtil;
import net.lariverosc.util.EnvironmentUtils;
import net.lariverosc.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AnnotationsAnalysis {

    private static final Logger log = LoggerFactory.getLogger(AnnotationsAnalysis.class);

    public static void main(String[] args) {
        try {
            final String dataDir = IOUtils.fixPath("/Users/desarrollo");
            EnvironmentUtils.exportEnv("MONGO_URL", "mongodb://localhost:27017/clef.sample100");
            ClassPathXmlApplicationContext context = initSpringContext();
            EntityKnowledgeProvider entityKnowledgeProvider = context.getBean("cachedSnomedEntityKnowledgeProvider", EntityKnowledgeProvider.class);
            HierarchyKnowledgeProvider hierarchyKnowledgeProvider = context.getBean("snomedHierarchyKnowledgeProvider", HierarchyKnowledgeProvider.class);
            DocumentsProvider documentsProvider = context.getBean("mongoDocumentsProvider", DocumentsProvider.class);
            AnnotationsProvider annotationsProvider = context.getBean("mongoAnnotationsProvider", AnnotationsProvider.class);
            CSVUtil.createCSV(dataDir + "annotations/annotations-by-document.csv", new AnnotationByDocumentRecordGenerator(entityKnowledgeProvider, hierarchyKnowledgeProvider, documentsProvider, annotationsProvider));
            CSVUtil.createCSV(dataDir + "annotations/annotations-detail.csv", new AnnotationDetailsRecordGenerator(entityKnowledgeProvider, hierarchyKnowledgeProvider, documentsProvider, annotationsProvider));
        } catch (Throwable t) {
            log.error("Fatal Error!!! {}", t);
        }
    }

    private static ClassPathXmlApplicationContext initSpringContext() {
        log.info("Starting Spring Application Context");
        List<String> contextConfigLocations = new ArrayList<>();
        contextConfigLocations.add("classpath*:index-context.xml");
        contextConfigLocations.add("classpath*:snomed-context.xml");
        contextConfigLocations.add("classpath*:mongodb-context.xml");
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(contextConfigLocations.toArray(new String[0]));
        applicationContext.registerShutdownHook();
        return applicationContext;
    }
}

class AnnotationByDocumentRecordGenerator implements CSVUtil.CSVRecordGenerator {

    private final EntityKnowledgeProvider entityKnowledgeProvide;

    private final HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private final DocumentsProvider documentsProvider;

    private final AnnotationsProvider annotationsProvider;

    private final Iterator<String> iterator;

    public AnnotationByDocumentRecordGenerator(EntityKnowledgeProvider entityKnowledgeProvide, HierarchyKnowledgeProvider hierarchyKnowledgeProvider, DocumentsProvider documentsProvider, AnnotationsProvider annotationsProvider) {
        this.entityKnowledgeProvide = entityKnowledgeProvide;
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
        this.documentsProvider = documentsProvider;
        this.annotationsProvider = annotationsProvider;
        this.iterator = documentsProvider.getAllDocumentIds().iterator();
    }

    @Override
    public Object[] getHeader() {
        return new Object[]{
            "documentId",
            "titleNumAnnotations",
            "abstractNumAnnotations",
            "fulltextNumAnnotations",
            "captionsNumAnnotations"};
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public List next() {
        String documentId = iterator.next();
        List annotationRecord = new ArrayList();
        annotationRecord.add(documentId);
        Set<Annotation> annotations = annotationsProvider.getAnnotations(documentId);
        Map<String, Set<Annotation>> annotationsByField = getAnnotationsByField(annotations);
        annotationRecord.add(annotationsByField.get("title") != null ? annotationsByField.get("title").size() : 0);
        annotationRecord.add(annotationsByField.get("abstract") != null ? annotationsByField.get("abstract").size() : 0);
        annotationRecord.add(annotationsByField.get("fulltext") != null ? annotationsByField.get("fulltext").size() : 0);
        int totalCaptionAnnotations = 0;
        for (String field : annotationsByField.keySet()) {
            if (field.endsWith("caption")) {
                totalCaptionAnnotations += annotationsByField.get(field).size();
            }
        }
        annotationRecord.add(totalCaptionAnnotations);
        return annotationRecord;
    }

    private Map<String, Set<Annotation>> getAnnotationsByField(Set<Annotation> annotations) {
        Map<String, Set<Annotation>> map = new HashMap<>();
        for (Annotation annotation : annotations) {
            String field = annotation.getAttribute("field");
            Set<Annotation> fieldAnnotations = map.get(field);
            if (fieldAnnotations == null) {
                fieldAnnotations = new HashSet<>();
            }
            fieldAnnotations.add(annotation);
            map.put(field, fieldAnnotations);
        }
        return map;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

class AnnotationDetailsRecordGenerator implements CSVUtil.CSVRecordGenerator {

    private final EntityKnowledgeProvider entityKnowledgeProvide;

    private final HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private final DocumentsProvider documentsProvider;

    private final AnnotationsProvider annotationsProvider;

    private final Iterator<String> documentIterator;

    private String currentDocumentId;

    private Iterator<Annotation> annotationIterator;

    public AnnotationDetailsRecordGenerator(EntityKnowledgeProvider entityKnowledgeProvide, HierarchyKnowledgeProvider hierarchyKnowledgeProvider, DocumentsProvider documentsProvider, AnnotationsProvider annotationsProvider) {
        this.entityKnowledgeProvide = entityKnowledgeProvide;
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
        this.documentsProvider = documentsProvider;
        this.annotationsProvider = annotationsProvider;
        this.documentIterator = documentsProvider.getAllDocumentIds().iterator();

    }

    @Override
    public Object[] getHeader() {
        return new Object[]{
            "annotationId",
            "prefTerm",
            "category",
            "hierarchyDepth",
            "isLeave",
            "documentField",
            "numSpans",
            "spans"};
    }

    @Override
    public boolean hasNext() {
        return documentIterator.hasNext() || annotationIterator.hasNext();
    }

    @Override
    public List next() {
        while (annotationIterator == null || !annotationIterator.hasNext()) {
            currentDocumentId = documentIterator.next();
            annotationIterator = annotationsProvider.getAnnotations(currentDocumentId).iterator();
        }
        Annotation annotation = annotationIterator.next();
        Document document = documentsProvider.getDocument(currentDocumentId);
        List annotationRecord = new ArrayList();
        annotationRecord.add(annotation.getId());
        Concept concept = entityKnowledgeProvide.getByConceptId(annotation.getId());
        annotationRecord.add(concept.getDescription());
        annotationRecord.add(concept.getCategory());
        annotationRecord.add(hierarchyKnowledgeProvider.getConceptDepth(concept));
        annotationRecord.add(hierarchyKnowledgeProvider.getChildrens(concept).isEmpty());
        String field = annotation.getAttribute("field");
        String fieldValue = document.getFieldValue(field);
        StringBuilder sb = new StringBuilder();
        if (field != null && fieldValue != null) {
            for (TextSpan textSpan : annotation.getTextSpans()) {
                String coveredText = textSpan.getCoveredText(fieldValue);
                sb.append(coveredText).append(",");
            }
        }
        annotationRecord.add(field);
        annotationRecord.add(annotation.getTextSpans().size());
        annotationRecord.add(sb.length() > 0 ? sb.substring(0, sb.length() - 1) : "");

        return annotationRecord;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
