package net.lariverosc.experiments;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;
import static net.lariverosc.util.EnvironmentUtils.exportEnv;
import net.lariverosc.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class KSAmbiguity {

    private static final Logger log = LoggerFactory.getLogger(KSAmbiguity.class);

    private static final TextPipeConfig textPipeConfig = new TextPipeConfig()
            .withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD)
            .withLanguage(TextPipeConfig.Language.ENGLISH)
            .withTokenTransforms(TextPipeConfig.TokenTransform.LOWER_CASE)
            .withTokenFilters(TextPipeConfig.TokenFilter.STOP_WORD)
            .withTokenMatchers(
                    TextPipeConfig.TokenMatcher.DECIMAL_NUMBER,
                    TextPipeConfig.TokenMatcher.ROMAN_NUMBER,
                    TextPipeConfig.TokenMatcher.PERCENTAGE,
                    TextPipeConfig.TokenMatcher.GENE,
                    TextPipeConfig.TokenMatcher.ALPHANUMERIC
            );

    public static TextPipe textPipe = new TextPipe(textPipeConfig);

    public static void main(String[] args) {
        final String dataDir = IOUtils.fixPath("/home/bio/TERM_MAPPER_HOME/data");
        //exportEnv("MODELS_PATH", dataDir + "opennlp");
        exportEnv("MODELS_PATH", dataDir);
        exportEnv("LANGUAGE", "en");
        ClassPathXmlApplicationContext context = initSpringContext();
        EntityKnowledgeProvider entityKnowledgeProvider = context.getBean("cachedSnomedEntityKnowledgeProvider", EntityKnowledgeProvider.class);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = context.getBean("snomedHierarchyKnowledgeProvider", HierarchyKnowledgeProvider.class);
        Map<String, Term> allTerms = entityKnowledgeProvider.getAllTerms();
        SetMultimap<String, String> tempMap = HashMultimap.create();

        for (Term term : allTerms.values()) {
            List<TextToken> termTokens = textPipe.getTokens(term.getTerm());
            String joinTokens = joinTokens(term, termTokens);
            tempMap.put(joinTokens, term.getConceptId());
        }
        int i = 0;
        for (String tokensKey : tempMap.asMap().keySet()) {
            Set<String> conceptIds = tempMap.get(tokensKey);
            if (conceptIds.size() > 1) {
                System.out.println((i++) + " " + tokensKey);
                for (String conceptId : conceptIds) {
                    Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
                    System.out.println(concept.getId() + " - " + concept.getCategory() + " - " + concept.getDescription());
                }
            }
        }

    }

    private static ClassPathXmlApplicationContext initSpringContext() {
        log.info("Starting Spring Application Context");
        List<String> contextConfigLocations = new ArrayList<>();
        contextConfigLocations.add("classpath*:text-pipe-context.xml");
        contextConfigLocations.add("classpath*:index-context.xml");
        contextConfigLocations.add("classpath*:snomed-context.xml");
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(contextConfigLocations.toArray(new String[0]));
        applicationContext.registerShutdownHook();
        return applicationContext;
    }

    private static String joinTokens(Term term, List<TextToken> termTokens) {
        TextToken.sortAlphabetically(termTokens);
        StringBuilder sb = new StringBuilder();
        for (TextToken textToken : termTokens) {
            sb.append(textToken.getText()).append("_");
        }
        if (sb.length() > 0) {
            return sb.substring(0, sb.length() - 1);
        }
        return "";
    }
}
