package net.lariverosc.experiments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.collection.CollectionLoader;
import net.lariverosc.index.Index;
import net.lariverosc.index.TextIndexAgent;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;
import net.lariverosc.text.stemming.SnowballStemmer;
import net.lariverosc.text.stemming.Stemmer;
import net.lariverosc.text.util.TokenFeatures;
import net.lariverosc.util.CSVUtil;
import net.lariverosc.util.IOUtils;
import static net.lariverosc.util.EnvironmentUtils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class KSAnalysis {

    private static final Logger log = LoggerFactory.getLogger(KSAnalysis.class);

    private static final TextPipeConfig textPipeConfig = new TextPipeConfig()
            .withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD)
            .withLanguage(TextPipeConfig.Language.ENGLISH)
            .withTokenTransforms(TextPipeConfig.TokenTransform.LOWER_CASE)
            .withTokenMatchers(
                    TextPipeConfig.TokenMatcher.DECIMAL_NUMBER,
                    TextPipeConfig.TokenMatcher.ROMAN_NUMBER,
                    TextPipeConfig.TokenMatcher.PERCENTAGE,
                    TextPipeConfig.TokenMatcher.GENE,
                    TextPipeConfig.TokenMatcher.STOP,
                    TextPipeConfig.TokenMatcher.ALPHANUMERIC
            );

    public static TextPipe textPipe = new TextPipe(textPipeConfig);

    public static void main(String[] args) {
        try {
            final String dataDir = IOUtils.fixPath(getEnvValue("DATA_DIR"));
            exportEnv("MODELS_PATH", dataDir + "opennlp");
            exportEnv("language", "en");
            ClassPathXmlApplicationContext context = initSpringContext();
            EntityKnowledgeProvider entityKnowledgeProvider = context.getBean("cachedSnomedEntityKnowledgeProvider", EntityKnowledgeProvider.class);
            HierarchyKnowledgeProvider hierarchyKnowledgeProvider = context.getBean("snomedHierarchyKnowledgeProvider", HierarchyKnowledgeProvider.class);

            TextIndexAgent textIndexAgent = new TextIndexAgent(textPipeConfig);
            textIndexAgent.setCollectionLoader(new TermLoader("snomeden-terms", entityKnowledgeProvider));
            Index index = textIndexAgent.buildIndex();

            CSVUtil.createCSV(dataDir + "snomed/snomeden-concepts-data.csv", new ConceptRecordGenerator(entityKnowledgeProvider, hierarchyKnowledgeProvider));
            CSVUtil.createCSV(dataDir + "snomed/snomeden-terms-data.csv", new TermRecordGenerator(entityKnowledgeProvider, hierarchyKnowledgeProvider, index));
            CSVUtil.createCSV(dataDir + "snomed/snomeden-words-data.csv", new VocabularyRecordGenerator(entityKnowledgeProvider, hierarchyKnowledgeProvider, index));

        } catch (Throwable t) {
            log.error("Fatal Error!!! {}", t);
        }
    }

    private static ClassPathXmlApplicationContext initSpringContext() {
        log.info("Starting Spring Application Context");
        List<String> contextConfigLocations = new ArrayList<String>();
        contextConfigLocations.add("classpath*:text-pipe-context.xml");
        contextConfigLocations.add("classpath*:index-context.xml");
        contextConfigLocations.add("classpath*:snomed-context.xml");
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(contextConfigLocations.toArray(new String[0]));
        applicationContext.registerShutdownHook();
        return applicationContext;
    }
}

class TermLoader implements CollectionLoader {

    private final String collectionName;

    private final Map<String, String> collection;

    public TermLoader(String collectionName, EntityKnowledgeProvider entityKnowledgeProvider) {
        this.collectionName = collectionName;
        collection = new HashMap<String, String>();
        for (Term term : entityKnowledgeProvider.getAllTerms().values()) {
            collection.put(term.getId(), term.getTerm());
        }
    }

    @Override
    public String getCollectionName() {
        return collectionName;
    }

    @Override
    public Map<String, String> getCollection() {
        return collection;
    }
}

class VocabularyRecordGenerator implements CSVUtil.CSVRecordGenerator {

    private final Map<String, Concept> allConcepts;

    private final EntityKnowledgeProvider entityKnowledgeProvide;

    private final HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private final Index index;

    private final Set<String> indexTerms;

    private final Iterator<String> iterator;

    private final Stemmer stemmer;

    public VocabularyRecordGenerator(EntityKnowledgeProvider entityKnowledgeProvide, HierarchyKnowledgeProvider hierarchyKnowledgeProvider, Index index) {
        this.allConcepts = entityKnowledgeProvide.getAllConcepts();
        this.entityKnowledgeProvide = entityKnowledgeProvide;
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
        this.index = index;
        this.stemmer = new SnowballStemmer(TextPipeConfig.Language.ENGLISH);
        this.indexTerms = this.index.getTerms();
        this.iterator = indexTerms.iterator();
    }

    @Override
    public Object[] getHeader() {
        return new Object[]{"iterm", "globalFreq", "documentFreq", "stem", "suffix"};
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public List next() {
        String iterm = iterator.next();
        List itermRecord = new ArrayList();
        itermRecord.add(iterm);
        itermRecord.add(index.getGlobalFrequency(iterm));
        itermRecord.add(index.getDocumentsByTerm(iterm).size());
        TokenFeatures tokenFeatures = TokenFeatures.extract(iterm);

        if (tokenFeatures.isAllLetter()) {
            String stem = stemmer.stem(iterm);
            if (!stem.equals(iterm)) {
                itermRecord.add(stem);
                itermRecord.add(iterm.substring(stem.length()));
            } else {
                itermRecord.add("NA");
                itermRecord.add("NA");
            }
        } else {
            itermRecord.add("NA");
            itermRecord.add("NA");
        }

        return itermRecord;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

class ConceptRecordGenerator implements CSVUtil.CSVRecordGenerator {

    private final Map<String, Concept> allConcepts;

    private final EntityKnowledgeProvider entityKnowledgeProvide;

    private final HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private final Iterator<Concept> iterator;

    public ConceptRecordGenerator(EntityKnowledgeProvider entityKnowledgeProvide, HierarchyKnowledgeProvider hierarchyKnowledgeProvider) {
        this.allConcepts = entityKnowledgeProvide.getAllConcepts();
        this.entityKnowledgeProvide = entityKnowledgeProvide;
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
        this.iterator = this.allConcepts.values().iterator();
    }

    @Override
    public Object[] getHeader() {
        return new Object[]{"conceptId", "category", "numTerms", "depth", "isLeaf"};
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public List next() {
        Concept concept = iterator.next();
        List conceptRecord = new ArrayList();
        conceptRecord.add(concept.getId());
        if (concept.getCategory().length() > 18) {
            conceptRecord.add(concept.getCategory().substring(0, 13) + "..");
        } else {
            conceptRecord.add(concept.getCategory());
        }
        conceptRecord.add(entityKnowledgeProvide.getTermsByConceptId(concept.getId()).size());
        conceptRecord.add(hierarchyKnowledgeProvider.getConceptDepth(concept));
        conceptRecord.add(hierarchyKnowledgeProvider.getChildrens(concept).isEmpty());
        return conceptRecord;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

class TermRecordGenerator implements CSVUtil.CSVRecordGenerator {

    private final Map<String, Concept> allConcepts;

    private final Map<String, Term> allTerms;

    private final EntityKnowledgeProvider entityKnowledgeProvide;

    private final HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private final Index index;

    private final Iterator<Term> iterator;

    public TermRecordGenerator(EntityKnowledgeProvider entityKnowledgeProvide, HierarchyKnowledgeProvider hierarchyKnowledgeProvider, Index index) {
        this.allConcepts = entityKnowledgeProvide.getAllConcepts();
        this.allTerms = entityKnowledgeProvide.getAllTerms();
        this.entityKnowledgeProvide = entityKnowledgeProvide;
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
        this.index = index;
        iterator = this.allTerms.values().iterator();
    }

    @Override
    public Object[] getHeader() {
        return new Object[]{"termId", "category", "length", "numTokens",
            TextToken.Type.WORD.getName(),
            TextToken.Type.ALPHANUMERIC.getName(),
            TextToken.Type.STOP.getName(),
            TextToken.Type.GENE_SEQ.getName(),
            TextToken.Type.NUMBER.getName(),
            TextToken.Type.ROMAN.getName(),
            TextToken.Type.PERCENTAGE.getName(),};
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public List next() {
        Term term = iterator.next();
        List termRecord = new ArrayList();
        termRecord.add(term.getId());

        Concept concept = allConcepts.get(term.getConceptId());
        if (concept != null) {
            String category = allConcepts.get(term.getConceptId()).getCategory().toLowerCase();
            if (category.length() > 18) {
                termRecord.add(category.substring(0, 13) + "..");
            } else {
                termRecord.add(category);
            }
        } else {
            termRecord.add("unknown");
        }
        termRecord.add(term.getTerm().length());
        termRecord.add(index.getDocumentNumTerms(term.getId()));

        List<TextToken> tokens = KSAnalysis.textPipe.getTokens(term.getTerm());
        Map<TextToken.Type, IntCount> tokenTypeCounts = countTokenTypes(tokens);
        termRecord.add(getTokenTypeCount(tokenTypeCounts, TextToken.Type.WORD));
        termRecord.add(getTokenTypeCount(tokenTypeCounts, TextToken.Type.ALPHANUMERIC));
        termRecord.add(getTokenTypeCount(tokenTypeCounts, TextToken.Type.STOP));
        termRecord.add(getTokenTypeCount(tokenTypeCounts, TextToken.Type.GENE_SEQ));
        termRecord.add(getTokenTypeCount(tokenTypeCounts, TextToken.Type.NUMBER));
        termRecord.add(getTokenTypeCount(tokenTypeCounts, TextToken.Type.ROMAN));
        termRecord.add(getTokenTypeCount(tokenTypeCounts, TextToken.Type.PERCENTAGE));

        return termRecord;
    }

    private Map<TextToken.Type, IntCount> countTokenTypes(List<TextToken> tokens) {
        Map<TextToken.Type, IntCount> countsMap = new HashMap<TextToken.Type, IntCount>();
        for (TextToken token : tokens) {
            IntCount count = countsMap.get(token.getType());
            if (count == null) {
                countsMap.put(token.getType(), new IntCount());
            } else {
                count.increment();
            }
        }
        return countsMap;
    }

    private int getTokenTypeCount(Map<TextToken.Type, IntCount> tokenTypeCounts, TextToken.Type type) {
        IntCount count = tokenTypeCounts.get(type);
        if (count != null) {
            return count.getValue();
        }
        return 0;
    }

    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}

class RelationsRecordGenerator implements CSVUtil.CSVRecordGenerator {

    private final Map<String, Relationship> allRelations;

    private final EntityKnowledgeProvider entityKnowledgeProvide;

    private final HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private final Iterator<Relationship> iterator;

    public RelationsRecordGenerator(EntityKnowledgeProvider entityKnowledgeProvide, HierarchyKnowledgeProvider hierarchyKnowledgeProvider) {
        this.allRelations = entityKnowledgeProvide.getAllRelations(Relationship.RelationType.IS_A);
        this.entityKnowledgeProvide = entityKnowledgeProvide;
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
        this.iterator = allRelations.values().iterator();
    }

    @Override
    public Object[] getHeader() {
        return new Object[]{"idxterm", "globalFreq", "documentFreq", "stem", "suffix"};
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public List next() {
        Relationship relationship = iterator.next();
        List relationshipRecord = new ArrayList();
        relationshipRecord.add(relationship.getId());
        Concept relConcept = entityKnowledgeProvide.getByConceptId(relationship.getType());
        if (relConcept != null) {
            relationshipRecord.add(relConcept.getDescription());
        } else {
            relationshipRecord.add(relationship.getType());
        }
        return relationshipRecord;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

class IntCount {

    private int value;

    public IntCount() {
        this.value = 1;
    }

    public int getValue() {
        return value;
    }

    public void increment() {
        value++;
    }

}
