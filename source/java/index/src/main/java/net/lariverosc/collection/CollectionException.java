package net.lariverosc.collection;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CollectionException extends RuntimeException {

    public CollectionException() {
    }

    public CollectionException(String string) {
        super(string);
    }

    public CollectionException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }
}
