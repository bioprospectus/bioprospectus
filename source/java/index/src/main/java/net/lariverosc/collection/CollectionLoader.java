package net.lariverosc.collection;

import java.util.Map;

/**
 * This interface abstract the collection loading process.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface CollectionLoader {

    String getCollectionName();

    /**
     * This methods loads a collection and returns it in a map.
     *
     * @return a map in which its keys must be the documentIds and the values
     * must be the document content.
     */
    Map<String, String> getCollection();

}
