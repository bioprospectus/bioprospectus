package net.lariverosc.collection;

import com.google.common.base.Splitter;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.lariverosc.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is designed to load a dictionary for a file, in which each line contains an entry.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DictionaryCollectionLoader implements CollectionLoader {

    private final Logger log = LoggerFactory.getLogger(DictionaryCollectionLoader.class);

    private final String collectionName;

    private final String filePath;

    private final String encoding;

    private final String separator;

    public DictionaryCollectionLoader(String filePath, String encoding, String separator) throws IOException {
        this.collectionName = IOUtils.getFileName(filePath, false);
        this.filePath = filePath;
        this.encoding = encoding;
        this.separator = separator;
    }

    @Override
    public String getCollectionName() {
        return collectionName;
    }

    @Override
    public Map<String, String> getCollection() {
        log.info("Loading file {} ...", filePath);
        BufferedReader bufferedReader = null;
        try {
            Map<String, String> temp = new HashMap<String, String>();
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), encoding));
            String line;
            long count = 1L;
            while ((line = bufferedReader.readLine()) != null) {
                if (separator == null) {
                    temp.put(String.valueOf(count), line);
                } else {
                    List<String> lineSplited = Splitter.on(separator)
                            .trimResults()
                            .omitEmptyStrings()
                            .limit(2)
                            .splitToList(line);
                    try {
                        String id = lineSplited.get(0);
                        log.info(lineSplited.toString());
                        temp.put(id, lineSplited.get(1));
                    } catch (NumberFormatException nfe) {
                        log.error("Exception while parsing line: " + line, nfe);
                    }
                    catch (IndexOutOfBoundsException iob){
                        log.error(lineSplited.get(1));
                        break;
                    }
                }
                count++;
            }
            bufferedReader.close();
            return temp;
        } catch (IOException ex) {
            log.error("Error while loading the collection", ex);
            throw new CollectionException("Error while loading the collection file" + filePath, ex);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                }
            }
        }
    }
}
