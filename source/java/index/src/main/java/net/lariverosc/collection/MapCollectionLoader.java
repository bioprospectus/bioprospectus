package net.lariverosc.collection;

import java.util.Collections;
import java.util.Map;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MapCollectionLoader implements CollectionLoader {

    private final String collectionName;
    private final Map<String, String> collection;

    public MapCollectionLoader(String collectionName, Map<String, String> collection) {
        this.collectionName = collectionName;
        this.collection = Collections.unmodifiableMap(collection);
    }

    @Override
    public String getCollectionName() {
        return collectionName;
    }

    @Override
    public Map<String, String> getCollection() {
        return collection;
    }

}
