package net.lariverosc.collection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MySqlCollectionLoader implements CollectionLoader {

    private static final Logger log = LoggerFactory.getLogger(MySqlCollectionLoader.class);

    private final String host;

    private final int port;

    private final String dbName;

    private final String dbUser;

    private final String dbPass;

    private final String collectionQuery;

    public MySqlCollectionLoader(String host, int port, String dbName, String dbUser, String dbPass, String collectionQuery) {
        this.host = host;
        this.port = port;
        this.dbName = dbName;
        this.dbUser = dbUser;
        this.dbPass = dbPass;
        this.collectionQuery = collectionQuery;
    }

    @Override
    public String getCollectionName() {
        return dbName;
    }

    @Override
    public Map<String, String> getCollection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.error("MySql jdbc driver not loaded");
            return null;
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbName, dbUser, dbPass);
        } catch (SQLException sqle) {
            log.error("Error while try to connect with MySql", sqle);
            return null;
        }
        ResultSet resultSet = null;
        try {
            Map<String, String> collection = new HashMap<String, String>();
            resultSet = connection.prepareStatement(collectionQuery).executeQuery();
            while (resultSet.next()) {
                String id = resultSet.getString(1);
                String text = resultSet.getString(2);
                collection.put(id, text);
            }
            return collection;
        } catch (SQLException sqle) {
            log.error("Error while execute collection query", sqle);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException sqle) {
                    log.error("Error while closing ResultSet", sqle);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException sqle) {
                    log.error("Error while closing Connection", sqle);
                }
            }
        }
        return null;
    }
}
