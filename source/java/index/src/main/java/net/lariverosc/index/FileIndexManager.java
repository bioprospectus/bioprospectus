package net.lariverosc.index;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import net.lariverosc.collection.CollectionException;
import net.lariverosc.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class FileIndexManager implements IndexManager {

    private final Logger log = LoggerFactory.getLogger(FileIndexManager.class);

    private final String indexPath;

    private final String FILE_EXTENSION = ".sobj";

    private final Map<String, Index> indexReferences;

    public FileIndexManager(String indexPath) {
        this.indexPath = IOUtils.fixPath(indexPath);
        this.indexReferences = new HashMap<String, Index>();
    }

    @Override
    public Index loadIndex(String indexName) throws IndexException {
        log.info("Loading index {}", indexName);
        final String indexFullPath = indexPath + indexName + FILE_EXTENSION;
        File file = new File(indexFullPath);
        if (!file.exists()) {
            throw new IndexException("Index file not exist: " + indexFullPath);
        }
        Index index = indexReferences.get(indexName);
        if (index == null) {
            index = deserializeIndex(indexFullPath);
            indexReferences.put(indexName, index);
        }
        return index;
    }

    private Index deserializeIndex(String indexPath) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(indexPath);
            ObjectInputStream objectInputStream = new ObjectInputStream(new BufferedInputStream(fileInputStream));
            Index dictionaryIndex = (Index) objectInputStream.readObject();
            objectInputStream.close();
            return dictionaryIndex;
        } catch (ClassNotFoundException cnfe) {
            log.error("Error while deserialize index", cnfe);
        } catch (IOException ioex) {
            log.error("Error while deserialize index", ioex);
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException ex) {
                }
            }
        }
        return null;
    }

    @Override
    public void saveIndex(Index index) {
        log.info("Saving index {}", index.getIndexName());
        FileOutputStream fileOutputStream = null;
        try {
            File file = new File(indexPath + File.separator + index.getIndexName() + FILE_EXTENSION);
            if (!file.exists()) {
                file.createNewFile();
            }
            fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new BufferedOutputStream(fileOutputStream));
            objectOutputStream.writeObject(index);
            objectOutputStream.close();
        } catch (IOException ioex) {
            log.error("Error while serialize index", ioex);
            throw new CollectionException("Error while persist index", ioex);
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException ex) {
                }
            }
        }
    }
}
