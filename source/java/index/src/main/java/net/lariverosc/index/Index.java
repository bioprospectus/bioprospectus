package net.lariverosc.index;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a simple inverted index for a collection.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Index implements Serializable {

    private static final long serialVersionUID = 2354916668229110803L;

    private final Logger log = LoggerFactory.getLogger(Index.class);

    /**
     * The index name.
     */
    private final String indexName;

    /**
     * This map stores the document identifier and data about the document
     */
    private Map<String, String> indexDocuments;

    /**
     * This map stores the document tokens
     */
    private Map<String, List<String>> documentsTokens;

    /**
     * This map stores the main inverted index. The entries in this map have the following form < String, Set<TermOccurence> >,
     * where the String key correspond to a term, and the Set of {@link TermOccurrence} contains the details for every term
     * occurrence.
     */
    private Map<String, Set<TermOccurrence>> invertedIndex;

    /**
     * This map stores the term frequencies within the whole index. The entries in this map have the following form
     * <String, IntCount>, where the String key correspond to a term and the {@link IntCount} correspond to the global frequency
     * of the term.
     */
    private Map<String, IntCount> termGlobalFrequencies;

    /**
     * This map stores the term frequencies within each document. The entries in this map have the following form < String,
     * Map<String, IntCount> >, where the String key correspond to a documentId and the map value contains one entry for each
     * term in the document along with its corresponding frequency.
     */
    private Map<String, Map<String, IntCount>> termByDocumentFrequencies;

    /**
     * This map stores the documents associated with a term. The entries in this map have the following form <String,
     * Set<String>>, where the String key corresponds to a term and the contains all the documents id's that contains such term.
     */
    private Map<String, Set<String>> documentByTerm;

    /**
     * This map stores the number of terms for each document. The entries in this map have the following form
     * <String, IntCount>, where the String key correspond to a documentId and the {@link IntCount} correspond to the total
     * number of terms in that document.
     */
    private Map<String, IntCount> documentNumTerms;

    /**
     * This map allows to group index terms, it is useful to store category, stemming or synonym information
     */
    private Map<String, Set<String>> groupTermsMap;

    /**
     * Default constructor used to initialize the data structures.
     *
     * @param indexName
     */
    public Index(String indexName) {
        this.indexName = indexName;
        initEmptyIndex();
    }

    private void initEmptyIndex() {
        indexDocuments = new HashMap<String, String>();
        documentsTokens = new HashMap<String, List<String>>();
        invertedIndex = new HashMap<String, Set<TermOccurrence>>();
        termGlobalFrequencies = new HashMap<String, IntCount>();
        termByDocumentFrequencies = new HashMap<String, Map<String, IntCount>>();
        documentByTerm = new HashMap<String, Set<String>>();
        documentNumTerms = new HashMap<String, IntCount>();
        groupTermsMap = new HashMap<String, Set<String>>();
    }

    /**
     * This method is used to add a term occurrence to the index. Each call to this method will add a {@link TermOccurrence} in
     * the invertedIndex, updates the term index and document frequencies and documentNumTokens counts and add or update the
     * stems mapping.
     *
     * @param term The term to be indexed
     * @param documentId The documentId of the document currently indexed
     * @param position The position of the term within the document.
     */
    public void addTermOccurence(String term, String documentId, int position) {

        updateInvertedIndex(term, new TermOccurrence(documentId, position));

        updateGlobalFrequencies(term);

        updateTermByDocumentFrequencies(documentId, term);

        updateDocumentByTerm(term, documentId);

        updateDocumentNumTerms(documentId);

    }

    private void updateInvertedIndex(String term, TermOccurrence termOccurence) {
        Set<TermOccurrence> tempSet = invertedIndex.get(term);
        if (tempSet == null) {
            tempSet = new HashSet<TermOccurrence>();
        }
        tempSet.add(termOccurence);
        invertedIndex.put(term, tempSet);
    }

    private void updateGlobalFrequencies(String term) {
        if (termGlobalFrequencies.containsKey(term)) {
            termGlobalFrequencies.get(term).increment();
        } else {
            termGlobalFrequencies.put(term, new IntCount());
        }
    }

    private void updateTermByDocumentFrequencies(String documentId, String term) {
        Map<String, IntCount> documentTermsMap = termByDocumentFrequencies.get(documentId);
        if (documentTermsMap == null) {
            documentTermsMap = new HashMap<String, IntCount>();
        }
        if (documentTermsMap.containsKey(term)) {
            documentTermsMap.get(term).increment();
        } else {
            documentTermsMap.put(term, new IntCount());
        }
        termByDocumentFrequencies.put(documentId, documentTermsMap);
    }

    private void updateDocumentByTerm(String term, String documentId) {
        Set<String> documentIds = documentByTerm.get(term);
        if (documentIds == null) {
            documentIds = new HashSet<String>();
            documentByTerm.put(term, documentIds);
        }
        documentIds.add(documentId);
    }

    private void updateDocumentNumTerms(String documentId) {
        IntCount numTerms = documentNumTerms.get(documentId);
        if (numTerms != null) {
            numTerms.increment();
        } else {
            documentNumTerms.put(documentId, new IntCount());
        }
    }

    /**
     * Perform an inverted index lookup using the given term.
     *
     * @param term the term to find.
     *
     * @return A Set with all the {@link TermOccurrence} in the index, this Set can be empty.
     */
    public Set<TermOccurrence> lookup(String term) {
        Set<TermOccurrence> tempSet = invertedIndex.get(term);
        if (tempSet != null) {
            return tempSet;
        }
        return Collections.EMPTY_SET;
    }

    public void addTermGroupEntry(String groupId, String term) {
        Set<String> groupTerms = groupTermsMap.get(groupId);
        if (groupTerms == null) {
            groupTerms = new HashSet<String>();
        }
        groupTerms.add(term);
        groupTermsMap.put(groupId, groupTerms);
    }

    public Set<String> getGroupTerms(String groupId) {
        Set<String> groupTerms = groupTermsMap.get(groupId);
        if (groupTerms != null) {
            return groupTerms;
        }
        return Collections.EMPTY_SET;
    }

    public Set<TermOccurrence> lookupByGroup(String groupId) {
        Set<String> groupTerms = groupTermsMap.get(groupId);
        Set<TermOccurrence> returnSet = new HashSet<TermOccurrence>();
        for (String tempTerm : groupTerms) {
            Set<TermOccurrence> tempSet = invertedIndex.get(tempTerm);
            if (tempSet != null && !tempSet.isEmpty()) {
                returnSet.addAll(tempSet);
            }
        }
        return returnSet;
    }

    public Set<String> getIndexGroups() {
        return groupTermsMap.keySet();
    }

    public void removeTerm(String term) {

    }

    public int getTotalTerms() {
        return invertedIndex.size();
    }

    /**
     * This method returns the set of terms in the index
     *
     * @return A Set that contains all the terms in the index.
     */
    public Set<String> getTerms() {
        return invertedIndex.keySet();
    }

    /**
     * This method returns the frequency of the given term in the whole index.
     *
     * @param term
     *
     * @return the term frequency, can be zero.
     */
    public int getGlobalFrequency(String term) {
        IntCount termFrequency = termGlobalFrequencies.get(term);
        if (termFrequency != null) {
            return termFrequency.getValue();
        }
        return 0;
    }

    /**
     *
     * @return
     */
    public Map<String, Integer> getGlobalFrequencies() {
        Map<String, Integer> tempMap = new HashMap<String, Integer>();
        for (String term : termGlobalFrequencies.keySet()) {
            tempMap.put(term, termGlobalFrequencies.get(term).getValue());
        }
        return tempMap;
    }

    /**
     * This method returns the frequency of the given term within the given document.
     *
     * @param documentId
     * @param term
     *
     * @return the term frequency in the given document, can be zero.
     */
    public int getDocumentFrequency(String documentId, String term) {
        Map<String, IntCount> documentTermsMap = termByDocumentFrequencies.get(documentId);
        if (documentTermsMap != null) {
            IntCount termFrequecy = documentTermsMap.get(term);
            if (termFrequecy != null) {
                return termFrequecy.getValue();
            }
        }
        return 0;
    }

    /**
     *
     * @param term
     *
     * @return
     */
    public Set<String> getDocumentsByTerm(String term) {
        return documentByTerm.get(term);
    }

    /**
     * This method returns the total number of terms in the given document.
     *
     * @param documentId
     *
     * @return the number of terms in the given document, can be zero.
     */
    public int getDocumentNumTerms(String documentId) {
        IntCount numTerms = documentNumTerms.get(documentId);
        if (numTerms != null) {
            return numTerms.getValue();
        }
        return 0;
    }

    /**
     * Stores the document source within the index.
     *
     * @param documentId
     * @param data
     */
    public void addDocument(String documentId, String data) {
        indexDocuments.put(documentId, data);
    }

    /**
     * Retrieves the document source from the index.
     *
     * @param documentId
     *
     * @return
     */
    public String getDocument(String documentId) {
        return indexDocuments.get(documentId);
    }

    public void addDocumentTokens(String documentId, List<String> tokens) {
        documentsTokens.put(documentId, tokens);
    }

    public List<String> getDocumentTokens(String documentId) {
        return documentsTokens.get(documentId);
    }

    /**
     *
     * @return
     */
    public int getTotalDocuments() {
        return indexDocuments.size();
    }

    /**
     * Returns the index name
     *
     * @return The index name
     */
    public String getIndexName() {
        return indexName;
    }

    //protected methods for indexManager implementations only
    protected Map<String, Set<TermOccurrence>> getInvertedIndex() {
        return invertedIndex;
    }

    protected void setInvertedIndex(Map<String, Set<TermOccurrence>> invertedIndex) {
        this.invertedIndex = invertedIndex;
    }

    protected Map<String, IntCount> getTermGlobalFrequencies() {
        return termGlobalFrequencies;
    }

    protected void setTermGlobalFrequencies(Map<String, IntCount> termGlobalFrequencies) {
        this.termGlobalFrequencies = termGlobalFrequencies;
    }

    protected Map<String, Map<String, IntCount>> getTermByDocumentFrequencies() {
        return termByDocumentFrequencies;
    }

    protected void setTermByDocumentFrequencies(Map<String, Map<String, IntCount>> termByDocumentFrequencies) {
        this.termByDocumentFrequencies = termByDocumentFrequencies;
    }

    protected Map<String, Set<String>> getDocumentByTerm() {
        return documentByTerm;
    }

    protected void setDocumentByTerm(Map<String, Set<String>> documentByTerm) {
        this.documentByTerm = documentByTerm;
    }

    protected Map<String, IntCount> getDocumentNumTerms() {
        return documentNumTerms;
    }

    protected void setDocumentNumTerms(Map<String, IntCount> documentNumTerms) {
        this.documentNumTerms = documentNumTerms;
    }

    protected Map<String, String> getIndexDocuments() {
        return indexDocuments;
    }

    protected void setIndexDocuments(Map<String, String> indexDocuments) {
        this.indexDocuments = indexDocuments;
    }

    protected Map<String, List<String>> getDocumentsTokens() {
        return documentsTokens;
    }

    protected void setDocumentsTokens(Map<String, List<String>> documentsTokens) {
        this.documentsTokens = documentsTokens;
    }

    protected Map<String, Set<String>> getGroupTermsMap() {
        return groupTermsMap;
    }

    protected void setGroupTermsMap(Map<String, Set<String>> groupTermsMap) {
        this.groupTermsMap = groupTermsMap;
    }

    /**
     * Helper method used to print the index term list with its frequencies.
     */
    public void printTermsFrequency() {
        System.out.println("TERM FREQUENCY LIST");
        System.out.println("TOTAL TERMS:" + getTerms().size());
        List<String> termsList = new ArrayList<String>(getTerms());
        Collections.sort(termsList);
        for (String term: termsList) {
            System.out.println(term + " " + getGlobalFrequency(term));
        }
    }

    protected class IntCount implements Serializable {

        private static final long serialVersionUID = 3614545631758636230L;

        private int value;

        /**
         * Instance a count initialized in 1.
         *
         */
        public IntCount() {
            this.value = 1;
        }

        /**
         * This method returns the count value.
         *
         * @return the current count value.
         */
        public int getValue() {
            return value;
        }

        /**
         * This method increments the count current value by one.
         */
        public void increment() {
            value++;
        }

    }
}
