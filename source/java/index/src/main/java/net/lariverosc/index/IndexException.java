package net.lariverosc.index;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class IndexException extends RuntimeException {

    public IndexException(String message) {
        super(message);
    }

    public IndexException(String message, Throwable cause) {
        super(message, cause);
    }

}
