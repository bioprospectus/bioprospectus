package net.lariverosc.index;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface IndexManager {

    public Index loadIndex(String indexName) throws IndexException;

    public void saveIndex(Index index) throws IndexException;
}
