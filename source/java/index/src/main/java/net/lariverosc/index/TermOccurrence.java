package net.lariverosc.index;

import java.io.Serializable;
import java.util.Objects;

/**
 * This class represents a term occurrence within a document as a pair {documentId, position}.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TermOccurrence implements Serializable {

    private static final long serialVersionUID = -203011557022705264L;

    /*
     * The document id
     */
    private String documentId;

    /*
     * The position of the term ocurrence within this document.
     */
    private int position;

    public TermOccurrence(String documentId, int position) {
        this.documentId = documentId;
        this.position = position;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.documentId);
        hash = 43 * hash + this.position;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TermOccurrence other = (TermOccurrence) obj;
        if (this.documentId != other.documentId) {
            return false;
        }
        if (this.position != other.position) {
            return false;
        }
        return true;
    }
}
