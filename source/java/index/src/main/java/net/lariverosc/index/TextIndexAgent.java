package net.lariverosc.index;

import java.util.List;
import java.util.Map;
import net.lariverosc.collection.CollectionLoader;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;
import net.lariverosc.text.stemming.SnowballStemmer;
import net.lariverosc.text.stemming.Stemmer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextIndexAgent {

    private final Logger log = LoggerFactory.getLogger(TextIndexAgent.class);

    private CollectionLoader collectionLoader;

    private final TextPipe textPipe;

    public TextIndexAgent() {
        TextPipeConfig textPipeConfig = new TextPipeConfig().withLanguage(TextPipeConfig.Language.ENGLISH)
                .withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD)
                .withMaxTokenLength(25)
                .addTokenFilter(TextPipeConfig.TokenFilter.STOP_WORD)
                .withTokenTransforms(TextPipeConfig.TokenTransform.LOWER_CASE);
        textPipe = new TextPipe(textPipeConfig);
    }

    public TextIndexAgent(TextPipeConfig textPipeConfig) {
        textPipe = new TextPipe(textPipeConfig);
    }

    public Index buildIndex() {
        Map<String, String> collection = collectionLoader.getCollection();
        log.info("{} documents in collection {}", new Object[]{collection.size(), collectionLoader.getCollectionName()});
        Index index = new Index(collectionLoader.getCollectionName());
        int progress = 1;
        Stemmer stemmer = new SnowballStemmer(textPipe.getTextPipeConfig().getLanguage());

        for (Map.Entry<String, String> document : collection.entrySet()) {

            String documentId = document.getKey();
            String documentContent = document.getValue();
            log.info("{}/{} -> [{}, {}]", new Object[]{progress, collection.size(), documentId, documentContent});
            List<String> tokens = textPipe.getTokensAsString(documentContent);
            index.addDocumentTokens(documentId, tokens);
            for (int i = 0; i < tokens.size(); i++) {
                String token = tokens.get(i);
                String stem = stemmer.stem(token);
                index.addTermOccurence(token, documentId, i);
                index.addTermGroupEntry(stem, token);
            }
            index.addDocument(documentId, documentContent);
            progress++;
        }

        log.info("{} documents indexed", collection.size());

        return index;

    }

    public void setCollectionLoader(CollectionLoader collectionLoader) {
        this.collectionLoader = collectionLoader;
    }

}
