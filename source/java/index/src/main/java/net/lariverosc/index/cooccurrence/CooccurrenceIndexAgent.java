package net.lariverosc.index.cooccurrence;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.lariverosc.collection.CollectionLoader;
import net.lariverosc.collection.DictionaryCollectionLoader;
import net.lariverosc.index.Index;
import net.lariverosc.text.util.CollocationUtils;
import net.lariverosc.text.nlp.SentenceDetector;
import net.lariverosc.text.nlp.opennlp.OpenNlpSentenceDetector;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CooccurrenceIndexAgent {

    private final Logger log = LoggerFactory.getLogger(CooccurrenceIndexAgent.class);

    private CollectionLoader collectionLoader;

    private TextPipe textPipe;

    private SentenceDetector sentenceDetector;

    public Map<String, Index> buildIndex() throws IOException {
        Map<String, String> collection = collectionLoader.getCollection();
        Index index_1g = new Index(collectionLoader.getCollectionName() + "-1g");
        Index index_2g = new Index(collectionLoader.getCollectionName() + "-2g");
        CollocationUtils windowedFeature = new CollocationUtils(4, 4);
        int progress = 1;
        for (String documentId: collection.keySet()) {
            String documentContent = collection.get(documentId);
            log.debug("{}/{} -> [{}, {}]", new Object[]{progress, collection.size(), documentId, documentContent.substring(0, 30)});
            String[] documentSentences = sentenceDetector.getSentencesAsString(documentContent);//TODO param to control this
            for (String sentence: documentSentences) {
                List<String> textTokens = textPipe.getTokensAsString(sentence);
                List<List<String>> windowList = windowedFeature.getWindowList(textTokens);
                for (int i = 0; i < textTokens.size(); i++) {
                    String token = textTokens.get(i);
                    index_1g.addTermOccurence(token, documentId, i);
                    List<String> tokenWindow = windowList.get(i);
                    for (String tokenInWindow: tokenWindow) {
                        String[] coocurrence = new String[]{token, tokenInWindow};//Bigrams
                        Arrays.sort(coocurrence);
                        index_2g.addTermOccurence(coocurrence[0] + "_" + coocurrence[1], documentId, i);
                    }
                }
                index_1g.addDocument(documentId, documentContent);
                index_2g.addDocument(documentId, documentContent);
            }
            progress++;
        }
        Map<String, Index> indexMap = new HashMap<String, Index>();
        indexMap.put("1g", index_1g);
        indexMap.put("2g", index_2g);
        return indexMap;
    }

    public void setCollectionLoader(CollectionLoader collectionLoader) {
        this.collectionLoader = collectionLoader;
    }

    public void setTextPipe(TextPipe textPipe) {
        this.textPipe = textPipe;
    }

    public void setSentenceDetector(SentenceDetector sentenceDetector) {
        this.sentenceDetector = sentenceDetector;
    }

    public static void main(String[] args) throws IOException {
        TextPipeConfig textPipeConfig = new TextPipeConfig().withLanguage(TextPipeConfig.Language.ENGLISH)
                .withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD)
                .withMaxTokenLength(25)
                .addTokenFilter(TextPipeConfig.TokenFilter.STOP_WORD)
                .addTokenTransform(TextPipeConfig.TokenTransform.STEM);
        CooccurrenceIndexAgent cia = new CooccurrenceIndexAgent();
        //
        cia.setCollectionLoader(new DictionaryCollectionLoader("/home/bio/TERM_MAPPER_HOME/data/terminos.txt", "utf-8", null));
        cia.setTextPipe(new TextPipe(textPipeConfig));
        cia.setSentenceDetector(new OpenNlpSentenceDetector("/home/bio/TERM_MAPPER_HOME/data/en-sent.bin"));
        cia.buildIndex();

    }

}
