package net.lariverosc.index.cooccurrence;

import java.util.Arrays;
import net.lariverosc.index.Index;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MutualInformation {

    public static double getPMI(Index index1g, Index index2g, String term1, String term2) {
        return getPMI(index1g, index2g, term1, term2, 0);
    }

    public static double getPMI(Index index1g, Index index2g, String term1, String term2, int freqThreshold) {
        int term1Freq = index1g.getGlobalFrequency(term1);
        int term2Freq = index1g.getGlobalFrequency(term2);
        if (term1Freq < freqThreshold || term2Freq < freqThreshold) {
            return 0;
        }
        double totalTerms1g = (double) index1g.getTotalTerms();
        double p1 = (double) term1Freq / totalTerms1g;
        double p2 = (double) term2Freq / totalTerms1g;

        String mergedTerm = getMergedTerm(term1, term2);
        double totalTerms2g = (double) index2g.getTotalTerms();
        double p1_p2 = (double) index2g.getGlobalFrequency(mergedTerm) / totalTerms2g;

        if (p1_p2 == 0) {
            return 0;
        }
        return Math.log(p1 * p2) / Math.log(p1_p2) - 1;
    }

    private static String getMergedTerm(String... terms) {
        Arrays.sort(terms);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < terms.length; i++) {
            if (i < terms.length - 1) {
                sb.append(terms[i]).append("_");
            } else {
                sb.append(terms[i]);
            }
        }
        return sb.toString();
    }

}
