package net.lariverosc.index.weigthing;

import net.lariverosc.index.Index;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TFIDF {

    private static final double LOG2 = Math.log(2);

    public static double getWeight(Index index, String documentId, String term) {
        double tf = index.getDocumentFrequency(documentId, term);
        double idf = Math.log(index.getTotalDocuments() / index.getDocumentsByTerm(term).size()) / LOG2;
        return tf * idf;
    }

}
