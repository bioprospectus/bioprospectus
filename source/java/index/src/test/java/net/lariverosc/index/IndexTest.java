package net.lariverosc.index;

import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class IndexTest {

    @Test
    public void shouldIndexOneTerm() {
        Index collectionIndex = new Index("test");

        collectionIndex.addTermOccurence("word", "1", 1);

        assertEquals(1, collectionIndex.getTerms().size());
    }

    @Test
    public void shouldIndexOneTermTwoTimes() {
        Index index = new Index("test");
        String term = "word";

        index.addTermOccurence(term, "1", 1);
        index.addTermOccurence(term, "1", 2);

        assertEquals(1, index.getTerms().size());
        assertEquals(2, index.getGlobalFrequency("word"));
    }

    @Test
    public void shouldIndexTwoTerms() {
        Index index = new Index("test");

        index.addTermOccurence("word1", "1", 1);
        index.addTermOccurence("word2", "1", 2);

        assertEquals(2, index.getTerms().size());
        assertEquals(1, index.getGlobalFrequency("word1"));
        assertEquals(1, index.getGlobalFrequency("word2"));
    }

    @Test
    public void shouldIndexOneTermInTwoDocuments() {
        Index index = new Index("test");
        String term = "word";

        index.addTermOccurence(term, "1", 1);
        index.addTermOccurence(term, "2", 1);

        assertEquals(1, index.getTerms().size());
        assertEquals(2, index.getGlobalFrequency(term));
        assertEquals(1, index.getDocumentFrequency("1", term));
        assertEquals(1, index.getDocumentFrequency("2", term));
    }

    @Test
    public void shouldIndexTwoTermsInOneDocument() {
        Index index = new Index("test");

        index.addTermOccurence("word1", "1", 1);
        index.addTermOccurence("word2", "1", 2);

        assertEquals(2, index.getTerms().size());
        assertEquals(1, index.getGlobalFrequency("word1"));
        assertEquals(1, index.getGlobalFrequency("word2"));
        assertEquals(1, index.getDocumentFrequency("1", "word1"));
        assertEquals(1, index.getDocumentFrequency("1", "word2"));
        assertEquals(2, index.getDocumentNumTerms("1"));
    }

    @Test
    public void shouldReturnZeroOnNonIndexTerm() {
        Index index = new Index("test");
        assertEquals(0, index.getGlobalFrequency("word"));
    }

    @Test
    public void shouldReturnZeroOnNonIndexDocument() {
        Index index = new Index("test");

        assertEquals(0, index.getDocumentFrequency("1", "word"));
        assertEquals(0, index.getDocumentNumTerms("1"));
    }

    @Test
    public void sholdIndexAndReturnOneTermAndOneDocument() {
        Index index = new Index("test");
        String term = "word";

        index.addTermOccurence(term, "1", 1);

        assertEquals(1, index.lookup(term).size());
        assertEquals(1, index.lookup(term).iterator().next().getPosition());
    }

    @Test
    public void sholdIndexAndReturnTwoTermsAndOneDocument() {
        Index index = new Index("test");

        index.addTermOccurence("word1", "1", 1);
        index.addTermOccurence("word2", "1", 2);

        assertEquals(1, index.lookup("word1").size());
        assertEquals(1, index.lookup("word1").iterator().next().getPosition());
        assertEquals(1, index.lookup("word2").size());
        assertEquals(2, index.lookup("word2").iterator().next().getPosition());
    }

    @Test
    public void sholdIndexAndReturnOneTermAndTwoDocuments() {
        Index index = new Index("test");
        String term = "word";

        index.addTermOccurence(term, "1", 1);
        index.addTermOccurence(term, "2", 1);

        assertEquals(2, index.lookup(term).size());
    }
}
