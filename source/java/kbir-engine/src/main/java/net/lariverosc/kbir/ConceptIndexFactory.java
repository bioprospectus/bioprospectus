package net.lariverosc.kbir;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import net.lariverosc.text.index.Index;
import net.lariverosc.text.index.MemoryIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ConceptIndexFactory {

    private final Logger log = LoggerFactory.getLogger(ConceptIndexFactory.class);

    private DBCollection dbCollection;

    public Index create() {
        log.info("Initializing mappings index");
        MemoryIndex<String> memoryIndex = new MemoryIndex<>("conceptsIndex");
        BasicDBObject query = new BasicDBObject("mappings", new BasicDBObject("$exists", true));
        BasicDBObject fields = new BasicDBObject();
        fields.put("_id", 1);
        fields.put("article.@pmid", 1);
        fields.put("mappings.all_mappings_freq", 1);
        DBCursor dbCursor = dbCollection.find(query, fields);
        while (dbCursor.hasNext()) {
            DBObject dbObject = dbCursor.next();
            DBObject article = (DBObject) dbObject.get("article");
            String pmid = article.get("@pmid").toString();
            DBObject mappings = (DBObject) dbObject.get("mappings");
            BasicDBObject mappingsFreq = (BasicDBObject) mappings.get("all_mappings_freq");
            for (String mappingId : mappingsFreq.keySet()) {
                int freq = (Integer) mappingsFreq.get(mappingId);
                for (int i = 0; i < freq; i++) {
                    memoryIndex.addTermOccurrence(pmid, mappingId, null);
                }
            }
        }
        return memoryIndex;
    }

    public void setDbCollection(DBCollection dbCollection) {
        this.dbCollection = dbCollection;
    }

}
