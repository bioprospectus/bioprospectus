package net.lariverosc.kbir;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.kbir.similarity.SetSemanticSimilarity;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.semsim.SimilarConcept;
import net.lariverosc.semsim.data.SemanticSimilarityManager;
import net.lariverosc.text.index.Index;
import net.lariverosc.text.util.StringUtils;
import net.lariverosc.util.Maps;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.FilteredQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class KnowledegeBasedRetrieval {

    private final Logger log = LoggerFactory.getLogger(KnowledegeBasedRetrieval.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private SemanticSimilarityManager semanticSimilarityManager;

    private SetSemanticSimilarity setSemanticSimilarity;

    private Index<String> index;

    private Client client;

    private final int NUM_SIMILAR_CONCEPTS = 5;

    private final int PRF_DOC_THRESHOLD = 10;

    private final int PRF_CONCEPT_THRESHOLD = 20;

    private final String ES_INDEX_NAME = "clef-snomeden-def"; //1095804002

    private static final Set<Character> ESUnwantedChars = new HashSet<Character>() {
        {
            add('[');
            add(']');
            add('{');
            add('}');
            add('¡');
            add('¿');
            add('|');
            add('^');
            add('~');
            add('(');
            add(')');
            add('/');
            add('\\');
        }
    };

    public Map<String, Double> retrieve(final String queryStr) {
        log.info("executing pseudo-relevance-feedback for: {}", queryStr);
        String text = queryStr;
        text = StringUtils.foldToASCII(text);
        text = StringUtils.removePunctuation(text);
        text = StringUtils.removeUnwantedChars(text, ESUnwantedChars);
        Map<String, Double> feedbackConcepts = relevanceFeedback(text);
        if (feedbackConcepts.size() > PRF_CONCEPT_THRESHOLD) {
            Map<String, Double> feedbackConceptsSubset = new LinkedHashMap<>();
            Iterator<Map.Entry<String, Double>> iterator = feedbackConcepts.entrySet().iterator();
            for (int i = 0; i < PRF_CONCEPT_THRESHOLD; i++) {
                Map.Entry<String, Double> next = iterator.next();
                feedbackConceptsSubset.put(next.getKey(), next.getValue());
            }
            logfb(feedbackConceptsSubset);
            return retrieve(feedbackConceptsSubset);
        }
        logfb(feedbackConcepts);
        return retrieve(feedbackConcepts);
    }

    private void logfb(Map<String, Double> feedbackConcepts2) {
        for (Map.Entry<String, Double> entrySet: feedbackConcepts2.entrySet()) {
            Concept concept = entityKnowledgeProvider.getByConceptId(entrySet.getKey());
            log.info(concept + " - weight:" + entrySet.getValue());
        }
    }

    public Map<String, Double> retrieve(final Map<String, Double> conceptsQuery) {
        Map<Concept, Double> conceptsQueryEntity = new HashMap<>();
        for (String conceptId: conceptsQuery.keySet()) {
            conceptsQueryEntity.put(entityKnowledgeProvider.getByConceptId(conceptId), conceptsQuery.get(conceptId));
        }

        Set<Concept> allConcepts = new HashSet<>();
        for (Concept concept: conceptsQueryEntity.keySet()) {
            log.debug("Finding similar concepts for: {}", concept);
            List<SimilarConcept> similarConcepts = semanticSimilarityManager.getTopKValues(concept, NUM_SIMILAR_CONCEPTS);
            for (SimilarConcept similarConcept: similarConcepts) {
                log.debug("Similar concept for {} found: {}", new Object[]{concept.getId(), similarConcept});
                allConcepts.add(similarConcept.getConcept());
            }
        }

        Set<String> allDocuments = new HashSet<>();
        for (Concept concept: allConcepts) {
            Set<String> documentIds = index.getDocuments(concept.getId());
            log.debug("{} documents found for concept {}", new Object[]{documentIds.size(), concept});
            if (documentIds.size() < 100) {
                allDocuments.addAll(documentIds);
            }
        }

        Map<String, Double> similarityMap = new HashMap<>();
        int i = 1;
        for (String documentId: allDocuments) {
            log.debug("{}/{} Computing semantic similarity for document {}", new Object[]{(i++), allDocuments.size(), documentId});
            List<String> documentConcepts = index.getDocumentTerms(documentId);
            Map<Concept, Double> documentWeigths = new HashMap<>();
            for (String conceptId: documentConcepts) {
                documentWeigths.put(entityKnowledgeProvider.getByConceptId(conceptId), index.getTFIDF(documentId, conceptId));
            }
            similarityMap.put(documentId, setSemanticSimilarity.getValue(conceptsQueryEntity, documentWeigths));
        }

        Map<String, Double> sortSimilarityValues = Maps.sortByValues(similarityMap, true);
        return sortSimilarityValues;
    }

    private Map<String, Double> relevanceFeedback(String queryStr) {
        SearchResponse searchResponse = client.prepareSearch(ES_INDEX_NAME)
            .setSearchType(SearchType.DEFAULT)
            .setQuery(buildQueryString(queryStr))
            .addField("article.pmid")
            .setFrom(0).setSize(PRF_DOC_THRESHOLD)
            .execute()
            .actionGet();

        SearchHits hits = searchResponse.getHits();
        List<String> pmids = new ArrayList<>();
        for (SearchHit searchHit: hits.getHits()) {
            String pmid = searchHit.field("article.pmid").getValue().toString();
            pmids.add(pmid);
        }

        Map<String, Double> termAverageWeights = new HashMap<>();
        for (String pmid: pmids) {
            Set<String> documentTerms = new HashSet<>(index.getDocumentTerms(pmid));
            for (String term: documentTerms) {
                double tfidf = index.getTFIDF(pmid, term);
                Double tfidfAverage = termAverageWeights.get(term);
                if (tfidfAverage == null) {
                    tfidfAverage = 0d;
                }
                tfidfAverage += tfidf / (double) pmids.size();
                termAverageWeights.put(term, tfidfAverage);
            }
        }

        Map<String, Double> termAverageWeightsSorted = Maps.sortByValues(termAverageWeights, true);

        return termAverageWeightsSorted;
    }

    private static FilteredQueryBuilder buildFilteredQuery(QueryStringQueryBuilder queryStringBuilder, FilterBuilder FilterBuilder) {
        return QueryBuilders.filteredQuery(queryStringBuilder, FilterBuilder);
    }

    private QueryStringQueryBuilder buildQueryString(String queryStr) {
        QueryStringQueryBuilder queryStringBuilder = QueryBuilders.queryString(queryStr);
        queryStringBuilder.field("article.title");
        queryStringBuilder.field("article.abstract");
        return queryStringBuilder;
    }

    private FilterBuilder buildMatchAllFilter() {
        return FilterBuilders.matchAllFilter();
    }

    private static FilterBuilder buildCompoundOrFilter(FilterBuilder... filters) {
        return FilterBuilders.orFilter(filters);
    }

    private FilterBuilder buildCompoundAndFilter(FilterBuilder... filters) {
        return FilterBuilders.andFilter(filters);
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setSemanticSimilarityManager(SemanticSimilarityManager semanticSimilarityManager) {
        this.semanticSimilarityManager = semanticSimilarityManager;
    }

    public void setSetSemanticSimilarity(SetSemanticSimilarity setSemanticSimilarity) {
        this.setSemanticSimilarity = setSemanticSimilarity;
    }

    public void setIndex(Index<String> index) {
        this.index = index;
    }

    public void setClient(Client client) {
        this.client = client;
    }

}
