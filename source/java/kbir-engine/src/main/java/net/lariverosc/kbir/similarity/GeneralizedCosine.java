package net.lariverosc.kbir.similarity;

import java.util.Map;
import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class GeneralizedCosine extends SetSemanticSimilarity {

    @Override
    public double getValue(Map<Concept, Double> conceptMap1, Map<Concept, Double> conceptMap2) {
        double dot = getDot(conceptMap1, conceptMap2);
        double norm1 = getNorm(conceptMap1);
        double norm2 = getNorm(conceptMap2);
        return dot / (norm1 * norm2);
    }

    private double getDot(Map<Concept, Double> conceptMap1, Map<Concept, Double> conceptMap2) {
        double dot = 0;
        for (Concept concept1 : conceptMap1.keySet()) {
            for (Concept concept2 : conceptMap2.keySet()) {
                double c1_c2_sim = semanticSimilarityManager.getValue(concept1, concept2);
                dot += conceptMap1.get(concept1) * conceptMap2.get(concept2) * c1_c2_sim;
            }
        }
        return dot;
    }

    private double getNorm(Map<Concept, Double> conceptMap) {
        double sum = 0;
        for (Concept concept : conceptMap.keySet()) {
            sum += conceptMap.get(concept);
        }
        return Math.sqrt(sum);
    }
    
    @Override
    public String getName() {
        return "GeneralizedCosine";
    }
}
