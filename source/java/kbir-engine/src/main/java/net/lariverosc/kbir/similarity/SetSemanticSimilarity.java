package net.lariverosc.kbir.similarity;

import java.util.Map;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.semsim.data.SemanticSimilarityManager;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public abstract class SetSemanticSimilarity {

    /**
     *
     */
    protected EntityKnowledgeProvider entityKnowledgeProvider;

    /**
     *
     */
    protected SemanticSimilarityManager semanticSimilarityManager;

    /**
     *
     * @param conceptSet1
     * @param conceptSet2
     * @return
     */
    public abstract double getValue(Map<Concept,Double> conceptSet1, Map<Concept,Double> conceptSet2);

    /**
     *
     * @return
     */
    public abstract String getName();

    /**
     *
     * @param entityKnowledgeProvider
     */
    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    /**
     *
     * @param semanticSimilarityManager
     */
    public void setSemanticSimilarityManager(SemanticSimilarityManager semanticSimilarityManager) {
        this.semanticSimilarityManager = semanticSimilarityManager;
    }
}
