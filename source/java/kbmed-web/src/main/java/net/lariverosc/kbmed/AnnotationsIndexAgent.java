package net.lariverosc.kbmed;

import java.util.List;
import java.util.Set;
import net.lariverosc.index.Index;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.AnnotationsProvider;
import net.lariverosc.text.document.DocumentsProvider;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AnnotationsIndexAgent {

    private DocumentsProvider documentsProvider;

    private AnnotationsProvider annotationsProvider;

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    public Index buildIndex() {
        Index index = new Index("annotations-index");
        List<String> allDocumentIds = documentsProvider.getAllDocumentIds();
        for (String documentId : allDocumentIds) {
            Set<Annotation> annotations = annotationsProvider.getAnnotations(documentId);
            for (Annotation annotation : annotations) {
                Set<TextSpan> textSpans = annotation.getTextSpans();
                for (TextSpan textSpan : textSpans) {
                    index.addTermOccurence(annotation.getId(), documentId, textSpan.getStart());
                    Concept concept = entityKnowledgeProvider.getByConceptId(annotation.getId());
                    index.addTermGroupEntry(concept.getCategory(), annotation.getId());
                }
            }
        }
        return index;
    }


}
