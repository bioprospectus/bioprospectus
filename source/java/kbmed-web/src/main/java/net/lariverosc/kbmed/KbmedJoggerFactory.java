package net.lariverosc.kbmed;

import com.elibom.jogger.Jogger;
import com.elibom.jogger.Middleware;
import com.elibom.jogger.MiddlewaresFactory;
import com.elibom.jogger.middleware.router.RouterMiddleware;
import com.elibom.jogger.middleware.router.loader.RoutesLoader;
import com.elibom.jogger.middleware.statik.StaticMiddleware;
import com.elibom.jogger.template.FreemarkerTemplateEngine;
import freemarker.template.Configuration;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class KbmedJoggerFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    private RoutesLoader routesLoader;

    private String staticDirectory = "static";

    private Configuration freeMarker;

    public Jogger create() throws Exception {
        com.elibom.jogger.Jogger joggerApp = new com.elibom.jogger.Jogger(new MiddlewaresFactory() {

            @Override
            public Middleware[] create() throws Exception {
                RouterMiddleware routerMiddleware = new RouterMiddleware();
                routerMiddleware.setRoutes(routesLoader.load());
                return new Middleware[]{new StaticMiddleware(staticDirectory, ""), routerMiddleware};
            }
        });
        joggerApp.setTemplateEngine(new FreemarkerTemplateEngine(freeMarker));

        return joggerApp;
    }

    public void setRoutesLoader(RoutesLoader routesLoader) {
        this.routesLoader = routesLoader;
    }

    public void setStaticDirectory(String staticDirectory) {
        this.staticDirectory = staticDirectory;
    }

    public void setFreeMarker(Configuration freeMarker) {
        this.freeMarker = freeMarker;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
