 package net.lariverosc.kbmed;

import net.lariverosc.util.EnvironmentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            EnvironmentUtils.exportEnv("MONGO_URL", "mongodb://localhost:27017/clef.articles_raw");
            String[] contextLocations = {
                "classpath*:knowledge-source-context.xml",
                "classpath*:snomed-context.xml",
                "classpath*:mongodb-context.xml",
                "classpath*:application-context.xml",
                "classpath*:jogger-context.xml"
            };
            ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(contextLocations);
            context.registerShutdownHook();
        } catch (Throwable e) {
            log.error("::*********:: FATAL ERROR - AN-VIEW ::********::", e);
        }
    }
}