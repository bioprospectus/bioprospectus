package net.lariverosc.kbmed.controllers;

import com.elibom.jogger.http.Request;
import com.elibom.jogger.http.Response;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.AnnotationsProvider;
import net.lariverosc.text.annotation.highlight.AnnotationHighlighter;
import net.lariverosc.text.annotation.highlight.DefaultAnnotationHighlighter;
import net.lariverosc.text.document.Document;
import net.lariverosc.text.document.DocumentsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Index {

    private final Logger log = LoggerFactory.getLogger(Index.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private DocumentsProvider documentsProvider;

    private AnnotationsProvider annotationsProvider;

    public void index(Request request, Response response) {
        Map<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("configuration", "");
        //response.render("search.ftl", attributes);
        response.render("main.ftl", attributes);
    }

    public void showDocument(Request request, Response response) {
        String documentId = request.getParameter("documentId");
        Document document = documentsProvider.getDocument(documentId);
        Set<Annotation> annotations = annotationsProvider.getAnnotations(documentId);
        addAttributes(annotations, ImmutableMap.of("class", "annotation"));

        Map<String, String> highlightsByField = highlightDocument(document, annotations);

        Map<String, Object> attributes = new HashMap<>();
        attributes.putAll(highlightsByField);
        attributes.put("annotations", buildJsonAnnotationsArray(annotations).toString());

        response.render("viz-clef.ftl", attributes);
    }

    public void about(Request request, Response response) {
        //response.render("about.ftl");
        response.redirect("http://sbarguil.bitbucket.org/");
    }

    private void addAttributes(Set<Annotation> annotations, Map<String, String> additionalAttributes) {
        for (Annotation annotation : annotations) {
            for (Map.Entry<String, String> entry : additionalAttributes.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if ("class".equalsIgnoreCase(key)) {
                    annotation.appendAttribute("class", annotation.getId(), " ");
                    annotation.appendAttribute("class", value, " ");
                } else {
                    annotation.addAttribute(key, value);
                }
            }
        }
    }

    private Map<String, String> highlightDocument(Document document, Set<Annotation> annotations) {
        AnnotationHighlighter annotationHighlighter = new DefaultAnnotationHighlighter("em", "em");
        Map<String, Set<Annotation>> annotationsByField = getAnnotationsByField(annotations);
        Map<String, String> highlightsByField = new HashMap<String, String>();
        for (String fieldName : document.fieldNames()) {
            String fieldValue = document.getFieldValue(fieldName);
            Set<Annotation> fieldAnnotations = annotationsByField.get(fieldName);
            if (fieldAnnotations != null && !fieldAnnotations.isEmpty()) {
                String fieldHighlighted = annotationHighlighter.highlight(fieldValue, fieldAnnotations);
                highlightsByField.put(fieldName, fieldHighlighted);
            } else {
                highlightsByField.put(fieldName, fieldValue);
            }
        }
        return highlightsByField;
    }

    private Map<String, Set<Annotation>> getAnnotationsByField(Set<Annotation> annotations) {
        Map<String, Set<Annotation>> annotationsByField = new HashMap<String, Set<Annotation>>();
        for (Annotation annotation : annotations) {
            String fieldName = annotation.getAttribute("field");
            if (fieldName != null && !fieldName.isEmpty()) {
                Set<Annotation> fieldAnnotations = annotationsByField.get(fieldName);
                if (fieldAnnotations == null) {
                    fieldAnnotations = new HashSet<Annotation>();
                    annotationsByField.put(fieldName, fieldAnnotations);
                }
                fieldAnnotations.add(annotation);
            }
        }
        return annotationsByField;
    }

    private JsonArray buildJsonAnnotationsArray(Set<Annotation> annotations) {
        JsonArray jsonAnnotations = new JsonArray();
        for (Annotation annotation : annotations) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("conceptId", annotation.getId());
            if ("CUI-less".equals(annotation.getId())) {
                jsonObject.addProperty("prefTerm", "CUI-less");
            } else {
                Concept concept = entityKnowledgeProvider.getByConceptId(annotation.getId());
                if (concept != null) {
                    if (concept.getPrefTerm() != null) {
                        jsonObject.addProperty("prefTerm", concept.getPrefTerm());
                    } else {
                        jsonObject.addProperty("prefTerm", concept.getDescription());
                    }
                } else {
                    jsonObject.addProperty("prefTerm", annotation.getId());
                }
            }
            jsonAnnotations.add(jsonObject);
        }
        return jsonAnnotations;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setDocumentsProvider(DocumentsProvider documentsProvider) {
        this.documentsProvider = documentsProvider;
    }

    public void setAnnotationsProvider(AnnotationsProvider annotationsProvider) {
        this.annotationsProvider = annotationsProvider;
    }

}
