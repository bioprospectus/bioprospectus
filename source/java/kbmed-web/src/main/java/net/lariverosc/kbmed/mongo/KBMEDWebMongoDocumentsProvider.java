package net.lariverosc.kbmed.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import net.lariverosc.mongodb.document.MongoDocumentsProvider;
import net.lariverosc.text.document.Document;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class KBMEDWebMongoDocumentsProvider extends MongoDocumentsProvider {
    @Override
    public Document getDocument(String docId) {
        BasicDBObject query = new BasicDBObject("article.@pmid", docId);
        DBObject dbObject = dbCollection.findOne(query);
        return documentConverter.toDocument(dbObject);
    }
}
