var knowledgeSourceName = kbmed.properties.knowledgeSource.name;
var knowledgeSourceHostAndPort = kbmed.properties.knowledgeSource.hostAndPort;
var tooltipTemplate = kbmed.getResource('/tooltip-template.html');

var qtipDefaultOptions = {
    show: 'click',
    hide: 'unfocus',
    overwrite: false,
    position: {
        viewport: $('body')
    },
    style: {
        classes: 'qtip-custom qtip-shadow'
    },
    events: {
        hidden: function (event, api) {
            $(this).detach();
        },
        show: function (event, api) {
            $(this).appendTo(api.get('position.container'));
        }
    }
};

var TooltipView = Backbone.View.extend({
    initialize: function () {
        _.bindAll(this, 'initToolTip', 'initHierarchy');
        this.annotationTemplate = _.template(tooltipTemplate);
        this.initToolTip();
    },
    initToolTip: function () {
        var that = this;
        var conceptId = this.$el.attr('conceptId');
        this.$el.qtip(_.extend({}, qtipDefaultOptions, {
            content: {
                text: function (event, api) {
                    var requestUrl = 'http://' + knowledgeSourceHostAndPort + '/knowledge/' + knowledgeSourceName + '/concepts/' + conceptId + '/summary';
                    var jqXHR = $.ajax({url: requestUrl});
                    jqXHR.then(function (content) {
                        api.set('content.text', that.annotationTemplate(content));
                        that.initHierarchy();
                        that.$el.qtip('reposition');
                    }, function (xhr, status, error) {
                        api.set('content.text', status + ': ' + error);
                    });
                    return 'Loading...';
                }
            }
        }));
    },
    initHierarchy: function () {
        $('.tree li:has(ul)').addClass('parent_li').find(' > span');

        $('.tree li.parent_li > span').on('click', function (e) {
            var children = $(this).parent('li.parent_li').find(' > ul > li');
            if (children.is(":visible")) {
                children.hide('fast');
                $(this).attr('title', 'Expand').find(' > i').addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
            } else {
                children.show('fast');
                $(this).attr('title', 'Collapse').find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
            }
            e.stopPropagation();
        });
    }

});


