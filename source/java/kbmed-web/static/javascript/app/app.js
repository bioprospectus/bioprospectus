/*
 * Main file for the backbone application
 *
 * Author: Diego Rueda
 *
 */

/* global kbmed, app */

// Initialize application components

// Collections
app.Collections.articlesCollection = new ArticlesCollection();
app.Collections.concepts = new ConceptsCollection();
app.Collections.activity = new Family();
app.Collections.plants = new Family();
app.Collections.compounds = new Family();
app.Collections.location = new Family();
app.Collections.hierarchy = new HierarchyCollection();
//Check last ones
app.Collections.children = new ChildrenCollection();
app.Collections.relatedTerms = new TermsCollection();

// Views
app.Views.articlesList = new ArticlesList();
app.Views.breadCrumbs = new BreadCrumbs();
app.Views.conceptsList = new Concepts();
app.Views.history = new History();
app.Views.queryInput = new QueryInput();
app.Views.queryPanel = new QueryPanelView();
app.Views.selectRegion = new SelectRegion();
app.Views.trees = new Trees();
