/*
 * Collection Articles
 * 
 * Author: Diego Rueda
 */

/* global Backbone */

var app = app || {};

// Articles Collection
// -------------------

// The articles collection changes after each search
var ArticlesCollection = Backbone.Collection.extend({
    
    // Reference to this collection model
    model: app.Models.Article
    
});
