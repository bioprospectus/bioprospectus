/*
 * Concepts Collection, used to fetch a list of concepts from ontology.
 * 
 * Author: Diego Rueda
 * 
 */

/* global Backbone, knowledgeSourceName, knowledgeSourceHostAndPort */

var app = app || {};

var ConceptsCollection = Backbone.Collection.extend({
    model: app.Models.Concept,
    
    initialize: function(options){
        this.data = [];
    },
    url: function(){
        return "http://" + knowledgeSourceHostAndPort + '/knowledge/' + knowledgeSourceName + '/concepts/' + this.data.toString();
    },
    fetch2: function(options){
        if (options.data){
            this.data = options.data;
        }
        return this.fetch(options);
    }
});

