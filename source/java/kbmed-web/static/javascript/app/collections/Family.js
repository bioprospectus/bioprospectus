/*
 * Family Collection, is used to fetch the entire hierarchy 
 * starting from a Root concept, it returns a string which
 * can be converted to JSON and is compatible with the plugin
 * Backbone-tree-view.
 * 
 * https://github.com/kirill-zhirnov/backbone-tree-view
 * 
 * Author: Diego Rueda
 * 
 */

/* global Backbone, knowledgeSourceName, knowledgeSourceHostAndPort */

var app = app || {};

var Family = Backbone.Collection.extend({
    model: app.Models.Concept,
    initialize: function () {
        this.concept_id = 0;
        this.country = "Region";
    },
    url: function () {
        return "http://" + knowledgeSourceHostAndPort + '/knowledge/' + knowledgeSourceName + '/concepts/' + this.concept_id + '/full/' + this.country;
    },
    fetch2: function (options) {
        if (options.concept_id) {
            this.concept_id = options.concept_id;
        }
        if (options.country) {
            this.country = options.country;
        }

        return this.fetch(options);
    }
});
