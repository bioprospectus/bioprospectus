/*
 * Hierarchy Collection, used to fetch the entire hierarchy 
 * for a Concept going up the ladder to the highest Concept 
 * in the hierarchy.
 * 
 * Author: Diego Rueda
 * 
 */

/* global Backbone, knowledgeSourceName, knowledgeSourceHostAndPort */

var app = app || {};

var HierarchyCollection = Backbone.Collection.extend({
    model: app.Models.Concept,
    initialize: function(options){
        this.concept_id = 0;
        
    },
    url: function(){
        return "http://" + knowledgeSourceHostAndPort + '/knowledge/' + knowledgeSourceName + '/hierarchy/full/' + this.concept_id +'/parents';
    },
    fetch2: function(options){
        if (options.concept_id) {
            this.concept_id = options.concept_id;
        }
        return this.fetch(options);
    }
});

