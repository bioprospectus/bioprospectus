/*
 * Concepts Collection, used to fetch a list of concepts from ontology.
 *
 * Author: Sebastian Sierra
 *
 */

/* global Backbone, knowledgeSourceName, knowledgeSourceHostAndPort */

var app = app || {};

var TermsCollection = Backbone.Collection.extend({
  model: app.Models.Term,
  initialize: function(options){
      this.concept_id = 0;

  },
  url: function(){
      return "http://" + knowledgeSourceHostAndPort + '/knowledge/' + knowledgeSourceName + '/concepts/' + this.concept_id +'/terms';
  },
  fetch2: function(options){
      if (options.concept_id) {
          this.concept_id = options.concept_id;
      }
      return this.fetch(options);
  }
});
