/*
 * This file purpose is to gather all code necessary 
 * for the application but not belonging to other components.
 * 
 * Author: Diego Rueda
 * 
 */

/* global BackTree, kbmed */

var app = app || {};

// Initialize config options
var ipdir = "localhost";
var knowledgeSourceName = kbmed.properties.knowledgeSource.name;
var knowledgeSourceHostAndPort = kbmed.properties.knowledgeSource.hostAndPort;


var ks = new knowledgeSource(kbmed.properties.knowledgeSource.hostAndPort, kbmed.properties.knowledgeSource.name);


app.Extra.dataPlant = new BackTree.Collection([{
        'id': 1,
        'title': 'Taxonomy',
        'open': true,
        'nodes': [{
                id: 2,
                title: 'Loading...',
                open: false,
                nodes: []
            }]
    }]);

app.Extra.dataCompound = new BackTree.Collection([{
        'id': 1,
        'title': 'Compounds',
        'open': true,
        'nodes': [{
                id: 2,
                title: 'Loading',
                open: false,
                nodes: []
            }]
}]);

app.Extra.dataActivity = new BackTree.Collection([{
        'id': 1,
        'title': 'Activity',
        'open': true,
        'nodes': [{
                id: 2,
                title: 'Loading...',
                open: false,
                nodes: []
            }]
}]);

app.Extra.dataLocation = new BackTree.Collection([{
        'id': 1,
        'title': 'Location',
        'open': true,
        'nodes': [{
                id: 2,
                title: 'Loading...',
                open: false,
                nodes: []
            }]
    }]);

var ItemView = BackTree.Item.extend({
    events: function () {
        var events = ItemView.__super__.events.apply(this, arguments);
        events['click > .wrapper .add'] = function (e) {
            //e.preventDefault();
            //alert('Add clicked!');
        },
                events['click > .wrapper .add-mustnot'] = function (e) {
        },
                events['click > .wrapper .add-should'] = function (e) {
        },
                events['click > .wrapper .info'] = function (e) {
            e.preventDefault();
        };

        return events;
    },
    getRightPart: function () {
        var out = '<a href="#" id="' + this.model.id + '" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></a> ';
        out += '<a href="#" id="' + this.model.id + '" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></a> ';
        out += '<a href="#" id="' + this.model.id + '" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></a> ';

        return out;
    }
});

app.Extra.treePlant = new BackTree.Tree({
    collection: app.Extra.dataPlant,
    settings: {
        ItemConstructor: ItemView
    }
});

app.Extra.treeCompound = new BackTree.Tree({
    collection: app.Extra.dataCompound,
    settings: {
        ItemConstructor: ItemView
    }
});

app.Extra.treeActivity = new BackTree.Tree({
    collection: app.Extra.dataActivity,
    settings: {
        ItemConstructor: ItemView
    }
});

app.Extra.treeLocation = new BackTree.Tree({
    collection: app.Extra.dataLocation,
    settings: {
        ItemConstructor: ItemView
    }
});