/*
 * Concept Article
 * 
 * Author: Diego Rueda
 */

/* global Backbone */

var app = app || {};

// Concept Article
// ----------
app.Models.Article = Backbone.Model.extend({
    
    defaults: {
        attributes: {
                "article.abstract": "",
                "article.pmc_article_url": "",
                "article.pmid": "",
                "article.title": "",
                "article.authors": "",
                "article.doi": ""
        }
    }
});

