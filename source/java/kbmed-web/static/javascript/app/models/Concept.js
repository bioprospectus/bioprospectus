/*
 * Concept Model
 * 
 * Author: Diego Rueda
 */

/* global Backbone */

var app = app || {};

// Concept Model
// ----------
app.Models.Concept = Backbone.Model.extend({
    
    // Default attributes
    defaults: {
        id: "",
        category: "",
        description: "",
        prefTerm: ""
    }
    
});
