/*
 * Concept Model
 *
 * Author: Sebastian Sierra
 */

/* global Backbone */

var app = app || {};

// Concept Model
// ----------
app.Models.Term = Backbone.Model.extend({

    // Default attributes
    defaults: {
        id: "",
        conceptId: "",
        term: ""
    }

});
