/*
 * Articles List renders all articles from Elasticsearch
 * after a Search.
 *
 * Author: Diego Rueda
 *
 */

/* global Backbone, _ */

var app = app || {};

// Shows a view of all articles filtered for a query
// ---------------

var ArticlesList = Backbone.View.extend({

    // Binding the view to an existing element in the skeleton
    el: '#articles',

    // Our template to show all articles
    articlesTemplate : _.template($('#article-template').html()),

    mustString :"",
    mustnotString : "",
    shouldString : "",

    // At initialization
    initialize: function(options){
        _.bindAll(this, 'render', 'loadMore', 'prepareData');
        this.render();
    },
    events: {
        'click .loadMore' : 'loadMore'
    },
    unrender: function(){
        this.$el.empty();
    },
    render: function (options) {
        var scroll = $('.articles-body').scrollTop();
        this.$el.empty();
        if (options !== undefined){
            this.perPage += options.perPage;
        } else {
            this.perPage = 5;
        }
        this.$el.append(this.articlesTemplate({articles: app.Collections.articlesCollection, perPage: this.perPage, numberOfHits: this.numberOfHits}));
        $('.articles-body').scrollTop(scroll);
    },
    loadMore: function () {
        var new_perPage = 5;
        this.render({perPage: new_perPage});
        this.highlight(this.currentQuery);

    },
    highlight: function(query){
        this.currentQuery = query;

        $.each(query.keywords, function(i, keyword){
            $('.articles-body').mark(keyword);
        });

        $.each(query.concepts, function(i, concept){
            $('.articles-body').mark(concept[1].replace('and','').replace('or',''));
        });

    },
    prepareData: function (articles, numberOfHits) {

        var data = [];
        $.each(articles, function (i, article) {
            data.push(article.fields);
        });
        app.Collections.articlesCollection.reset(data);
        this.numberOfHits = numberOfHits;
    }
});
