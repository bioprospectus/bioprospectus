/*
 * Breadcrumbs shows the hierarchy for a concept
 * in the query when the mouse is over the concept.
 * 
 * Also adds a tooltip when a breadcrumb concept has
 * been clicked, and boolean options to include it in the query.
 * 
 * Author: Diego Rueda
 * 
 */


/* global Backbone, _ */

var app = app || {};

var BreadCrumbs = Backbone.View.extend({
    
    el: $('#breadcrumbs'),
    
    breadcrumbTemplate: _.template($('#breadcrumb-template').html()),
    
    initialize: function(){
        _.bindAll(this, 'setNewEvents', 'showPopOver', 'addConceptMust', 'addConceptMustNot', 'addConceptShould', 'addCustomEvents', 'getTooltipText', 'showToolTip', 'render');
        this.collection = app.Collections.hierarchy;
        this.render();
    },
    events: {
        'click a': "showPopOver"
    },
    setNewEvents: function(tooltip){
        tooltip.events['click .add'] = this.addConceptMust;
        tooltip.events['click .add-mustnot'] = this.addConceptMustNot;
        tooltip.events['click .add-should'] = this.addConceptShould;
        tooltip.delegateEvents();
    },
    showPopOver: function(event){
        var $el = $(event.target);
        if ($el.attr('data-bbtooltip-triger')){
            return;
        }
        this.showToolTip($el);
    },
    addConceptMust: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).attr('data-term');
        var category = $(event.currentTarget).attr('data-category');
        if (app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'must'})){
            app.Views.queryPanel.search({fromHistory: false});
        }
    },
    addConceptMustNot: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).attr('data-term');
        var category = $(event.currentTarget).attr('data-category');
        if (app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'mustnot'})){
            app.Views.queryPanel.search({fromHistory: false});
        }
    },
    addConceptShould: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).attr('data-term');
        var category = $(event.currentTarget).attr('data-category');
        if (app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'should'})){
            app.Views.queryPanel.search({fromHistory: false});
        }
    },
    addCustomEvents: function(){
        var self = this;
        $('[data-bbtooltip-trigger]', this.el).each(function(){
            self.showTooltip($(this));
        });
    },
    getTooltipText: function(text){
        var array = text.split("=");
        return '<button style="float: right; margin-left: 3px;" href="#" id="' + array[0] + '" data-term ="' + array[1] + '" data-category="' + array[2] + '" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></button>'
                + '<button style="float: right; margin-left: 3px;" href="#" id="' + array[0] + '" data-term ="' + array[1] + '" data-category="' + array[2] + '" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></button>'
                + '<button style="float: right; margin-left: 3px;" href="#" id="' + array[0] + '" data-term ="' + array[1] + '" data-category="' + array[2] + '" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></button>';
    },
    showToolTip: function($el){
      var tooltip = new Backbone.Tooltip({
        $el: $el,
        text: this.getTooltipText($el.attr('data-bbtooltip')),
        align: $el.attr('data-bbtooltip-align'),
        timeout: $el.attr('data-bbtooltip-timeout'),
        context: $el.attr('data-bbtooltip-context'),
        trigger: $el.attr('data-bbtooltip-trigger'),
        exit: $el.attr('data-bbtooltip-exit'),
        speed: $el.attr('data-bbtooltip-speed'),
        interrupt: $el.attr('data-bbtooltip-interrupt'),
        feedback: $el.attr('data-bbtooltip-feedback'),
        animation: $el.attr('data-bbtooltip-animation'),
        prefix: $el.attr('data-bbtooltip-prefix')
      });
      
      this.setNewEvents(tooltip);
      
    },
    render: function(options){
        this.$el.empty();
        if (options !== undefined){
            this.$el.append(this.breadcrumbTemplate({collection: options.collection}));
            this.addCustomEvents();
            return this;
        }
    }
});

