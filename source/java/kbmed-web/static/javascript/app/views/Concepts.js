/*
 * Concepts is a view that shows all concepts
 * found in the articles, related to the current query.
 *
 * Author: Diego Rueda
 *
 */

/* global Backbone, _ */

var app = app || {};

var Concepts = Backbone.View.extend({
    el: '#concepts',
    initialize: function (options) {
        _.bindAll(this, 'addConceptMust', 'addConceptMustNot', 'addConceptShould', 'unrender',  'render');
        this.conceptosTemplate = _.template($('#concepts-template').html()),
        this.render(options);
    },
    events: {
        'click .add': "addConceptMust",
        'click .add-mustnot': "addConceptMustNot",
        'click .add-should': "addConceptShould"
    },
    addConceptMust: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).attr('data-term');
        var category = $(event.currentTarget).attr('data-category');
        if (app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'must'})){
            app.Views.queryPanel.search({fromHistory: false});
        }

    },
    addConceptMustNot: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).attr('data-term');
        var category = $(event.currentTarget).attr('data-category');
        if (app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'mustnot'})){
            app.Views.queryPanel.search({fromHistory: false});
        }
    },
    addConceptShould: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).attr('data-term');
        var category = $(event.currentTarget).attr('data-category');
        if (app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'should'})){
            app.Views.queryPanel.search({fromHistory: false});
        }
    },
    unrender: function () {
        this.$el.empty();
    },
    cleanConcepts: function () {
        this.$el.empty();
        this.$el.append(this.conceptosTemplate({
            plantsFam: null,
            plantsGen: null,
            plantsSpe: null,
            compounds: null,
            activities: null
        }));
    },
    render: function (options) {
        this.$el.empty();
        if (options !== undefined) {
            //Fetch all concepts from database
            function getConceptKeys(item){
                return String(item.key);
            }
            // Get all concepts from answer
            function getConcepts(item) {
                return {
                    key: String(item.key),
                    doc_count: item.doc_count
                };
            }
            var data = [];
            Array.prototype.push.apply(data,options.actconcepts.map(getConceptKeys));
            Array.prototype.push.apply(data,options.comconcepts.map(getConceptKeys));
            Array.prototype.push.apply(data,options.plaFamConcepts.map(getConceptKeys));
            Array.prototype.push.apply(data,options.plaGenConcepts.map(getConceptKeys));
            Array.prototype.push.apply(data,options.plaSpeConcepts.map(getConceptKeys));
            app.Collections.concepts.fetch2({data: data, async: false});

            function Comparator(a, b) {
                if (a[1] < b[1]) return 1;
                if (a[1] > b[1]) return -1;
                return 0;
            }

            var plantsFam = [];
            var plant_tuples = options.plaFamConcepts.map(getConcepts);
            $.each(plant_tuples, function(i, tuple){
                var model = app.Collections.concepts.findWhere({id: String(tuple.key)});
                if (model.attributes.category === 'Plant'){
                    plantsFam.push([model, tuple.doc_count]);
                }
                if (i > 25){
                    return false;
                }
            });

            plantsFam.sort(Comparator);

            var plantsGen = [];
            var plant_tuples = options.plaGenConcepts.map(getConcepts);
            $.each(plant_tuples, function(i, tuple){
                var model = app.Collections.concepts.findWhere({id: String(tuple.key)});
                if (model.attributes.category === 'Plant'){
                    plantsGen.push([model, tuple.doc_count]);
                }
                if (i > 25){
                    return false;
                }
            });

            plantsGen.sort(Comparator);

            var plantsSpe = [];
            var plant_tuples = options.plaSpeConcepts.map(getConcepts);
            $.each(plant_tuples, function(i, tuple){
                var model = app.Collections.concepts.findWhere({id: String(tuple.key)});
                if (model.attributes.category === 'Plant'){
                    plantsSpe.push([model, tuple.doc_count]);
                }
                if (i > 25){
                    return false;
                }
            });

            plantsSpe.sort(Comparator);

            var compounds= [];
            var compound_tuples = options.comconcepts.map(getConcepts);
            $.each(compound_tuples, function(i, tuple){
                var model = app.Collections.concepts.findWhere({id: String(tuple.key)});
                if (model.attributes.category === 'Compound'){
                    compounds.push([model, tuple.doc_count]);
                }
                if (i > 25){
                    return false;
                }
            });
            compounds.sort(Comparator);

            var activities = [];
            var activity_tuples = options.actconcepts.map(getConcepts);
            $.each(activity_tuples, function(i, tuple){
                var model = app.Collections.concepts.findWhere({id: String(tuple.key)});
                if (model.attributes.category === 'Activity'){
                    activities.push([model, tuple.doc_count]);
                }
                if (i > 25){
                    return false;
                }
            });
            activities.sort(Comparator);

            this.$el.append(this.conceptosTemplate({
                plantsFam: plantsFam,
                plantsGen: plantsGen,
                plantsSpe: plantsSpe,
                compounds: compounds,
                activities: activities
            }));
        }
    }
});
