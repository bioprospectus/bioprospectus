/*
 * History is a view that shows the latest queries
 * searched in the application, and allows the user
 * to reload them.
 *
 * Author: Diego Rueda
 *
 */

/* global Backbone, _, app */

var History = Backbone.View.extend({

    el: '#history',

    template: _.template($('#history-template').html()),

    initialize: function(){
        _.bindAll(this, 'addSearch', 'cleanHistory', 'prepareQuery',  'addConcept', 'addQuery', 'highlightRow', 'loadSearch', 'render');
        this.search_history = [];
        this.search_index = 0;
    },
    events: {
        'mouseenter td.first-column':'highlightRow',
        'click .search-history':'loadSearch',
        'click .clean': 'cleanHistory'
    },
    addSearch: function(query){
        this.search_history.unshift(query);
        $("#query-textarea").val("");
        this.search_index = this.search_history.length -1;
        this.render();
        this.makeQueryString(query);
    },
    cleanHistory: function(){
        this.search_history = [];
        this.search_index = 0;
        this.render();
    },
    prepareQuery: function(query){
        $('#query-terms').empty();
        for (var i=0;i<query.concepts.length;i++){
            this.addConcept(query.concepts[i]);
        }
        for (var i=0;i<query.keywords.length;i++){
            this.addQuery(query.keywords[i]);
        }
    },
    addConcept: function(concept){
        app.Views.queryPanel.addConcept(concept);
    },
    addQuery: function(keyword){
        app.Views.queryPanel.addQueryTerm(keyword);
    },
    highlightRow: function(event){
        //console.log('highlight row, not implemented');
    },
    loadSearch: function(event){
        $('#query-terms').empty();
        var that = this;
        var concepts = $(event.currentTarget).parent().parent().children('.content').children();
        for (var i=0;i<concepts.length;i++){
            var attr = concepts[i].attributes;
            if (attr.length === 2){
                var term = attr[1].textContent;
                that.addQuery(term);
            } else {
                var id = attr[1].value;
                var term = attr[2].value;
                var category = attr[3].value;
                var type = attr[4].value;
                that.addConcept({conceptId: id, term: term, category: category, type: type});
            }
        }
        app.Views.queryPanel.search({fromHistory: true});
    },
    makeQueryString: function(query){
        var latestQuery = "";
        //var keywords = query.keywords;
        var keywords = query.addKeywords;
        var concepts = query.concepts;
        if (concepts !== undefined){
            for (var i=0; i<concepts.length; i++){
                if (concepts[i][3] == "should"){
                    continue;
                }
                latestQuery += concepts[i][1] + " [" + concepts[i][3] + "], ";
            }
        }
        if (keywords !== undefined){
            for (var i=0; i<keywords.length; i++){
                latestQuery += keywords[i] + ", ";
            }
        }
        $('#query-textarea').val(latestQuery);

    },
    render: function(){
        this.$el.empty();
        this.$el.append(this.template({history: this.search_history}));
    }
});
