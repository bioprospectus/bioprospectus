/*
 * Query Input is a view used to create queries 
 * using the keyword, it provides concept and text suggestions.
 * 
 * Author: Diego Rueda
 * 
 */

/* global _, Backbone, kbmed, Bloodhound */

var app = app || {};


var QueryInput = Backbone.View.extend({
    el: $('#query-input'),
    initialize: function (options) {
        _.bindAll(this, 'clearInput', 'initWordSuggester', 'initConceptSuggester', 'initTypeAhead');
        this.initTypeAhead();
    },
    events: {
        'keyup': function (e) {
            if (e.keyCode === 13) {
                var value = this.$el.typeahead('val').trim();
                if (value !== '') {
                    kbmed.events.trigger('query-term:add', value);
                    this.clearInput();
                }
            }
        }
    },
    clearInput: function () {
        this.$el.typeahead('val', '');
    },
    initWordSuggester: function () {
        var suggestWordsQuery = {
            "text": "%QUERY",
            "phraseSuggestions": {
                "phrase": {
                    "field": "article.fulltext",
                    "max_errors": 1,
                    "gram_size": 3,
                    "direct_generator": [
                        {
                            "field": "article.fulltext",
                            "suggest_mode": "popular",
                            "prefix_length": 2,
                            "min_doc_freq": 3
                        }
                    ]
                }
            }
        };

        var engineWords = new Bloodhound({
            name: 'words',
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'http://' + kbmed.properties.es.hostAndPort + '/' + kbmed.properties.es.docIndex + '/_suggest?source=' + JSON.stringify(suggestWordsQuery),
                filter: function (response) {
                    var suggestions = response.phraseSuggestions[0].options;
                    if (suggestions && suggestions.length > 0) {
                        return $.map(suggestions, function (suggestion) {
                            return {
                                value: suggestion.text
                            };
                        });
                    } else {
                        kbmed.log('Not suggestions');
                        return {};
                    }
                }
            }
        });
        engineWords.initialize();
        return engineWords;
    },
    initConceptSuggester: function () {
        var suggestConceptsQuery = {
            "query": {
                "query_string": {
                    "default_field": "term",
                    "query": "%QUERY"
                }
            },
            "suggest": {
                "text": "%QUERY",
                "completitionSuggestions": {
                    "completion": {
                        "field": "term_suggest"
                    }
                }
            }
        };

        var engineConcepts = new Bloodhound({
            name: 'concepts',
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: 'http://' + kbmed.properties.es.hostAndPort + '/' + kbmed.properties.es.sugIndex + '/_search?source=' + JSON.stringify(suggestConceptsQuery),
                filter: function (response) {
                    var completitionSuggestions = response.suggest.completitionSuggestions[0].options;
                    if (completitionSuggestions && completitionSuggestions.length > 0) {
                        return $.map(completitionSuggestions, function (suggestion) {
                            var concept = JSON.parse(suggestion.text);
                            return {
                                concept: concept,
                                value: concept.term
                            };
                        });
                    } else {
                        var hits = response.hits.hits;
                        if (hits && hits.length > 0) {
                            return $.map(hits, function (hit) {
                                var concept = hit._source;
                                return {
                                    concept: concept,
                                    value: concept.term
                                };
                            });
                        } else {
                            kbmed.log('Not suggestions');
                            return {};
                        }
                    }
                }
            }
        });
        engineConcepts.initialize();
        return engineConcepts;
    },
    initTypeAhead: function () {
        var that = this;
        this.$el.typeahead({
            minLength: 3,
            highlight: true
        },
                {
                    source: this.initWordSuggester().ttAdapter()
                },
                {
                    source: this.initConceptSuggester().ttAdapter(),
                    templates: 
                            {
                        header: '<h4 class="concept-name">Concepts</h4>',
                        suggestion: function (data) 
                        {
                            var concept = data.concept;
                            return '<div id="'
                                    + concept.conceptId
                                    + '" class="concept-suggestion" data-id="' + concept.conceptId +'" data-term="' + concept.term + '" data-category="' + concept.category.charAt(0).toUpperCase() + concept.category.slice(1) +'">'
                                    + concept.term.substring(0, 45)
                                    + '<a style="float: right; margin-left: 3px;" href="#" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></a>'
                                    + '<a style="float: right; margin-left: 3px;" href="#" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></a>'
                                    + '<a style="float: right; margin-left: 3px;" href="#" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></a>'
                                    + '<strong style="float: right; font-size: 8pt; color: #837F7F;">'
                                    + concept.category.toUpperCase()
                                    + '</strong>'
                                    + '</div>';
                        }
                    }
                }
        ).on('typeahead:selected', function (e, datum) {
            if (datum.concept) 
            {
                //kbmed.events.trigger('query-concept:add', datum.concept);
            } else 
            {
                //kbmed.events.trigger('query-term:add', datum.value);
                app.Views.queryPanel.addQueryTerm(datum.value);
            }
            that.clearInput();
        });
        
        $(document).on('click','.concept-suggestion .add', function(event){
            var id = $(event.currentTarget).parent().attr('data-id');
            var term = $(event.currentTarget).parent().attr('data-term');
            var category = $(event.currentTarget).parent().attr('data-category');
            app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type':'must'});          
            return false;
        });
        $(document).on('click','.concept-suggestion .add-mustnot', function(event){
            var id = $(event.currentTarget).parent().attr('data-id');
            var term = $(event.currentTarget).parent().attr('data-term');
            var category = $(event.currentTarget).parent().attr('data-category');
            app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type':'mustnot'});          
            return false;
        });
        $(document).on('click','.concept-suggestion .add-should', function(event){
            var id = $(event.currentTarget).parent().attr('data-id');
            var term = $(event.currentTarget).parent().attr('data-term');
            var category = $(event.currentTarget).parent().attr('data-category');
            app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type':'should'});          
            return false;
        });
    }
});

