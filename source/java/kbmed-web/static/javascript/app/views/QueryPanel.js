/*
 * Query Panel is a view that receives all concepts and terms for search.
 * It includes options to modify the query included and trigger a search.
 *
 * Author: Diego Rueda
 *
 */

/* global _, Backbone, kbmed, Bloodhound, pdfMake */

var app = app || {};

var QueryPanelView = Backbone.View.extend({
    el: $('#query-panel'),
    alertTemplate: _.template('<div class="alert <%= alert_type %>"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><%= text %></div>'),
    initialize: function (options) {
        _.bindAll(this, 'addConcept','addQueryTerm', 'addQueryConceptMust', 'addQueryConceptMustNot', 'addQueryConceptShould', 'getQuery', 'search', 'buildESQuery', 'ESSearch', 'clearQuery');
        this.queryTermTextTemplate = _.template('<li class="search-term-text"><%= term %><a class="x-close glyphicon glyphicon-remove" href="#"></a></li>');
        this.queryTermConceptTemplate = _.template('<li id="<%= conceptId %>" class="search-term-concept"><%= term %><a class="x-close glyphicon glyphicon-remove" href="#"></a></li>');

        this.queryTermConceptMustTemplate = _.template('<li id="<%= conceptId %>" class="search-term-concept must-term-concept" data-category="<%= category %>" data-term="<%= term %>"><img class="icon" src="/img/icons/<%=category%>.png"><%= term %><a class="x-close glyphicon glyphicon-remove" href="#"></a></li>');
        this.queryTermConceptMustNotTemplate = _.template('<li id="<%= conceptId %>" class="search-term-concept mustnot-term-concept" data-category="<%= category %>" data-term="<%= term %>"><img class="icon" src="/img/icons/<%=category%>.png"><%= term %><a class="x-close glyphicon glyphicon-remove" href="#"></a></li>');
        this.queryTermConceptShouldTemplate = _.template('<li id="<%= conceptId %>" class="search-term-concept should-term-concept" data-category="<%= category %>" data-term="<%= term %>"><img class="icon" src="/img/icons/<%=category%>.png"><%= term %><a class="x-close glyphicon glyphicon-remove" href="#"></a></li>');

        kbmed.events.on('query-term:add', this.addQueryTerm);
    },
    events: {
        'click #search-btn': 'search',
        'click #clear-query': 'clearQuery',
        'click #get-report': 'getReport',
        'mouseover .search-term-concept': 'showHierarchy',
        'click .search-term-concept': 'changeConcept',
        'click .left-part .toggle': 'justChecking'
    },
    justChecking: function(term){
        alert('both work');

    },
    addQueryTerm: function (term) {
        var that = this;
        $('#query-terms', this.$el).append(this.queryTermTextTemplate({term: term}));
        $('li:last a', this.$el).click(function () {
            $(this).parent().remove();
            if ($('#query-terms li').size() === 0){
                that.clearQuery();
            } else {
                app.Views.queryPanel.search();
            }
        });
    },
    addConcept: function(concept){
        this.closeAlert();
        if(this.checkIfPresent(concept.conceptId)){
            var text = 'This concept is already present in the query.';
            var alert_type = 'alert-danger';
            this.showAlert(alert_type, text);
            return false;
        } else {
            if (concept.type === 'must'){
                this.addQueryConceptMust(concept);
            } else if (concept.type === 'mustnot'){
                this.addQueryConceptMustNot(concept);
            } else if (concept.type === 'should'){
                this.addQueryConceptShould(concept);
            }
            return true;
        }
    },
    addQueryConceptMust: function (concept) {
        var that = this;
        $('#query-terms', this.$el).append(this.queryTermConceptMustTemplate(concept));
        $('li:last a', this.$el).click(function () {
            $(this).parent().remove();
            $('#breadcrumbs').empty();
            if ($('#query-terms li').size() === 0){
                that.clearQuery();
            } else {
                app.Views.queryPanel.search();
            }
        });
    },
    addQueryConceptMustNot: function (concept) {
        var that = this;
        $('#query-terms', this.$el).append(this.queryTermConceptMustNotTemplate(concept));
        $('li:last a', this.$el).click(function () {
            $(this).parent().remove();
            $('#breadcrumbs').empty();
            if ($('#query-terms li').size() === 0){
                that.clearQuery();
            } else {
                app.Views.queryPanel.search();
            }
        });
    },
    addQueryConceptShould: function (concept) {
        var that = this;
        $('#query-terms', this.$el).append(this.queryTermConceptShouldTemplate(concept));
        $('li:last a', this.$el).click(function () {
            $(this).parent().remove();
            $('#breadcrumbs').empty();
            if ($('#query-terms li').size() === 0){
                that.clearQuery();
            } else {
                app.Views.queryPanel.search();
            }
        });
    },
    changeConcept: function (event){
        var concept = $(event.currentTarget).attr('class');
        var must = "must-term-concept";
        var mustnot = "mustnot-term-concept";
        var should = "should-term-concept";
        if (concept.indexOf(must) !== -1){
            $(event.currentTarget).removeClass( must ).addClass( mustnot );
            app.Views.queryPanel.search({fromHistory: false});
        } else if (concept.indexOf(mustnot) !== -1){
            $(event.currentTarget).removeClass( mustnot ).addClass( should );
            app.Views.queryPanel.search({fromHistory: false});
        } else if (concept.indexOf(should) !== -1){
            $(event.currentTarget).removeClass( should ).addClass( must );
            app.Views.queryPanel.search({fromHistory: false});
        };
    },
    showHierarchy: function(event){
        var id = $(event.currentTarget).attr('id');
        app.Views.breadCrumbs.collection.fetch2({concept_id: id,
            success: function(response){
                app.Views.breadCrumbs.render({collection: response});
            }
        });
    },
    closeAlert: function () {
        $('#alert-section .alert').alert('close');
    },
    showAlert: function (alert_type, text) {
        $('#alert-section').append(this.alertTemplate({alert_type: alert_type,text: text}));
    },
    checkIfPresent: function (id) {
        if (jQuery.inArray(id, this.getCurrentConcepts()) !== -1) {
            return true;
        }
        return false;
    },
    getCurrentConcepts: function () {
        var currentConcepts = [];
        $('#query-terms .search-term-concept').each(function (i, value) {
            currentConcepts.push($(this).attr('id'));
        });
        return currentConcepts;
    },
    getQuery: function () {
        var query = {};
        var keywords = [];
        $('#query-terms .search-term-text').each(function (i, item) {
            keywords.push($(item).text());

        });

        var concepts = [];
        //$('.search-term-concept').each(function (i, item) {
        //    concepts.push($(item).attr('id'));
        //});

        var concepts_must = [];
        $('#query-terms .must-term-concept').each(function (i, item) {
            var id = $(item).attr('id');
            var term = $(item).attr('data-term');
            var category = $(item).attr('data-category');

            concepts_must.push(id);
            concepts.push([id, term, category, 'must']);
        });

        var concepts_mustnot = [];
        $('#query-terms .mustnot-term-concept').each(function (i, item) {
            var id = $(item).attr('id');
            var term = $(item).attr('data-term');
            var category = $(item).attr('data-category');

            concepts_mustnot.push(id);
            concepts.push([id, term, category, 'mustnot']);
        });

        var concepts_should = [];
        $('#query-terms .should-term-concept').each(function (i, item) {
            var id = $(item).attr('id');
            var term = $(item).attr('data-term');
            var category = $(item).attr('data-category');

            concepts_should.push(id);
            //Line commented so query expanded concepts are not taken into account
            concepts.push([id, term, category, 'should']);
        });



        //In case of raw text search, take all text and search as
        var input_text = document.getElementById("query-input").value;
        if (input_text.length > 0) {
            this.addQueryTerm(input_text);
            document.getElementById("query-input").value = "";
            keywords.push(input_text);
        }

        query['keywords'] = keywords;
        query['concepts'] = concepts;
        query['concepts_must'] = concepts_must;
        query['concepts_mustnot'] = concepts_mustnot;
        query['concepts_should'] = concepts_should;
        return query;
    },
    buildESQuery: function (queryObj) {
        var ESQuery = {
            "query": {
                "filtered": {
                }
            },
            "fields": [
                "article.title",
                "article.abstract",
                "article.authors",
                "article.pmc_article_url",
                "article.doi",
                "article.pmid",
                "article.figures.iri",
                "article.figures.caption"
            ],
            "aggregations": {
                "familyConcepts": {
                    "significant_terms": {
                        "field": "mappings.all_mappings",
                        "size": 5,
                        "include": {
                            "pattern": "17.*",
                            "flags": "CASE_INSENSITIVE"
                        }
                    }
                },
                "genusConcepts": {
                    "significant_terms": {
                        "field": "mappings.all_mappings",
                        "size": 5,
                        "include": {
                            "pattern": "16.*",
                            "flags": "CASE_INSENSITIVE"
                        }
                    }
                },
                "speciesConcepts": {
                    "significant_terms": {
                        "field": "mappings.all_mappings",
                        "size": 5,
                        "include": {
                            "pattern": "15.*",
                            "flags": "CASE_INSENSITIVE"
                        }
                    }
                },
                "compoundConcepts": {
                    "significant_terms": {
                        "field": "mappings.all_mappings",
                        "size": 5,
                        "include": {
                            "pattern": "20.*",
                            "flags": "CASE_INSENSITIVE"
                        }
                    }
                },
                "activityConcepts": {
                    "significant_terms": {
                        "field": "mappings.all_mappings",
                        "size": 5,
                        "include": {
                            "pattern": "14.*",
                            "flags": "CASE_INSENSITIVE"
                        }
                    }
                }
            },
            "highlight": {
                "pre_tags": ["<em class='highlight'>"],
                "post_tags": ["</em>"],
                "fields": {"article.title": {}, "article.abstract": {}}
            }
        };
        var conceptKeywords = [];
        var addConceptKeywords = [];
        //Activate iff synonims report
        if (queryObj.concepts_should.length > 0 && true) {
            //var termsConcept = app.Collections.terms.fetch2({data: data, async: false});
            console.log(queryObj.concepts_should);
            var tmpAddKeywords = [];
            $.each(queryObj.concepts_should, function (index, concept) {
                //console.log(concept);
                app.Collections.relatedTerms.fetch2({concept_id: String(concept), async: false});
                Array.prototype.push.apply(tmpAddKeywords, app.Collections.relatedTerms.where({conceptId: String(concept)}));

                app.Collections.concepts.fetch2({data: String(concept), async: false});
                var mainConcept = app.Collections.concepts.findWhere({id: String(concept)});
                //addKeywords.push("+".concat(mainConcept.attributes.prefTerm));
                conceptKeywords.push(mainConcept.attributes.prefTerm);
                //addKeywords.push("+".concat(tmpAddKeywords[0].attributes.term));
                //tmpAddKeywords.shift();
                tmpAddKeywords.forEach(function(keyword){
                    if ((addConceptKeywords.indexOf(keyword.attributes.term) < 0) && (conceptKeywords.indexOf(keyword.attributes.term) < 0)) {
                        addConceptKeywords.push(keyword.attributes.term);
                    }
                });
            });
        }
        //Activate iff children report
        // if (queryObj.concepts.length > 0 && false) {
        //     //var termsConcept = app.Collections.terms.fetch2({data: data, async: false});
        //     console.log(queryObj.concepts_must);
        //     var tmpAddKeywords = [];
        //     $.each(queryObj.concepts_must, function (index, concept) {
        //         console.log(concept);
        //         app.Collections.children.fetch2({concept_id: String(concept), async: false});
        //         Array.prototype.push.apply(tmpAddKeywords, app.Collections.children.collect());
        //         console.log(addKeywords);
        //         console.log(app.Collections.children);
        //         app.Collections.concepts.fetch2({data: String(concept), async: false});
        //         var parentConcept = app.Collections.concepts.findWhere({id: String(concept)});
        //         addKeywords.push("+".concat(parentConcept.attributes.prefTerm));
        //         tmpAddKeywords.forEach(function(keyword){
        //             addKeywords.push(keyword.attributes.prefTerm);
        //         });
        //     });
        // }

        //Add conceptKeywords to query's keywords, so every query gets processed.
        if (queryObj.keywords.length > 0 || conceptKeywords.length > 0) {
            //var keywordsStr = queryObj.keywords.join(" ");
            var keywords = [];
            if (conceptKeywords.length > 0) {
                conceptKeywords.forEach(function(keyword){
                    keywords.push("+".concat(keyword));
                });
            }
            if (queryObj.keywords.length > 0) {
                queryObj.keywords.forEach(function(keyword){
                    keywords.push("+".concat(keyword));
                });
            }
            //test
            //queryObj['keywordStr'] = keywords;
            if (addConceptKeywords.length > 0) {
                addConceptKeywords.forEach(function(keyword){
                    var mystring = keyword.replace('/','');
                    keywords.push(mystring);
                });
            }
            console.log(keywords);
            //test
            queryObj['addKeywords'] = keywords;
            var keywordsStr = keywords.join(" ");
            var query = {
                "query": {
                    "query_string": {
                        "fields": ["article.title", "article.abstract", "article.fulltext"],
                        "query": keywordsStr
                    }
                }
            };
            $.extend(ESQuery.query.filtered, query);
        }

        //Filters partially activated
        if (queryObj.concepts_must.length > 0 || queryObj.concepts_mustnot.length > 0) {
            var must = [];
            var mustnot = [];
            //var should = [];
            //var termsConcept = app.Collections.terms.fetch2({data: data, async: false});
            //Filters partially suspended
            $.each(queryObj.concepts_must, function (index, concept) {
                must.push({"term": {"mappings.all_mappings": concept}});
            });
            $.each(queryObj.concepts_mustnot, function (index, concept) {
                mustnot.push({"term": {"mappings.all_mappings": concept}});
            });
            //$.each(queryObj.concepts_should, function (index, concept) {
            //    should.push({"term": {"mappings.all_mappings": concept}});
            //});
            var filter = {
                "filter": {
                    "bool": {
                        "must": must,
                        "must_not": mustnot
                    }
                }
            };
            $.extend(ESQuery.query.filtered, filter);
        }

        return $.extend(ESQuery, kbmed.properties.es.queryDefaults);
    },
    ESSearch: function (queryObj, callback) {
        $.ajax({
            type: 'POST',
            crossDomain: true,
            url: "http://" + kbmed.properties.es.hostAndPort + "/" + kbmed.properties.es.docIndex + "/_search",
            dataType: 'json',
            data: JSON.stringify(queryObj),
            success: function (response) {
                callback(response);
            },
            error: function (error) {
                kbmed.log(error);
            }
        });
    },
    search: function (data) {
        var that = this;
        var query = this.getQuery();
        this.closeAlert();
        if (query.concepts.length === 0 && query.keywords.length === 0){
            var text = 'Your query is empty.';
            var alert_type = 'alert-danger';
            this.showAlert(alert_type, text);
        } else {
            this.ESSearch(this.buildESQuery(query), function (response) {
                var actconcepts = response.aggregations.activityConcepts.buckets;
                var comconcepts = response.aggregations.compoundConcepts.buckets;
                var plaFamConcepts = response.aggregations.familyConcepts.buckets;
                var plaGenConcepts = response.aggregations.genusConcepts.buckets;
                var plaSpeConcepts = response.aggregations.speciesConcepts.buckets;
                if (actconcepts.length > 0 || comconcepts.length > 0 || plaFamConcepts.length > 0 || plaGenConcepts.length > 0 || plaSpeConcepts.length > 0 || response.hits.hits.length > 0)
                {
                    $('#results-tab a').removeClass('disabled').attr('data-toggle', 'pill');
                    $('#results-tab a[href="#resultados"]').tab('show');
                    // Send update signal to resultados View
                    app.Views.conceptsList.render({query: query, actconcepts: actconcepts, comconcepts: comconcepts, plaFamConcepts: plaFamConcepts, plaGenConcepts: plaGenConcepts, plaSpeConcepts: plaSpeConcepts});
                    app.Views.articlesList.prepareData(response.hits.hits, response.hits.total);
                    app.Views.articlesList.render();
                    app.Views.articlesList.highlight(query);
                    $('#get-report').show();
                    console.log(query);
                    if (data === undefined || data.fromHistory === undefined || data.fromHistory === false){
                        app.Views.history.addSearch(query);
                    }
                    if (data !== undefined && data.fromHistory !== undefined && data.fromHistory === true){
                        app.Views.history.makeQueryString(query);
                    }

                } else {
                    var text = 'No results have been found for this query.';
                    var alert_type = 'alert-danger';
                    that.showAlert(alert_type, text);
                    app.Views.conceptsList.cleanConcepts();
                    app.Views.articlesList.unrender();
                }
            });


        }
    },
    clearQuery: function () {
        this.closeAlert();
        $('#results-tab a').addClass('disabled').removeAttr('data-toggle');
        $('#query-terms').empty();
        app.Views.conceptsList.unrender();
        app.Views.articlesList.unrender();
        $('#busqueda-tab a[href="#trees"]').tab('show');
        $('#breadcrumbs').empty();
        $('#get-report').hide();
    },
    downloadFile: function(filename, urlData){
        var aLink = document.createElement('a');
        aLink.download = filename;
        aLink.href = urlData;
        var event = new MouseEvent('click');
        aLink.dispatchEvent(event);

    },
    getReport: function(){
        //Create csv for Search Query
        var keywords = $.map($('#query-terms .search-term-text'), function(e) { return ['"N/A' + '","' + e.textContent + '","' + 'keyword' + '","' + 'keyword']; });
        var must = $.map($('#query-terms .must-term-concept'), function(e) { return ['"' + e.id + '","' + e.attributes['data-term'].value + '","' + e.attributes['data-category'].value + '","' + 'must']; });
        var mustnot = $.map($('#query-terms .mustnot-term-concept'), function(e) { return ['"' + e.id + '","' + e.attributes['data-term'].value + '","' + e.attributes['data-category'].value + '","' + 'mustnot']; });
        var should = $.map($('#query-terms .should-term-concept'), function(e) { return ['"' + e.id + '","' + e.attributes['data-term'].value + '","' + e.attributes['data-category'].value + '","' + 'should']; });


        var query = [keywords, must, mustnot, should];
        var csvContent = "data:text/csv;charset=utf-8,";
        var dataString = "id,term,category,status\n";

        csvContent += dataString;
        query.forEach(function(row, index){
            csvContent += row.join("");
        });
        var csvQuery = encodeURI(csvContent);
        this.downloadFile('query.csv', csvQuery);

        //Create csv for related Concepts
        var plants = $.map($('#plant-list .add'), function(e) { return [['"' +e.id + '","' + e.attributes['data-term'].value +'","'+ e.attributes['data-category'].value + '"\n']]; });
        var compounds = $.map($('#compound-list .add'), function(e) { return [['"' + e.id + '","' +  e.attributes['data-term'].value + '","' + e.attributes['data-category'].value + '"\n']]; });
        var activities = $.map($('#activity-list .add'), function(e) { return [['"' + e.id + '","' +  e.attributes['data-term'].value + '","' +  e.attributes['data-category'].value + '"\n']]; });

        var query = [plants, compounds, activities];

        var csvContent = "data:text/csv;charset=utf-8,";
        var dataString = "id,term,category\n";

        csvContent += dataString;
        query.forEach(function(row, index){
            csvContent += row.join("");
        });
        var csvRelatedConcepts = encodeURI(csvContent);
        this.downloadFile('relatedConcepts.csv', csvRelatedConcepts);

        //Create csv for articles
        var articles = app.Collections.articlesCollection;
        var csvContent = "data:text/csv;charset=utf-8,";
        var dataString = "id,title,authors,url,doi,abstract\n";

        csvContent += dataString;
        articles.forEach(function(row, index){
            var title = row.get('article.title');
            var authors = row.get('article.authors');
            var url = row.get('article.pmc_article_url');
            var doi = row.get('article.doi');
            var abstract = row.get('article.abstract');

            var row = '"' + index + '","' + title  + '","' + authors  + '","' + url  + '","' + doi  + '","' + abstract  + '"\n';
            csvContent += row;
        });
        var csvArticles = encodeURI(csvContent);
        this.downloadFile('articles.csv', csvArticles);





    }


});
