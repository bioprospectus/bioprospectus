/*
 * SelectRegion is a view, used to filter Plants tree by Country or Region
 * 
 * Author: Diego Rueda
 * 
 */

/* global Backbone, _, knowledgeSourceName, knowledgeSourceHostAndPort */

var app = app || {};

var SelectRegion = Backbone.View.extend({
    el: '#select-country',
    initialize: function () {
        this.selectTemplate = _.template($('#select-template').html());
        this.countries = this.getCountries();
        this.render();
    },
    events: {
        "change #country-selector": "selectCountry"
    },
    getCountries: function () {
        var countries = [];
        $.ajax({
            url: "http://" + knowledgeSourceHostAndPort + '/knowledge/' + knowledgeSourceName + '/concepts/countries',
            type: "get",
            async: false,
            success: function (response) {
                countries = response;
            }
        });
        return countries;
    },
    selectCountry: function (event) {
        var country = this.$('#country-selector').val();

        //Top concept for Plants
        app.Collections.plants.fetch2({concept_id: '1800001', country: country,
            success: function (response) {
                app.Extra.treePlant.collection.reset(response.toJSON());
            }
        });
    },
    render: function () {
        this.$el.append(this.selectTemplate({countries: this.countries}));
    }
});

