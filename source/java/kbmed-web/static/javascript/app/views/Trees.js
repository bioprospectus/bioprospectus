/*
 * Trees is a view that renders the full ontology
 * using the plugin Backbone-tree-view. 
 * And adds functionality to add concepts to the query.
 * 
 * Author: Diego Rueda
 * 
 */

/* global Backbone, _, app */

var app = app || {};

var Trees = Backbone.View.extend({
    el: '#trees',
    initialize: function (options) {
        _.bindAll(this, 'addConceptMust', 'addConceptMustNot', 'addConceptShould', 'createTrees');

        this.render();
        this.createTrees();

    },
    events: {
        'click .add': "addConceptMust",
        'click .add-mustnot': "addConceptMustNot",
        'click .add-should': "addConceptShould"
    },
    createTrees: function () {
        //Top concept for Activities
        app.Collections.activity.fetch2({concept_id: '1800002',
            success: function (response) {
                app.Extra.treeActivity.collection.reset(response.toJSON());
            }
        });
        
        //Top concept for Compounds
        app.Collections.compounds.fetch2({concept_id: '1800003',
            success: function (response) {
                app.Extra.treeCompound.collection.reset(response.toJSON());
            }
        });
        
        //Top concept for Taxonomy
        app.Collections.plants.fetch2({concept_id: '1800005',
            success: function (response) {
                app.Extra.treePlant.collection.reset(response.toJSON());
            }
        });
        
        //Top concept for Region
        app.Collections.location.fetch2({concept_id: '1800006',
            success: function (response) {
                app.Extra.treeLocation.collection.reset(response.toJSON());
            }
        });

    },
    addConceptMust: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).parent().prev().text();
        var category = $(event.currentTarget).parents('.bt-root').find('.body-part')[0].innerText;
        app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'must'});
    },
    addConceptMustNot: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).parent().prev().text();
        var category = $(event.currentTarget).parents('.bt-root').find('.body-part')[0].innerText;
        app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'mustnot'});
    },
    addConceptShould: function (event) {
        // Add concept to filter list
        var id = $(event.currentTarget).attr('id');
        var term = $(event.currentTarget).parent().prev().text();
        var category = $(event.currentTarget).parents('.bt-root').find('.body-part')[0].innerText;
        app.Views.queryPanel.addConcept({'conceptId': id, 'term': term, 'category': category, 'type': 'should'});
    },
    render: function () {
        $('#treePlant').append(app.Extra.treePlant.render().$el);
        $('#treeCompound').append(app.Extra.treeCompound.render().$el);
        $('#treeActivity').append(app.Extra.treeActivity.render().$el);
        $('#treeLocation').append(app.Extra.treeLocation.render().$el);
    }
});

