var webdisIndex = function (indexName) {
    var initRedisNameSpace = function (indexName) {
        var SEP = ':';
        var getBasePrefix = function (indexName) {
            var basePrefixSize = 10;
            return indexName.length > basePrefixSize ? indexName.toUpperCase().substring(0, basePrefixSize) : indexName.toUpperCase();
        };
        var _INDEX_NAME = getBasePrefix(indexName);
        var _TERMS_KEY = _INDEX_NAME + SEP + 'TERMS';
        var _DOCUMENTS_KEY = _INDEX_NAME + SEP + 'DOCS';
        var _INVERTED_INDEX_PREFIX = _INDEX_NAME + SEP + 'I_IDX' + SEP;
        var _OCCURRENCES_INDEX_PREFIX = _INDEX_NAME + SEP + 'O_IDX' + SEP;
        var _DOCUMENT_TERMS_PREFIX = _INDEX_NAME + SEP + 'D_TER' + SEP;
        console.log("Init name space for index " + indexName + " ok");
        return function () {
            return {
                NAME: _INDEX_NAME,
                TERMS_KEY: _TERMS_KEY,
                DOCUMENTS_KEY: _DOCUMENTS_KEY,
                INVERTED_INDEX_PREFIX: _INVERTED_INDEX_PREFIX,
                OCCURRENCES_INDEX_PREFIX: _OCCURRENCES_INDEX_PREFIX,
                DOCUMENT_TERMS_PREFIX: _DOCUMENT_TERMS_PREFIX
            };
        }();
    };

    var redisNameSpace = initRedisNameSpace(indexName);

    var _webdisOptions = {
        hostAndPort: '10.0.2.15:7379'
    };
    
    var _ajaxGet = function (command, url, callback) {
        if (!callback) {
            callback = function (indexData) {
                console.log(indexData);
            };
        }
        $.ajax({
            type: 'GET',
            crossDomain: true,
            url: 'http://' + _webdisOptions.hostAndPort + '/' + command + '/' + url,
            dataType: 'json',
            success: function (response) {
                callback(response[command]);
            },
            error: function (error) {
                console.log(error);
            }
        });
    };

    var _getTerms = function (callback) {
        return _ajaxGet('hkeys', redisNameSpace.TERMS_KEY, callback);
    };

    var _getDocuments = function (term, callback) {
        return _ajaxGet('hkeys', redisNameSpace.INVERTED_INDEX_PREFIX + term, callback);
    };

    var _getOccurrences = function (term, callback) {
        return _ajaxGet('smembers', redisNameSpace.OCCURRENCES_INDEX_PREFIX + term, callback);
    };

    var _getGlobalFrequency = function (term, callback) {
        return _ajaxGet('hget', redisNameSpace.TERMS_KEY + '/' + term, callback);
    };

    var _getDocumentFrequency = function (term, callback) {
        return _ajaxGet('hkeys', redisNameSpace.INVERTED_INDEX_PREFIX + '/' + term, function (response) {
            callback(response.length);
        });
    };

    var _getInDocumentFrequency = function (documentId, term, callback) {
        return _ajaxGet('hget', redisNameSpace.INVERTED_INDEX_PREFIX + term + '/' + documentId, callback);
    };

    var _getTotalDocuments = function (callback) {
        return _ajaxGet('scard', redisNameSpace.DOCUMENTS_KEY, callback);
    };

    var _getDocumentTerms = function (documentId, callback) {
        return _ajaxGet('lrange', redisNameSpace.DOCUMENT_TERMS_PREFIX + documentId + '/0/-1', callback);
    };

    var _getDocumentLength = function (documentId, callback) {
        return _ajaxGet('hget', redisNameSpace.DOCUMENTS_KEY + '/' + documentId, callback);
    };

    return function () {
        return {
            'getTerms': _getTerms,
            'getDocuments': _getDocuments,
            'getOccurrences': _getOccurrences,
            'getGlobalFrequency': _getGlobalFrequency,
            'getDocumentFrequency': _getDocumentFrequency,
            'getInDocumentFrequency': _getInDocumentFrequency,
            'getTotalDocuments': _getTotalDocuments,
            'getDocumentTerms': _getDocumentTerms,
            'getDocumentLength': _getDocumentLength
        };
    }();
};

var index = webdisIndex("TESTIDX");

index.getTerms(function (indexData) {
    console.log(indexData);
});