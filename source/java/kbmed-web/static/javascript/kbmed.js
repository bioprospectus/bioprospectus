/* global Backbone, _ */

var kbmed = {
    events: _.extend({}, Backbone.Events),
    init: function () {
        _.bindAll(this, 'properties', 'getResource', 'log');
    },
    properties: {
        knowledgeSource: {
            name: 'snomeden',
            hostAndPort: '168.176.54.18:8184'
        },
        es: {
            hostAndPort: '168.176.54.18:9200',
            docIndex: 'clef-snomeden-def', //1095804002
            sugIndex: 'snomeden-terms',
            queryDefaults: {
                "from": 0,
                "size": 500 //10000 for larger results
            }
        }
    },
    getResource: function (url) {
        var data = "<h1> failed to load resource : " + url + "</h1>";
        $.ajax({
            async: false,
            url: url,
            success: function (response) {
                data = response;
            }
        });
        return data;
    },
    log: function (message) {
        if (console) {
            console.log(message);
        }
    }
};



