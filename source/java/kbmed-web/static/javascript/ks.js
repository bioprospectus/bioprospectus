var knowledgeSource = function (hostAndPort, ksName) {

    var ajaxCall = function (method, path, callback, urlParams, bodyObj) {
        var ajaxParams = {
            type: method,
            url: "http://" + hostAndPort + "/knowledge/" + ksName + "/" + path + "/" + urlParams,
            success: function (response) {
                callback(response);
            },
            error: function (error) {
                console.log(error);
            }
        };
        if (bodyObj) {
            $.extend(ajaxParams, {
                contentType: 'application/json',
                data: JSON.stringify(bodyObj)
            });
        }
        $.ajax(ajaxParams);
    };

    var _getConcepts = function (conceptIdsStr, callback) {
        ajaxCall('GET', 'concepts', callback, conceptIdsStr);
    };

    var _kbRetrievalAsync = function (queryObj, callback) {
        ajaxCall('POST', 'kbretrievalasync', callback, "", queryObj);
    };

    return function () {
        return {
            getConcepts: _getConcepts,
            kbRetrievalAsync: _kbRetrievalAsync
        };
    }();
};