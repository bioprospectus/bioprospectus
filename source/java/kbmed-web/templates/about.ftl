<!DOCTYPE html>
<#import "layout.ftl" as layout>
<@layout.appLayout>
<div class="row">
    <div class="jumbotron">
        <h1>Bioprospectus</h1>
        <p>Knowledge based search engine for the biomedical domain based on KBMED by Alejandro Riveros Cruz</p>
        <address>
            <a href="mailto:#">lariverosc@gmail.com</a>
        </address>
    </div>
</div>
</@layout.appLayout>