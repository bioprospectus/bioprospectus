<#macro appLayout stylesheets="" javascripts="" templates="">
<!DOCTYPE html>
<html>
    <head>
        <title>Bioprospectus</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap-3.3.4/bootstrap.css">
        <#if stylesheets?? >
            <#if stylesheets?is_directive>
                <@stylesheets/>
            <#else>
                ${stylesheets}
            </#if>
        </#if>
        <link rel="stylesheet" type="text/css" href="/css/kbmed-web.css">
        <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top collapsed" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" style="color: #5DA8E7; font-family: monospace; font-size: 28px; margin-top: -2px;" href="/about" >Bioprospectus</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li id="busqueda-tab" class="active"><a data-toggle="pill" href="#trees">Search</a></li>
                        <li id="results-tab"><a class="disabled" href="#resultados">Results</a></li>
                        <li id="help-tab"><a data-toggle="pill" href="#help">Help</a></li>
                        <li><a href="/about">About us</a></li>
                    </ul>
                </div>
            </div>
        </div>    

        <div class="container-fluid">
            <#nested/>
        </div>
       


        <#if templates?? >
            <#if templates?is_directive>
                <@templates/>
            <#else>
                ${templates}
            </#if>
        </#if>
        <script type="text/javascript" src="/javascript/lib/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="/javascript/lib/jquery-ui.js"></script>
        <script type="text/javascript" src="/javascript/lib/jquery.mark.min.js"></script>
        <script type="text/javascript" src="/javascript/lib/underscore.js"></script>
        <script type="text/javascript" src="/javascript/lib/underscore.string-2.3.0.js"></script>
        <script type="text/javascript" src="/javascript/lib/backbone.js"></script>
        <script type="text/javascript" src="/javascript/lib/bootstrap-3.3.4.js"></script>
        <script type="text/javascript" src="/javascript/kbmed.js"></script>


        
        <#if javascripts?? >
            <#if javascripts?is_directive>
                <@javascripts/>
            <#else>
                ${javascripts}
            </#if>
        </#if>



    </body>
</html>
</#macro>