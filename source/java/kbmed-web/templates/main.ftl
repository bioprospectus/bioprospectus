<!DOCTYPE html>
<#import "layout.ftl" as layout>
<@layout.appLayout stylesheets=stylesheets javascripts=javascripts templates=templates>

<#macro stylesheets>
    <link rel="stylesheet" type="text/css" href="/css/jquery-qtip-2.2.1.css">
    <link rel="stylesheet" type="text/css" href="/css/magnific-popup.css">
    <link href="/css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/backbone-tooltip.css">
</#macro>

<div class="row vertical-center">
    <div class="col-md-12 col-lg-12">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2 top-buffer">
                    <div id="query-panel" class="row">
                            <div id="breadcrumbs">
                            </div>
                            <div id="query-box" class="col-md-8 col-lg-8">
                                <div class="row">
                                    <ul id="query-terms">
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12" style="padding-left: 0px">
                                        <input id="query-input" type="text" size="60" placeholder="Type your query here..." autocomplete="off" spellcheck="false">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-2 col-lg-2">
                                <button id="search-btn" type="button" class="btn btn-primary btn-md">Search</button>
                                <button id="clear-query" type="button" class="btn btn-primary btn-md">Clear All</button>
                                <button id="get-report" type="button" class="btn btn-primary btn-md" style="display: none;">Get Report</button>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8" style="padding: 0px;">

                            <div id="alert-section">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="tab-content">
                    <div id="trees" class="tab-pane fade in active" >
                        <div class="row top-buffer">
                            <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
                                <div class="row">
                                    <div class="col-md-2 col-lg-2">
                                        <div id="select-country">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-lg-3">
                                        <div class="well">
                                            <div id="treePlant">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                        <div class="well">
                                            <div id="treeCompound">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                       <div class="well">
                                            <div id="treeActivity">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-lg-3">
                                       <div class="well">
                                            <div id="treeLocation">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div id="resultados" class="tab-pane fade">
                        <!-- Incluir vista de resultados -->
                        <div class="row minor-buffer">
                            <div class="col-md-3 top-buffer">
                                <div id="concepts">
                                </div>
                            </div>
                            <div class="col-md-6 top-buffer">
                                <div id="articles">
                                </div>
                            </div>
                            <div class="col-md-3 top-buffer">
                                <div id="history">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="help" class="tab-pane fade">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="row">
                                    <h1><span> HOW TO USE IT?</span></h1>
                                    <p><span> BIOPROSPECTUS </span> is a meta-searching engine desing in order to automate searching and bioproduct development.
                                    Thus, searching by concepts as plants, biological activites or chemical compounds can be done:
                                    <ol>
                                        <div class="row">
                                        <li> Using the curated ontology presented in the main window of the system. This ontology has information of:

                                            <ul>
                                                <li>Scientific and Common plant names</li>
                                                <li>Plant Locations</li>
                                                <li>Biological activities</li>
                                                <li>Chemical compounds</li>
                                            </ul>
                                            <br>
                                            For that, you must:
                                            <ul>
                                                <li>Select the desired <span> CONCEPT </span> (plant,activity or compund), and add to the search filters:
                                                    <div>
                                                        <img align="middle" width="900" height="170" src="/img/help/main_window_2.jpg" />
                                                    </div>
                                                    <br>
                                                    The selection can include one the following BOOLEAN structures:
                                                    <ul>
                                                        <li><font color="green"><b>MUST </b></font>(<font color="green"><b>+</b></font>): Means that the results that contains the specified <font color="green"><b>TERM </b></font> or <font color="green"><b>CONCEPT</b></font> must be included</li>
                                                        <li><font color="#cc3300"><b>MUST NOT</b></font>(<font color="#cc3300"><b>-</b></font>): Means that the results that contains the specified <font color="#cc3300"><b>TERM </b></font> or <font color="#cc3300"><b>CONCEPT</b></font> must be removed</li>
                                                        <li><font color="#cc9900"><b>SHOULD </b></font>(<font color="#cc9900"><b>&#x2714</b></font>): Means the results that contains the specified <font color="#cc9900"><b>TERM </b></font> or <font color="#cc9900"><b>CONCEPT</b></font> has a higher priority</li>
                                                    </ul>
                                                </li>

                                                <br>
                                                <li>Verify that the filter has the desired information</li>

                                                <div>
                                                    <img align="middle" width="900" height="100" src="/img/help/main_window_filters.jpg" />
                                                </div>
                                                <br>
                                                <li> And finally click on the search button</li>

                                                <div>
                                                    <img align="middle" width="220" height="71" src="/img/help/main_window_search.jpg" />
                                                </div>
                                            </ul>

                                            <span>NOTE:</span> You can add as many filters as you want and combine the BOOLEAN options as you prefer in order to improve your search.
                                        </li>
                                        </div>

                                        <br>
                                        <li>Writting free text in the search box:</li>
                                            <ul>
                                                <li>Type the queries as <span> CONCEPT </span> (plant,activity or compund), and add to the search filters :

                                                <div>
                                                    <img align="middle" width="985" height="324" src="/img/help/main_window_free_text.jpg" />
                                                </div>
                                                </li>
                                                <span>NOTE:</span> The system would sugest some recomendations according the typed text. You could add any of the suggestions using any of the BOOLEAN operators described before.
                                                <br></br>
                                                <li>Verify that the filter has the rigth information</li>
                                                    <div>
                                                        <img align="middle" width="900" height="100" src="/img/help/main_window_free_text_2.jpg" />
                                                    </div>
                                                </li>
                                                <li> And finally click on the search button</li>
                                                    <div>
                                                        <img align="middle" width="220" height="71" src="/img/help/main_window_search.jpg" />
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                        <span>NOTE:</span> You can add as many filters as you want and combine the BOOLEAN options as you prefer in order to improve your search.
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3 text-center top-buffer">
        <h4>Bioprospectus includes 49223 plants, 29043 compounds and 6422 activities used to index 11349 scientific articles.</h4>
    </div>
</div>
<div class="row top-buffer">
    <div class="col-lg-1 col-md-1 col-xs-3 col-md-offset-3">
        <a target="_blank" href="http://unal.edu.co/"><img src="/img/logos/unal.jpg" class="img-responsive"></a>
    </div>
    <div class="col-lg-2 col-md-2 col-xs-6" >
        <a target="_blank" href="http://www.ibun.unal.edu.co/"><img src="/img/logos/ibun.png" class="img-responsive"></a>
    </div>
    <div class="col-lg-1 col-md-1 col-xs-3">
        <a target="_blank" href="http://www.colciencias.gov.co/"><img src="/img/logos/colciencias.jpg" class="img-responsive"></a>
    </div>
    <div class="col-lg-1 col-md-1 col-xs-3">
        <a target="_blank" href="https://sites.google.com/a/unal.edu.co/mindlab/home"><img src="/img/logos/mindlab.png" class="img-responsive"></a>
    </div>
    <div class="col-lg-1 col-md-1 col-xs-3">
        <a target="_blank" href="http://www.uis.edu.co"><img src="/img/logos/uis.png" class="img-responsive"></a>
    </div>
</div>

<#macro templates>
<script id="trees-template" type="text/x-underscore-template">
</script>

<script id="history-template" type="text/x-underscore-template">
    <h4 style:"padding: 0px;> Latest query.</h4>
    <textarea id="query-textarea" rows="4" cols="50">
    </textarea>

    <h4 style:"padding: 0px;> Search history.</h4>
    <table class="table table-striped history">
    <% var opac = 1; %>
    <% _.each(history, function(item) { %>
        <tr>
            <td class="first-column"><button class="btn btn-primary search-history"><span class="glyphicon glyphicon-search"></span></button></td>
            <td class="content" style="opacity:<%=opac%>;">
        <% _.each(item.concepts, function(concept) { %>
            <span class="search-term-concept <%=concept[3]%>-term-concept" data-id="<%= concept[0] %>" data-term="<%= concept[1] %>" data-category="<%= concept[2] %>" data-type="<%= concept[3] %>"><%=concept[1]%></span>
        <% }) %>
        <% _.each(item.keywords, function(keyword) { %>
            <span class="search-term-text" data-term="<%=keyword%>"><%=keyword%></span>
        <% }) %>
            </td>
        </tr>
        <% if (opac > 0.4){ opac -= 0.1 } %>
    <% }) %>
    </table>
    <button class="btn btn-default clean">Clear History</button>
</script>

<script id="loading-template" type="text/x-underscore-template">
    <h4>Loading ontology, please wait...<%= data %>% <img src="/img/ajax-loader.gif" alt="logo" /></h4>
</script>

<script id="breadcrumb-template" type="text/x-underscore-template">
    <div class="btn-group btn-breadcrumb">
    <% if (collection.length === 1) { collection.each(function(item) {%>
        <% var category = collection.last().attributes.category %>
        <% var final = category.charAt(0).toUpperCase() + category.slice(1) %>
        <a data-bbtooltip="<%= item.attributes.id %>=<%= item.attributes.description %>=<%=final%>" data-bbtooltip="0" data-bbtooltip-align="right" data-bbtooltip-context="default" href="#" class="btn btn-default"><%= item.attributes.description %></a>
    <% })} else { collection.each(function(item) { %>
        <% var category = collection.last().attributes.category %>
        <% var final = category.charAt(0).toUpperCase() + category.slice(1) %>
        <a data-bbtooltip="<%= item.attributes.id %>=<%= item.attributes.description %>=<%=final%>" data-bbtooltip="0" data-bbtooltip-align="right" data-bbtooltip-context="default" href="#" class="btn btn-default"><%= item.attributes.description %></a>
    <% })}; %>
    </div>
</script>

<script id="concepts-template" type="text/x-underscore-template">
    <h4 style:"padding: 0px;> Concepts related to the query.</h4>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapsePlantsFam">Family</a>
                </h4>
            </div>
            <div id="collapsePlantsFam" class="panel-collapse collapse in" aria-expanded="true">
                <table id="plant-list" class="table">
                    <% _.each(plantsFam, function(plant) { %>
                    <tr>
                        <td class="first"><span class="badge"><%= plant[1] %></span></td>
                        <td class="second"><span class="desc"><%= plant[0].attributes.description %></span></td>
                        <td class="third">
                            <% if (plant[0].attributes.country !== "NULL") { %>
                                <span class="desc"><%= plant[0].attributes.country %></span>
                            <% } %>
                        </td>
                        <td class="fourth">
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></a>
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></a>
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></a>
                        </td>
                    </tr>
                    <%});%>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapsePlantsGen">Genus</a>
                </h4>
            </div>
            <div id="collapsePlantsGen" class="panel-collapse collapse in" aria-expanded="true">
                <table id="plant-list" class="table">
                    <% _.each(plantsGen, function(plant) { %>
                    <tr>
                        <td class="first"><span class="badge"><%= plant[1] %></span></td>
                        <td class="second"><span class="desc"><%= plant[0].attributes.description %></span></td>
                        <td class="third">
                            <% if (plant[0].attributes.country !== "NULL") { %>
                                <span class="desc"><%= plant[0].attributes.country %></span>
                            <% } %>
                        </td>
                        <td class="fourth">
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></a>
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></a>
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></a>
                        </td>
                    </tr>
                    <%});%>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapsePlantsSpe">Species</a>
                </h4>
            </div>
            <div id="collapsePlantsSpe" class="panel-collapse collapse in" aria-expanded="true">
                <table id="plant-list" class="table">
                    <% _.each(plantsSpe, function(plant) { %>
                    <tr>
                        <td class="first"><span class="badge"><%= plant[1] %></span></td>
                        <td class="second"><span class="desc"><%= plant[0].attributes.description %></span></td>
                        <td class="third">
                            <% if (plant[0].attributes.country !== "NULL") { %>
                                <span class="desc"><%= plant[0].attributes.country %></span>
                            <% } %>
                        </td>
                        <td class="fourth">
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></a>
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></a>
                            <a href="#" id="<%= plant[0].attributes.id %>" data-category="Plant" data-term="<%= plant[0].attributes.description %>" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></a>
                        </td>
                    </tr>
                    <%});%>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapseCompounds">Compounds</a>
                </h4>
            </div>
            <div id="collapseCompounds" class="panel-collapse collapse in" aria-expanded="true">
                <table id="compound-list" class="table">
                    <% _.each(compounds, function(compound) { %>
                    <tr>
                        <td class="first"><span class="badge"><%= compound[1] %></span></td>
                        <td class="second"><span class="desc"><%= compound[0].attributes.description %></span></td>
                        <td class="third"> </td>
                        <td class="fourth">
                            <a href="#" id="<%= compound[0].attributes.id %>" data-category="Compound" data-term="<%= compound[0].attributes.description %>" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></a>
                            <a href="#" id="<%= compound[0].attributes.id %>" data-category="Compound" data-term="<%= compound[0].attributes.description %>" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></a>
                            <a href="#" id="<%= compound[0].attributes.id %>" data-category="Compound" data-term="<%= compound[0].attributes.description %>" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></a>
                        </td>
                    </tr>
                    <%});%>
                </table>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapseActivities">Activities</a>
                </h4>
            </div>
            <div id="collapseActivities" class="panel-collapse collapse in" aria-expanded="true">
                <table id="activity-list" class="table">
                    <% _.each(activities, function(activity) { %>
                    <tr>
                        <td class="first"><span class="badge"><%= activity[1] %></span></td>
                        <td class="second"><span class="desc"><%= activity[0].attributes.description %></span></td>
                        <td class="third"> </td>
                        <td class="fourth">
                            <a href="#" id="<%= activity[0].attributes.id %>" data-category="Activity" data-term="<%= activity[0].attributes.description %>" class="btn btn-success btn-xs add"><span class="glyphicon glyphicon-plus"></span></a>
                            <a href="#" id="<%= activity[0].attributes.id %>" data-category="Activity" data-term="<%= activity[0].attributes.description %>" class="btn btn-danger btn-xs add-mustnot"><span class="glyphicon glyphicon-minus"></span></a>
                            <a href="#" id="<%= activity[0].attributes.id %>" data-category="Activity" data-term="<%= activity[0].attributes.description %>" class="btn btn-warning btn-xs add-should"><span class="glyphicon glyphicon-check"></span></a>
                        </td>
                    </tr>
                    <%});%>
                </table>
            </div>
        </div>
    </div>
</script>

<script id="select-template" type="text/x-underscore-template">
    <div class="from-group">
        <select id="country-selector" class="form-control">
            <option value="Region" selected="selected">Select region</option>
            <% _(countries).each(function(country) { %>
                <% if (country !== null) {%>
                    <option value="<%= country %>"><%= country %></option>
                <% } %>
            <% }); %>
        </select>
</script>

<script id="article-template" type="text/x-underscore-template">
    <% if (articles.length > perPage ) { %>
        <h4 id="article-count" style:"padding: 0px;> Showing <%= perPage %> of <%= numberOfHits %> articles related to the query.</h4>
    <% } else { %>
        <h4 id="article-count" style:"padding: 0px;> Showing <%= numberOfHits %> articles related to the query.</h4>
    <% } %>
    <div class="articles-body">
    <% _.each(articles.models.slice(0,perPage), function(model) { %>
        <% if (model !== undefined) { %>
            <div class="row">
                <div class="col-md-12 col-centered">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <a href="<%= model.attributes['article.pmc_article_url']%>" target="blank"><%= model.attributes['article.title'] %></a>
                            </div>
                            <div class="panel-body">
                                <span><strong>Authors:</strong> <%= model.attributes['article.authors'] %></span><br>
                                <span><strong>DOI:</strong> <%= model.attributes['article.doi'] %></span><br>
                                <span><strong>PubMed Central:</strong> <a class="url" href="<%= model.attributes['article.pmc_article_url'] %>" target="_blank"><%= model.attributes['article.pmc_article_url'] %></a></span>
                            </div>
                            <div class="panel-footer">
                                <% if (typeof(model.attributes['article.abstract']) !== 'undefined') { %>
                                    <p><%= model.attributes['article.abstract'] %> </p>
                                <% } %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <% } %>
    <% }); %>
    <% if (articles.length > 5 && perPage < articles.length) { %>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <a href=# class="btn btn-default loadMore">Load more</a>
            </div>
        </div>
    <% } %>
    </div>


</script>

<script id="family-modal-template" type="text/x-underscore-template">
    <div class="row" style="border-top: 1px solid #C3C1C1; border-top-left-radius: 8px; margin:0 5px;">
        <div class="col-md-12" style="color:#83858B;">
            <ul class="list-group">
                <%= console.log(parents) %>
                <% for(var model in parents.models){ %>
                    <%= console.log(model) %>
                    <li><%= model %></li>
                <% } %>
            </ul>
        </div>
    </div>
</script>



</#macro>

<#macro javascripts>

<script type="text/javascript" src="/javascript/lib/typeahead-0.10.5.js"></script>
<script type="text/javascript" src="/javascript/ks.js"></script>
<script type="text/javascript" src="/javascript/lib/backbone.treemodel.js"></script>
<!--Backbone-tree-view: -->
<script type="text/javascript" src="/javascript/lib/backbone-tree-view.js"></script>
<script type="text/javascript" src="/javascript/lib/backbone-tooltip.js"></script>

<script src="/javascript/lib/pdfmake.min.js"></script>
<script src="/javascript/lib/vfs_fonts.js"></script>

<!-- Initialize the application -->
<script type="text/javascript" src="/javascript/app/namespace.js"></script>

<!--Extra code used for plugins-->
<script type="text/javascript" src="/javascript/app/extra/extra.js"></script>

<!--Models used in Bioprospectus-->
<script type="text/javascript" src="/javascript/app/models/Concept.js"></script>
<script type="text/javascript" src="/javascript/app/models/Article.js"></script>
<script type="text/javascript" src="/javascript/app/models/Term.js"></script>

<!--Collections used in Bioprospectus-->
<script type="text/javascript" src="/javascript/app/collections/Concepts.js"></script>
<script type="text/javascript" src="/javascript/app/collections/Articles.js"></script>
<script type="text/javascript" src="/javascript/app/collections/Hierarchy.js"></script>
<script type="text/javascript" src="/javascript/app/collections/Family.js"></script>
<script type="text/javascript" src="/javascript/app/collections/Terms.js"></script>
<script type="text/javascript" src="/javascript/app/collections/Children.js"></script>

<!--Views used in Bioprospectus-->
<script type="text/javascript" src="/javascript/app/views/ArticlesList.js"></script>
<script type="text/javascript" src="/javascript/app/views/BreadCrumbs.js"></script>
<script type="text/javascript" src="/javascript/app/views/Concepts.js"></script>
<script type="text/javascript" src="/javascript/app/views/History.js"></script>
<script type="text/javascript" src="/javascript/app/views/QueryInput.js"></script>
<script type="text/javascript" src="/javascript/app/views/QueryPanel.js"></script>
<script type="text/javascript" src="/javascript/app/views/SelectRegion.js"></script>
<script type="text/javascript" src="/javascript/app/views/Trees.js"></script>

<script type="text/javascript" src="/javascript/app/app.js"></script>



</#macro>

</@layout.appLayout>
