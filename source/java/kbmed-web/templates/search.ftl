<!DOCTYPE html>
<#import "layout.ftl" as layout>
<@layout.appLayout stylesheets=stylesheets javascripts=javascripts templates=templates>

<#macro stylesheets>
    <link rel="stylesheet" type="text/css" href="/css/jquery-qtip-2.2.1.css">
    <link rel="stylesheet" type="text/css" href="/css/magnific-popup.css"> 
</#macro>

<div class="row" style="margin-top: 2px;">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="row">
                    <div id="clear-query" class="col-md-1 col-md-offset-8">
                        <a href="#"><p class="text-right" style="margin-bottom: -2px;">Clear All</p></a>
                    </div>
                </div>
                <div id="query-panel" class="row">
                    <div id="query-box" class="col-md-9" >
                        <div class="row">
                            <div class="col-md-12" style="padding-left: 0px">
                                <ul id="query-terms">
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-left: 0px">
                                <input id="query-input" type="text" size="60" placeholder="Type your query here ..." autocomplete="off" spellcheck="false">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" style="padding: 1px 0px;">
                        <button id="search-btn" type="button" class="btn btn-primary btn-md" style="width:100px;">Search</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="results" role="tabpanel" class="col-md-12" style="margin-top: 10px; padding-left: 0px;">
                <div id="results"  role="tabpanel" class="tab-pane active">
                    <div class="col-md-2" style="padding: 0 2px 0 2px;">
                        <div id="text-facets" class="panel-group" role="tablist" aria-multiselectable="true">
                        </div>
                    </div>
                    <div id="text-ranking" class="col-md-10" style="overflow-y: auto; height: 800px;">
                    </div>
<!--                <div id="image-ranking" class="col-md-5" style="overflow-y: auto; height: 800px;">                        
                    </div> -->
                    
            </div>
        </div>
    </div>
</div>




<#macro templates>
<script id="main-category" type="text/x-underscore-template">    
    <div class="panel-group" id="">
         <div class="panel <%=image.panelt%>">
            <div class="panel-heading" role="tab" id="heading<%= image.idcategory%>">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#<%= image.idcategory%>" aria-expanded="true" aria-controls="<%= image.idcategory %>">
                        <%= image.tcategory %>
                    </a>
                </h4>
            </div>
            <div id="<%= image.idcategory %>" class="panel-collapse collapse"> 
                <div class="panel-body">   
                    <div class="subcategories">                                            
                    </div>        
                    <br>           
                </div>                
            </div>
        </div>
    </div>
</script>


<!-- This section shows images found for a query -->
<!--
<script id="sub-category" type="text/x-underscore-template">
    <div id ="<%=image.category%>" class="col-md-6"> 
        <a href="#" style="font-size: 10pt;"><%=image.icategory%></a><span class="badge" style="float:right;">0</span>   
        <hr style="margin-top:0.2em;margin-bottom:0.2em;">
    </div> 
 </script>

<script id="images-thumbnail-template" type="text/x-underscore-template">
    <div id ="<%= image.iri %>" class="col-md-4"> 
        <br>  
          <img src="<%=image.urlImageService%>"/>     
    </div>  
</script>
-->       

<!-- This section shows each result found for a query -->
<script id="clef-snippet-template" type="text/x-underscore-template">
    <div class="row" style="border-top: 1px solid #C3C1C1; border-top-left-radius: 8px; margin:0 5px;">
        <div id="title" class="col-md-12">
            <h3 style="margin-top: 10px;"><a href="<%=pmc_article_url %>" target="blank" style="color:#000;"><%= title %></a></h3>
        </div>
        <div class="col-md-12" style="color:#83858B; margin-top: -10px;">
            <span>Authors: <%= authors %></span>
            <br>
            <span>DOI: <%= doi %></span>
            <br>
            <span>PubMed Central: <a href="<%= pmc_article_url %>" target="_blank"><%= pmc_article_url %></a></span>
        </div>
        <% if (typeof(abstract) !== 'undefined') { %>
            <div id="abstract" class="col-md-12">
            <p style="color: #666; text-align: justify; white-space: pre-line;"><%= abstract%></p>
            </div>
        <% } %>
    </div>
</script>

<!-- This section shows categories found in the database for a query  -->

<script id="facet-panel-template" type="text/x-underscore-template">
    <%
        var panelTypes = [];
        panelTypes.push("panel-primary");
        panelTypes.push("panel-success");
        panelTypes.push("panel-info");
        panelTypes.push("panel-warning");
        var count = 0;
        for (var category in categoriesMap){
        %>
        <div class="panel <%=panelTypes[count%4]%>">
            <div class="panel-heading" role="tab" id="heading<%print(count);%>">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#text-facets" href="#collapse<%print(count);%>" aria-expanded="true" aria-controls="collapse<%print(count);%>">
                        <% print(category.toUpperCase());%>
                    </a>
                </h4>
            </div>
            <div id="collapse<%print(count);%>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<%print(count++);%>">
                <div class="list-group">
                  <%
                  var categoryConcepts = categoriesMap[category];
                  for (var i = 0;i<categoryConcepts.length;i++){
                    var concept = categoryConcepts[i];
                    var description = concept.description;
                    if (description.length > 20){
                        description = description.substring(0,20)+"...";
                    }
                    print('<li class="list-group-item">'
                        + '<a id="' + concept.id + '" class="facet" href="#">' + description+ '</a>'
                        + '<span class="badge">'+concept.doc_count+'</span></li>');
                  }
                  %>
                </div>
            </div>
        </div>
    <% } %>
</script>

<script id="facet-template" type="text/x-underscore-template">
    <li class="list-group-item concept" style="padding: 5px 8px" conceptId="<%= id %>">
        <script type="text/javascript" src="/javascript/index.js"></script>

    </li>
</script>
</#macro>

<#macro javascripts>
<script type="text/javascript" src="/javascript/lib/typeahead-0.10.5.js"></script>
<script type="text/javascript" src="/javascript/lib/jquery-qtip-2.2.1.js"></script>
<script type="text/javascript" src="/javascript/annotation-tooltip.js"></script>
<script type="text/javascript" src="/javascript/lib/jquery.magnific-popup.js"></script>
<script type="text/javascript" src="/javascript/ks.js"></script>
<script type="text/javascript" src="/javascript/search.js"></script>
<script type="text/javascript" >
    App.initialize();
</script>


</#macro>

</@layout.appLayout>
