<#import "layout.ftl" as layout>
<@layout.appLayout stylesheets=stylesheets javascripts=javascripts templates=templates>
<#macro stylesheets>
    <link rel="stylesheet" type="text/css" href="/css/jquery-qtip-2.2.1.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" >
</#macro>
<div class="row" style="padding-top: 30px">
    <div class="col-md-3">
        <div style="position: fixed; border: none; padding-top: 10px;" data-spy="affix" data-offset-top="300" data-offset-bottom="100" class="panel panel-default" role="complementary">
            <div style="font-weight: bolder; border: solid 1px #ddd; text-align: center;"class="panel-heading">ANNOTATIONS</div>
            <div style=" height: 600px; overflow: auto;">
                <ul id="annotations-list" class="list-group">
                </ul>
            </div>
        </div>
    </div>
    <div id="text-content" class="col-md-9"
         style="color: #666;
         text-align: justify;
         white-space: pre-line;
         padding: 5px 60px 0px;">
        <#if title??>
        <div  style="font-weight: bold; text-align: center; color: #000;">
            <h3>${title}<h3>
        </div>
        </#if>

        <#if abstract??>
        <div id="text-content2">
            <h4>Abstract</h4>
            <p>${abstract}</p>
        </div>
        </#if>

        <#if fulltext??>
        <div id="text-content3" >
            <h4>Fulltext</h4>
            <p>${fulltext}</p>
        </div>
        </#if>
    </div>
</div>

<#macro templates>
<script id="list-item-template" type="text/x-underscore-template">
        <li class="list-group-item annotation-item" >
            <div class="checkbox" style="margin: 0px;">
               <label>
                  <input type="checkbox"><span class="conceptId" ><span class="description"><%print(prefTerm.substring(0,48))%></span>
               </label>
            </div>
        </li>
</script>
</#macro>

<#macro javascripts>
    <script type="text/javascript" src="/javascript/lib/jquery-qtip-2.2.1.js"></script>
    <script type="text/javascript" src="/javascript/annotation.js"></script>
    <script type="text/javascript" src="/javascript/viz.js"></script>
    <script type="text/javascript">
        var annotations = ${annotations};
    </script>
</#macro>
</@layout.appLayout>