package net.lariverosc.icontent;

import net.lariverosc.icontent.data.InformationContentManager;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * IC (a)= -log10((|leaves(a)|/|subsummers(a))|+1/(max_leaves+1)) Where: leaves(a) = {l e C | l is a hyponyms of a AND l is a
 * leaf} where l is leaf iff hyponyms(l)=0 subsummers(a) = {c e C | a is a hierarchical specialization of c} U {a} max_leaves =
 * the number of leaves corresponding to root (all leaves) Ontology-based information content computation. David Sánchez,
 * Montserrat Batet, David Isern
 *
 * @author Alejandro Riveros Cruz
 */
public class ComputeICValues {

    private final Logger log = LoggerFactory.getLogger(ComputeICValues.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private InformationContentManager informationContentManager;

    /**
     *
     */
    public void compute() {
        log.info("Starting the count procces");
        Map<Concept, Set<Concept>> leavesMap = countLeaves();
        Map<Concept, Set<Concept>> subsumersMap = countSubsumers();
        int totalLeaves = countTotalLeaves();
        Map<String, Double> ICValues = computeICValues(leavesMap, subsumersMap, totalLeaves);
        log.info("Storing a total of {} icvalues", ICValues.size());
        informationContentManager.save(ICValues);
    }

    protected Map<Concept, Set<Concept>> countLeaves() {
        log.info("Counting leaves");
        Map<Concept, Set<Concept>> leavesMap = new HashMap<Concept, Set<Concept>>();
        countLeaves(leavesMap, hierarchyKnowledgeProvider.getTopConcept(), new HashSet<Concept>());
        return leavesMap;
    }

    private void countLeaves(Map<Concept, Set<Concept>> leavesMap, Concept concept, Set<Concept> parentLeaves) {
        Set<Concept> currentLeaves;
        if (!leavesMap.containsKey(concept)) { //If leaves set if is not created
            currentLeaves = new HashSet<Concept>(); //Create new leaves set
            leavesMap.put(concept, currentLeaves);// Add to leaves map
        } else {
            currentLeaves = leavesMap.get(concept);//Get leaves set
        }
        //Validate if call more recursion
        if (hierarchyKnowledgeProvider.getInDegree(concept) == 0) {//Is leave
            parentLeaves.add(concept);
            return;//Recursion end
        } else {
            Set<Concept> childrens = hierarchyKnowledgeProvider.getChildrens(concept);
            for (Concept children : childrens) {//For each children
                countLeaves(leavesMap, children, currentLeaves); //Call the recursive method
            }
            parentLeaves.addAll(currentLeaves); // add this children as a leave
        }
    }

    protected Map<Concept, Set<Concept>> countSubsumers() {
        log.info("Counting subsummers");
        Map<Concept, Set<Concept>> subsumersMap = new HashMap<Concept, Set<Concept>>();
        countSubsumers(subsumersMap, hierarchyKnowledgeProvider.getTopConcept(), new HashSet<Concept>());
        return subsumersMap;
    }

    private void countSubsumers(Map<Concept, Set<Concept>> subsumersMap, Concept concept, Set<Concept> parentSubsumers) {
        Set<Concept> currentSubsummers;
        if (!subsumersMap.containsKey(concept)) { //If subsemmers set if is not created
            currentSubsummers = new HashSet<Concept>(); //Create new subsumers set
            subsumersMap.put(concept, currentSubsummers);// Add to subsumers map
        } else {
            currentSubsummers = subsumersMap.get(concept);//Get subsumers
        }
        currentSubsummers.addAll(parentSubsumers);// Add the parent subsumers
        currentSubsummers.add(concept);//Add current concept as subsummer of itself
        //Validate if call more recursion
        if (hierarchyKnowledgeProvider.getInDegree(concept) == 0) {//Is leave
            return;//Recursion end
        } else {
            Set<Concept> childrens = hierarchyKnowledgeProvider.getChildrens(concept);
            for (Concept children : childrens) {//For each children
                countSubsumers(subsumersMap, children, currentSubsummers); //Call the recursive method
            }
        }
    }

    /**
     *
     * @param leavesMap
     * @param subsumersMap
     * @param totalLeaves
     * @return
     */
    protected Map<String, Double> computeICValues(Map<Concept, Set<Concept>> leavesMap, Map<Concept, Set<Concept>> subsumersMap, int totalLeaves) {
        log.info("Computing ic values");
        double doubleTotalLeaves = totalLeaves;
        Map<String, Double> icValuesMap = new HashMap<String, Double>();
        double maxIC = -1 * Math.log(1 / (doubleTotalLeaves + 1));
        for (Concept concept : subsumersMap.keySet()) {
            double conceptLeaves = (double) leavesMap.get(concept).size();
            double conceptSubsumers = (double) subsumersMap.get(concept).size();
            double IC = -1 * Math.log(((conceptLeaves / conceptSubsumers) + 1) / (doubleTotalLeaves + 1));
            icValuesMap.put(concept.getId(), IC / maxIC);
        }
        return icValuesMap;
    }

    private static double LOG16 = Math.log(16);

    /**
     *
     * @param val
     *
     * @return
     */
    public static double log16(double val) {
        return Math.log(val) / LOG16;
    }

    /**
     *
     * @return
     */
    protected int countTotalLeaves() {
        int totalLeaves = 0;
        Map<String, Concept> allConcepts = entityKnowledgeProvider.getAllConcepts();
        for (Concept concept : allConcepts.values()) {
            //log.info(concept.toString());
            //log.info("inner: "+hierarchyKnowledgeProvider.getInDegree(concept));
            //log.info("outer: "+hierarchyKnowledgeProvider.getOutDegree(concept));
            if (hierarchyKnowledgeProvider.getInDegree(concept) == 0 && hierarchyKnowledgeProvider.getOutDegree(concept) > 0) {
                log.info(concept.toString());
                totalLeaves++;
            }
        }
        log.info("Total leaves {}", totalLeaves);
        return totalLeaves;
    }

    /**
     *
     * @param entityKnowledgeProvider
     */
    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    /**
     *
     * @param hierarchyKnowledgeProvider
     */
    public void setHierarchyKnowledgeProvider(HierarchyKnowledgeProvider hierarchyKnowledgeProvider) {
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
    }

    /**
     *
     * @param informationContentManager
     */
    public void setInformationContentManager(InformationContentManager informationContentManager) {
        this.informationContentManager = informationContentManager;
    }
}
