package net.lariverosc.icontent;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import net.lariverosc.icontent.data.InformationContentManager;
import net.lariverosc.icontent.data.RedisInformationContentManager;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ComputeICValuesMain {

    private static final Logger log = LoggerFactory.getLogger(ComputeICValuesMain.class);

    private static ComputeICValues initContext(InformationContentManager informationContentManager) {
        log.info("Starting Spring application context");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:snomed-context.xml");
        context.registerShutdownHook();

        EntityKnowledgeProvider entityKnowledgeProvider = context.getBean("entityKnowledgeProvider", EntityKnowledgeProvider.class);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = context.getBean("hierarchyKnowledgeProvider", HierarchyKnowledgeProvider.class);

        ComputeICValues computeICValues = new ComputeICValues();
        computeICValues.setEntityKnowledgeProvider(entityKnowledgeProvider);
        computeICValues.setHierarchyKnowledgeProvider(hierarchyKnowledgeProvider);
        computeICValues.setInformationContentManager(informationContentManager);
        return computeICValues;

    }

    private static InformationContentManager instanceInformationContentManager(Arguments arguments) {
        InformationContentManager informationContentManager = null;
        log.info("IC values will be stored in redis: {jedis://{}@{}:{}}", new Object[]{arguments.getPassword(), arguments.getHost(), arguments.getPort()});
        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), arguments.getHost(), arguments.getPort(), 7000, arguments.getPassword());
        informationContentManager = new RedisInformationContentManager(jedisPool);
        return informationContentManager;
    }

    private static Arguments parseArguments(String[] args) {
        ComputeICValuesMain.Arguments arguments = new Arguments();
        JCommander jCommander = new JCommander(arguments, args);
        jCommander.setProgramName("comp-ic");
        if (arguments.isHelp()) {
            jCommander.usage();
            return null;
        }
        return arguments;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            Arguments arguments = parseArguments(args);
            if (arguments == null) {
                return;
            }
            InformationContentManager informationContentManager = instanceInformationContentManager(arguments);

            ComputeICValues computeICValues = initContext(informationContentManager);

            computeICValues.compute();
        } catch (Exception e) {
            log.error("Fatal Error!!! ", e);
            throw new RuntimeException(e);
        }
    }

    private static class Arguments {

        @Parameter(names = "--redis-host", description = "Optional: Redis host", required = false)
        private String host = "localhost";

        @Parameter(names = "--redis-port", description = "Optional: Redis port", required = false)
        private Integer port = 6379;

        @Parameter(names = "--redis-password", description = "Optional: Redis password", required = false)
        private String password = null;

        @Parameter(names = {"-h", "--help"}, description = "print this message", help = true)
        private Boolean help = false;

        public String getHost() {
            return host;
        }

        public Integer getPort() {
            return port;
        }

        public String getPassword() {
            return password;
        }

        public Boolean isHelp() {
            return help;
        }
    }
}
