package net.lariverosc.icontent.data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import net.lariverosc.knowledge.domain.Concept;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CachedInformationContentManager implements InformationContentManager {
    
    private final Logger log = LoggerFactory.getLogger(CachedInformationContentManager.class);
    
    private InformationContentManager informationContentManager;
    
    private Map<String, Double> icValuesCache;
    
    /**
     *
     * @param informationContentManager
     */
    public CachedInformationContentManager(InformationContentManager informationContentManager) {
        this.informationContentManager = informationContentManager;
        preFetchValues();
    }
    
    private void preFetchValues() {
        log.info("Pre-fetching all available icvals");
        icValuesCache = new ConcurrentHashMap<String, Double>(getAllValues());
        if (icValuesCache.isEmpty()){
            log.warn("IC values not available");
        }
    }
    
    /**
     *
     * @param icValuesMap
     */
    @Override
    public void save(Map<String, Double> icValuesMap) {
        informationContentManager.save(icValuesMap);
    }
    
    /**
     *
     * @param concept
     * @return
     */
    @Override
    public double getValue(Concept concept) {
        Double ic = icValuesCache.get(concept.getId());
        if (ic == null) {
            return 0;
        }
        return ic;
    }
    
    /**
     *
     * @return
     */
    @Override
    public Map<String, Double> getAllValues() {
        return informationContentManager.getAllValues();
    }
}
