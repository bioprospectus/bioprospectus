package net.lariverosc.icontent.data;

import java.util.Map;
import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface InformationContentManager {

    /**
     *
     * @param icValuesMap
     */
    void save(Map<String, Double> icValuesMap);

    /**
     *
     * @param concept
     * @return
     */
    double getValue(Concept concept);

    /**
     *
     * @return
     */
    Map<String, Double> getAllValues();
}
