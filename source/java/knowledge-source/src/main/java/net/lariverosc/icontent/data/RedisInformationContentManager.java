package net.lariverosc.icontent.data;

import java.util.HashMap;
import java.util.Map;
import net.lariverosc.knowledge.domain.Concept;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class RedisInformationContentManager implements InformationContentManager {

    private final Logger log = LoggerFactory.getLogger(RedisInformationContentManager.class);

    private static final String REDIS_NAME_SPACE = "ICVALS";

    private final JedisPool jedisPool;

    /**
     *
     * @param jedisPool
     */
    public RedisInformationContentManager(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    /**
     *
     * @param icValuesMap
     */
    @Override
    public void save(Map<String, Double> icValuesMap) {
        log.info("Storing ic values in redis");
        Jedis jedis = jedisPool.getResource();
        Pipeline pipeline = jedis.pipelined();
        for (Map.Entry<String, Double> entry : icValuesMap.entrySet()) {
            pipeline.hset(REDIS_NAME_SPACE, entry.getKey(), String.valueOf(entry.getValue()));
        }
        pipeline.sync();
        jedisPool.returnResource(jedis);
    }

    /**
     *
     * @param concept
     * @return
     */
    @Override
    public double getValue(Concept concept) {
        Jedis jedis = jedisPool.getResource();
        String icValStr = jedis.hget(REDIS_NAME_SPACE, concept.getId());
        double ic = 0;
        if (icValStr == null) {
            log.warn("ic value not found for conceptId {}", concept.getId());
        } else {
            ic = Double.parseDouble(icValStr);
        }
        jedisPool.returnResource(jedis);
        return ic;
    }

    /**
     *
     * @return
     */
    @Override
    public Map<String, Double> getAllValues() {
        Map<String, Double> allIcValues = new HashMap<String, Double>();
        Jedis jedis = jedisPool.getResource();
        Map<String, String> tempMap = jedis.hgetAll(REDIS_NAME_SPACE);
        for (Map.Entry<String, String> entry : tempMap.entrySet()) {
            allIcValues.put(entry.getKey(), Double.parseDouble(entry.getValue()));
        }
        jedisPool.returnResource(jedis);
        return allIcValues;
    }
}
