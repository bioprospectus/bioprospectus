 package net.lariverosc.knowledge;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class KnowledgeSourceException extends RuntimeException {

    public KnowledgeSourceException() {
    }

    /**
     *
     * @param message
     */
    public KnowledgeSourceException(String message) {
        super(message);
    }

    /**
     *
     * @param message
     * @param thrwbl
     */
    public KnowledgeSourceException(String message, Throwable thrwbl) {
        super(message, thrwbl);
    }
}
