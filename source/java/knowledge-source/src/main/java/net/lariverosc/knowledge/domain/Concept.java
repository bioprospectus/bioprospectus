package net.lariverosc.knowledge.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 *
 */
public class Concept {

    private String id;

    private String category;

    private String description;

    private String prefTerm;
    
    private String country;

    private Map<String, String> attributes;

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *
     * @return
     */
    public Concept withId(String id) {
        setId(id);
        return this;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     *
     * @return
     */
    public Concept withDescription(String description) {
        setDescription(description);
        return this;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     *
     * @return
     */
    public Concept withCategory(String category) {
        setCategory(category);
        return this;
    }

    /**
     *
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     *
     * @return
     */
    public String getPrefTerm() {
        return prefTerm;
    }

    /**
     *
     * @param prefTerm
     */
    public void setPrefTerm(String prefTerm) {
        this.prefTerm = prefTerm;
    }
    
    public String getCountry() {
        return country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @param prefTerm
     *
     * @return
     */
    public Concept withPrefTerm(String prefTerm) {
        setPrefTerm(prefTerm);
        return this;
    }

    public void addAttribute(String attrName, String attrValue) {
        if (attributes == null) {
            attributes = new HashMap<String, String>();
        }
        attributes.put(attrName, attrValue);
    }

    public String getAttribute(String attrName) {
        if (attributes == null) {
            return null;
        }
        return attributes.get(attrName);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     *
     * @param obj
     *
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Concept other = (Concept) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Concept{" + "id=" + id + ", description=" + description + ", category=" + category + '}';
    }



    
}
