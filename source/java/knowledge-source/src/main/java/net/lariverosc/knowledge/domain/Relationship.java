package net.lariverosc.knowledge.domain;

import java.util.Objects;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Relationship {

    /**
     *
     */
    public enum RelationType {

        /**
         *
         */
        IS_A("IS_A");

        private final String name;

        private RelationType(String name) {
            this.name = name;
        }

        /**
         *
         * @return
         */
        public String getName() {
            return name;
        }
    }

    private String id;

    private String sourceId;

    private String targetId;

    private String type;

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @param id
     *
     * @return
     */
    public Relationship withId(String id) {
        setId(id);
        return this;
    }

    /**
     *
     * @return
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     *
     * @param sourceConceptId
     */
    public void setSourceId(String sourceConceptId) {
        this.sourceId = sourceConceptId;
    }

    /**
     *
     * @param sourceConceptId
     *
     * @return
     */
    public Relationship withSourceId(String sourceConceptId) {
        setSourceId(sourceConceptId);
        return this;
    }

    /**
     *
     * @return
     */
    public String getTargetId() {
        return targetId;
    }

    /**
     *
     * @param targetConceptId
     */
    public void setTargetId(String targetConceptId) {
        this.targetId = targetConceptId;
    }

    /**
     *
     * @param targetConceptId
     *
     * @return
     */
    public Relationship withTargetId(String targetConceptId) {
        setTargetId(targetConceptId);
        return this;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @param type
     *
     * @return
     */
    public Relationship whitType(String type) {
        setType(type);
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     *
     * @param obj
     *
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Relationship other = (Relationship) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Relationship{" + "id=" + id + ", sourceId=" + sourceId + ", targetId=" + targetId + ", type=" + type + '}';
    }
}
