package net.lariverosc.knowledge.domain;

import java.util.Objects;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Term {

    private String id;

    private String conceptId;

    private String term;

    /**
     *
     */
    public Term() {
    }

    /**
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *
     * @return
     */
    public Term withId(String id) {
        setId(id);
        return this;
    }

    /**
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getConceptId() {
        return conceptId;
    }

    /**
     *
     * @param conceptId
     *
     * @return
     */
    public Term withConceptId(String conceptId) {
        setConceptId(conceptId);
        return this;
    }

    /**
     *
     * @param conceptId
     */
    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }

    /**
     *
     * @return
     */
    public String getTerm() {
        return term;
    }

    /**
     *
     * @param term
     *
     * @return
     */
    public Term withTerm(String term) {
        setTerm(term);
        return this;
    }

    /**
     *
     * @param term
     */
    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    /**
     *
     * @param obj
     *
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Term other = (Term) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Term{" + "id=" + id + ", conceptId=" + conceptId + ", term=" + term + '}';
    }
}
