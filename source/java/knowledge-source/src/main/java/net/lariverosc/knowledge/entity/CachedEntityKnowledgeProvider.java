package net.lariverosc.knowledge.entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.KnowledgeSourceException;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.domain.Relationship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CachedEntityKnowledgeProvider implements EntityKnowledgeProvider {

    private final Logger log = LoggerFactory.getLogger(CachedEntityKnowledgeProvider.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private Map<String, Concept> allConceptsMap;

    private Map<String, Term> allTermsMap;

    private Map<String, Set<Term>> termsByConcept;

    private Concept topConcept;

    /**
     *
     */
    public void init() {
        getAllConcepts();
        getAllTerms();
    }

    @Override
    public String getTopConceptId() {
        return entityKnowledgeProvider.getTopConceptId();
    }

    @Override
    public Set<String> getCategories() {
        return entityKnowledgeProvider.getCategories();
    }

    @Override
    public Map<String, Concept> getAllConcepts() {
        if (allConceptsMap == null) {
            log.info("Initializing concepts cache");
            allConceptsMap = Collections.unmodifiableMap(entityKnowledgeProvider.getAllConcepts());
        }
        return allConceptsMap;
    }
    
    @Override
    public Set<String> getConceptsByCategory(String category_str){
        return entityKnowledgeProvider.getConceptsByCategory(category_str);
    }
    
    @Override
    public Map<String, Term> getAllTerms() {
        if (allTermsMap == null) {
            log.info("Initializing terms cache");
            allTermsMap = Collections.unmodifiableMap(entityKnowledgeProvider.getAllTerms());
            log.info("termsmap: "+ entityKnowledgeProvider.getClass().toString());
            termsByConcept = new HashMap<String, Set<Term>>();
            for (Term term : allTermsMap.values()) {
                String conceptId = term.getConceptId();
                Set<Term> terms = termsByConcept.get(conceptId);
                if (terms == null) {
                    terms = new HashSet<Term>();
                }
                terms.add(term);
                termsByConcept.put(conceptId, terms);
            }
        }
        return allTermsMap;
    }

    @Override
    public Map<String, Relationship> getAllRelations(Relationship.RelationType type) {
        return entityKnowledgeProvider.getAllRelations(type);
    }

    @Override
    public Map<String, Relationship> getAllRelations(String typeCode) {
        return entityKnowledgeProvider.getAllRelations(typeCode);
    }

    @Override
    public Concept getByConceptId(String conceptId) {
        Concept concept = getAllConcepts().get(conceptId);
        if (concept == null) {
            throw new KnowledgeSourceException(conceptId + " concept not found");
        }
        return concept;
    }

    @Override
    public Term getTerm(String termId) {
        Term term = getAllTerms().get(termId);
        if (term == null) {
            throw new KnowledgeSourceException("---"+termId + "---term not found");
        }
        return term;
    }

    @Override
    public String getPreferedTerm(String conceptId) {
        return entityKnowledgeProvider.getPreferedTerm(conceptId);
    }

    @Override
    public Relationship getByRelationById(String relationshipId) {
        return entityKnowledgeProvider.getByRelationById(relationshipId);
    }

    @Override
    public Set<Term> getTermsByConceptId(String conceptId) {
        return termsByConcept.get(conceptId);
    }

    @Override
    public Concept getConceptByTermId(String termId) {
        Term term = getTerm(termId);
        return getByConceptId(term.getConceptId());
    }

    @Override
    public Set<Relationship> getRelationBySourceConceptId(String sourceConceptId, String type) {
        return entityKnowledgeProvider.getRelationBySourceConceptId(sourceConceptId, type);
    }

    @Override
    public Set<Relationship> getRelationsByTargetConceptId(String targetConceptId, String type) {
        return entityKnowledgeProvider.getRelationsByTargetConceptId(targetConceptId, type);
    }

    /**
     *
     * @param entityKnowledgeProvider
     */
    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    @Override
    public Map<String, String> getRelationsTypes() {
        return entityKnowledgeProvider.getRelationsTypes();
    }

    @Override
    public Set<Relationship> getRelationsByConcepts(String sourceConceptId, String targetConceptId) {
        return entityKnowledgeProvider.getRelationsByConcepts(sourceConceptId, targetConceptId);
    }

    @Override
    public Set<Relationship> getRelationsBySource(String sourceConceptId) {
        return entityKnowledgeProvider.getRelationsBySource(sourceConceptId);
    }

    @Override
    public Set<Relationship> getRelationsByTarget(String targetConceptId) {
        return entityKnowledgeProvider.getRelationsByTarget(targetConceptId);
    }

    @Override
    public String getCountry(String conceptId) {
        return entityKnowledgeProvider.getCountry(conceptId);
    }

    @Override
    public Set<String> getAllCountries() {
        return entityKnowledgeProvider.getAllCountries();
    }

}
