package net.lariverosc.knowledge.entity;

import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.domain.Relationship;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface EntityKnowledgeProvider {

    /**
     *
     * @return
     */
    String getTopConceptId();

    /**
     *
     * @return
     */
    Set<String> getCategories();

    /**
     * 
     * @param category_str
     * 
     * @return
     */
    Set<String> getConceptsByCategory(String category_str);
    
    /**
     *
     * @return
     */
    Map<String, Concept> getAllConcepts();

    /**
     *
     * @return
     */
    Map<String, Term> getAllTerms();

    /**
     *
     * @return
     */
    Map<String, String> getRelationsTypes();

    /**
     *
     * @param type
     *
     * @return
     */
    Map<String, Relationship> getAllRelations(Relationship.RelationType type);

    /**
     *
     * @param type
     *
     * @return
     */
    Map<String, Relationship> getAllRelations(String type);

    /**
     *
     * @param conceptId
     *
     * @return
     */
    Concept getByConceptId(String conceptId);

    /**
     *
     * @param termId
     *
     * @return
     */
    Term getTerm(String termId);

    /**
     *
     * @param conceptId
     *
     * @return
     */
    String getPreferedTerm(String conceptId);

    /**
     * 
     * @param conceptId
     * 
     * @return
     */
    String getCountry(String conceptId);
    
    /**
     *
     * @param termId
     *
     * @return
     */
    Concept getConceptByTermId(String termId);

    /**
     *
     * @param conceptId
     *
     * @return
     */
    Set<Term> getTermsByConceptId(String conceptId);

    /**
     *
     * @param relationId
     *
     * @return
     */
    Relationship getByRelationById(String relationId);

    /**
     *
     * @param sourceConceptId
     * @param type
     *
     * @return
     */
    Set<Relationship> getRelationBySourceConceptId(String sourceConceptId, String type);

    /**
     *
     * @param targetConceptId
     * @param type
     *
     * @return
     */
    Set<Relationship> getRelationsByTargetConceptId(String targetConceptId, String type);

    /**
     *
     * @param sourceConceptId
     * @return
     */
    Set<Relationship> getRelationsBySource(String sourceConceptId);

    /**
     *
     * @param targetConceptId
     * @return
     */
    Set<Relationship> getRelationsByTarget(String targetConceptId);

    /**
     *
     * @param sourceConceptId
     * @param targetConceptId
     * @return
     */
    Set<Relationship> getRelationsByConcepts(String sourceConceptId, String targetConceptId);

    
    /**
     *
     * @return
     */
    Set<String> getAllCountries();


    


}
