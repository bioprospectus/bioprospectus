package net.lariverosc.knowledge.hierarchy;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
//TODO decouble this from concept, use generics and name it node, or use string as an identifier
public class ConceptNode {

    private final Concept concept;

    //Why this attribute
    private final Set<ConceptNode> childrenSet;

    private final Map<String, ConceptNode> childrenMap;

    /**
     *
     * @param concept
     */
    public ConceptNode(Concept concept) {
        this.concept = concept;
        this.childrenSet = new HashSet<ConceptNode>();
        this.childrenMap = new HashMap<String, ConceptNode>();
    }

    /**
     *
     * @return
     */
    @JsonIgnore
    public Concept getConcept() {
        return concept;
    }

    /**
     *
     * @return
     */
    public String getId() {
        return concept.getId();
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return concept.getDescription();
    }

    /**
     *
     * @return
     */
    public String getCategory() {
        return concept.getCategory();
    }
    
    /**
     *
     * @return
     */
    public Set<ConceptNode> getChildren() {
        return childrenSet;
    }

    /**
     *
     * @param conceptNode
     */
    public void addChildren(ConceptNode conceptNode) {
        childrenSet.add(conceptNode);
        childrenMap.put(conceptNode.getId(), conceptNode);
    }

    /**
     *
     * @param id
     * @return
     */
    public ConceptNode getChildrenById(String id) {
        return childrenMap.get(id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.concept != null ? this.concept.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConceptNode other = (ConceptNode) obj;
        if (this.concept != other.concept && (this.concept == null || !this.concept.equals(other.concept))) {
            return false;
        }
        return true;
    }
}
