package net.lariverosc.knowledge.hierarchy;

import java.util.List;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface HierarchyKnowledgeProvider {

    /**
     *
     */
    public final int NO_LIMIT = 0;

    /**
     *
     * @return
     */
    Concept getTopConcept();

    /**
     *
     * @param concept
     *
     * @return
     */
    int getInDegree(Concept concept);

    /**
     *
     * @param concept
     * @return
     */
    int getOutDegree(Concept concept);

    /**
     *
     * @param concept
     * @return
     */
    Set<Concept> getParents(Concept concept);

    /**
     *
     * @param concept
     * @return
     */
    Set<Concept> getChildrens(Concept concept);

    /**
     *
     * @param concept
     * @return
     */
    Set<Concept> getSiblings(Concept concept);

    /**
     *
     * @param concept
     * @return
     */
    Set<Concept> getNeighbors(Concept concept);

    /**
     *
     * @param concept
     * @return
     */
    List<Concept> getPathToRoot(Concept concept);

    /**
     *
     * @return
     */
    int getHierarchyDepth();

    /**
     *
     * @param concept
     * @return
     */
    int getConceptDepth(Concept concept);

    /**
     *
     * @param conceptSubSet
     * @return
     */
    Concept getLeastCommonSubsummer(Set<Concept> conceptSubSet);

    /**
     *
     * @param sourceConcept
     * @param targetConcept
     * @return
     */
    List<Concept> getArtificialShortestPath(Concept sourceConcept, Concept targetConcept);

    /**
     *
     * @param concepts
     * @return
     */
    ConceptNode getMinHierarchy(Set<Concept> concepts);

    /**
     *
     * @param concepts
     * @param limit 
     * @return
     */
    ConceptNode getSubHierarchy(Concept concepts, int limit);

    /**
     *
     * @param concept
     * @param limit 
     * @return
     */
    Set<Concept> getLeaves(Concept concept, int limit);

    /**
     * 
     * @param category
     * @return 
     */
    Set<String> getMainParentsByCategory(String category);
}
