package net.lariverosc.knowledge.jgrapht;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.KnowledgeSourceException;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.hierarchy.ConceptNode;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graphs;
import org.jgrapht.alg.BellmanFordShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JGraphTHierarchyKnowledgeProvider implements HierarchyKnowledgeProvider {

    private final Logger log = LoggerFactory.getLogger(JGraphTHierarchyKnowledgeProvider.class);

    private DirectedGraph<Concept, DefaultEdge> directedGraph;

    private DirectedGraph<Concept, DefaultEdge> reverseDirectedGraph;

    private BellmanFordShortestPath<Concept, DefaultEdge> bellmanFordShortestPath;

    private int maxDepth;

    private final EntityKnowledgeProvider entityKnowledgeProvider;

    /**
     *
     * @param entityKnowledgeProvider
     */
    public JGraphTHierarchyKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
        initHierarchyData();
    }

    private void initHierarchyData() {
        log.info("Initializing graph");
        initDirectedGraph();
        bellmanFordShortestPath = new BellmanFordShortestPath<Concept, DefaultEdge>(reverseDirectedGraph, getTopConcept());
        maxDepth = computeHierarchyMaxDepth();
    }

    private void initDirectedGraph() {
        directedGraph = new DefaultDirectedGraph<Concept, DefaultEdge>(DefaultEdge.class);
        reverseDirectedGraph = new DefaultDirectedGraph<Concept, DefaultEdge>(DefaultEdge.class);
        //add all concepts
        Map<String, Concept> allConcepts = entityKnowledgeProvider.getAllConcepts();
        for (Concept concept : allConcepts.values()) {
            directedGraph.addVertex(concept);
            reverseDirectedGraph.addVertex(concept);
        }
        //add all relationships
        Map<String, Relationship> allRelationships = entityKnowledgeProvider.getAllRelations(Relationship.RelationType.IS_A);
        for (Relationship relationship : allRelationships.values()) {
            try {
                Concept source = entityKnowledgeProvider.getByConceptId(relationship.getSourceId());
                Concept target = entityKnowledgeProvider.getByConceptId(relationship.getTargetId());
                if (source != null && target != null) {
                    directedGraph.addEdge(source, target);
                    reverseDirectedGraph.addEdge(target, source);
                }
            } catch (KnowledgeSourceException kse) {
                log.trace("Error while looking for relationship {}: ", new Object[]{relationship.getId(),kse.getMessage()});
            }
        }
    }

    private int computeHierarchyMaxDepth() {
        log.info("Computing hierarchy max depth");
        int tempMaxDepth = -1;
        Map<String, Concept> allConcepts = entityKnowledgeProvider.getAllConcepts();
        for (Concept concept : allConcepts.values()) {
            int conceptDepth = getConceptDepth(concept);
            if (tempMaxDepth < conceptDepth) {
                tempMaxDepth = conceptDepth;
            }
        }
        return tempMaxDepth;
    }

    @Override
    public Concept getTopConcept() {
        return entityKnowledgeProvider.getByConceptId(entityKnowledgeProvider.getTopConceptId());
    }

    @Override
    public int getInDegree(Concept concept) {
        return directedGraph.inDegreeOf(concept);
    }

    @Override
    public int getOutDegree(Concept concept) {
        return directedGraph.outDegreeOf(concept);
    }

    @Override
    public Set<Concept> getParents(Concept concept) {
        List<Concept> successors = Graphs.successorListOf(directedGraph, concept);
        return new HashSet<Concept>(successors);
    }

    @Override
    public Set<Concept> getChildrens(Concept concept) {
        List<Concept> predecessors = Graphs.predecessorListOf(directedGraph, concept);
        return new HashSet<Concept>(predecessors);
    }

    @Override
    public Set<Concept> getSiblings(Concept concept) {
        Set<Concept> parents = getParents(concept);
        Set<Concept> siblings = new HashSet<Concept>();
        for (Concept parent : parents) {
            siblings.addAll(getChildrens(parent));
        }
        siblings.remove(concept);
        return siblings;
    }

    @Override
    public Set<Concept> getNeighbors(Concept concept) {
        List<Concept> neighbors = Graphs.neighborListOf(directedGraph, concept);
        return new HashSet<Concept>(neighbors);
    }

    @Override
    public List<Concept> getPathToRoot(Concept concept) {
        if (concept.equals(getTopConcept())) {
            List<Concept> pathToRoot = new ArrayList<Concept>();
            pathToRoot.add(getTopConcept());
            return pathToRoot;
        }
        List<DefaultEdge> pathToRootInEdges = bellmanFordShortestPath.getPathEdgeList(concept);
        if (pathToRootInEdges != null) {
            List<Concept> pathToRootInConcepts = new ArrayList<Concept>();
            for (int i = pathToRootInEdges.size() - 1; i >= 0; i--) {
                DefaultEdge currentEdge = pathToRootInEdges.get(i);
                Concept currentConcept = reverseDirectedGraph.getEdgeTarget(currentEdge);
                pathToRootInConcepts.add(currentConcept);
            }
            pathToRootInConcepts.add(getTopConcept());
            return pathToRootInConcepts;
        }
        return Collections.<Concept>emptyList();
    }

    @Override
    public int getHierarchyDepth() {
        return maxDepth;
    }

    @Override
    public int getConceptDepth(Concept concept) {
        List<Concept> pathToRoot = getPathToRoot(concept);
        return pathToRoot.size() - 1;
    }

    @Override
    public Concept getLeastCommonSubsummer(Set<Concept> conceptSubSet) {
        ConceptNode currentConceptNode = getMinHierarchy(conceptSubSet);
        while (currentConceptNode.getChildren().size() == 1 && !conceptSubSet.contains(currentConceptNode.getConcept())) {
            currentConceptNode = currentConceptNode.getChildren().iterator().next();
        }
        return currentConceptNode.getConcept();
    }

    @Override
    public List<Concept> getArtificialShortestPath(Concept concept1, Concept concept2) {
        if (!concept1.getCategory().equals(concept2.getCategory())) {
            return null;
        }
        List<Concept> artificialShortestPath = new ArrayList<Concept>();

        List<Concept> pathToRootSourceConcept = getPathToRoot(concept1);
        List<Concept> pathToRootTargetConcept = getPathToRoot(concept2);

        for (int i = 0; i < pathToRootSourceConcept.size(); i++) {
            int idx;
            if ((idx = pathToRootTargetConcept.indexOf(pathToRootSourceConcept.get(i))) != -1) {
                List<Concept> subList1 = pathToRootSourceConcept.subList(0, i + 1);
                List<Concept> subList2 = pathToRootTargetConcept.subList(0, idx);
                Collections.reverse(subList2);
                artificialShortestPath.addAll(subList1);
                artificialShortestPath.addAll(subList2);
                break;
            }
        }
        return artificialShortestPath;
    }

    @Override
    public ConceptNode getMinHierarchy(Set<Concept> conceptSubSet) {
        ConceptNode topConceptNode = new ConceptNode(getTopConcept());

        Map<String, ConceptNode> nodeMap = new HashMap<String, ConceptNode>();
        nodeMap.put(getTopConcept().getId(), topConceptNode);

        for (Concept concept : conceptSubSet) {

            List<Concept> conceptPathToRoot = getPathToRoot(concept);

            if (!conceptPathToRoot.isEmpty()) {
                Collections.reverse(conceptPathToRoot);

                ConceptNode currentNode = topConceptNode;
                for (int i = 1; i < conceptPathToRoot.size(); i++) {
                    ConceptNode currentChildren = nodeMap.get(conceptPathToRoot.get(i).getId());
                    if (currentChildren == null) {
                        currentChildren = new ConceptNode(conceptPathToRoot.get(i));
                        nodeMap.put(currentChildren.getId(), currentChildren);
                    }
                    currentNode.addChildren(currentChildren);
                    currentNode = currentChildren;
                }
            }

        }
        return topConceptNode;
    }

    @Override
    public ConceptNode getSubHierarchy(Concept topConcept, int limit) {
        ConceptNode topConceptNode = new ConceptNode(topConcept);
        Map<String, ConceptNode> nodeMap = new HashMap<String, ConceptNode>();
        nodeMap.put(topConcept.getId(), topConceptNode);

        getSubHierarchyRec(nodeMap, topConceptNode, limit);

        return topConceptNode;
    }

    private void getSubHierarchyRec(Map<String, ConceptNode> nodeMap, ConceptNode topConceptNode, int limit) {
        if (limit != HierarchyKnowledgeProvider.NO_LIMIT && nodeMap.size() >= limit) {
            return;
        }
        Set<Concept> childrens = getChildrens(topConceptNode.getConcept());
        for (Concept children : childrens) {
            ConceptNode childrenNode = nodeMap.get(children.getId());
            if (childrenNode == null) {
                childrenNode = new ConceptNode(children);
                nodeMap.put(childrenNode.getId(), childrenNode);
            }
            topConceptNode.addChildren(childrenNode);
        }
        for (Concept children : childrens) {
            getSubHierarchyRec(nodeMap, nodeMap.get(children.getId()), limit);
        }
    }

    @Override
    public Set<Concept> getLeaves(Concept concept, int limit) {
        Set<Concept> leafs = new HashSet<Concept>();
        if (isLeaf(concept)) {
            leafs.add(concept);
            return leafs;
        } else {
            getLeafsRec(leafs, concept, limit);
            return leafs;
        }
    }

    private void getLeafsRec(Set<Concept> leafs, Concept currentConcept, int limit) {
        if (limit != HierarchyKnowledgeProvider.NO_LIMIT && leafs.size() >= limit) {
            return;
        }
        Set<Concept> childrens = getChildrens(currentConcept);
        for (Concept concept : childrens) {
            if (isLeaf(concept)) {
                leafs.add(concept);
            } else {
                getLeafsRec(leafs, concept, limit);
            }
        }
    }

    private boolean isLeaf(Concept concept) {
        return directedGraph.inDegreeOf(concept) == 0;
    }

    @Override
    public Set<String> getMainParentsByCategory(String category) {
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
