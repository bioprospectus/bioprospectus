package net.lariverosc.knowledge.mesh;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MeshAttributes {

    /**
     *
     */
    public static final String ANNOTATION = "AN";

    /**
     *
     */
    public static final String ALLOWABLE_TOPICAL_QUALIFIERS = "AQ";

    /**
     *
     */
    public static final String CONSIDER_ALSO = "CX";

    /**
     *
     */
    public static final String DESCRIPTOR_CLASS = "DC";

    /**
     *
     */
    public static final String ENTRY_TERM = "ENTRY";

    /**
     *
     */
    public static final String FORWARD_CROSS_REFERENCE = "FX";

    /**
     *
     */
    public static final String MESH_HEADING = "MH";

    /**
     *
     */
    public static final String MESH_HEADING_THESAURUS_ID = "MH_TH";

    /**
     *
     */
    public static final String MESH_TREE_NUMBER = "MN";

    /**
     *
     */
    public static final String MESH_SCOPE_NOTE = "MS";

    /**
     *
     */
    public static final String PHARMACOLOGICAL_ACTION = "PA";

    /**
     *
     */
    public static final String PUBLIC_MESH_NOTE = "PM";

    /**
     *
     */
    public static final String RECORD_TYPE = "RECTYPE";

    /**
     *
     */
    public static final String RUNNING_HEAD = "RH";

    /**
     *
     */
    public static final String SEMANTIC_TYPE = "ST";

    /**
     *
     */
    public static final String UNIQUE_IDENTIFIER = "UI";

}
