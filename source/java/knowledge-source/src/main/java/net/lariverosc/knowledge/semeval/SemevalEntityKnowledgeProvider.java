package net.lariverosc.knowledge.semeval;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SemevalEntityKnowledgeProvider implements EntityKnowledgeProvider {

    private final Logger log = LoggerFactory.getLogger(SemevalEntityKnowledgeProvider.class);

    private final Map<String, Concept> concepts;

    private final Map<String, Term> terms;

    private final Multimap<String, Term> conceptToTerms;

    /**
     *
     * @param filePath
     * @throws IOException
     */
    public SemevalEntityKnowledgeProvider(String filePath) throws IOException {
        concepts = new HashMap<>();
        terms = new HashMap<>();
        conceptToTerms = HashMultimap.create();

        Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(new FileReader(filePath));
        for (CSVRecord record: records) {
            try {
                String CUI = record.get(0);
                String SAUI = record.get(2);
                Boolean ISPREF = Boolean.parseBoolean(record.get(3));
                String STY = record.get(4);
                String STR = record.get(5);

                Concept concept = new Concept()
                        .withId(CUI)
                        .withCategory(STY)
                        .withDescription(STR)
                        .withPrefTerm(STR);
                concepts.put(CUI, concept);
                if (ISPREF) {
                    concepts.get(CUI).setPrefTerm(STR);
                }

                Term term = new Term()
                        .withConceptId(CUI)
                        .withId(SAUI)
                        .withTerm(STR);
                terms.put(SAUI, term);
                conceptToTerms.put(CUI, term);
            } catch (NumberFormatException nfe) {
                log.error("Error while parsing record {}", record);
            }
        }

    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new HashSet<String>();
        for (Concept concept : concepts.values()) {
            categories.add(concept.getCategory());
        }
        return categories;
    }

    @Override
    public String getTopConceptId() {
        return "NO";
    }

    @Override
    public Map<String, Concept> getAllConcepts() {
        return concepts;
    }

    @Override
    public Set<String> getConceptsByCategory(String category_str){
        // Method not implemented
        Set<String> categoriesMap = new HashSet<>();
        return categoriesMap;
    }
    
    @Override
    public Map<String, Term> getAllTerms() {
        return terms;
    }

    @Override
    public Map<String, Relationship> getAllRelations(Relationship.RelationType type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, Relationship> getAllRelations(String type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Concept getByConceptId(String conceptId) {
        return concepts.get(conceptId);
    }

    @Override
    public Term getTerm(String termId) {
        return terms.get(termId);
    }

    @Override
    public String getPreferedTerm(String conceptId) {
        return concepts.get(conceptId).getPrefTerm();
    }

    @Override
    public Concept getConceptByTermId(String termId) {
        Term term = terms.get(termId);
        return concepts.get(term.getConceptId());
    }

    @Override
    public Set<Term> getTermsByConceptId(String conceptId) {
        return new HashSet<>(conceptToTerms.get(conceptId));
    }

    @Override
    public Relationship getByRelationById(String relationshipId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<Relationship> getRelationBySourceConceptId(String sourceConceptId, String type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<Relationship> getRelationsByTargetConceptId(String targetConceptId, String type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, String> getRelationsTypes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Relationship> getRelationsByConcepts(String sourceConceptId, String targetConceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Relationship> getRelationsBySource(String sourceConceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Relationship> getRelationsByTarget(String targetConceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCountry(String conceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getAllCountries() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
