package net.lariverosc.knowledge.snomed;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.snomed.mapper.SnomedTermRowMapper;
import net.lariverosc.knowledge.snomed.mapper.SnomedRelationshipRowMapper;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.snomed.mapper.SnomedConceptRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SnomedJdbcEntityKnowledgeProvider implements EntityKnowledgeProvider {



    /**
     *
     */
    public enum SnomedRelationshipType {

        /**
         *
         */
        IS_A(Relationship.RelationType.IS_A.getName(), "116680003"),
        /**
         *
         */
        EPISODICITY("Episodicity", "246456000"),

        /**
         *
         */
        CLINICAL_COURSE("Clinical course", "263502005"),

        /**
         *
         */
        SEVERITY("Severity", "246112005"),

        /**
         *
         */
        FINDING_SITE("Finding site", "363698007"),

        /**
         *
         */
        METHOD("Method", "260686004"),

        /**
         *
         */
        PRIORITY("Priority", "260870009"),

        /**
         *
         */
        ASSOCIATED_MORPHOLOGY("Associated morphology", "116676008"),

        /**
         *
         */
        PART_OF("Part of", "123005000"),

        /**
         *
         */
        SAME_AS("SAME AS", "168666000"),

        /**
         *
         */
        PROCEDURE_SITE_DIRECT("Procedure site - Direct", "405813007"),

        /**
         *
         */
        MAYBE_A("MAY BE A", "149016008"),

        /**
         *
         */
        ACCESS("Access", "260507000"),

        /**
         *
         */
        INTERPRETETS("Interprets", "363714003"),

        /**
         *
         */
        WAS_A("WAS A", "159083000"),

        /**
         *
         */
        HAS_ACTIVE_INGREDIENT("Has active ingredient", "127489000"),

        /**
         *
         */
        LATERALITY("Laterality", "272741003");

        private final String relationshipType;

        private final String code;

        private SnomedRelationshipType(String relationshipType, String code) {
            this.relationshipType = relationshipType;
            this.code = code;
        }

        /**
         *
         * @return
         */
        public String getRelationshipType() {
            return relationshipType;
        }

        /**
         *
         * @return
         */
        public String getCode() {
            return code;
        }
    }
    private final Logger log = LoggerFactory.getLogger(SnomedJdbcEntityKnowledgeProvider.class);

    private final JdbcTemplate jdbcTemplate;

    private final EnumMap<Relationship.RelationType, String> relationshipTypeMap;

    public SnomedJdbcEntityKnowledgeProvider(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        relationshipTypeMap = new EnumMap<>(Relationship.RelationType.class);
        relationshipTypeMap.put(Relationship.RelationType.IS_A, SnomedRelationshipType.IS_A.getCode());
    }

    @Override
    public String getTopConceptId() {
        //return "147155";
        //return "138875005"; //original
        //return "246061005"; //Second original
        return "1800000";
    }

    @Override
    public Set<String> getCategories() {
        List<String> array = jdbcTemplate.queryForList("SELECT DISTINCT(category) FROM concepts", String.class);
        return new HashSet<>(array);
    }

    @Override
    public Map<String, Concept> getAllConcepts() {
        log.info("Loading concepts");
        List<Concept> concepts = jdbcTemplate.query("SELECT * FROM concepts", new SnomedConceptRowMapper());
        Map<String, Concept> tempConceptsMap = new HashMap<>();
        for (Concept concept: concepts) {
            tempConceptsMap.put(concept.getId(), concept);
        }
        return tempConceptsMap;
    }

    @Override
    public Set<String> getConceptsByCategory(String category_str){
        List<String> concepts = jdbcTemplate.queryForList("SELECT conceptID FROM concepts where category=? order by fullySpecifiedName", new Object[]{category_str}, String.class);
        return new LinkedHashSet<>(concepts);
    }

    @Override
    public Map<String, Term> getAllTerms() {
        log.info("Loading terms");
        List<Term> terms = jdbcTemplate.query("SELECT * FROM descriptors", new SnomedTermRowMapper());
        Map<String, Term> tempMap = new HashMap<>();
        for (Term term: terms) {
            tempMap.put(term.getId(), term);
        }
        return tempMap;
    }

    @Override
    public Map<String, String> getRelationsTypes() {

        List<Map.Entry<String, String>> resultList = jdbcTemplate.query("SELECT DISTINCT(c.conceptID) as conceptID,c.fullySpecifiedName as fullySpecifiedName  FROM snomeden.concepts c, snomeden.relationships r WHERE r.relationshipType=c.conceptId", new RowMapper<Map.Entry<String, String>>() {

            @Override
            public Map.Entry<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
                return new AbstractMap.SimpleEntry<String, String>(rs.getString("conceptID"), rs.getString("fullySpecifiedName"));
            }
        });
        Map<String, String> map = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : resultList) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    @Override
    public Map<String, Relationship> getAllRelations(Relationship.RelationType type) {
        String typeCode = relationshipTypeMap.get(type);
        return getAllRelations(typeCode);
    }

    @Override
    public Map<String, Relationship> getAllRelations(String typeCode) {
        List<Relationship> relationships = jdbcTemplate.query("SELECT * FROM relationships WHERE relationshipType=?", new Object[]{typeCode}, new SnomedRelationshipRowMapper());
        Map<String, Relationship> relationshipsMap = new HashMap<String, Relationship>();
        for (Relationship relationship: relationships) {
            relationshipsMap.put(relationship.getId(), relationship);
        }
        return relationshipsMap;
    }

    @Override
    public Concept getByConceptId(String conceptId) {
        return jdbcTemplate.queryForObject("SELECT * FROM concepts WHERE conceptID=?", new Object[]{conceptId}, new SnomedConceptRowMapper());
    }

    @Override
    public Term getTerm(String termId) {
        return jdbcTemplate.queryForObject("SELECT * FROM descriptors WHERE descriptorID=?", new Object[]{termId}, new SnomedTermRowMapper());
    }

    @Override
    public Set<String> getAllCountries() {
        List<String> concepts = jdbcTemplate.queryForList("SELECT Distinct(country) FROM concepts order by country", String.class);
        return new LinkedHashSet<>(concepts);
    }

    @Override
    public String getPreferedTerm(String conceptId) {
        //1 = “preferred” description (term)
        //2 = synonym (alternate)
        //3 = fully specified name
        try{
            Term term = jdbcTemplate.queryForObject("SELECT * FROM descriptors WHERE conceptId=? LIMIT 1", new Object[]{conceptId}, new SnomedTermRowMapper());
            return term.getTerm();
        }
        catch(EmptyResultDataAccessException e){
            return conceptId;
        }
    }

    @Override
    public String getCountry(String conceptId){
        Concept concept = jdbcTemplate.queryForObject("SELECT * FROM concepts where conceptId=?", new Object[]{conceptId}, new SnomedConceptRowMapper());
        return concept.getCountry();

    }

    @Override
    public Relationship getByRelationById(String relationshipId) {
        return jdbcTemplate.queryForObject("SELECT * FROM relationships WHERE relationshipID=?", new Object[]{relationshipId}, new SnomedRelationshipRowMapper());
    }

    @Override
    public Set<Term> getTermsByConceptId(String conceptId) {
        return new HashSet<Term>(jdbcTemplate.query("SELECT * FROM descriptors WHERE conceptId=?", new Object[]{conceptId}, new SnomedTermRowMapper()));
    }

    @Override
    public Concept getConceptByTermId(String termId) {
        return jdbcTemplate.queryForObject("SELECT c.* FROM concepts c, descriptors d WHERE c.conceptId=d.conceptId AND d.descriptorId=?", new Object[]{termId}, new SnomedConceptRowMapper());
    }

    @Override
    public Set<Relationship> getRelationBySourceConceptId(String sourceConceptId, String type) {
        return new HashSet<>(jdbcTemplate.query("SELECT * FROM relationships WHERE conceptId1=? AND relationshipType=?", new Object[]{sourceConceptId, type}, new SnomedRelationshipRowMapper()));
    }

    @Override
    public Set<Relationship> getRelationsByTargetConceptId(String targetConceptId, String type) {
        return new HashSet<>(jdbcTemplate.query("SELECT * FROM relationships WHERE conceptId2=? AND relationshipType=?", new Object[]{targetConceptId, type}, new SnomedRelationshipRowMapper()));
    }

    @Override

    public Set<Relationship> getRelationsBySource(String sourceConceptId) {
        return new HashSet<>(jdbcTemplate.query("SELECT * FROM relationships WHERE conceptId1=?", new Object[]{sourceConceptId}, new SnomedRelationshipRowMapper()));
    }

    @Override
    public Set<Relationship> getRelationsByTarget(String targetConceptId) {
        return new HashSet<>(jdbcTemplate.query("SELECT * FROM relationships WHERE conceptId2=?", new Object[]{targetConceptId}, new SnomedRelationshipRowMapper()));
    }

    @Override
    public Set<Relationship> getRelationsByConcepts(String sourceConceptId, String targetConceptId) {
        return new HashSet<>(jdbcTemplate.query("SELECT * FROM relationships WHERE conceptId1=? AND conceptId2=?", new Object[]{sourceConceptId, targetConceptId}, new SnomedRelationshipRowMapper()));
    }
}
