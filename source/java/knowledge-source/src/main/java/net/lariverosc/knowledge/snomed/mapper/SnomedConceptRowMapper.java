package net.lariverosc.knowledge.snomed.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import net.lariverosc.knowledge.domain.Concept;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SnomedConceptRowMapper implements RowMapper<Concept> {

    @Override
    public Concept mapRow(ResultSet rs, int i) throws SQLException {
        Concept concept = new Concept();
        concept.setId(rs.getString("conceptId"));
        concept.setCategory(rs.getString("category"));
        concept.setDescription(rs.getString("fullySpecifiedName"));
        concept.setCountry(rs.getString("country"));
        return concept;
    }
}
