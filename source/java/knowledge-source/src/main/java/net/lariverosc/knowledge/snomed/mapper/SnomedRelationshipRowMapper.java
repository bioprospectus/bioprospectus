package net.lariverosc.knowledge.snomed.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import net.lariverosc.knowledge.domain.Relationship;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SnomedRelationshipRowMapper implements RowMapper<Relationship> {

    

    @Override
    public Relationship mapRow(ResultSet rs, int i) throws SQLException {
        Relationship relationship = new Relationship();
        relationship.setId(rs.getString("relationshipId"));
        relationship.setType(rs.getString("relationshipType"));
        relationship.setSourceId(rs.getString("conceptId1"));
        relationship.setTargetId(rs.getString("conceptId2"));
        return relationship;
    }

}
