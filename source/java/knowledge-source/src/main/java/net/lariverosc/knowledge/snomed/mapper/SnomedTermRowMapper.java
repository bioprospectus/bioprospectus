package net.lariverosc.knowledge.snomed.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import net.lariverosc.knowledge.domain.Term;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SnomedTermRowMapper implements RowMapper<Term> {

    @Override
    public Term mapRow(ResultSet rs, int i) throws SQLException {
        Term term = new Term();
        term.setId(rs.getString("descriptorId"));
        term.setConceptId(rs.getString("conceptId"));
        boolean initialCapitalStatus = rs.getBoolean("initialCapitalStatus");
        String termStr = rs.getString("term");
        if (termStr != null && termStr.length() > 1 && !initialCapitalStatus) {
            termStr = termStr.substring(0, 1).toLowerCase() + termStr.substring(1);
        }
        term.setTerm(termStr);
        return term;
    }
}
