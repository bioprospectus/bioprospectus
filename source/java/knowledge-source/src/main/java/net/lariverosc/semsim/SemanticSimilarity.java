package net.lariverosc.semsim;

import net.lariverosc.icontent.data.InformationContentManager;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;

/**
 *
 * @author Alejandro Riveros Cruz
 */
public abstract class SemanticSimilarity {

    /**
     *
     */
    protected EntityKnowledgeProvider entityKnowledgeProvider;

    /**
     *
     */
    protected HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    /**
     *
     */
    protected InformationContentManager informationContentManager;

    /**
     *
     * @param concept1
     * @param concept2
     *
     * @return
     */
    public abstract double getValue(Concept concept1, Concept concept2);

    /**
     *
     * @return
     */
    public abstract String getName();

    /**
     *
     * @param entityKnowledgeProvider
     */
    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    /**
     *
     * @param hierarchyKnowledgeProvider
     */
    public void setHierarchyKnowledgeProvider(HierarchyKnowledgeProvider hierarchyKnowledgeProvider) {
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
    }

    /**
     *
     * @param informationContentManager
     */
    public void setInformationContentManager(InformationContentManager informationContentManager) {
        this.informationContentManager = informationContentManager;
    }
}
