package net.lariverosc.semsim;

import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SimilarConcept implements Comparable<SimilarConcept> {

    private final Concept concept;

    private final double score;

    /**
     *
     * @param concept
     * @param score
     */
    public SimilarConcept(Concept concept, double score) {
        this.concept = concept;
        this.score = score;
    }

    /**
     *
     * @return
     */
    public Concept getConcept() {
        return concept;
    }

    /**
     *
     * @return
     */
    public double getScore() {
        return score;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.concept != null ? this.concept.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimilarConcept other = (SimilarConcept) obj;
        if (this.concept != other.concept && (this.concept == null || !this.concept.equals(other.concept))) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(SimilarConcept other) {
        return -1 * Double.compare(score, other.getScore());
    }

    @Override
    public String toString() {
        return "SimilarConcept{" + "concept=" + concept + ", score=" + score + '}';
    }
}
