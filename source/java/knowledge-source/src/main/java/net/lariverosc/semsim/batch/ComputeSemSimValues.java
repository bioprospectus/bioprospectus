package net.lariverosc.semsim.batch;

import com.google.common.collect.MinMaxPriorityQueue;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.semsim.SemanticSimilarity;
import net.lariverosc.semsim.SimilarConcept;
import net.lariverosc.semsim.data.SemanticSimilarityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ComputeSemSimValues {

    private final Logger log = LoggerFactory.getLogger(ComputeSemSimValues.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private SemanticSimilarity semanticSimilarity;

    private SemanticSimilarityManager semanticSimilarityManager;

    private volatile AtomicInteger counter = new AtomicInteger(0);

    /**
     *
     * @param topK
     */
    public void computeAllSimilaritiesSingleThread(int topK) {
        Map<String, Concept> allConcepts = entityKnowledgeProvider.getAllConcepts();
        computeSimilarities(new ArrayList<String>(allConcepts.keySet()), topK);
    }

    /**
     *
     * @param topK
     * @param numThreads
     */
    public void computeAllSimilaritiesMultiThread(int topK, int numThreads) {
        Map<String, Concept> allConcepts = entityKnowledgeProvider.getAllConcepts();
        List<String> allConceptIds = new ArrayList<String>(allConcepts.keySet());
        int blockSize = (allConceptIds.size() + numThreads - 1) / numThreads;
        for (int i = 0; i < numThreads; i++) {
            int start = i * blockSize;
            int end = Math.min((i + 1) * blockSize, allConceptIds.size());
            List<String> blockConceptIds = new ArrayList<String>(allConceptIds.subList(start, end));
            try {
                log.info("Launching new thread #{} interval[{},{})", new Object[]{(i + 1), start, end});
                launchThread(blockConceptIds, topK);
            } catch (InterruptedException ie) {
                log.error("Error while launching thread for block: " + (i + 1), ie);
            }
        }
    }

    private void launchThread(final List<String> blockConceptIds, final int topK) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    computeSimilarities(blockConceptIds, topK);
                } catch (Exception e) {
                    log.error("Fatal error while executing!!!", e);
                }
            }
        });
        thread.start();
    }

    private void computeSimilarities(List<String> blockConceptIds, int topK) {
        log.info("Computing semantic similarity values using metric: " + semanticSimilarity.getName());
        Map<String, Concept> allConcepts = entityKnowledgeProvider.getAllConcepts();
        int total = allConcepts.size();
        int localCount = 0;
        for (String concept1Id : blockConceptIds) {
            long start = System.currentTimeMillis();

            Concept concept1 = allConcepts.get(concept1Id);

            MinMaxPriorityQueue<SimilarConcept> minMaxPriorityQueue = MinMaxPriorityQueue.maximumSize(topK).create();
            for (Concept concept2 : allConcepts.values()) {
                double score = semanticSimilarity.getValue(concept1, concept2);
                minMaxPriorityQueue.add(new SimilarConcept(concept2, score));
            }

            semanticSimilarityManager.saveValues(concept1.getId(), minMaxPriorityQueue);
            log.info("global {}/{} local {}/{} compute ss values for concept {} took {}ms", new Object[]{counter.incrementAndGet(), total, (++localCount), blockConceptIds.size(), concept1.getId(), System.currentTimeMillis() - start});
        }
    }

    /**
     *
     * @param entityKnowledgeProvider
     */
    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    /**
     *
     * @param semanticSimilarity
     */
    public void setSemanticSimilarity(SemanticSimilarity semanticSimilarity) {
        this.semanticSimilarity = semanticSimilarity;
    }

    /**
     *
     * @param semanticSimilarityManager
     */
    public void setSemanticSimilarityManager(SemanticSimilarityManager semanticSimilarityManager) {
        this.semanticSimilarityManager = semanticSimilarityManager;
    }
}
