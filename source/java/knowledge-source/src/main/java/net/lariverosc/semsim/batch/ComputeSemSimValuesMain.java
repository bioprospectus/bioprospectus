package net.lariverosc.semsim.batch;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.semsim.SemanticSimilarity;
import net.lariverosc.semsim.data.SemanticSimilarityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ComputeSemSimValuesMain {

    private static final Logger log = LoggerFactory.getLogger(ComputeSemSimValuesMain.class);

    private static ComputeSemSimValues initContext(ComputeSemSimValuesMain.Arguments arguments) {
        log.info("Init spring application context");
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:snomed-context.xml");
        context.registerShutdownHook();
        if (!context.containsBean(arguments.getMetric())) {
            throw new IllegalArgumentException("Unsupported metric: use one of jcn,lch,lin,wp");
        }
        EntityKnowledgeProvider entityKnowledgeProvider = context.getBean("entityKnowledgeProvider", EntityKnowledgeProvider.class);
        SemanticSimilarity semanticSimilarity = context.getBean(arguments.getMetric(), SemanticSimilarity.class);
        SemanticSimilarityManager semanticSimilarityManager = context.getBean("redisSemanticSimilarityManager", SemanticSimilarityManager.class);

        ComputeSemSimValues computeSemSimValues = new ComputeSemSimValues();
        computeSemSimValues.setEntityKnowledgeProvider(entityKnowledgeProvider);
        computeSemSimValues.setSemanticSimilarity(semanticSimilarity);
        computeSemSimValues.setSemanticSimilarityManager(semanticSimilarityManager);
        return computeSemSimValues;
    }

    private static ComputeSemSimValuesMain.Arguments parseArguments(String[] args) {
        ComputeSemSimValuesMain.Arguments arguments = new Arguments();
        JCommander jCommander = new JCommander(arguments, args);
        jCommander.setProgramName("comp-ss");
        if (arguments.isHelp()) {
            jCommander.usage();
            return null;
        }
        return arguments;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        ComputeSemSimValuesMain.Arguments arguments = parseArguments(args);
        if (arguments == null) {
            return;
        }
        try {
            ComputeSemSimValues computeSemSimValues = initContext(arguments);
            int availableProcessors = Runtime.getRuntime().availableProcessors();
            computeSemSimValues.computeAllSimilaritiesMultiThread(arguments.getK(), availableProcessors);
        } catch (Exception e) {
            log.error("Fatal Error!!! ", e);
            throw new RuntimeException(e);
        }
    }

    private static class Arguments {

        @Parameter(names = "--metric", description = "Optinal: the metric name, can be: lin,jcn,lch,wp", required = false)
        private String metric = "lin";

        @Parameter(names = "--neighbors", description = "Optinal: the number of neighbors that will be stored", required = false)
        private Integer k = 50;

        @Parameter(names = "--redis", description = "Use Redis to store semantic similarity values", required = false)
        private Boolean redis = false;

        @Parameter(names = "--redis-host", description = "Optional: Redis host", required = false)
        private String host = "localhost";

        @Parameter(names = "--redis-port", description = "Optional: Redis port", required = false)
        private Integer port = 6379;

        @Parameter(names = "--redis-password", description = "Optional: Redis password", required = false)
        private String password = null;

        @Parameter(names = {"-h", "--help"}, description = "print this message", help = true)
        private Boolean help = false;

        public String getMetric() {
            return metric;
        }

        public Integer getK() {
            return k;
        }

        public Boolean isRedis() {
            return redis;
        }

        public String getHost() {
            return host;
        }

        public Integer getPort() {
            return port;
        }

        public String getPassword() {
            return password;
        }

        public Boolean isHelp() {
            return help;
        }
    }
}
