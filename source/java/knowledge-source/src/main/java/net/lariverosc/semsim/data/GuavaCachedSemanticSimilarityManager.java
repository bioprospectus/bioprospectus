package net.lariverosc.semsim.data;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.semsim.SimilarConcept;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class GuavaCachedSemanticSimilarityManager implements SemanticSimilarityManager {

    private final Logger log = LoggerFactory.getLogger(GuavaCachedSemanticSimilarityManager.class);

    private final SemanticSimilarityManager semanticSimilarityManager;

    private final Cache<ConceptPairKey, Double> conceptPairCache;

    private final Cache<Concept, List<SimilarConcept>> topKCache;

    /**
     *
     * @param semanticSimilarityManager
     */
    public GuavaCachedSemanticSimilarityManager(SemanticSimilarityManager semanticSimilarityManager) {
        this.semanticSimilarityManager = semanticSimilarityManager;

        conceptPairCache = CacheBuilder.newBuilder()
                .expireAfterAccess(100, TimeUnit.SECONDS)
                .maximumSize(100000)
                .build();
        topKCache = CacheBuilder.newBuilder()
                .expireAfterAccess(100, TimeUnit.SECONDS)
                .maximumSize(20000)
                .build();
    }

    /**
     *
     * @param conceptId
     * @param semanticSimilarityValues
     */
    @Override
    public void saveValues(String conceptId,Collection<SimilarConcept> semanticSimilarityValues) {
        semanticSimilarityManager.saveValues(conceptId, semanticSimilarityValues);
    }

    /**
     *
     * @param concept1
     * @param concept2
     * @return
     */
    @Override
    public double getValue(final Concept concept1, final Concept concept2) {
        try {
            return conceptPairCache.get(new ConceptPairKey(concept1, concept2), new Callable<Double>() {
                @Override
                public Double call() throws Exception {
                    return semanticSimilarityManager.getValue(concept1, concept2);
                }
            });
        } catch (ExecutionException ex) {
            log.error("Error accesing conceptPairCache", ex);
            throw new RuntimeException(ex);
        }

    }

    /**
     *
     * @param concept
     * @param k
     * @return
     */
    @Override
    public List<SimilarConcept> getTopKValues(final Concept concept, final int k) {
        try {
            List<SimilarConcept> topKSimilar = topKCache.getIfPresent(concept);
            if (topKSimilar != null) {
                if (topKSimilar.size() >= k) {
                    return topKSimilar.subList(0, k);
                } else {
                    topKCache.invalidate(concept);
                }
            }
            return topKCache.get(concept, new Callable<List<SimilarConcept>>() {
                @Override
                public List<SimilarConcept> call() throws Exception {
                    return semanticSimilarityManager.getTopKValues(concept, k);
                }
            });
        } catch (ExecutionException ex) {
            log.error("Error accesing topKCache", ex);
            throw new RuntimeException(ex);
        }
    }

    private class ConceptPairKey {

        private Concept concept1;

        private Concept concept2;

        public ConceptPairKey(Concept concept1, Concept concept2) {
            this.concept1 = concept1;
            this.concept2 = concept2;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 59 * hash + (this.concept1 != null ? this.concept1.hashCode() : 0);
            hash = 59 * hash + (this.concept2 != null ? this.concept2.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ConceptPairKey other = (ConceptPairKey) obj;
            if (this.concept1 != other.concept1 && (this.concept1 == null || !this.concept1.equals(other.concept1))) {
                return false;
            }
            if (this.concept2 != other.concept2 && (this.concept2 == null || !this.concept2.equals(other.concept2))) {
                return false;
            }
            return true;
        }
    }
}
