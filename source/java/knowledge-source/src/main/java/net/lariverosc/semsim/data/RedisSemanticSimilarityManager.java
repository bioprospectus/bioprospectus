package net.lariverosc.semsim.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.semsim.SimilarConcept;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Tuple;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class RedisSemanticSimilarityManager implements SemanticSimilarityManager {

    private final Logger log = LoggerFactory.getLogger(RedisSemanticSimilarityManager.class);

    /**
     *
     */
    public static final String REDIS_NAME_SPACE = "SS:";

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private JedisPool jedisPool;

    /**
     *
     * @param entityKnowledgeProvider
     * @param jedisPool
     */
    public RedisSemanticSimilarityManager(EntityKnowledgeProvider entityKnowledgeProvider, JedisPool jedisPool) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
        this.jedisPool = jedisPool;
    }

    /**
     *
     * @param conceptId
     * @param semanticSimilarityValues
     */
    @Override
    public void saveValues(String conceptId, Collection<SimilarConcept> semanticSimilarityValues) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            for (Iterator<SimilarConcept> it = semanticSimilarityValues.iterator(); it.hasNext();) {
                SimilarConcept ssValue = it.next();
                jedis.zadd(REDIS_NAME_SPACE + conceptId, ssValue.getScore(), String.valueOf(ssValue.getConcept().getId()));
                //log.info(conceptId + " - " + ssValue.getScore() + " - " + String.valueOf(ssValue.getConcept().getId()));
            }
        } catch (Exception e) {
            log.error("Error while saving values on redis", e);
        } finally {
            releaseJedisConnection(jedis);
        }

    }

    /**
     *
     * @param concept1
     * @param concept2
     * @return
     */
    @Override
    public double getValue(Concept concept1, Concept concept2) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            Double sim = jedis.zscore(REDIS_NAME_SPACE + concept1.getId(), String.valueOf(concept2.getId()));
            if (sim != null) {
                return sim;
            }
            return 0;
        } catch (Exception e) {
            log.error("Error while retriving values from redis", e);
            return 0;
        } finally {
            releaseJedisConnection(jedis);
        }
    }

    /**
     *
     * @param concept
     * @param k
     * @return
     */
    @Override
    public List<SimilarConcept> getTopKValues(Concept concept, int k) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            Set<Tuple> topkConceptSet = jedis.zrevrangeWithScores(REDIS_NAME_SPACE + concept.getId(), 0, k);
            List<SimilarConcept> topKList = new ArrayList<SimilarConcept>();
            for (Iterator<Tuple> it = topkConceptSet.iterator(); it.hasNext();) {
                Tuple tuple = it.next();
                Concept tempConcept = entityKnowledgeProvider.getByConceptId(tuple.getElement());
                topKList.add(new SimilarConcept(tempConcept, tuple.getScore()));
            }
            return Collections.unmodifiableList(topKList);
        } catch (Exception e) {
            log.error("Error while retriving values from redis", e);
            return null;
        } finally {
            releaseJedisConnection(jedis);
        }
    }

    private void releaseJedisConnection(Jedis jedis) {
        if (jedis != null) {
            try {
                jedisPool.returnResource(jedis);
            } catch (Exception e) {
            }
        }
    }
}
