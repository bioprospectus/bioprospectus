package net.lariverosc.semsim.data;

import java.util.Collection;
import java.util.List;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.semsim.SimilarConcept;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface SemanticSimilarityManager {

    /**
     *
     * @param conceptId
     * @param semanticSimilarityValues
     */
    void saveValues(String conceptId, Collection<SimilarConcept> semanticSimilarityValues);

    /**
     *
     * @param concept1
     * @param concept2
     * @return
     */
    double getValue(Concept concept1, Concept concept2);

    /**
     *
     * @param concept
     * @param k
     * @return
     */
    List<SimilarConcept> getTopKValues(Concept concept, int k);
}
