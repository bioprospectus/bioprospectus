package net.lariverosc.semsim.metrics;

import java.util.Arrays;
import java.util.HashSet;
import net.lariverosc.semsim.SemanticSimilarity;
import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * sim (p1,p2)= IC(LCS(p1,p2))/(IC(p1)+IC(p2)-IC(LCS(p1,p2)))
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JCN extends SemanticSimilarity {

    @Override
    public double getValue(Concept concept1, Concept concept2) {
        if (!concept1.getCategory().equalsIgnoreCase(concept2.getCategory())) {
            return 0;
        }
        Concept leastCommonSubsummer = hierarchyKnowledgeProvider.getLeastCommonSubsummer(new HashSet<Concept>(Arrays.asList(concept1, concept2)));
        double distance = informationContentManager.getValue(concept1)
                + informationContentManager.getValue(concept2)
                - 2 * informationContentManager.getValue(leastCommonSubsummer);

        return 1 - distance;
    }

    @Override
    public String getName() {
        return "JCN";
    }
}
