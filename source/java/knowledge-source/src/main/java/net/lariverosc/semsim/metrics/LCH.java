package net.lariverosc.semsim.metrics;

import net.lariverosc.semsim.SemanticSimilarity;
import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * simlch(c1,c2)=-log(p/(2*depth)) Where: p = |shortestPath(c1,c2)| depth = maximum depth of the hierarchy
* @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class LCH extends SemanticSimilarity {

    @Override
    public double getValue(Concept concept1, Concept concept2) {
        if (!concept1.getCategory().equalsIgnoreCase(concept2.getCategory())) {
            return 0;
        }
        //TODO s de maximum depth of the hierarchy not the concept2
        //TODO to work well we must use the category maximun depth not the hiearchy maximun
        double depth = hierarchyKnowledgeProvider.getConceptDepth(concept2);
        double p = hierarchyKnowledgeProvider.getArtificialShortestPath(concept1, concept2).size() + 1;
        return -1 * Math.log(p / (2 * depth));
    }

    @Override
    public String getName() {
        return "LCH";
    }
}
