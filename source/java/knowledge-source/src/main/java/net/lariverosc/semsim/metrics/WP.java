package net.lariverosc.semsim.metrics;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import net.lariverosc.semsim.SemanticSimilarity;
import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * simwp(p1,p2)=2*N3/(N1+N2+2*N3) Where: p3 = leastCommon sub-summer of p1 and p2, N1 = path length from p1 to p3 N2 = path
 * length from p2 to p3 N3 = path length from p3 to root. Verbs semantics and lexical selection by: Zhibiao Wu, Martha Palmer
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class WP extends SemanticSimilarity {

    @Override
    public double getValue(Concept concept1, Concept concept2) {
        if (!concept1.getCategory().equalsIgnoreCase(concept2.getCategory())) {
            return 0;
        }
        Concept p3 = hierarchyKnowledgeProvider.getLeastCommonSubsummer(new HashSet<Concept>(Arrays.asList(concept1, concept2)));
        double N1 = hierarchyKnowledgeProvider.getArtificialShortestPath(concept1, p3).size();
        double N2 = hierarchyKnowledgeProvider.getArtificialShortestPath(concept2, p3).size();
        List<Concept> lcsPathToRoot = hierarchyKnowledgeProvider.getPathToRoot(p3);
        double N3 = lcsPathToRoot.isEmpty() ? 1 : lcsPathToRoot.size() + 1;
        return (2 * N3) / (N1 + N2 + 2 * N3);
    }

    @Override
    public String getName() {
        return "WP";
    }
}
