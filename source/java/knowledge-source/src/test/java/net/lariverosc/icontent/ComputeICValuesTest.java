package net.lariverosc.icontent;

import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.jgrapht.JGraphTHierarchyKnowledgeProvider;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.provider.FakeEntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.Assert;


/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ComputeICValuesTest {

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    /**
     *
     */
    @BeforeSuite
    public void init() {
        entityKnowledgeProvider = new FakeEntityKnowledgeProvider();
        hierarchyKnowledgeProvider = new JGraphTHierarchyKnowledgeProvider(entityKnowledgeProvider);
    }

    /**
     *
     */
    @Test
    public void shouldCountLeaves() {
        ComputeICValues computeICValues = getComputeICValues();
        Map<Concept, Set<Concept>> countLeaves = computeICValues.countLeaves();
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("0")).size(), 8);
        //Category 1
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("1")).size(), 6);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("2")).size(), 4);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("3")).size(), 2);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("4")).size(), 3);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("5")).size(), 0);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("6")).size(), 2);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("7")).size(), 0);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("8")).size(), 0);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("9")).size(), 0);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("10")).size(), 2);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("11")).size(), 2);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("12")).size(), 0);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("13")).size(), 0);

        //Category 2
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("14")).size(), 2);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("15")).size(), 1);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("16")).size(), 2);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("17")).size(), 1);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("18")).size(), 1);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("19")).size(), 0);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("20")).size(), 1);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("21")).size(), 1);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("22")).size(), 1);
        Assert.assertEquals(countLeaves.get(entityKnowledgeProvider.getByConceptId("23")).size(), 0);
    }

    /**
     *
     */
    @Test
    public void shouldCountSubsumers() {
        ComputeICValues computeICValues = getComputeICValues();
        Map<Concept, Set<Concept>> countSubsumers = computeICValues.countSubsumers();
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("0")).size(), 1);
        //Category 1
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("1")).size(), 2);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("2")).size(), 3);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("3")).size(), 3);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("4")).size(), 4);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("5")).size(), 4);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("6")).size(), 4);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("7")).size(), 5);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("8")).size(), 5);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("9")).size(), 5);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("10")).size(), 5);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("11")).size(), 6);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("12")).size(), 7);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("13")).size(), 7);

        //Category 2
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("14")).size(), 2);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("15")).size(), 3);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("16")).size(), 3);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("17")).size(), 4);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("18")).size(), 4);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("19")).size(), 4);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("20")).size(), 5);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("21")).size(), 8);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("22")).size(), 9);
        Assert.assertEquals(countSubsumers.get(entityKnowledgeProvider.getByConceptId("23")).size(), 10);

    }

    /**
     *
     */
    @Test
    public void shouldCountTotalLeaves() {
        ComputeICValues computeICValues = getComputeICValues();
        int totalLeaves = computeICValues.countTotalLeaves();
        Assert.assertEquals(totalLeaves, 8);
    }

    /**
     *
     */
    @Test
    public void shouldComputeICValues() {
        ComputeICValues computeICValues = getComputeICValues();
        Map<Concept, Set<Concept>> countLeaves = computeICValues.countLeaves();
        Map<Concept, Set<Concept>> countSubsumers = computeICValues.countSubsumers();
        int totalLeaves = computeICValues.countTotalLeaves();
        Map<String, Double> icValues = computeICValues.computeICValues(countLeaves, countSubsumers, totalLeaves);

        double delta = 0.001;
        Assert.assertEquals(icValues.get("0"), 0, delta);
        //Category 1
        Assert.assertEquals(icValues.get("1"), 0.369, delta);
        Assert.assertEquals(icValues.get("2"), 0.614, delta);
        Assert.assertEquals(icValues.get("3"), 0.768, delta);
        Assert.assertEquals(icValues.get("4"), 0.745, delta);
        Assert.assertEquals(icValues.get("5"), 1, delta);
        Assert.assertEquals(icValues.get("6"), 0.815, delta);
        Assert.assertEquals(icValues.get("7"), 1, delta);
        Assert.assertEquals(icValues.get("8"), 1, delta);
        Assert.assertEquals(icValues.get("9"), 1, delta);
        Assert.assertEquals(icValues.get("10"), 0.847, delta);
        Assert.assertEquals(icValues.get("11"), 0.869, delta);
        Assert.assertEquals(icValues.get("12"), 1, delta);
        Assert.assertEquals(icValues.get("13"), 1, delta);

        //Category 2
        Assert.assertEquals(icValues.get("14"), 0.685, delta);
        Assert.assertEquals(icValues.get("15"), 0.869, delta);
        Assert.assertEquals(icValues.get("16"), 0.768, delta);
        Assert.assertEquals(icValues.get("17"), 0.898, delta);
        Assert.assertEquals(icValues.get("18"), 0.898, delta);
        Assert.assertEquals(icValues.get("19"), 1, delta);
        Assert.assertEquals(icValues.get("20"), 0.917, delta);
        Assert.assertEquals(icValues.get("21"), 0.946, delta);
        Assert.assertEquals(icValues.get("22"), 0.952, delta);
        Assert.assertEquals(icValues.get("23"), 1, delta);

    }

    private ComputeICValues getComputeICValues() {
        ComputeICValues cicv = new ComputeICValues();
        cicv.setEntityKnowledgeProvider(entityKnowledgeProvider);
        cicv.setHierarchyKnowledgeProvider(hierarchyKnowledgeProvider);
        return cicv;
    }
}
