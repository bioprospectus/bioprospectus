package net.lariverosc.icontent;

import java.util.HashMap;
import java.util.Map;
import net.lariverosc.icontent.data.InformationContentManager;
import net.lariverosc.knowledge.domain.Concept;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class FakeInformationContentProvider implements InformationContentManager {

    Map<String, Double> icValues;

    /**
     *
     */
    public FakeInformationContentProvider() {
        icValues = new HashMap<String, Double>();
        icValues.put("0", 0.0);
        //Category 1
        icValues.put("1", 0.369);
        icValues.put("2", 0.614);
        icValues.put("3", 0.768);
        icValues.put("4", 0.745);
        icValues.put("5", 1.0);
        icValues.put("6", 0.815);
        icValues.put("7", 1.0);
        icValues.put("8", 1.0);
        icValues.put("9", 1.0);
        icValues.put("10", 0.847);
        icValues.put("11", 0.869);
        icValues.put("12", 1.0);
        icValues.put("13", 1.0);
        //Category 2
        icValues.put("14", 0.685);
        icValues.put("15", 0.869);
        icValues.put("16", 0.768);
        icValues.put("17", 0.898);
        icValues.put("18", 0.898);
        icValues.put("19", 1.0);
        icValues.put("20", 0.917);
        icValues.put("21", 0.946);
        icValues.put("22", 0.952);
        icValues.put("23", 1.0);

    }

    @Override
    public void save(Map<String, Double> icValuesMap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getValue(Concept concept) {
        return icValues.get(concept.getId());
    }

    @Override
    public Map<String, Double> getAllValues() {
        return icValues;
    }
}
