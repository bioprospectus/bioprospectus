package net.lariverosc.knowledge.jgrapht;

import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.knowledge.provider.HierarchyKnowledgeProviderBaseTest;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JGraphTHierarchyKnowledgeProviderTest extends HierarchyKnowledgeProviderBaseTest {

    /**
     *
     * @return
     */
    @Override
    protected HierarchyKnowledgeProvider getHierarchyKnowlegeProvider() {
        return new JGraphTHierarchyKnowledgeProvider(entityKnowledgeProvider);
    }
}
