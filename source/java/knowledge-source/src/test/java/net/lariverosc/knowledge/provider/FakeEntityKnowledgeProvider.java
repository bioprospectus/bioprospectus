package net.lariverosc.knowledge.provider;

import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.domain.Relationship;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class FakeEntityKnowledgeProvider implements EntityKnowledgeProvider {

    private Map<String, Concept> concepts = initConcepts();

    private Map<String, Term> terms;

    private Map<String, Relationship> relationships = iniRelationships();

    private static Map<String, Concept> initConcepts() {
        Map<String, Concept> temp = new HashMap<String, Concept>();
        temp.put("0", new Concept().withId("0").withDescription("Root concept").withCategory(""));
        //cat1
        temp.put("1", new Concept().withId("1").withCategory("cat1").withDescription("concept1"));
        temp.put("2", new Concept().withId("2").withCategory("cat1").withDescription("concept2"));
        temp.put("3", new Concept().withId("3").withCategory("cat1").withDescription("concept3"));
        temp.put("4", new Concept().withId("4").withCategory("cat1").withDescription("concept4"));
        temp.put("5", new Concept().withId("5").withCategory("cat1").withDescription("concept5"));
        temp.put("6", new Concept().withId("6").withCategory("cat1").withDescription("concept6"));
        temp.put("7", new Concept().withId("7").withCategory("cat1").withDescription("concept7"));
        temp.put("8", new Concept().withId("8").withCategory("cat1").withDescription("concept8"));
        temp.put("9", new Concept().withId("9").withCategory("cat1").withDescription("concept9"));
        temp.put("10", new Concept().withId("10").withCategory("cat1").withDescription("concept10"));
        temp.put("11", new Concept().withId("11").withCategory("cat1").withDescription("concept11"));
        temp.put("12", new Concept().withId("12").withCategory("cat1").withDescription("concept12"));
        temp.put("13", new Concept().withId("13").withCategory("cat1").withDescription("concept13"));

        //cat2
        temp.put("14", new Concept().withId("14").withCategory("cat2").withDescription("concept14"));
        temp.put("15", new Concept().withId("15").withCategory("cat2").withDescription("concept15"));
        temp.put("16", new Concept().withId("16").withCategory("cat2").withDescription("concept16"));
        temp.put("17", new Concept().withId("17").withCategory("cat2").withDescription("concept17"));
        temp.put("18", new Concept().withId("18").withCategory("cat2").withDescription("concept18"));
        temp.put("19", new Concept().withId("19").withCategory("cat2").withDescription("concept19"));
        temp.put("20", new Concept().withId("20").withCategory("cat2").withDescription("concept20"));
        temp.put("21", new Concept().withId("21").withCategory("cat2").withDescription("concept21"));
        temp.put("22", new Concept().withId("22").withCategory("cat2").withDescription("concept22"));
        temp.put("23", new Concept().withId("23").withCategory("cat2").withDescription("concept23"));
        return temp;
    }

    private static Map<String, Relationship> iniRelationships() {
        Map<String, Relationship> temp = new HashMap<String, Relationship>();
        //cat1
        temp.put("1", new Relationship().withId("1").withSourceId("1").withTargetId("0").whitType("IS_A"));
        temp.put("2", new Relationship().withId("2").withSourceId("2").withTargetId("1").whitType("IS_A"));
        temp.put("3", new Relationship().withId("3").withSourceId("3").withTargetId("1").whitType("IS_A"));
        temp.put("4", new Relationship().withId("4").withSourceId("4").withTargetId("2").whitType("IS_A"));
        temp.put("5", new Relationship().withId("5").withSourceId("5").withTargetId("2").whitType("IS_A"));
        temp.put("6", new Relationship().withId("6").withSourceId("6").withTargetId("3").whitType("IS_A"));
        temp.put("7", new Relationship().withId("7").withSourceId("7").withTargetId("4").whitType("IS_A"));
        temp.put("8", new Relationship().withId("8").withSourceId("8").withTargetId("4").whitType("IS_A"));
        temp.put("9", new Relationship().withId("9").withSourceId("9").withTargetId("4").whitType("IS_A"));
        temp.put("10", new Relationship().withId("10").withSourceId("10").withTargetId("6").whitType("IS_A"));
        temp.put("11", new Relationship().withId("11").withSourceId("11").withTargetId("10").whitType("IS_A"));
        temp.put("12", new Relationship().withId("12").withSourceId("12").withTargetId("11").whitType("IS_A"));
        temp.put("13", new Relationship().withId("13").withSourceId("13").withTargetId("11").whitType("IS_A"));
        //cat2
        temp.put("14", new Relationship().withId("14").withSourceId("14").withTargetId("0").whitType("IS_A"));
        temp.put("15", new Relationship().withId("15").withSourceId("15").withTargetId("14").whitType("IS_A"));
        temp.put("16", new Relationship().withId("16").withSourceId("16").withTargetId("14").whitType("IS_A"));
        temp.put("17", new Relationship().withId("17").withSourceId("17").withTargetId("15").whitType("IS_A"));
        temp.put("18", new Relationship().withId("18").withSourceId("18").withTargetId("16").whitType("IS_A"));
        temp.put("19", new Relationship().withId("19").withSourceId("19").withTargetId("16").whitType("IS_A"));
        temp.put("20", new Relationship().withId("20").withSourceId("20").withTargetId("17").whitType("IS_A"));
        temp.put("21", new Relationship().withId("21").withSourceId("21").withTargetId("20").whitType("IS_A"));
        temp.put("22", new Relationship().withId("22").withSourceId("21").withTargetId("18").whitType("IS_A"));
        temp.put("23", new Relationship().withId("23").withSourceId("22").withTargetId("20").whitType("IS_A"));
        temp.put("24", new Relationship().withId("24").withSourceId("22").withTargetId("21").whitType("IS_A"));
        temp.put("25", new Relationship().withId("25").withSourceId("23").withTargetId("22").whitType("IS_A"));

        return temp;
    }

    @Override
    public String getTopConceptId() {
        return "0";
    }

    @Override
    public Set<String> getCategories() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Map<String, Concept> getAllConcepts() {
        return concepts;
    }

    @Override
    public Set<String> getConceptsByCategory(String category_str){
        // Method not implemented
        Set<String> categoriesMap = new HashSet<>();
        return categoriesMap;
    }
    
    @Override
    public Map<String, Term> getAllTerms() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Relationship> getAllRelations(Relationship.RelationType type) {
        return relationships;
    }

    @Override
    public Map<String, Relationship> getAllRelations(String type) {
        return relationships;
    }

    @Override
    public Concept getByConceptId(String conceptId) {
        return concepts.get(conceptId);
    }

    @Override
    public Term getTerm(String termId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getPreferedTerm(String conceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Relationship getByRelationById(String relationshipId) {
        return relationships.get(relationshipId);
    }

    @Override
    public Set<Term> getTermsByConceptId(String conceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Relationship> getRelationBySourceConceptId(String sourceConceptId, String type) {
        Set<Relationship> temp = new HashSet<Relationship>();
        for (Map.Entry<String, Relationship> entry: relationships.entrySet()) {
            Relationship relationship = entry.getValue();
            if (relationship.getSourceId().equals(sourceConceptId)) {
                temp.add(relationship);
            }
        }
        return temp;
    }

    @Override
    public Set<Relationship> getRelationsByTargetConceptId(String targetConceptId, String type) {
        Set<Relationship> temp = new HashSet<Relationship>();
        for (Map.Entry<String, Relationship> entry: relationships.entrySet()) {
            Relationship relationship = entry.getValue();
            if (relationship.getTargetId().equals(targetConceptId)) {
                temp.add(relationship);
            }
        }
        return temp;
    }

    @Override
    public Concept getConceptByTermId(String termId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, String> getRelationsTypes() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Relationship> getRelationsByConcepts(String sourceConceptId, String targetConceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Relationship> getRelationsBySource(String sourceConceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Relationship> getRelationsByTarget(String targetConceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCountry(String conceptId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getAllCountries() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
