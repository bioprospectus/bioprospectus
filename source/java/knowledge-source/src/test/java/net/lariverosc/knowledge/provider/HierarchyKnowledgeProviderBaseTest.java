package net.lariverosc.knowledge.provider;

import java.util.Arrays;
import java.util.HashSet;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import java.util.List;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.hierarchy.ConceptNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public abstract class HierarchyKnowledgeProviderBaseTest {

    /**
     *
     */
    protected EntityKnowledgeProvider entityKnowledgeProvider;

    /**
     *
     */
    @BeforeClass
    public void init() {
        entityKnowledgeProvider = new FakeEntityKnowledgeProvider();
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchHiearchyDepth() throws Exception {
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        int hierarchyDepth = hierarchyKnowledgeProvider.getHierarchyDepth();
        Assert.assertEquals(hierarchyDepth, 6);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldComputeConceptDepth() throws Exception {
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        int c1Depth = hierarchyKnowledgeProvider.getConceptDepth(c1);
        Assert.assertEquals(c1Depth, 1);

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        int c10Depth = hierarchyKnowledgeProvider.getConceptDepth(c10);
        Assert.assertEquals(c10Depth, 4);

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        int c18Depth = hierarchyKnowledgeProvider.getConceptDepth(c18);
        Assert.assertEquals(c18Depth, 3);


        Concept c22 = entityKnowledgeProvider.getByConceptId("22");
        int c22Depth = hierarchyKnowledgeProvider.getConceptDepth(c22);
        Assert.assertEquals(c22Depth, 5);

        Concept c23 = entityKnowledgeProvider.getByConceptId("23");
        int c23Depth = hierarchyKnowledgeProvider.getConceptDepth(c23);
        Assert.assertEquals(c23Depth, 6);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptInDegree() throws Exception {
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        int c1InDegree = hierarchyKnowledgeProvider.getInDegree(c1);
        Assert.assertEquals(c1InDegree, 2);

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        int c10InDegree = hierarchyKnowledgeProvider.getInDegree(c10);
        Assert.assertEquals(c10InDegree, 1);

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        int c18InDegree = hierarchyKnowledgeProvider.getInDegree(c18);
        Assert.assertEquals(c18InDegree, 1);

        Concept c20 = entityKnowledgeProvider.getByConceptId("20");
        int c20InDegree = hierarchyKnowledgeProvider.getInDegree(c20);
        Assert.assertEquals(c20InDegree, 2);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptOutDegree() throws Exception {
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        int c1OutDegree = hierarchyKnowledgeProvider.getOutDegree(c1);
        Assert.assertEquals(c1OutDegree, 1);

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        int c10OutDegree = hierarchyKnowledgeProvider.getOutDegree(c10);
        Assert.assertEquals(c10OutDegree, 1);

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        int c18OutDegree = hierarchyKnowledgeProvider.getOutDegree(c18);
        Assert.assertEquals(c18OutDegree, 1);

        Concept c22 = entityKnowledgeProvider.getByConceptId("22");
        int c22OutDegree = hierarchyKnowledgeProvider.getOutDegree(c22);
        Assert.assertEquals(c22OutDegree, 2);
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptParents() throws Exception {

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        Concept c0 = entityKnowledgeProvider.getByConceptId("0");
        Set<Concept> c0Parents = hierarchyKnowledgeProvider.getParents(c0);
        Assert.assertEquals(c0Parents.size(), 0);

        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        Set<Concept> c1Parents = hierarchyKnowledgeProvider.getParents(c1);
        Assert.assertEquals(c1Parents.size(), 1);
        Assert.assertTrue(c1Parents.contains(entityKnowledgeProvider.getByConceptId("0")));

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        Set<Concept> c10Parents = hierarchyKnowledgeProvider.getParents(c10);
        Assert.assertEquals(c10Parents.size(), 1);
        Assert.assertTrue(c10Parents.contains(entityKnowledgeProvider.getByConceptId("6")));

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        Set<Concept> c18Parents = hierarchyKnowledgeProvider.getParents(c18);
        Assert.assertEquals(c18Parents.size(), 1);
        Assert.assertTrue(c18Parents.contains(entityKnowledgeProvider.getByConceptId("16")));

        Concept c22 = entityKnowledgeProvider.getByConceptId("22");
        Set<Concept> c22Parents = hierarchyKnowledgeProvider.getParents(c22);
        Assert.assertEquals(c22Parents.size(), 2);
        Assert.assertTrue(c22Parents.contains(entityKnowledgeProvider.getByConceptId("20")));
        Assert.assertTrue(c22Parents.contains(entityKnowledgeProvider.getByConceptId("21")));
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptChildrens() throws Exception {
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        Set<Concept> c1Childrens = hierarchyKnowledgeProvider.getChildrens(c1);
        Assert.assertEquals(c1Childrens.size(), 2);
        Assert.assertTrue(c1Childrens.contains(entityKnowledgeProvider.getByConceptId("2")));
        Assert.assertTrue(c1Childrens.contains(entityKnowledgeProvider.getByConceptId("3")));

        Concept c4 = entityKnowledgeProvider.getByConceptId("4");
        Set<Concept> c4Childrens = hierarchyKnowledgeProvider.getChildrens(c4);
        Assert.assertEquals(c4Childrens.size(), 3);
        Assert.assertTrue(c4Childrens.contains(entityKnowledgeProvider.getByConceptId("7")));
        Assert.assertTrue(c4Childrens.contains(entityKnowledgeProvider.getByConceptId("8")));
        Assert.assertTrue(c4Childrens.contains(entityKnowledgeProvider.getByConceptId("9")));

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        Set<Concept> c10Childrens = hierarchyKnowledgeProvider.getChildrens(c10);
        Assert.assertEquals(c10Childrens.size(), 1);
        Assert.assertTrue(c10Childrens.contains(entityKnowledgeProvider.getByConceptId("11")));

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        Set<Concept> c18Childrens = hierarchyKnowledgeProvider.getChildrens(c18);
        Assert.assertEquals(c18Childrens.size(), 1);
        Assert.assertTrue(c18Childrens.contains(entityKnowledgeProvider.getByConceptId("21")));

        Concept c20 = entityKnowledgeProvider.getByConceptId("20");
        Set<Concept> c20Childrens = hierarchyKnowledgeProvider.getChildrens(c20);
        Assert.assertEquals(c20Childrens.size(), 2);
        Assert.assertTrue(c20Childrens.contains(entityKnowledgeProvider.getByConceptId("21")));
        Assert.assertTrue(c20Childrens.contains(entityKnowledgeProvider.getByConceptId("22")));
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptSiblings() throws Exception {

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        Concept c0 = entityKnowledgeProvider.getByConceptId("0");
        Set<Concept> c0siblings = hierarchyKnowledgeProvider.getSiblings(c0);
        Assert.assertEquals(c0siblings.size(), 0);

        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        Set<Concept> c1Siblings = hierarchyKnowledgeProvider.getSiblings(c1);
        Assert.assertEquals(c1Siblings.size(), 1);
        Assert.assertTrue(c1Siblings.contains(entityKnowledgeProvider.getByConceptId("14")));

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        Set<Concept> c10Siblings = hierarchyKnowledgeProvider.getSiblings(c10);
        Assert.assertEquals(c10Siblings.size(), 0);

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        Set<Concept> c18Siblings = hierarchyKnowledgeProvider.getSiblings(c18);
        Assert.assertEquals(c18Siblings.size(), 1);
        Assert.assertTrue(c18Siblings.contains(entityKnowledgeProvider.getByConceptId("19")));
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptNeighbors() throws Exception {

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        Concept c0 = entityKnowledgeProvider.getByConceptId("0");
        Set<Concept> c0Neighbors = hierarchyKnowledgeProvider.getNeighbors(c0);
        Assert.assertEquals(c0Neighbors.size(), 2);
        Assert.assertTrue(c0Neighbors.contains(entityKnowledgeProvider.getByConceptId("1")));
        Assert.assertTrue(c0Neighbors.contains(entityKnowledgeProvider.getByConceptId("14")));

        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        Set<Concept> c1Neighbors = hierarchyKnowledgeProvider.getNeighbors(c1);
        Assert.assertEquals(c1Neighbors.size(), 3);
        Assert.assertTrue(c1Neighbors.contains(entityKnowledgeProvider.getByConceptId("0")));
        Assert.assertTrue(c1Neighbors.contains(entityKnowledgeProvider.getByConceptId("2")));
        Assert.assertTrue(c1Neighbors.contains(entityKnowledgeProvider.getByConceptId("3")));

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        Set<Concept> c10Neighbors = hierarchyKnowledgeProvider.getNeighbors(c10);
        Assert.assertEquals(c10Neighbors.size(), 2);
        Assert.assertTrue(c10Neighbors.contains(entityKnowledgeProvider.getByConceptId("6")));
        Assert.assertTrue(c10Neighbors.contains(entityKnowledgeProvider.getByConceptId("11")));

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        Set<Concept> c18Neighbors = hierarchyKnowledgeProvider.getNeighbors(c18);
        Assert.assertEquals(c18Neighbors.size(), 2);
        Assert.assertTrue(c18Neighbors.contains(entityKnowledgeProvider.getByConceptId("16")));
        Assert.assertTrue(c18Neighbors.contains(entityKnowledgeProvider.getByConceptId("21")));
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptPathToRoot() throws Exception {

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();

        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        List<Concept> c1PathToRoot = hierarchyKnowledgeProvider.getPathToRoot(c1);
        Assert.assertEquals(c1PathToRoot.size(), 2);
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("1"), c1PathToRoot.get(0));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("0"), c1PathToRoot.get(1));

        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        List<Concept> c10PathToRoot = hierarchyKnowledgeProvider.getPathToRoot(c10);
        Assert.assertEquals(c10PathToRoot.size(), 5);
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("10"), c10PathToRoot.get(0));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("6"), c10PathToRoot.get(1));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("3"), c10PathToRoot.get(2));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("1"), c10PathToRoot.get(3));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("0"), c10PathToRoot.get(4));


        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        List<Concept> c18PathToRoot = hierarchyKnowledgeProvider.getPathToRoot(c18);
        Assert.assertEquals(c18PathToRoot.size(), 4);
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("18"), c18PathToRoot.get(0));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("16"), c18PathToRoot.get(1));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("14"), c18PathToRoot.get(2));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("0"), c18PathToRoot.get(3));


        Concept c23 = entityKnowledgeProvider.getByConceptId("23");
        List<Concept> c23PathToRoot = hierarchyKnowledgeProvider.getPathToRoot(c23);
        Assert.assertEquals(c23PathToRoot.size(), 7);
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("23"), c23PathToRoot.get(0));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("22"), c23PathToRoot.get(1));

        Assert.assertTrue(c23PathToRoot.get(2).equals(entityKnowledgeProvider.getByConceptId("20")) || c23PathToRoot.get(2).equals(entityKnowledgeProvider.getByConceptId("21")));
        Assert.assertTrue(c23PathToRoot.get(3).equals(entityKnowledgeProvider.getByConceptId("17")) || c23PathToRoot.get(3).equals(entityKnowledgeProvider.getByConceptId("18")));
        Assert.assertTrue(c23PathToRoot.get(4).equals(entityKnowledgeProvider.getByConceptId("15")) || c23PathToRoot.get(4).equals(entityKnowledgeProvider.getByConceptId("16")));

        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("14"), c23PathToRoot.get(5));
        Assert.assertEquals(entityKnowledgeProvider.getByConceptId("0"), c23PathToRoot.get(6));

    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptsLCS() throws Exception {

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();

        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        Concept c2 = entityKnowledgeProvider.getByConceptId("2");
        Concept c1_c2_lcs = hierarchyKnowledgeProvider.getLeastCommonSubsummer(new HashSet<Concept>(Arrays.asList(c1, c2)));
        Assert.assertNotNull(c1_c2_lcs);
        Assert.assertEquals(c1_c2_lcs, entityKnowledgeProvider.getByConceptId("1"));

        Concept c6 = entityKnowledgeProvider.getByConceptId("6");
        Concept c20 = entityKnowledgeProvider.getByConceptId("20");
        Concept c6_c20_lcs = hierarchyKnowledgeProvider.getLeastCommonSubsummer(new HashSet<Concept>(Arrays.asList(c6, c20)));
        Assert.assertNotNull(c6_c20_lcs);
        Assert.assertEquals(c6_c20_lcs, hierarchyKnowledgeProvider.getTopConcept());


        Concept c7 = entityKnowledgeProvider.getByConceptId("7");
        Concept c10 = entityKnowledgeProvider.getByConceptId("10");
        Concept c7_c10_lcs = hierarchyKnowledgeProvider.getLeastCommonSubsummer(new HashSet<Concept>(Arrays.asList(c7, c10)));
        Assert.assertNotNull(c7_c10_lcs);
        Assert.assertEquals(c7_c10_lcs, entityKnowledgeProvider.getByConceptId("1"));

        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        Concept c19 = entityKnowledgeProvider.getByConceptId("19");
        Concept c18_c19_lcs = hierarchyKnowledgeProvider.getLeastCommonSubsummer(new HashSet<Concept>(Arrays.asList(c18, c19)));
        Assert.assertNotNull(c18_c19_lcs);
        Assert.assertEquals(c18_c19_lcs, entityKnowledgeProvider.getByConceptId("16"));

    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldMatchConceptsShortesPath() throws Exception {

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();

        Concept c1 = entityKnowledgeProvider.getByConceptId("1");
        Concept c2 = entityKnowledgeProvider.getByConceptId("2");
        List<Concept> c1_c2_shortestPath = hierarchyKnowledgeProvider.getArtificialShortestPath(c1, c2);
        Assert.assertEquals(c1_c2_shortestPath.size(), 2);
        Assert.assertEquals(c1_c2_shortestPath.get(0), entityKnowledgeProvider.getByConceptId("1"));
        Assert.assertEquals(c1_c2_shortestPath.get(1), entityKnowledgeProvider.getByConceptId("2"));


        Concept c5 = entityKnowledgeProvider.getByConceptId("5");
        Concept c7 = entityKnowledgeProvider.getByConceptId("7");
        List<Concept> c5_c7_shortestPath = hierarchyKnowledgeProvider.getArtificialShortestPath(c5, c7);
        Assert.assertEquals(c5_c7_shortestPath.size(), 4);
        Assert.assertEquals(c5_c7_shortestPath.get(0), entityKnowledgeProvider.getByConceptId("5"));
        Assert.assertEquals(c5_c7_shortestPath.get(1), entityKnowledgeProvider.getByConceptId("2"));
        Assert.assertEquals(c5_c7_shortestPath.get(2), entityKnowledgeProvider.getByConceptId("4"));
        Assert.assertEquals(c5_c7_shortestPath.get(3), entityKnowledgeProvider.getByConceptId("7"));


        Concept c16 = entityKnowledgeProvider.getByConceptId("16");
        Concept c20 = entityKnowledgeProvider.getByConceptId("20");
        List<Concept> c16_c20_shortestPath = hierarchyKnowledgeProvider.getArtificialShortestPath(c16, c20);
        Assert.assertEquals(c16_c20_shortestPath.size(), 5);
        Assert.assertEquals(c16_c20_shortestPath.get(0), entityKnowledgeProvider.getByConceptId("16"));
        Assert.assertEquals(c16_c20_shortestPath.get(1), entityKnowledgeProvider.getByConceptId("14"));
        Assert.assertEquals(c16_c20_shortestPath.get(2), entityKnowledgeProvider.getByConceptId("15"));
        Assert.assertEquals(c16_c20_shortestPath.get(3), entityKnowledgeProvider.getByConceptId("17"));
        Assert.assertEquals(c16_c20_shortestPath.get(4), entityKnowledgeProvider.getByConceptId("20"));


        Concept c1b = entityKnowledgeProvider.getByConceptId("1");
        Concept c14 = entityKnowledgeProvider.getByConceptId("14");
        List<Concept> c1_c14_shortestPath = hierarchyKnowledgeProvider.getArtificialShortestPath(c1b, c14);
        Assert.assertNull(c1_c14_shortestPath);

    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void shouldBuildSubHierarchy() throws Exception {
        Concept c3 = entityKnowledgeProvider.getByConceptId("3");
        Concept c18 = entityKnowledgeProvider.getByConceptId("18");
        Concept c19 = entityKnowledgeProvider.getByConceptId("19");
        Set<Concept> conceptSet = new HashSet<Concept>();
        conceptSet.add(c3);
        conceptSet.add(c18);
        conceptSet.add(c19);

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = getHierarchyKnowlegeProvider();
        ConceptNode subHierarchyRoot = hierarchyKnowledgeProvider.getMinHierarchy(conceptSet);

        Assert.assertEquals(subHierarchyRoot, new ConceptNode(hierarchyKnowledgeProvider.getTopConcept()));
        Assert.assertEquals(subHierarchyRoot.getChildren().size(), 2);
        Assert.assertNotNull(subHierarchyRoot.getChildrenById("1"));
        Assert.assertNotNull(subHierarchyRoot.getChildrenById("14"));

        ConceptNode conceptNode1 = subHierarchyRoot.getChildrenById("1");
        Assert.assertEquals(conceptNode1.getChildren().size(), 1);
        Assert.assertNotNull(conceptNode1.getChildrenById("3"));

        ConceptNode conceptNode3 = conceptNode1.getChildrenById("3");
        Assert.assertEquals(conceptNode3.getChildren().size(), 0);


        ConceptNode conceptNode14 = subHierarchyRoot.getChildrenById("14");
        Assert.assertEquals(conceptNode14.getChildren().size(), 1);
        Assert.assertNotNull(conceptNode14.getChildrenById("16"));

        ConceptNode conceptNode16 = conceptNode14.getChildrenById("16");
        Assert.assertEquals(conceptNode16.getChildren().size(), 2);
        Assert.assertNotNull(conceptNode16.getChildrenById("18"));
        Assert.assertNotNull(conceptNode16.getChildrenById("19"));
    }

    /**
     *
     * @return
     */
    protected abstract HierarchyKnowledgeProvider getHierarchyKnowlegeProvider();
}
