package net.lariverosc.semsim.metrics;

import java.util.Map;
import net.lariverosc.icontent.FakeInformationContentProvider;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.jgrapht.JGraphTHierarchyKnowledgeProvider;
import net.lariverosc.knowledge.provider.FakeEntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.semsim.SemanticSimilarity;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SemanticSimilarityTest {

    /**
     *
     */
    @Test
    public void mustComputeJCN() {
        showSimilarityValues(new Lin());
        showSimilarityValues(new LCH());
        showSimilarityValues(new JCN());
        showSimilarityValues(new WP());
    }

    /**
     *
     */
    public void shouldBeWithinZeroAndOne() {
    }

    /**
     *
     */
    public void shouldBeSimetric() {
    }

    /**
     *
     */
    public void shouldRespectTriangularInequality() {
    }

    private void showSimilarityValues(SemanticSimilarity semanticSimilarity) {
        FakeEntityKnowledgeProvider fakeEntityKnowledgeProvider = new FakeEntityKnowledgeProvider();
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = new JGraphTHierarchyKnowledgeProvider(fakeEntityKnowledgeProvider);
        FakeInformationContentProvider fakeInformationContentProvider = new FakeInformationContentProvider();


        semanticSimilarity.setEntityKnowledgeProvider(fakeEntityKnowledgeProvider);
        semanticSimilarity.setHierarchyKnowledgeProvider(hierarchyKnowledgeProvider);
        semanticSimilarity.setInformationContentManager(fakeInformationContentProvider);
        Map<String, Concept> allConcepts = fakeEntityKnowledgeProvider.getAllConcepts();
        for (Map.Entry<String, Concept> entry : allConcepts.entrySet()) {
            Concept concept1 = entry.getValue();
            for (Map.Entry<String, Concept> innerEntry : allConcepts.entrySet()) {
                Concept concept2 = innerEntry.getValue();
                System.out.println(semanticSimilarity.getName() + "  c1: " + concept1.getId() + " c2: " + concept2.getId() + " -> " + semanticSimilarity.getValue(concept1, concept2));

            }

        }
    }
}
