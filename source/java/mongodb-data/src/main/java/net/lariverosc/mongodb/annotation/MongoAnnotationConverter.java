package net.lariverosc.mongodb.annotation;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.AnnotationConverter;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MongoAnnotationConverter implements AnnotationConverter<BasicDBObject> {

    @Override
    public Annotation toAnnotation(BasicDBObject basicDBObject) {
        String id = (String) basicDBObject.get("id");
        BasicDBList spans = (BasicDBList) basicDBObject.get("spans");
        Annotation annotation = new Annotation(id);
        annotation.addAttribute("conceptId", id);
        for (Object span: spans) {
            String multispanStr = (String) span;
            String[] multispanSplited = multispanStr.split(",");
            for (String spanStr: multispanSplited) {
                String[] spanStrSplited = spanStr.split("-");
                annotation.addTextSpan(new TextSpan(Integer.parseInt(spanStrSplited[0]), Integer.parseInt(spanStrSplited[1])));
            }

        }
        return annotation;
    }

}
