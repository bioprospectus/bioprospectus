package net.lariverosc.mongodb.annotation;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import java.util.HashSet;
import java.util.Set;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.AnnotationsProvider;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MongoAnnotationsProvider extends AnnotationsProvider {

    private final Logger log = LoggerFactory.getLogger(MongoAnnotationsProvider.class);

    private DBCollection dbCollection;

    @Override
    public Set<Annotation> getAnnotations(String docId) {
        Set<Annotation> annotations = new HashSet<>();
        BasicDBObject query = new BasicDBObject("_id", new ObjectId(docId));
        DBObject dbObject = dbCollection.findOne(query);
        BasicDBObject mappings = (BasicDBObject) dbObject.get("mappings");
        if (mappings != null) {
            BasicDBObject mappingsByField = (BasicDBObject) mappings.get("mappings_by_field");
            for (String fieldName : mappingsByField.keySet()) {
                BasicDBList fieldMappings = (BasicDBList) mappingsByField.get(fieldName);
                for (Object mapping : fieldMappings) {
                    Annotation annotation = annotationConverter.toAnnotation((BasicDBObject) mapping);
                    annotation.addAttribute("field", fieldName);
                    annotations.add(annotation);
                }
            }
        }
        return annotations;
    }

    public void setDbCollection(DBCollection dbCollection) {
        this.dbCollection = dbCollection;
    }

}
