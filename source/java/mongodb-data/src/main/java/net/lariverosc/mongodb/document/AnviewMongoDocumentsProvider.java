package net.lariverosc.mongodb.document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AnviewMongoDocumentsProvider extends MongoDocumentsProvider {

    @Override
    public List<String> getAllDocumentIds() {
        BasicDBObject query = new BasicDBObject("mappings", new BasicDBObject("$exists", true));
        BasicDBObject fields = new BasicDBObject("_id", 1);
        DBCursor dbCursor = dbCollection.find(query, fields);
        List<String> documentIds = new ArrayList<>();
        while (dbCursor.hasNext()) {
            documentIds.add(dbCursor.next().get("_id").toString());
        }
        return documentIds;
    }

}
