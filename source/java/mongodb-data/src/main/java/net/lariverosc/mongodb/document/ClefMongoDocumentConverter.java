package net.lariverosc.mongodb.document;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import java.util.Iterator;
import net.lariverosc.text.document.Document;
import net.lariverosc.text.document.DocumentConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ClefMongoDocumentConverter implements DocumentConverter<DBObject> {

    private final Logger log = LoggerFactory.getLogger(ClefMongoDocumentConverter.class);

    
    @Override
    public Document toDocument(DBObject inDbObject) {
        Document document = new Document();
        document.setId(inDbObject.get("_id").toString());
        if (inDbObject.containsField("article")) {
            DBObject dbArticle = (DBObject) inDbObject.get("article");
            //Add document attributes
            document.addDocAttribute("pmid", (String) dbArticle.get("@pmid"));
            document.addDocAttribute("doi", (String) dbArticle.get("@doi"));
            document.addDocAttribute("pmc_article_url", (String) dbArticle.get("@pmc_article_url"));
            Object authorsObject = dbArticle.get("authors");
            if (authorsObject != null) {
                Object authorObject = ((DBObject) authorsObject).get("author");
                if (authorObject != null) {
                    if (authorObject instanceof BasicDBList) {
                        BasicDBList authorsList = (BasicDBList) authorObject;
                        for (int i = 0; i < authorsList.size(); i++) {
                            Object authorItem = authorsList.get(i);
                            if (authorItem != null) {
                                document.addDocAttribute("author" + (i + 1), authorItem.toString());
                            }
                        }
                    } else {
                        document.addDocAttribute("author", authorObject.toString());
                    }
                }
            }

            //Add document fields
            mapFieldToDocument("title", dbArticle, document);
            mapFieldToDocument("abstract", dbArticle, document);
            mapFieldToDocument("fulltext", dbArticle, document);

            if (dbArticle.containsField("figures")) {
                DBObject figures = (DBObject) dbArticle.get("figures");
                if (figures != null) {
                    if (figures.containsField("figure")) {
                        if (figures.get("figure") instanceof BasicDBList) {
                            BasicDBList figuresList = (BasicDBList) figures.get("figure");
                            for (Iterator<Object> it = figuresList.iterator(); it.hasNext();) {
                                DBObject figure = (DBObject) it.next();
                                mapFigure(figure, document);
                            }
                        } else {
                            DBObject figure = (DBObject) figures.get("figure");
                            mapFigure(figure, document);
                        }
                    }
                }
            }
        }
        return document;
    }

    private void mapFigure(DBObject dBObject, Document document) {
        String iri = (String) dBObject.get("@iri");
        mapFieldToDocument(dBObject, "caption", document, iri + "-caption");
    }

    private void mapFieldToDocument(String sharedFieldName, DBObject inDbObject, Document document) {
        mapFieldToDocument(inDbObject, sharedFieldName, document, sharedFieldName);
    }

    private void mapFieldToDocument(DBObject inDbObject, String dbFieldName, Document document, String documentFieldName) {
        if (inDbObject.containsField(dbFieldName)) {
            document.addField(documentFieldName, (String) inDbObject.get(dbFieldName));
        }
    }

}
