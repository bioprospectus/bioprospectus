/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.lariverosc.mongodb.document;

import com.mongodb.DBObject;
import net.lariverosc.text.document.Document;
import net.lariverosc.text.document.DocumentConverter;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MongoDocumentConverter implements DocumentConverter<DBObject> {

    @Override
    public Document toDocument(DBObject dbObject) {
        Document document = new Document();
        document.setId((String) dbObject.get("_id"));
        for (String fieldName : dbObject.keySet()) {
            document.addField(fieldName, dbObject.get(fieldName).toString());
        }
        return document;
    }
}
