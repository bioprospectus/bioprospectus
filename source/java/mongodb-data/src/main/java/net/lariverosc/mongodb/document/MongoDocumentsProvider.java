package net.lariverosc.mongodb.document;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.List;
import net.lariverosc.text.document.Document;
import net.lariverosc.text.document.DocumentsProvider;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MongoDocumentsProvider extends DocumentsProvider {

    private final Logger log = LoggerFactory.getLogger(MongoDocumentsProvider.class);

    protected DBCollection dbCollection;


    public MongoDocumentsProvider() {
        this.documentConverter = new MongoDocumentConverter();
    }

    @Override
    public List<String> getAllDocumentIds() {
        BasicDBObject query = new BasicDBObject();
        BasicDBObject fields = new BasicDBObject("_id", 1);
        DBCursor dbCursor = dbCollection.find(query, fields);
        List<String> documentIds = new ArrayList<>();
        while (dbCursor.hasNext()) {
            documentIds.add(dbCursor.next().get("_id").toString());
        }
        return documentIds;
    }

    @Override
    public Document getDocument(String docId) {
        BasicDBObject query = new BasicDBObject("_id", new ObjectId(docId));
        DBObject dbObject = dbCollection.findOne(query);
        return documentConverter.toDocument(dbObject);
    }

    public void setDbCollection(DBCollection dbCollection) {
        this.dbCollection = dbCollection;
    }

}
