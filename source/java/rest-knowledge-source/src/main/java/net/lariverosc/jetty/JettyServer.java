package net.lariverosc.jetty;

import com.google.common.base.Joiner;
import net.lariverosc.knowledge.filter.CorsFilter;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.servlet.DispatcherServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JettyServer {

    private final Logger log = LoggerFactory.getLogger(JettyServer.class);

    private final int port;

    private Server server;

    public JettyServer(int port) {
        this.port = port;
    }

    public void start() {
        try {
            server = new Server(port);

            HandlerList handlers = new HandlerList();
            handlers.setHandlers(new Handler[]{getServletHandler(), getDefaultHandler()});
            server.setHandler(handlers);

            server.start();
        } catch (Exception e) {
            log.error("Failed to start server", e);
            throw new RuntimeException(e);
        }
    }

    public void stop() throws Exception {
        server.stop();
    }

    public void join() throws InterruptedException {
        server.join();
    }

    private DefaultHandler getDefaultHandler() {
        DefaultHandler defaultHandler = new DefaultHandler();
        defaultHandler.setServeIcon(true);
        return defaultHandler;
    }

    private ServletContextHandler getServletHandler() {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        String[] contextLocations = new String[]{
            "classpath*:snomed-context.xml",
            "classpath*:kbir-context.xml",
            "classpath:web-context.xml"};
        dispatcherServlet.setContextConfigLocation(Joiner.on(",").join(contextLocations));

        ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        servletContextHandler.setContextPath("/");
        servletContextHandler.setClassLoader(Thread.currentThread().getContextClassLoader());
        servletContextHandler.addFilter(CorsFilter.class, "/*", null);
        servletContextHandler.addServlet(new ServletHolder(dispatcherServlet), "/knowledge/*");
        return servletContextHandler;
    }
}
