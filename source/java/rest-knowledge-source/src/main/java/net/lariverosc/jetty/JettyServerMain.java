package net.lariverosc.jetty;

import net.lariverosc.util.EnvironmentUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JettyServerMain {

    private static final Logger log = LoggerFactory.getLogger(JettyServerMain.class);

    public static void main(String args[]) throws Exception {
        try {
            EnvironmentUtils.exportEnv("MONGO_URL", "mongodb://localhost:27017/clef.articles_raw");
            final JettyServer jettyServer = new JettyServer(8184);
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        log.info("Stopping server");
                        jettyServer.stop();
                    } catch (Exception ex) {
                        log.error("Error while stop Jetty server", ex);
                    }
                }
            });
            jettyServer.start();
            log.info("Jetty server started");
            jettyServer.join();
        } catch (Throwable t) {
            log.error("!!!FATAL ERROR...", t);
        }
    }
}
