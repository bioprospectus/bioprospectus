package net.lariverosc.knowledge;

import java.util.HashMap;
import java.util.Map;
import net.lariverosc.icontent.data.InformationContentManager;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.semsim.data.SemanticSimilarityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class KnowledgeProviderManager implements ApplicationContextAware {

    private final Logger log = LoggerFactory.getLogger(KnowledgeProviderManager.class);

    private ApplicationContext applicationContext;

    private final Map<String, EntityKnowledgeProvider> entityKnowledgeProvidersMap = new HashMap<>();

    private final Map<String, HierarchyKnowledgeProvider> hierarchyKnowledgeProvidersMap = new HashMap<>();

    private final Map<String, InformationContentManager> informationContentManagerMap = new HashMap<>();

    private final Map<String, SemanticSimilarityManager> semanticSimilarityManagerMap = new HashMap<>();

    public void init() {
        initSnomed();
//        initSemeval();
    }

    private void initSnomed() {
        log.info("initializing snomed providers");
        EntityKnowledgeProvider entityKnowledgeProvider = applicationContext.getBean("cachedSnomedEntityKnowledgeProvider", EntityKnowledgeProvider.class);
        entityKnowledgeProvidersMap.put("snomeden", entityKnowledgeProvider);

        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = applicationContext.getBean("snomedHierarchyKnowledgeProvider", HierarchyKnowledgeProvider.class);
        hierarchyKnowledgeProvidersMap.put("snomeden", hierarchyKnowledgeProvider);

    }

//    private void initSemeval() {
//        log.info("initializing semeval providers");
//        EntityKnowledgeProvider entityKnowledgeProvider = applicationContext.getBean("semevalEntityKnowledgeProvider", EntityKnowledgeProvider.class);
//        entityKnowledgeProvidersMap.put("semeval", entityKnowledgeProvider);
//
//    }

    public EntityKnowledgeProvider getEntityKnowledgeProvider(String name) {
        return entityKnowledgeProvidersMap.get(name);
    }

    public HierarchyKnowledgeProvider getHierarchyKnowledgeProvider(String name) {
        return hierarchyKnowledgeProvidersMap.get(name);
    }

    public InformationContentManager getInformationContentManager(String name) {
        return informationContentManagerMap.get(name);
    }

    public SemanticSimilarityManager getSemanticSimilarityManager(String name) {
        return semanticSimilarityManagerMap.get(name);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
