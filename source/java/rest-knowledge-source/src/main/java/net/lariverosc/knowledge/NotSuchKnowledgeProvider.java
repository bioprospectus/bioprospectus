package net.lariverosc.knowledge;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class NotSuchKnowledgeProvider extends RuntimeException {

    public NotSuchKnowledgeProvider() {
    }

    public NotSuchKnowledgeProvider(String message) {
        super(message);
    }

    public NotSuchKnowledgeProvider(String message, Throwable cause) {
        super(message, cause);
    }

}
