package net.lariverosc.knowledge.rest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import net.lariverosc.knowledge.KnowledgeProviderManager;
import net.lariverosc.knowledge.NotSuchKnowledgeProvider;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
@Controller
public class Concepts {

    private final Logger log = LoggerFactory.getLogger(Concepts.class);

    private KnowledgeProviderManager knowledgeProviderManager;

    @RequestMapping(value = "/{source}/concepts/categories", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<String> getCategories(@PathVariable String source) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        if (entityKnowledgeProvider == null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        return entityKnowledgeProvider.getCategories();
    }

    @RequestMapping(value = "/{source}/concepts/countries", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<String> getCountries(@PathVariable String source){
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        if (entityKnowledgeProvider == null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        return entityKnowledgeProvider.getAllCountries();

    }

    @RequestMapping(value = "/{source}/concepts/{conceptIdStr}/full/{country}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getFullHierarchyByConcept(@PathVariable String source, @PathVariable String conceptIdStr, @PathVariable String country){
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Map<String, String> map = new HashMap<>();
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptIdStr);
        Set<Concept> childrens = hierarchyKnowledgeProvider.getChildrens(concept);

        String test = getPath(concept, childrens, country, entityKnowledgeProvider, hierarchyKnowledgeProvider);

        test = test.replace("},]","}]");
        test = test.substring(0, test.length()-1);
        return test;

    }

    public String getPath(Concept concept, Set<Concept> children, String country, EntityKnowledgeProvider entity, HierarchyKnowledgeProvider hierarchy ){
        if (children.size() > 0){
            String currentString = getChildrenString(concept, children, country, entity, hierarchy);
            return currentString;
        }
        return getEmptyString(concept, country);

    }

    public String getChildrenString(Concept concept, Set<Concept> children, String country, EntityKnowledgeProvider entity, HierarchyKnowledgeProvider hierarchy ){
        ArrayList<String> returnedStrings = new ArrayList<>();

        for (Concept node : children){
            String nodeString = getPath(node, hierarchy.getChildrens(node), country, entity, hierarchy);
            returnedStrings.add(nodeString);
        }
        Collections.sort(returnedStrings);
        StringBuilder fullList = new StringBuilder();
        for (String s: returnedStrings){
            fullList.append(s+"");
        }
        if (fullList.toString().equals(""))
            return "";
        return "{\"id\":\""+ concept.getId() + "\","
                    + "\"title\":\"" + concept.getDescription() + "\","
                    + "\"nodes\": [" + fullList.toString() + "]},";

    }

    public String getEmptyString(Concept concept, String country){

        if (country.equals(concept.getCountry()) || country.equals("Region")){
            return "{\"id\":\""+ concept.getId() + "\","
                    + "\"title\":\"" + concept.getDescription() + "\","
                    + "\"nodes\": []},";
        }
        return "";
    }



    @RequestMapping(value = "/{source}/categories/{category_str}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Concept> getConceptsByCategory(@PathVariable String source, @PathVariable String category_str) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        if (entityKnowledgeProvider == null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        Set<String> conceptIds = entityKnowledgeProvider.getConceptsByCategory(category_str);
        List<Concept> concepts = new ArrayList<>();
        for (String conceptId: conceptIds){
            try {
                Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
                if (concept != null) {
                    String preferedTerm = entityKnowledgeProvider.getPreferedTerm(conceptId);
                    concept.setPrefTerm(preferedTerm);
                    String country = entityKnowledgeProvider.getCountry(conceptId);
                    concept.setCountry(country);
                    concepts.add(concept);
                }
            } catch (NumberFormatException nfe) {
                log.error("NumberFormatException for ", conceptId);
            }
        }
        return concepts;
    }

    /*
    This method receives a string with one or more concept id separated by commas.

    It returns a list of concepts, one concept for each id in the input parameters.
    */
    @RequestMapping(value = "/{source}/concepts/{conceptIdStr}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Concept> getConceptByConceptId(@PathVariable String source, @PathVariable String conceptIdStr) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        if (entityKnowledgeProvider == null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        String[] conceptIds = conceptIdStr.split(",");
        List<Concept> concepts = new ArrayList<>();
        for (String conceptId : conceptIds) {
            try {
                Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
                if (concept != null) {
                    String preferedTerm = entityKnowledgeProvider.getPreferedTerm(conceptId);
                    concept.setPrefTerm(preferedTerm);
                    concepts.add(concept);
                }
            } catch (NumberFormatException nfe) {
                log.error("NumberFormatException for ", conceptId);
            }
        }
        return concepts;
    }

    @RequestMapping(value = "/{source}/concepts/{conceptId}/terms", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    Set<Term> getTermsByConceptId(@PathVariable String source, @PathVariable String conceptId) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        if (entityKnowledgeProvider == null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        return entityKnowledgeProvider.getTermsByConceptId(conceptId);
    }





    @RequestMapping(value = "/{source}/concepts/{conceptId}/children", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    Set<Concept> getChildrenByConceptId(@PathVariable String source, @PathVariable String conceptId) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        if (entityKnowledgeProvider == null && hierarchyKnowledgeProvider != null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        Set<Concept> children = hierarchyKnowledgeProvider.getChildrens(concept);
        for (Concept childConcept: children) {
            String preferedTerm = entityKnowledgeProvider.getPreferedTerm(childConcept.getId());
            childConcept.setPrefTerm(preferedTerm);
        }
        return children;
    }





    @RequestMapping(value = "/{source}/concepts/{conceptIdStr}/prefTerm", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    Map<String, String> getPrefTermByConceptId(@PathVariable String source, @PathVariable String conceptIdStr) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        if (entityKnowledgeProvider == null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        Map<String, String> map = new HashMap<String, String>();
        String[] conceptIds = conceptIdStr.split(",");
        for (String conceptId: conceptIds) {
            try {
                String preferedTerm = entityKnowledgeProvider.getPreferedTerm(conceptId);
                if (preferedTerm != null) {
                    map.put(conceptId, preferedTerm);
                } else {
                    log.warn("prefered term for conceptId {} not found", conceptId);
                }
            } catch (NumberFormatException nfe) {
                log.error("error parsing conceptId {}", conceptId);
            }
        }
        return map;
    }

    @RequestMapping(value = "/{source}/concepts/{conceptId}/summary", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getConceptSummary(@PathVariable String source, @PathVariable String conceptId) {

        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        if (entityKnowledgeProvider == null) {
            throw new NotSuchKnowledgeProvider("knowledgeProviders for " + source + " not found");
        }
        try {
            Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
            if (concept != null) {
                Gson gson = new Gson();
                JsonObject jsonObject = new JsonObject();
                jsonObject.add("concept", gson.toJsonTree(concept));

                Set<Term> terms = entityKnowledgeProvider.getTermsByConceptId(conceptId);
                if (terms != null && !terms.isEmpty()) {
                    jsonObject.add("terms", gson.toJsonTree(terms));
                }

                if (hierarchyKnowledgeProvider != null) {
                    List<Concept> pathToRoot = hierarchyKnowledgeProvider.getPathToRoot(concept);
                    Set<Concept> childrens = hierarchyKnowledgeProvider.getChildrens(concept);

                    if (pathToRoot != null && !pathToRoot.isEmpty()) {
                        jsonObject.add("pathToRoot", gson.toJsonTree(pathToRoot));
                    }
                    if (childrens != null && !childrens.isEmpty()) {
                        jsonObject.add("childrens", gson.toJsonTree(childrens));
                    }
                }

                return jsonObject.toString();
            } else {
                throw new IllegalArgumentException("not found");
            }
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException("bad conceptId", nfe);

        }
    }

    @Autowired
    public void setKnowledgeProviderManager(KnowledgeProviderManager knowledgeProviderManager) {
        this.knowledgeProviderManager = knowledgeProviderManager;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public void handleIllegalArgumentException(IllegalArgumentException ex, HttpServletResponse response) throws IOException {
        log.error("Error on request", ex);
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(NotSuchKnowledgeProvider.class)
    public void handleNotSuchKnowledgeProvider(NotSuchKnowledgeProvider ex, HttpServletResponse response) throws IOException {
        log.warn("Warning on request", ex);
        response.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
    }

}
