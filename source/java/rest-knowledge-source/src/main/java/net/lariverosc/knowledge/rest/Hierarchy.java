package net.lariverosc.knowledge.rest;

import java.io.IOException;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import net.lariverosc.knowledge.KnowledgeProviderManager;
import net.lariverosc.knowledge.KnowledgeSourceException;
import net.lariverosc.knowledge.NotSuchKnowledgeProvider;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.hierarchy.ConceptNode;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
@Controller
public class Hierarchy {

    private final Logger log = LoggerFactory.getLogger(Hierarchy.class);

    private KnowledgeProviderManager knowledgeProviderManager;

    @RequestMapping(value = "/{source}/hierarchy/parents/{category}", method=RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<String> getMainParents(@PathVariable String source, @PathVariable String category) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        return hierarchyKnowledgeProvider.getMainParentsByCategory(category);
    }
    
    @RequestMapping(value = "/{source}/hierarchy/{conceptId}/parents", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<Concept> getParents(@PathVariable String source, @PathVariable String conceptId) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        if (concept != null) {
            return hierarchyKnowledgeProvider.getParents(concept);
        } else {
            throw new IllegalArgumentException("Must be a valid conceptId");
        }
    }
    
    @RequestMapping(value = "/{source}/hierarchy/full/{conceptId}/parents", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Concept> getFullParents(@PathVariable String source, @PathVariable String conceptId){
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        ArrayList<Concept> hierarchy = new ArrayList<>();
        Concept parent;
        do{
            Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
            hierarchy.add(concept);
            parent = hierarchyKnowledgeProvider.getParents(concept).iterator().next();
            conceptId = parent.getId();
        }
        while(!parent.getId().equals("1800000"));
        Collections.reverse(hierarchy);
        return hierarchy;
    }

    @RequestMapping(value = "/{source}/hierarchy/{conceptId}/childrens", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<Concept> getChildrens(@PathVariable String source, @PathVariable String conceptId) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        if (concept != null) {
            return hierarchyKnowledgeProvider.getChildrens(concept);
        } else {
            throw new IllegalArgumentException("Must be a valid conceptId");
        }
    }

    @RequestMapping(value = "/{source}/hierarchy/{conceptId}/siblings", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<Concept> getSiblings(@PathVariable String source, @PathVariable String conceptId) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        if (concept != null) {
            return hierarchyKnowledgeProvider.getSiblings(concept);
        } else {
            throw new IllegalArgumentException("Must be a valid conceptId");
        }
    }

    @RequestMapping(value = "/{source}/hierarchy/{conceptId}/neighbors", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<Concept> getNeighbors(@PathVariable String source, @PathVariable String conceptId) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        if (concept != null) {
            return hierarchyKnowledgeProvider.getNeighbors(concept);
        } else {
            throw new IllegalArgumentException("Must be a valid conceptId");
        }
    }

    @RequestMapping(value = "/{source}/hierarchy/{conceptId}/leaves", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Set<Concept> getLeafs(@PathVariable String source, @PathVariable String conceptId, @RequestParam(value = "limit", required = false) Integer limit) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        if (concept == null) {
            throw new IllegalArgumentException("Must contain almost one valid conceptId");
        }
        if (limit == null) {
            return hierarchyKnowledgeProvider.getLeaves(concept, HierarchyKnowledgeProvider.NO_LIMIT);
        } else {
            return hierarchyKnowledgeProvider.getLeaves(concept, limit);
        }
    }

    @RequestMapping(value = "/{source}/hierarchy/{conceptId1}/path/{conceptId2}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Concept> getPath(@PathVariable String source, @PathVariable String conceptId1, @PathVariable String conceptId2) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept1 = entityKnowledgeProvider.getByConceptId(conceptId1);
        Concept concept2 = entityKnowledgeProvider.getByConceptId(conceptId2);
        if (concept1 != null && concept2 != null) {
            return hierarchyKnowledgeProvider.getArtificialShortestPath(concept1, concept2);
        } else {
            throw new IllegalArgumentException("Must be a valid conceptIds");
        }
    }

    @RequestMapping(value = "/{source}/hierarchy/{conceptId}/path/root", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Concept> getPathToRoot(@PathVariable String source, @PathVariable String conceptId) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        return hierarchyKnowledgeProvider.getPathToRoot(concept);
    }

    @RequestMapping(value = "/{source}/hierarchy/lcs/{conceptIdStr}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Concept getLeastCommonSubsummer(@PathVariable String source, @PathVariable String conceptIdStr) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        String[] conceptIds = conceptIdStr.split(",");
        Set<Concept> conceptsSet = new HashSet<Concept>();
        for (String conceptId: conceptIds) {
            Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
            if (concept != null) {
                conceptsSet.add(concept);
            }
        }
        return hierarchyKnowledgeProvider.getLeastCommonSubsummer(conceptsSet);
    }

    @RequestMapping(value = "/{source}/hierarchy/minh/{conceptListStr}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ConceptNode getMinHierarchyGet(@PathVariable String source, @PathVariable String conceptListStr) {
        return getMinHierarchy(source, conceptListStr);
    }

    @RequestMapping(value = "/{source}/hierarchy/minh/", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    ConceptNode getMinHierarchyPost(@PathVariable("source") String source, @RequestBody String conceptListStr) {
        return getMinHierarchy(source, conceptListStr);
    }

    private ConceptNode getMinHierarchy(String source, String conceptListStr) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Set<Concept> conceptSubSet = new HashSet<>();
        String[] conceptList = conceptListStr.split(",");
        for (String conceptId: conceptList) {
            try {
                Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
                if (concept != null) {
                    conceptSubSet.add(concept);
                }
            } catch (NumberFormatException nfe) {
                log.error("NumberFormatException for ", conceptId);
            }
        }
        if (!conceptSubSet.isEmpty()) {
            return hierarchyKnowledgeProvider.getMinHierarchy(conceptSubSet);
        } else {
            throw new IllegalArgumentException("Must contain almost one valid conceptId");
        }
    }

    @RequestMapping(value = "/{source}/hierarchy/subh/{conceptId}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ConceptNode getSubHierarchy(@PathVariable String source, @PathVariable String conceptId, @RequestParam(value = "limit", required = false) Integer limit) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        if (concept == null) {
            throw new IllegalArgumentException("Must contain almost one valid conceptId");
        }
        if (limit == null) {
            return hierarchyKnowledgeProvider.getSubHierarchy(concept, HierarchyKnowledgeProvider.NO_LIMIT);
        } else {
            return hierarchyKnowledgeProvider.getSubHierarchy(concept, limit);
        }
    }

    @Autowired
    public void setKnowledgeProviderManager(KnowledgeProviderManager knowledgeProviderManager) {
        this.knowledgeProviderManager = knowledgeProviderManager;
    }

    @ExceptionHandler(KnowledgeSourceException.class)
    public void handleKnowledgeSourceException(KnowledgeSourceException ex, HttpServletResponse response) throws IOException {
        log.error("Error on request", ex);
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public void handleIllegalArgumentException(IllegalArgumentException ex, HttpServletResponse response) throws IOException {
        log.error("Error on request", ex);
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(NotSuchKnowledgeProvider.class)
    public void handleIllegalArgumentException(NotSuchKnowledgeProvider ex, HttpServletResponse response) throws IOException {
        log.warn("Warning on request", ex);
        response.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
    }

}
