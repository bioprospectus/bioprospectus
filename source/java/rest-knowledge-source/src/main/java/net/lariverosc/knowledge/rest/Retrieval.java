package net.lariverosc.knowledge.rest;

import com.google.gson.Gson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.servlet.http.HttpServletResponse;
import net.lariverosc.kbir.KnowledegeBasedRetrieval;
import net.lariverosc.knowledge.NotSuchKnowledgeProvider;
import net.lariverosc.util.JSON;
import net.lariverosc.util.Maps;
import org.apache.commons.lang3.RandomStringUtils;
import org.elasticsearch.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
@Controller
public class Retrieval {

    private final Logger log = LoggerFactory.getLogger(Retrieval.class);

    private final ExecutorService executor = Executors.newFixedThreadPool(5);

    private KnowledegeBasedRetrieval knowledegeBasedRetrieval;

    private JedisPool jedisPool;

    /**
     * This method receives its parameters within the body in JSON format.The expected format is as follows:
     * {
     *    query : {
     *       keywords : [
     *         "keyword1"
     *       ],
     *       concepts : [
     *         "conceptId1"
     *       ]
     *    }
     * }
     *
     * @param source
     * @param jsonBodyStr
     * @return
     * @throws java.io.IOException
     */
    @RequestMapping(value = "/{source}/kbretrieval", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    List<Map.Entry<String, Double>> getRanking(@PathVariable String source, @RequestBody String jsonBodyStr) throws IOException {
        JSON json = new JSON(jsonBodyStr);

        List<String> keywords = json.getAsStringList("query.keywords");
        List<String> concepts = json.getAsStringList("query.concepts");
        if (concepts.isEmpty()) {
            if (!keywords.isEmpty()) {
                String allKeywords = Joiner.on(" ").join(keywords);
                Map<String, Double> sortByValues = Maps.sortByValues(knowledegeBasedRetrieval.retrieve(allKeywords), true);
                return new ArrayList<>(sortByValues.entrySet());
            }
        } else {
            Map<String, Double> conceptsQuery = new HashMap<>();
            for (String conceptId : concepts) {
                conceptsQuery.put(conceptId, 1d);
            }

            Map<String, Double> sortByValues = Maps.sortByValues(knowledegeBasedRetrieval.retrieve(conceptsQuery), true);
            return new ArrayList<>(sortByValues.entrySet());
        }

        return Collections.EMPTY_LIST;
    }

    @RequestMapping(value = "/{source}/kbretrievalasync", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    String getRankingAsync(@PathVariable String source, @RequestBody String jsonBodyStr) throws IOException {
        JSON json = new JSON(jsonBodyStr);
        final String key = generateKey();
        executor.execute(new AsyncKBRetrieval(key, json));
        return new Gson().toJson(key);
    }

    private String generateKey() {
        StringBuilder sb = new StringBuilder();
        sb.append("KB:RES:").append(RandomStringUtils.randomNumeric(10));
        return sb.toString();
    }

    @Autowired
    public void setKnowledegeBasedRetrieval(KnowledegeBasedRetrieval knowledegeBasedRetrieval) {
        this.knowledegeBasedRetrieval = knowledegeBasedRetrieval;
    }

    @Autowired
    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    @ExceptionHandler(NotSuchKnowledgeProvider.class)
    public void handleNotSuchKnowledgeProvider(NotSuchKnowledgeProvider ex, HttpServletResponse response) throws IOException {
        log.warn("Warning on request", ex);
        response.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
    }

    private class AsyncKBRetrieval implements Runnable {

        private final String key;

        private final JSON jsonQuery;

        private final Gson gson = new Gson();

        public AsyncKBRetrieval(String key, JSON jsonQuery) {
            this.key = key;
            this.jsonQuery = jsonQuery;
        }

        @Override
        public void run() {

            List<String> keywords = jsonQuery.getAsStringList("query.keywords");
            List<String> concepts = jsonQuery.getAsStringList("query.concepts");

            if (concepts.isEmpty()) {
                if (!keywords.isEmpty()) {
                    String allKeywords = Joiner.on(" ").join(keywords);
                    Map<String, Double> sortByValues = Maps.sortByValues(knowledegeBasedRetrieval.retrieve(allKeywords), true);
                    List<String> ranking = new ArrayList<>();
                    for (Map.Entry<String, Double> documentEntry : sortByValues.entrySet()) {
                        ranking.add(documentEntry.getKey());
                    }
                    String jsonRanking = gson.toJson(ranking);
                    saveResult(key, jsonRanking);
                }
            } else {
                Map<String, Double> conceptsQuery = new HashMap<>();
                for (String conceptId : concepts) {
                    conceptsQuery.put(conceptId, 1d);
                }

                Map<String, Double> sortByValues = Maps.sortByValues(knowledegeBasedRetrieval.retrieve(conceptsQuery), true);
                List<String> ranking = new ArrayList<>();
                for (Map.Entry<String, Double> documentEntry : sortByValues.entrySet()) {
                    ranking.add(documentEntry.getKey());
                }
                String jsonRanking = gson.toJson(ranking);
                saveResult(key, jsonRanking);
            }
        }

        private void saveResult(String key, String jsonRanking) {
            Jedis jedis = jedisPool.getResource();
            try {
                jedis.setex(key, 600, jsonRanking);
            } finally {
                jedisPool.returnResource(jedis);
            }
        }
    }

}
