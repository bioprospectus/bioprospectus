package net.lariverosc.knowledge.rest;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.lariverosc.icontent.data.InformationContentManager;
import net.lariverosc.knowledge.KnowledgeProviderManager;
import net.lariverosc.knowledge.NotSuchKnowledgeProvider;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.semsim.SemanticSimilarity;
import net.lariverosc.kbir.similarity.SetSemanticSimilarity;
import net.lariverosc.semsim.SimilarConcept;
import net.lariverosc.semsim.data.SemanticSimilarityManager;
import net.lariverosc.kbir.similarity.GeneralizedCosine;
import net.lariverosc.semsim.metrics.Lin;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
@Controller
public class Similarity {

    private final int DEFAULT_TOPK = 10;

    private final int MAXIMUN_TOPK = 50;

    private final Logger log = LoggerFactory.getLogger(Similarity.class);

    private KnowledgeProviderManager knowledgeProviderManager;

    @RequestMapping(value = "/{source}/icontent/{conceptIdStr}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Map<String, Double> getInfomartionContent(@PathVariable String source, @PathVariable String conceptIdStr) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        InformationContentManager informationContentManager = knowledgeProviderManager.getInformationContentManager(source);
        String[] conceptIds = conceptIdStr.split(",");
        Map<String, Double> icValues = new HashMap<>();
        for (String conceptId : conceptIds) {
            Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
            if (concept != null) {
                icValues.put(concept.getId(), informationContentManager.getValue(concept));
            }
        }
        return icValues;
    }

    @RequestMapping(value = "/{source}/semsim/{conceptId1}/{conceptId2}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    double getSemanticSimilarty(@PathVariable String source, @PathVariable String conceptId1, @PathVariable String conceptId2) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        Concept concept1 = entityKnowledgeProvider.getByConceptId(conceptId1);
        Concept concept2 = entityKnowledgeProvider.getByConceptId(conceptId2);
        if (!concept1.getCategory().equalsIgnoreCase(concept2.getCategory())) {
            return 0;
        }
        return buildSemanticSimilarity(source).getValue(concept1, concept2);
    }

    @RequestMapping(value = "/{source}/semsim/{conceptId}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<SimilarConcept> getTopSimilar(@PathVariable String source, @PathVariable String conceptId, @RequestParam(value = "topK", required = false) Integer topK) throws IOException {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        SemanticSimilarityManager semanticSimilarityManager = knowledgeProviderManager.getSemanticSimilarityManager(source);
        Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
        if (concept == null) {
            throw new IllegalArgumentException("Must contain almost one valid conceptId");
        }

        if (topK != null) {
            if (topK > MAXIMUN_TOPK) {
                log.warn("The maximum value for top is: {}", MAXIMUN_TOPK);
                throw new IllegalArgumentException("The maximum value for topK is:" + MAXIMUN_TOPK);
            }
            return semanticSimilarityManager.getTopKValues(concept, topK);
        } else {
            return semanticSimilarityManager.getTopKValues(concept, DEFAULT_TOPK);
        }
    }

    @RequestMapping(value = "/{source}/semsim/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody
    List<Map.Entry<String, Double>> getRanking(@PathVariable String source, @RequestBody String jsonBody) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonMainNode = mapper.readValue(jsonBody, JsonNode.class);

        JsonNode jsonQueryNode = jsonMainNode.get("query");
        Map<Concept, Double> queryConceptMap = getConceptMap(source, jsonQueryNode);

        List<Map.Entry<String, Double>> resultList = new ArrayList<>();
        JsonNode jsonSetsNode = jsonMainNode.get("sets");

        for (Iterator<Map.Entry<String, JsonNode>> it = jsonSetsNode.getFields(); it.hasNext();) {
            Map.Entry<String, JsonNode> setsEntryNode = it.next();
            Map<Concept, Double> setConceptMap = getConceptMap(source, setsEntryNode.getValue());
            double value = buildSetSemanticSimilarity(source).getValue(queryConceptMap, setConceptMap);
            resultList.add(new AbstractMap.SimpleImmutableEntry<>(setsEntryNode.getKey(), value));
        }

        Collections.sort(resultList, new EntryComparator());

        return resultList;
    }

    private Map<Concept, Double> getConceptMap(String source, JsonNode jsonNode) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        Map<Concept, Double> tempMap = new HashMap<>();
        for (Iterator<Map.Entry<String, JsonNode>> it = jsonNode.getFields(); it.hasNext();) {
            Map.Entry<String, JsonNode> jsonEntry = it.next();
            try {
                String conceptId = jsonEntry.getKey();
                Double weigth = Double.parseDouble(jsonEntry.getValue().asText());
                Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
                if (concept != null) {
                    tempMap.put(concept, weigth);
                }
            } catch (NumberFormatException nfe) {
                log.error("NumberFormatException for ", jsonEntry.getKey());
            }
        }
        return tempMap;
    }

    private class EntryComparator implements Comparator<Map.Entry<String, Double>> {

        @Override
        public int compare(Map.Entry<String, Double> entry1, Map.Entry<String, Double> entry2) {
            if (entry1.getValue() < entry2.getValue()) {
                return -1;
            } else if (entry1.getValue().equals(entry2.getValue())) {
                return 0;
            } else {
                return 1;
            }
        }
    }

    private SemanticSimilarity buildSemanticSimilarity(String source) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        HierarchyKnowledgeProvider hierarchyKnowledgeProvider = knowledgeProviderManager.getHierarchyKnowledgeProvider(source);
        InformationContentManager informationContentManager = knowledgeProviderManager.getInformationContentManager(source);

        SemanticSimilarity similarity = new Lin();
        similarity.setEntityKnowledgeProvider(entityKnowledgeProvider);
        similarity.setHierarchyKnowledgeProvider(hierarchyKnowledgeProvider);
        similarity.setInformationContentManager(informationContentManager);
        return similarity;
    }

    private SetSemanticSimilarity buildSetSemanticSimilarity(String source) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        SemanticSimilarityManager semanticSimilarityManager = knowledgeProviderManager.getSemanticSimilarityManager(source);

        SetSemanticSimilarity setSemanticSimilarity = new GeneralizedCosine();
        setSemanticSimilarity.setEntityKnowledgeProvider(entityKnowledgeProvider);
        setSemanticSimilarity.setSemanticSimilarityManager(semanticSimilarityManager);
        return setSemanticSimilarity;
    }

    @Autowired
    public void setKnowledgeProviderManager(KnowledgeProviderManager knowledgeProviderManager) {
        this.knowledgeProviderManager = knowledgeProviderManager;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public void handleIllegalArgumentException(IllegalArgumentException ex, HttpServletResponse response) throws IOException {
        log.error("Error on request", ex);
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(NotSuchKnowledgeProvider.class)
    public void handleNotSuchKnowledgeProvider(NotSuchKnowledgeProvider ex, HttpServletResponse response) throws IOException {
        log.warn("Warning on request", ex);
        response.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
    }
}
