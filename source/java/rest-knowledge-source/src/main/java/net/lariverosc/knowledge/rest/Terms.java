package net.lariverosc.knowledge.rest;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import net.lariverosc.knowledge.KnowledgeProviderManager;
import net.lariverosc.knowledge.NotSuchKnowledgeProvider;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
@Controller
public class Terms {

    private final Logger log = LoggerFactory.getLogger(Terms.class);

    private KnowledgeProviderManager knowledgeProviderManager;

    @RequestMapping(value = "/{source}/terms/{termIdStr}", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody
    Set<Term> getTerms(@PathVariable String source, @PathVariable String termIdStr) {
        EntityKnowledgeProvider entityKnowledgeProvider = knowledgeProviderManager.getEntityKnowledgeProvider(source);
        String[] termIds = termIdStr.split(",");
        Set<Term> terms = new HashSet<>();
        for (String termId: termIds) {
            Term term = entityKnowledgeProvider.getTerm(termId);
            if (term != null) {
                terms.add(term);
            }
        }
        return terms;
    }

    @Autowired
    public void setKnowledgeProviderManager(KnowledgeProviderManager knowledgeProviderManager) {
        this.knowledgeProviderManager = knowledgeProviderManager;
    }

    @ExceptionHandler(NotSuchKnowledgeProvider.class)
    public void handleIllegalArgumentException(NotSuchKnowledgeProvider ex, HttpServletResponse response) throws IOException {
        log.warn("Warning on request", ex);
        response.sendError(HttpServletResponse.SC_NOT_FOUND, ex.getMessage());
    }

}
