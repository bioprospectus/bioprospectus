package net.lariverosc.mapperrest.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
@Path("/helloworld")
public class TermMapper {

    @POST
    @Produces("application/json")
    public String mapText() {
        return "hello";
    }

    @POST
    @Produces("application/json")
    public String mapSentence() {
        return "hello";
    }

    @GET
    @Produces("application/json")
    public String getMappings() {
        return "hello";
    }
}
