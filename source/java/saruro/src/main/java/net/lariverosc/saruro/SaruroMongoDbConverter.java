package net.lariverosc.saruro;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.annotation.Annotation;
import net.lariverosc.annotation.AnnotationConverter;
import net.lariverosc.document.Document;
import net.lariverosc.document.DocumentConverter;
import net.lariverosc.mapping.document.DocumentConverter;
import net.lariverosc.mapping.vo.MatchGroup;
import net.lariverosc.mapping.vo.SentenceMapping;
import net.lariverosc.text.annotation.AnnotationConverter;
import org.dom4j.Document;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SaruroMongoDbConverter implements AnnotationConverter<DBObject>, DocumentConverter<DBObject>{

    @Override
    public DBObject fromAnnotation(Annotation annotation) throws Exception {
        DBObject byFieldsdbObject = new BasicDBObject();

        String docId = annotation.getDocId();
        Map<String, List<SentenceMapping>> annotationsMap = annotation.getMap();

        Set<String> allConcepts = new HashSet<String>();
        for (Map.Entry<String, List<SentenceMapping>> entry : annotationsMap.entrySet()) {
            Set<String> fieldConcepts = new HashSet<String>();
            String fieldName = entry.getKey();
            List<SentenceMapping> mappings = entry.getValue();
            for (SentenceMapping sentenceMapping : mappings) {
                List<MatchGroup> matchList = sentenceMapping.getMatchList();
                for (MatchGroup matchGroup : matchList) {
                    String stringTermId = String.valueOf(matchGroup.getTermId());
                    allConcepts.add(stringTermId);
                    fieldConcepts.add(stringTermId);
                }
            }
            if (!fieldConcepts.isEmpty()) {
                BasicDBList fieldbasicDBList = new BasicDBList();
                fieldbasicDBList.addAll(fieldConcepts);
                byFieldsdbObject.put(fieldName, fieldbasicDBList);
            }
        }

        DBObject dbObject = new BasicDBObject();
        dbObject.put("id", docId);
        if (!byFieldsdbObject.keySet().isEmpty()) {
            dbObject.put("concepts_by_field", byFieldsdbObject);
        }
        if (!allConcepts.isEmpty()) {
            BasicDBList conceptsAllBasicDBList = new BasicDBList();
            conceptsAllBasicDBList.addAll(allConcepts);
            dbObject.put("concepts_all", conceptsAllBasicDBList);
        }
        return dbObject;
    }

    @Override
    public Document toDocument(DBObject inObject) {
        String id = (String) inObject.get("id");

        Document document = new Document(id);

        mapFieldToDocument("cups", inObject, document);
        mapFieldToDocument("titulo", inObject, document);
        mapFieldToDocument("texto", inObject, document);

        if (inObject.containsField("características_dermatológicas")) {
            DBObject caracteristicasDermatologicas = (DBObject) inObject.get("características_dermatológicas");
            mapFieldToDocument(caracteristicasDermatologicas, "localizacion", document, "caracteristicas_dermatologicas-localizacion");
            mapFieldToDocument(caracteristicasDermatologicas, "periferia", document, "características_dermatologicas-periferia");
            mapFieldToDocument(caracteristicasDermatologicas, "sintomas", document, "características_dermatologicas-sintomas");
            mapFieldToDocument(caracteristicasDermatologicas, "tipo_de_lesion", document, "características_dermatologicas-tipo_de_lesion");
        }


        if (inObject.containsField("examen_físico")) {
            DBObject examenFisico = (DBObject) inObject.get("examen_físico");
            mapFieldToDocument(examenFisico, "abdomen", document, "examen_fisico-abdomen");
            mapFieldToDocument(examenFisico, "cabeza", document, "examen_fisico-cabeza");
            mapFieldToDocument(examenFisico, "cuello", document, "examen_fisico-cuello");
            mapFieldToDocument(examenFisico, "extremidades", document, "examen_fisico-extremidades");
            mapFieldToDocument(examenFisico, "paresias", document, "examen_fisico-paresias");
            mapFieldToDocument(examenFisico, "piel_y_faneras", document, "examen_fisico-piel_y_faneras");
            mapFieldToDocument(examenFisico, "sistema_cardiovascular", document, "examen_fisico-sistema_cardiovascular");
            mapFieldToDocument(examenFisico, "sistema_respiratorio", document, "examen_fisico-sistema_respiratorio");
            mapFieldToDocument(examenFisico, "tracto_vaginal", document, "examen_fisico-tracto_vaginal");
        }

        if (inObject.containsField("observaciones")) {
            DBObject observaciones = (DBObject) inObject.get("observaciones");
            mapFieldToDocument(observaciones, "descripción", document, "observaciones-descripcion");
            mapFieldToDocument(observaciones, "examen_fisico", document, "observaciones-examen_fisico");
            mapFieldToDocument(observaciones, "examen_neurologico", document, "observaciones-examen_neurologico");
            mapFieldToDocument(observaciones, "localizacion", document, "observaciones-localizacion");
            mapFieldToDocument(observaciones, "otros", document, "observaciones-otros");
            mapFieldToDocument(observaciones, "periferia", document, "observaciones-periferia");
            mapFieldToDocument(observaciones, "sintomas", document, "observaciones-sintomas");
            mapFieldToDocument(observaciones, "tipo_de_lesion", document, "observaciones-tipo_de_lesion");
            mapFieldToDocument(observaciones, "texto", document, "observaciones-texto");
        }

        if (inObject.containsField("paraclinicos")) {
            DBObject paraclinicos = (DBObject) inObject.get("paraclinicos");
            mapFieldToDocument(paraclinicos, "citologia", document, "paraclinicos-citologia");
            mapFieldToDocument(paraclinicos, "ecografia", document, "paraclinicos-ecografia");
            mapFieldToDocument(paraclinicos, "electrocardiograma", document, "paraclinicos-electrocardiograma");
            mapFieldToDocument(paraclinicos, "otros", document, "paraclinicos-otros");
            mapFieldToDocument(paraclinicos, "piel_y_faneras", document, "paraclinicos-piel_y_faneras");
            mapFieldToDocument(paraclinicos, "radiografias", document, "paraclinicos-radiografias");
        }


        if (inObject.containsField("respuestas")) {
            DBObject respuestas = (DBObject) inObject.get("respuestas");

            mapFieldToDocument(respuestas, "cie-1", document, "respuestas-cie_1");
            mapFieldToDocument(respuestas, "cie-2", document, "respuestas-cie_2");
            mapFieldToDocument(respuestas, "cie-3", document, "respuestas-cie_3");
            mapFieldToDocument(respuestas, "cie-4", document, "respuestas-cie_4");
            mapFieldToDocument(respuestas, "cie-5", document, "respuestas-cie_5");
            mapFieldToDocument(respuestas, "cie-6", document, "respuestas-cie_6");
            mapFieldToDocument(respuestas, "conclusion", document, "respuestas-conclusion");
            mapFieldToDocument(respuestas, "entrada-1", document, "respuestas-entrada_1");
            mapFieldToDocument(respuestas, "entrada-2", document, "respuestas-entrada_2");
            mapFieldToDocument(respuestas, "texto", document, "respuestas-texto");
            mapFieldToDocument(respuestas, "titulo", document, "respuestas-titulo");
            mapFieldToDocument(respuestas, "valor", document, "respuestas-valor");

        }

        return document;

    }

    private void mapFieldToDocument(String sharedFieldName, DBObject inObject, Document document) {
        mapFieldToDocument(inObject, sharedFieldName, document, sharedFieldName);
    }

    private void mapFieldToDocument(DBObject inObject, String objectFieldName, Document document, String documentFieldName) {
        if (inObject.containsField(objectFieldName)) {
            document.addField(documentFieldName, (String) inObject.get(objectFieldName));
        }

    }
}
