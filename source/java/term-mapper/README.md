#Term-mapper

###Quick Intro

Term-Mapper is an information extraction tool developed in Java, build in the top of many open source projects like Spring, Lucene  and openNLP within others.
Term-Mapper implements a dictionary lookup strategy based on the Metamap algorithm with the advantage that can works in language other than English and with any any knowledge source(i.e. not only UMLS).

The method implemented by Term-Mapper works as follows:
1. Indexing and vocabulary generation: an inverted index and other data structures are built to perform fast lookups over the dictionary and the vocabulary list in Cg and Cs 

2. Sentence detection and tokenization: the input text is divided into sentences and then each sentence is divided into tokens using a whitespace as separator.

3. Spelling correction: to deal with noise and simple morphological variations, each token that does not match a word within the vocabulary is replaced by the most frequent word among the most similar words found above a threshold of 0.75. The similarity is computed using a normalized score based on the Levensthein distance.

4. Candidate generation and scoring: a subset that contains all the terms that match at least one of the words in the sentence is generated, the terms contained in this set are called candidates. Once this subset is built, each of the candidate terms is scored using a simplified version of Metamap’s scoring function (Aronson, 2001). In comparison, Term-mapper’s function uses only variation, coverage and cohesiveness as criteria, excluding centrality, since it is language dependant.

5. Candidate selection and disambiguation: the score computed in the previous step is used to choose the candidates that will be used as mappings. Ambiguity can occur because of two reasons: a tie in the scores or by overlapping over the sentence tokens. In the first case, the Lin’s measure (Lin, 1998) is used as disambiguation criteria between the candidates an the previous detected concepts.In the second case, the most concrete term is chosen according to the UMLS hierarchy.


### Developer considerations

* Term-Mapper use Maven as build system.
* Basic knowledge in dependency injection and Spring is required.
* Term-Mapper is only one part of a big project called kbmed, so is structured as a multi-module.
* Term-Mapper depends of the following modules:
    * Text-Pipe for text processing
    * Index which is a simple in-memory inverted index implementation
    * Knowledge source as an interface to any knowledge source, currently with implementations for SnomedCT (Radlex cooming soon).

### Getting started

1. Export TERM_MAPPER_HOME env (You can use -DTERM_MAPPER_HOME="PATH" also) which must point a directory with the following structure:

    * bin: which stores the launch scripts 
    * log: which stores the log files
    * index: which stores the index created by Term-Mapper
    * data: which must contain the stops word list files and the openNLP models
        - en-stop.txt.gz
        - en-chunker.bin
        - en-pos.bin
        - en-sent.bin
        
    Which can be obtained from [OpenNLP models page](http://opennlp.sourceforge.net/models-1.5/)
    
2. Once the project is built successfully, you can run the script map.sh located in  **./target/appassembler/bin**, which support the following parameters.
    * -l --lang language
    * -t --terms-dict The dictionary path, the file name will be used to store the index within TERM_MAPPER_HOME/index
    * -g --gen-dict Optional: A general dictionary file with additional vocabulary
    * -i --in-path The directory path where documents to map are located
    * -o --out-path The directory path where the mapping will be located

3. Given than the most of the data is loaded in memory for fast lookup is recommended to increase the heap size i.e. -Xmx3g
