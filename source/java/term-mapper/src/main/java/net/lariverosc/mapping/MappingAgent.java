package net.lariverosc.mapping;

import net.lariverosc.mapping.data.DocumentMappingsConsumer;
import net.lariverosc.mapping.data.DocumentMappings;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import net.lariverosc.mapping.data.ChunkAnnotations;
import net.lariverosc.text.document.Document;
import net.lariverosc.text.document.DocumentsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MappingAgent {

    private final Logger log = LoggerFactory.getLogger(MappingAgent.class);

    private MappingAlgorithm mappingAlgorithm;

    private DocumentsProvider documentsProvider;

    private Collection<DocumentMappingsConsumer> documentMappingsConsumers;

    private final AtomicInteger counter = new AtomicInteger(0);

    public MappingAgent() {
        documentMappingsConsumers = new ArrayList<>();
    }

    public void processSingleThread() {
        List<String> allDocumentIds = documentsProvider.getAllDocumentIds();
        if (!allDocumentIds.isEmpty()) {
            doMapping(allDocumentIds);
        }
    }

    public void processMultiThread(int numThreads) {
        List<String> allDocumentIds = documentsProvider.getAllDocumentIds();
        int totalDocuments = allDocumentIds.size();
        log.info("Total documents {}", totalDocuments);
        if (!allDocumentIds.isEmpty()) {
            int blockSize = (totalDocuments + numThreads - 1) / numThreads;
            for (int i = 0; i < numThreads; i++) {
                int start = i * blockSize;
                int end = Math.min((i + 1) * blockSize, totalDocuments);
                List<String> blockDocumentIds = new ArrayList<String>(allDocumentIds.subList(start, end));
                try {
                    log.info("Launching new thread #{} interval[{},{})", new Object[]{(i + 1), start, end});
                    launchThread(blockDocumentIds);
                } catch (InterruptedException ie) {
                    log.error("Error while launching thread for block: " + (i + 1), ie);
                }
            }
        }
    }

    private void launchThread(final List<String> blockConceptIds) throws InterruptedException {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    doMapping(blockConceptIds);
                } catch (Throwable t) {
                    log.error("Fatal error while executing!!!", t);
                }
            }
        });
        thread.start();
    }

    private void doMapping(List<String> blockDocumentIds) {
        int localCount = 1;
        for (String documentId : blockDocumentIds) {
            log.info("global {} -- local {}/{} - Mapping document start id={}", counter.incrementAndGet(), (localCount++), blockDocumentIds.size(), documentId);
            Document document = documentsProvider.getDocument(documentId);          
            DocumentMappings documentMappings = new DocumentMappings(document);
            for (String fieldName : document.fieldNames()) {
                List<ChunkAnnotations> fieldMappings = mappingAlgorithm.mapText(document.getFieldValue(fieldName));
                if (fieldMappings != null && !fieldMappings.isEmpty()) {  
                    documentMappings.addMappings(fieldName, fieldMappings);
                }
                
            }
            log.info("Mapping document success id={} ", documentId);
            if (documentMappings.hasMappings()) {
                for (DocumentMappingsConsumer documentMappingsConsumer : documentMappingsConsumers) {
                    documentMappingsConsumer.consume(documentMappings);
                }
                log.info("Storing document mappings success id={} ", documentId);
            } else {
                log.warn("Not mappings found for document: " + documentMappings.getDocumentId());
            }
        }
        log.info("Mapping thread proccess ends succesfully");
    }

    public void setMappingAlgorithm(MappingAlgorithm mappingAlgorithm) {
        this.mappingAlgorithm = mappingAlgorithm;
    }

    public void setDocumentsProvider(DocumentsProvider documentsProvider) {
        this.documentsProvider = documentsProvider;
    }

    public void addDocumentMappingsConsumer(DocumentMappingsConsumer documentMappingsConsumer) {
        this.documentMappingsConsumers.add(documentMappingsConsumer);
    }

}
