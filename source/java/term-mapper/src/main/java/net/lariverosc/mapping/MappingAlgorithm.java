package net.lariverosc.mapping;

import net.lariverosc.mapping.data.ChunkAnnotations;
import java.util.List;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface MappingAlgorithm {

    List<ChunkAnnotations> mapText(String text);
}
