package net.lariverosc.mapping;

import java.text.DecimalFormat;
import net.lariverosc.mapping.data.ChunkAnnotations;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.step.CandidateSelectionStep;
import net.lariverosc.mapping.step.CandidateScoringStep;
import net.lariverosc.mapping.step.CandidateGenerationStep;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.mapping.data.Match;
import net.lariverosc.mapping.step.PreprocessingStep;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.TextToken;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MappingAlgorithmImpl implements MappingAlgorithm {

    private final Logger log = LoggerFactory.getLogger(MappingAlgorithmImpl.class);

    private PreprocessingStep preprocessingStep;

    private CandidateGenerationStep candidateGenerationStep;

    private CandidateScoringStep candidateScoringStep;

    private CandidateSelectionStep candidateSelectionStep;

    private EntityKnowledgeProvider entityKnowledgeProvider;

    /**
     *
     * @param text
     * @return
     */
    @Override
    public List<ChunkAnnotations> mapText(String text) {
        List<TextChunk> textChunks = preprocessingStep.doAnalysis(text);
        List<ChunkAnnotations> chunkMappingList = new ArrayList<ChunkAnnotations>();
        for (TextChunk textChunk: textChunks) {
            ChunkAnnotations chunkMapping = mapChunk(textChunk);
            if (chunkMapping != null) {
                chunkMappingList.add(chunkMapping);
            }
        }
        return chunkMappingList;
    }

    private ChunkAnnotations mapChunk(TextChunk textChunk) {
        Set<Mapping> candidates = candidateGenerationStep.getCandidates(textChunk);
        log.debug("STEP Candidate Generation Total  {}", candidates.size());

        candidateScoringStep.evaluate(textChunk, candidates);
        log.debug("STEP Scoring candidates success");

        Set<Mapping> filteredMappings = candidateSelectionStep.filterCandidates(textChunk, candidates);
        log.debug("STEP Filtering candidates Total After filtering {}", filteredMappings.size());

        if (!filteredMappings.isEmpty()) {
            //Sort the remaining mappings according to its score in decreasing order
            List<Mapping> sortedMappings = Mapping.sortByScore(filteredMappings);
            if (!sortedMappings.isEmpty()) {
                log.debug(textChunk.getTokensAsString());
                for (Mapping mapping: sortedMappings) {
                    log.debug(getMappingAsString(textChunk, mapping));
                }
            }
            //Build the chunk mapping
            ChunkAnnotations chunkAnnotations = new ChunkAnnotations(textChunk, sortedMappings);
            return chunkAnnotations;
        }

        return null;
    }

    public String getMappingAsString(TextChunk textChunk, Mapping mapping) {
        List<TextToken> chunkTokens = textChunk.getTokens();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chunkTokens.size(); i++) {
            SortedSet<Match> matchesByChunkPosition = mapping.getMatchesByChunkPosition(i);
            if (matchesByChunkPosition.isEmpty()) {
                sb.append(chunkTokens.get(i).getText().replaceAll(".", "-")).append(" ");
            } else {
                Match.Type currentMatchType = matchesByChunkPosition.first().getMatchType();
                if (currentMatchType != Match.Type.EXACT) {
                    String text = chunkTokens.get(i).getText();
                    String mathTypeString = currentMatchType.toString();
                    if (text.length() <= mathTypeString.length()) {
                        sb.append(mathTypeString.substring(0, text.length())).append(" ");
                    } else {
                        sb.append(StringUtils.rightPad(currentMatchType.toString(), text.length(), "-")).append(" ");
                    }
                } else {
                    sb.append(chunkTokens.get(i).getText()).append(" ");
                }
            }
        }
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        Concept concept = entityKnowledgeProvider.getConceptByTermId(mapping.getTermId());
        sb.append(" -> termId:").append(mapping.getTermId())
            .append(" term:").append(mapping.getTerm())
            .append(" category:").append(concept.getCategory())
            .append(" score:").append(decimalFormat.format(mapping.getScore()));
        return sb.toString();
    }

    public void setPreprocessingStep(PreprocessingStep preprocessingStep) {
        this.preprocessingStep = preprocessingStep;
    }

    public void setCandidateGenerationStep(CandidateGenerationStep candidateGenerationStep) {
        this.candidateGenerationStep = candidateGenerationStep;
    }

    public void setCandidateScoringStep(CandidateScoringStep candidateScoringStep) {
        this.candidateScoringStep = candidateScoringStep;
    }

    public void setCandidateSelectionStep(CandidateSelectionStep candidateSelectionStep) {
        this.candidateSelectionStep = candidateSelectionStep;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

}
