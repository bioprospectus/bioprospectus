package net.lariverosc.mapping;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MappingException extends RuntimeException {

    public MappingException() {
        super();
    }

    public MappingException(String message) {
        super(message);
    }

    public MappingException(String message, Throwable thrwbl) {
        super(message, thrwbl);
    }
}
