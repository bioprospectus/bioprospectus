package net.lariverosc.mapping.ambiguity;

import java.util.Set;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface AmbiguityResolver {

    Set<Mapping> resolve(TextChunk textChunk,Set<Mapping> candidateMappings);
}
