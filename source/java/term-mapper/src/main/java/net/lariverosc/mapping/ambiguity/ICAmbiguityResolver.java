package net.lariverosc.mapping.ambiguity;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.icontent.data.InformationContentManager;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;
import net.lariverosc.util.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ICAmbiguityResolver implements AmbiguityResolver {

    private final Logger log = LoggerFactory.getLogger(ICAmbiguityResolver.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private InformationContentManager informationContentManager;

    @Override
    public Set<Mapping> resolve(TextChunk textChunk, Set<Mapping> candidateMappings) {
        Multimap<String, Mapping> positionsMap = HashMultimap.create();
        for (Mapping mapping : candidateMappings) {
            positionsMap.put(mapping.getChunkPositionsAsString(), mapping);
        }

        Set<Mapping> filteredMappings = new HashSet<>();
        for (String position : positionsMap.keySet()) {
            Collection<Mapping> positionMappings = positionsMap.get(position);
            if (positionMappings.size() > 1) { //Ambiguity
                Collection<Mapping> finalMappings = solveAmbiguity(textChunk, positionMappings);
                filteredMappings.addAll(finalMappings);
            } else {
                filteredMappings.addAll(positionMappings);
            }
        }
        return filteredMappings;
    }

    private Collection<Mapping> solveAmbiguity(TextChunk textChunk, Collection<Mapping> positionMappings) {
        Map<Mapping, Double> ICValues = new HashMap<>();
        for (Mapping mapping : positionMappings) {
            log.debug("Ambiguous mappings {} ", mapping);
            Concept concept = entityKnowledgeProvider.getConceptByTermId(mapping.getTermId());
            ICValues.put(mapping, informationContentManager.getValue(concept));
        }
        Map<Mapping, Double> sortedICValues = Maps.sortByValues(ICValues, true);
        Set<Mapping> finalMapping = new HashSet<>();
        Mapping choosenMapping = sortedICValues.keySet().iterator().next();
        log.debug("Choosen mappings {} ", choosenMapping);
        finalMapping.add(choosenMapping);
        return finalMapping;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setInformationContentManager(InformationContentManager informationContentManager) {
        this.informationContentManager = informationContentManager;
    }

}
