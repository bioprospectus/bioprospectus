package net.lariverosc.mapping.ambiguity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.mapping.score.StructuralMappingScore;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.semsim.data.SemanticSimilarityManager;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SemSimAmbiguityResolver implements AmbiguityResolver {

    private final Logger log = LoggerFactory.getLogger(SemSimAmbiguityResolver.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private SemanticSimilarityManager semanticSimilarityManager;

    @Override
    public Set<Mapping> resolve(TextChunk textChunk,Set<Mapping> candidateMappings) {
        Map<String, Set<Mapping>> mappingsByChunkPositionMap = buildMappingByChunkPositionMap(candidateMappings);
        Set<String> context = buildContext(mappingsByChunkPositionMap);

        Set<Mapping> filteredMappings = new HashSet<Mapping>();
        for (Set<Mapping> tempSet: mappingsByChunkPositionMap.values()) {
            if (existPositionAmbiguity(tempSet) && existScoreAmbiguity(tempSet)) {
                filteredMappings.add(disambiguate(context, tempSet));
            } else {
                filteredMappings.addAll(tempSet);
            }
        }
        return filteredMappings;
    }

    private Map<String, Set<Mapping>> buildMappingByChunkPositionMap(Set<Mapping> candidateMappings) {
        Map<String, Set<Mapping>> mappingsByChunkPositionMap = new HashMap<String, Set<Mapping>>();
        for (Mapping mapping: candidateMappings) {
            String chunkPositionsAsString = mapping.getChunkPositionsAsString();
            Set<Mapping> mappings = mappingsByChunkPositionMap.get(chunkPositionsAsString);
            if (mappings == null) {
                Set<Mapping> tempSet = new HashSet<Mapping>();
                tempSet.add(mapping);
                mappingsByChunkPositionMap.put(chunkPositionsAsString, tempSet);
            } else {
                mappings.add(mapping);
            }
        }
        return mappingsByChunkPositionMap;
    }

    private Set<String> buildContext(Map<String, Set<Mapping>> matchesByChunkPositionMap) {
        Set<String> context = new HashSet<String>();
        for (Map.Entry<String, Set<Mapping>> entry: matchesByChunkPositionMap.entrySet()) {
            Set<Mapping> tempSet = entry.getValue();
            if (tempSet.size() == 1) {
                context.add(tempSet.iterator().next().getTermId());
            }
        }
        return context;
    }

    private Mapping disambiguate(Set<String> context, Set<Mapping> mappingsByChunkPositionSet) {
        if (existConceptAmbiguity(mappingsByChunkPositionSet)) {
            return findMostSimilarMapping(context, mappingsByChunkPositionSet);
        } else {
            return mappingsByChunkPositionSet.iterator().next();
        }
    }

    private boolean existPositionAmbiguity(Set<Mapping> mappingsByChunkPositionSet) {
        return mappingsByChunkPositionSet.size() > 1;
    }

    private boolean existScoreAmbiguity(Set<Mapping> mappingsByChunkPositionSet) {
        Set<Double> tempSet = new HashSet<Double>();
        for (Mapping mapping: mappingsByChunkPositionSet) {
            tempSet.add(mapping.getScore());
        }
        return tempSet.size() == 1;
    }

    private boolean existConceptAmbiguity(Set<Mapping> mappingsByChunkPositionSet) {
        Set<String> tempSet = new HashSet<String>();
        for (Mapping mapping: mappingsByChunkPositionSet) {
            Concept concept = entityKnowledgeProvider.getConceptByTermId(mapping.getTermId());
            tempSet.add(concept.getId());
        }
        return tempSet.size() > 1;
    }

    private Mapping findMostSimilarMapping(Set<String> context, Set<Mapping> mappings) {
        double maxSimilarityToContext = -1;
        Mapping mostSimilarMapping = null;
        for (Mapping currentMapping: mappings) {
            double currentSimilarityToContext = computeSimiliarityToContext(context, currentMapping);
            log.info("{} - {}", currentMapping, currentSimilarityToContext);
            if (currentSimilarityToContext > maxSimilarityToContext) {
                maxSimilarityToContext = currentSimilarityToContext;
                mostSimilarMapping = currentMapping;
            }
        }
        return mostSimilarMapping;
    }

    private double computeSimiliarityToContext(Set<String> context, Mapping mapping) {
        double average = 0;
        Concept currentConcept = entityKnowledgeProvider.getConceptByTermId(mapping.getTermId());
        for (String termId: context) {
            average += semanticSimilarityManager.getValue(currentConcept, entityKnowledgeProvider.getConceptByTermId(termId));
        }
        return average / (context.size() + 1);
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setSemanticSimilarityManager(SemanticSimilarityManager semanticSimilarityManager) {
        this.semanticSimilarityManager = semanticSimilarityManager;
    }
}
