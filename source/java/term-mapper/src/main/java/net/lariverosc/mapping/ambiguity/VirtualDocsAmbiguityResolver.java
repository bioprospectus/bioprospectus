package net.lariverosc.mapping.ambiguity;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.util.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.FilteredQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class VirtualDocsAmbiguityResolver implements AmbiguityResolver {

    private final Logger log = LoggerFactory.getLogger(VirtualDocsAmbiguityResolver.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private Client client;

    private String ESIndexName;

    private final int NUM_DOCS = 10;

    private final int CONTEXT_SIZE = 5;

    private static final Set<Character> ESUnwantedChars = new HashSet<Character>() {
        {
            add('[');
            add(']');
            add('{');
            add('}');
            add('¡');
            add('¿');
            add('|');
            add('^');
            add('~');
            add('(');
            add(')');
            add('/');
            add('\\');
        }
    };

    @Override
    public Set<Mapping> resolve(TextChunk textChunk, Set<Mapping> candidateMappings) {
        Multimap<String, Mapping> positionsMap = HashMultimap.create();
        for (Mapping mapping : candidateMappings) {
            positionsMap.put(mapping.getChunkPositionsAsString(), mapping);
        }

        Set<Mapping> filteredMappings = new HashSet<>();
        for (String position : positionsMap.keySet()) {
            Collection<Mapping> positionMappings = positionsMap.get(position);
            if (positionMappings.size() > 1) { //Ambiguity

                Collection<Mapping> finalMappings = solveAmbiguity(textChunk, positionMappings);
                filteredMappings.addAll(finalMappings);
            } else {
                filteredMappings.addAll(positionMappings);
            }
        }
        return filteredMappings;
    }

    private Collection<Mapping> solveAmbiguity(TextChunk textChunk, Collection<Mapping> positionMappings) {
        Map<String, Mapping> mappingsByConceptId = new HashMap<>();

        int minMappingPosition = Integer.MAX_VALUE;
        int maxMappingPosition = Integer.MIN_VALUE;
        for (Mapping mapping: positionMappings) {
            log.debug("Ambiguous mappings {} ", mapping);
            Term term = entityKnowledgeProvider.getTerm(mapping.getTermId());
            mappingsByConceptId.put(term.getConceptId(), mapping);
            if (mapping.getFirstChunkPosition() < minMappingPosition) {
                minMappingPosition = mapping.getFirstChunkPosition();
            }
            if (mapping.getLastChunkPosition() > maxMappingPosition) {
                maxMappingPosition = mapping.getLastChunkPosition();
            }
        }
        minMappingPosition = minMappingPosition - CONTEXT_SIZE < 0 ? 0 : minMappingPosition - CONTEXT_SIZE;
        maxMappingPosition = maxMappingPosition + CONTEXT_SIZE > textChunk.getTokens().size() - 1 ? textChunk.getTokens().size() - 1 : maxMappingPosition + CONTEXT_SIZE;
        List<TextToken> choosenTokens = textChunk.getTokens().subList(minMappingPosition, maxMappingPosition);

        QueryStringQueryBuilder queryStringBuilder = buildQueryString(TextToken.joinTokens(choosenTokens));
        FilterBuilder conceptIdFilterBuilder = buildConceptIdFilters(mappingsByConceptId.keySet());

        SearchResponse searchResponse = client.prepareSearch(ESIndexName)
                .setSearchType(SearchType.DEFAULT)
                .setQuery(buildFilteredQuery(queryStringBuilder, conceptIdFilterBuilder))
                .addField("conceptId")
                .setFrom(0).setSize(NUM_DOCS)
                .execute()
                .actionGet();

        SearchHits hits = searchResponse.getHits();
        if (hits.getTotalHits() > 0) {
            SearchHit searchHit = hits.getHits()[0];
            String conceptId = searchHit.field("conceptId").getValue().toString();
            Set<Mapping> finalMapping = new HashSet<>();
            Mapping choosenMapping = mappingsByConceptId.get(conceptId);
            log.debug("Choosen mappings {} ", choosenMapping);
            finalMapping.add(choosenMapping);
            return finalMapping;
        } else {
            return positionMappings;
        }
    }

    private static FilteredQueryBuilder buildFilteredQuery(QueryStringQueryBuilder queryStringBuilder, FilterBuilder FilterBuilder) {
        return QueryBuilders.filteredQuery(queryStringBuilder, FilterBuilder);
    }

    private QueryStringQueryBuilder buildQueryString(final String queryStr) {
        String text = queryStr;
        text = StringUtils.foldToASCII(text);
        text = StringUtils.removePunctuation(text);
        text = StringUtils.removeUnwantedChars(text, ESUnwantedChars);
        QueryStringQueryBuilder queryStringBuilder = QueryBuilders.queryString(text.toLowerCase());
        queryStringBuilder.field("description^3");
        queryStringBuilder.field("category^3");
        queryStringBuilder.field("terms^2");
        queryStringBuilder.field("neighbors");
        return queryStringBuilder;
    }

    private static FilterBuilder buildConceptIdFilters(Collection<String> conceptIds) {
        return FilterBuilders.termsFilter("conceptId", conceptIds);
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setESIndexName(String ESIndexName) {
        this.ESIndexName = ESIndexName;
    }
}
