package net.lariverosc.mapping.ambiguity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.conn.es.ESFactory;
import net.lariverosc.knowledge.KnowledgeSourceException;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class VirtualDocsIndexAgent {

    private final Logger log = LoggerFactory.getLogger(VirtualDocsIndexAgent.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private Client ESClient;

    public void buildIndex() {
        Map<String, Concept> allConcepts = entityKnowledgeProvider.getAllConcepts();

        for (String conceptId : allConcepts.keySet()) {
            Map<String, Object> virtualDocument = new HashMap<>();

            Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
            virtualDocument.put("conceptId", conceptId);
            virtualDocument.put("category", concept.getCategory());
            virtualDocument.put("description", concept.getDescription());

            log.info(conceptId);
            Set<Term> conceptTerms = entityKnowledgeProvider.getTermsByConceptId(conceptId);
            virtualDocument.put("terms", getTermsStrings(conceptTerms));

            Set<Relationship> relationsBySource = entityKnowledgeProvider.getRelationsBySource(conceptId);
            Set<String> allRelatedTerms = new HashSet<>();
            for (Relationship sourceRelation : relationsBySource) {
                try {
                    Concept targetConcept = entityKnowledgeProvider.getByConceptId(sourceRelation.getTargetId());
                    Set<Term> targetTerms = entityKnowledgeProvider.getTermsByConceptId(targetConcept.getId());
                    Set<String> targetTermStrings = getTermsStrings(targetTerms);
                    allRelatedTerms.addAll(targetTermStrings);
                } catch (KnowledgeSourceException kse) {
                    log.warn("Error listing source relations", kse.getMessage());
                }
            }
            Set<Relationship> relationsByTarget = entityKnowledgeProvider.getRelationsByTarget(conceptId);
            for (Relationship targetRelation : relationsByTarget) {
                try {
                    Concept sourceConcept = entityKnowledgeProvider.getByConceptId(targetRelation.getSourceId());
                    Set<Term> sourceTerms = entityKnowledgeProvider.getTermsByConceptId(sourceConcept.getId());
                    Set<String> sourceTermStrings = getTermsStrings(sourceTerms);
                    allRelatedTerms.addAll(sourceTermStrings);
                } catch (KnowledgeSourceException kse) {
                    log.warn("Error listing target relations", kse.getMessage());
                }
            }
            virtualDocument.put("neighbors", allRelatedTerms);

            IndexResponse response = ESClient.prepareIndex("snomeden-disamb", "vdocs")
                    .setSource(virtualDocument)
                    .execute()
                    .actionGet();

            log.info("VirtualDoc for conceptId={} sucessfully indexed {}", new Object[]{conceptId, response.getId()});
        }
    }

    private Set<String> getTermsStrings(Set<Term> terms) {
        Set<String> tempSet = new HashSet<>();
        for (Term term : terms) {
            tempSet.add(term.getTerm());
        }
        return tempSet;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setESClient(Client ESClient) {
        this.ESClient = ESClient;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:snomed-context.xml");
        context.registerShutdownHook();

        VirtualDocsIndexAgent virtualDocsIndexAgent = new VirtualDocsIndexAgent();

        EntityKnowledgeProvider entityKnowledgeProvider = context.getBean("entityKnowledgeProvider", EntityKnowledgeProvider.class);
        virtualDocsIndexAgent.setEntityKnowledgeProvider(entityKnowledgeProvider);

        ESFactory ESFactory = new ESFactory("http://10.0.2.15:9300", "elasticsearch");
        Client client = ESFactory.create();
        virtualDocsIndexAgent.setESClient(client);
        virtualDocsIndexAgent.buildIndex();

    }

}
