package net.lariverosc.mapping.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.annotation.Annotation;

/**
 * This class represents the mappings found within a text chunk
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ChunkAnnotations {

    private final TextChunk textChunk;

    private final Collection<Mapping> mappings;

    private final Set<Annotation> annotations;

    public ChunkAnnotations(TextChunk textChunk, Collection<Mapping> mappings) {
        this.textChunk = textChunk;
        this.mappings = mappings;
        this.annotations = new HashSet<>();
        for (Mapping mapping : mappings) {
            annotations.add(new Annotation(mapping.getTermId(), extractSpans(mapping)));
        }
    }

    private Set<TextSpan> extractSpans(Mapping mapping) {
        List<TextToken> chunkTokens = textChunk.getTokens();
        Set<TextSpan> mappingSpans = new HashSet<>();
        for (Integer chunkIndex : mapping.getChunkPositions()) {
            mappingSpans.add(chunkTokens.get(chunkIndex).getTextSpan());
        }
        return mappingSpans;
    }

    public TextChunk getTextChunk() {
        return textChunk;
    }

    public Set<Annotation> getAnnotations() {
        return annotations;
    }

    public Collection<Mapping> getMappings() {
        return mappings;
    }

}
