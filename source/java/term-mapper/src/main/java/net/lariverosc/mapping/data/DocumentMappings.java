package net.lariverosc.mapping.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.text.document.Document;

/**
 * This class represents a document annotation.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DocumentMappings {

    /**
     * The document that is annotated
     */
    private final Document document;

    /**
     * The map in which the annotations are stored, each entry is in the form <documentFieldName,List<Mapping>>
     */
    private final Map<String, List<ChunkAnnotations>> mappingsByField;

    public DocumentMappings(Document document) {
        this.document = document;
        mappingsByField = new HashMap<String, List<ChunkAnnotations>>();
    }

    public Document getDocument() {
        return document;
    }

    public String getDocumentId() {
        return document.getId();
    }

    public List<ChunkAnnotations> getFieldMappings(String fieldName) {
        return mappingsByField.get(fieldName);
    }

    public Set<String> fieldNames() {
        return mappingsByField.keySet();
    }

    public void addMappings(String fieldName, List<ChunkAnnotations> chunkMappings) {
        if ((fieldName != null && !fieldName.isEmpty()) && (chunkMappings != null && !chunkMappings.isEmpty())) {
            if (mappingsByField.containsKey(fieldName)) {
                mappingsByField.get(fieldName).addAll(chunkMappings);
            } else {
                mappingsByField.put(fieldName, chunkMappings);
            }
        }
    }

    public boolean hasMappings() {
        return !mappingsByField.isEmpty();
    }

}
