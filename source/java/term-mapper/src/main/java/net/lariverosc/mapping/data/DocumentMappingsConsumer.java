package net.lariverosc.mapping.data;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface DocumentMappingsConsumer {

    void consume(DocumentMappings documentMappings);
}
