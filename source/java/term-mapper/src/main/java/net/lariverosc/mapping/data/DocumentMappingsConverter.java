package net.lariverosc.mapping.data;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 * @param <T>
 */
public interface DocumentMappingsConverter<T> {

    T fromDocumentMappings(DocumentMappings documentMappings) throws Exception;
}
