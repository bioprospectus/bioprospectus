package net.lariverosc.mapping.data;

import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;

/**
 * This class stores a group of {@link Match}, and makes it accessible by both
 * chunkPosition and termPosition, and also provides some useful methods to
 * iterate and access its data.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Mapping {

    /**
     * The termId associated to this mapping
     */
    private final String termId;

    /**
     * The term associated to this mapping
     */
    private final String term;

    /**
     * The tokens of the term associated to this mapping
     */
    private final List<String> termTokens;

    /**
     * A collection that stores the Matches by termPosition and then by
     * chunkPosition
     */
    private final TreeMultimap<Integer, Match> matchesByTermPosition;

    /**
     * A collection that stores the Matches by chunkPosition and then by termPosition
     */
    private final TreeMultimap<Integer, Match> matchesByChunkPosition;

    private double score = -1;

    public Mapping(Mapping mapping) {
        this.termId = mapping.getTermId();
        this.term = mapping.getTerm();
        this.termTokens = mapping.getTermTokens();
        this.score = mapping.getScore();
        this.matchesByTermPosition = TreeMultimap.create(Ordering.natural(), new MatchByChunkPositionComparator());
        this.matchesByChunkPosition = TreeMultimap.create(Ordering.natural(), new MatchByTermPositionComparator());
    }

    /**
     * Constructor.
     *
     * @param termId
     * @param term
     * @param termTokens
     */
    public Mapping(String termId, String term, List<String> termTokens) {
        this.termId = termId;
        this.term = term;
        this.termTokens = termTokens;
        this.matchesByTermPosition = TreeMultimap.create(Ordering.natural(), new MatchByChunkPositionComparator());
        this.matchesByChunkPosition = TreeMultimap.create(Ordering.natural(), new MatchByTermPositionComparator());
    }

    private Mapping(String termId, String term, TreeMultimap<Integer, Match> matchesByTermPosition, TreeMultimap<Integer, Match> matchesByChunkPosition) {
        this.termId = termId;
        this.term = term;
        this.termTokens = null;
        this.matchesByTermPosition = matchesByTermPosition;
        this.matchesByChunkPosition = matchesByChunkPosition;
    }

    /**
     * Returns the termId associated to this mapping
     *
     * @return termId
     */
    public String getTermId() {
        return termId;
    }

    /**
     * Returns the term associated to this mapping
     *
     * @return term
     */
    public String getTerm() {
        return term;
    }

    public List<String> getTermTokens() {
        return termTokens;
    }

    /**
     * Returns the scores given to this mapping
     *
     * @return scores
     */
    public double getScore() {
        return score;
    }

    /**
     * Set the structuralScore obtained by this mapping
     *
     * @param value
     */
    public void setScore(double value) {
        score = value;
    }

    /**
     * Returns and iterator that moves over the term positions
     *
     * @return term positions iterator
     */
    public Iterator<Integer> iteratorByTermPosition() {
        return matchesByTermPosition.keySet().iterator();
    }

    /**
     * Returns and iterator that moves over the chunk positions
     *
     * @return chunk positions iterator
     */
    public Iterator<Integer> iteratorByChunkPosition() {
        return matchesByChunkPosition.keySet().iterator();
    }

    /**
     * Adds a match to this mapping
     *
     * @param match
     */
    public void addMatch(Match match) {
        matchesByTermPosition.put(match.getTermPosition(), match);
        matchesByChunkPosition.put(match.getChunkPosition(), match);
    }

    /**
     * Return the matches in the given term position, sorted by its chunk position.
     *
     * @param termPosition
     *
     * @return A set of {@link TokenMatch} sorted by chunk position
     */
    public SortedSet<Match> getMatchesByTermPosition(int termPosition) {
        return matchesByTermPosition.get(termPosition);
    }

    /**
     * Return the matches in the given chunk position, sorted by its term position.
     *
     * @param chunkPosition
     *
     * @return A set of {@link TokenMatch} sorted by term position
     */
    public SortedSet<Match> getMatchesByChunkPosition(int chunkPosition) {
        return matchesByChunkPosition.get(chunkPosition);
    }

    /**
     * Return the first chunk position in this match group
     *
     * @return first chunk position
     */
    public int getFirstChunkPosition() {
        return matchesByChunkPosition.keySet().first();
    }

    /**
     * Return the last chunk position in this match group
     *
     * @return last chunk position
     */
    public int getLastChunkPosition() {
        return matchesByChunkPosition.keySet().last();
    }

    /**
     * Return the first term position in this match group
     *
     * @return first term position
     */
    public int getFirstTermPosition() {
        return matchesByTermPosition.keySet().first();
    }

    /**
     * Return the last term position in this match group
     *
     * @return last term position
     */
    public int getLastTermPosition() {
        return matchesByTermPosition.keySet().last();
    }

    /**
     * Return the total number of chunk positions involved in this mapping
     * group
     *
     * @return total chunk positions matched
     */
    public int getChunkMatches() {
        return matchesByChunkPosition.keySet().size();
    }

    /**
     * Return the total number of term positions involved in this mapping
     *
     * @return total term positions matched
     */
    public int getTermMatches() {
        return matchesByTermPosition.keySet().size();
    }

    /**
     * Returns a SortedSet that contains the chunk positions involved in this mapping
     *
     * @return the chunk positions involved in this match group sorted by its
     * natural order
     */
    public SortedSet<Integer> getChunkPositions() {
        return matchesByChunkPosition.keySet();
    }

    /**
     * Returns a String that contains the chunk positions involved in this mapping separated by a script
     *
     * @return the chunk positions involved in this match group sorted by its
     * natural order
     */
    public String getChunkPositionsAsString() {
        StringBuilder sb = new StringBuilder();
        for (Integer index : matchesByChunkPosition.keySet()) {
            sb.append(index).append(",");
        }
        if (sb.length() > 0) {
            return sb.substring(0, sb.length() - 1);
        }
        return "";

    }

    /**
     * Returns a SortedSet that contains the term positions involved in this
     * match group
     *
     * @return the term positions involved in this match group sorted by its
     * natural order
     */
    public SortedSet<Integer> getTermMatchesPositions() {
        return matchesByTermPosition.keySet();
    }

    /**
     * Returns true if the given term position is involved in any match within
     * this group.
     *
     * @param termPosition the term position
     *
     * @return true if the given term position is involved in any match within
     * this group, false in other case.
     */
    public boolean containsMatchesInTermPosition(int termPosition) {
        return matchesByTermPosition.containsKey(termPosition);
    }

    /**
     * Returns a collection view of the matches in this group
     *
     * @return The matches in this group
     */
    public Collection<Match> getAllMatches() {
        return matchesByChunkPosition.values();
    }

    public Mapping withScore(double score) {
        setScore(score);
        return this;
    }

    public Mapping withMatches(Match[] matchs) {
        for (Match match : matchs) {
            addMatch(match);
        }
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.termId);
        hash = 23 * hash + Objects.hashCode(this.matchesByTermPosition);
        hash = 23 * hash + Objects.hashCode(this.matchesByChunkPosition);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mapping other = (Mapping) obj;
        if (!Objects.equals(this.termId, other.termId)) {
            return false;
        }
        if (!Objects.equals(this.matchesByTermPosition.values(), other.matchesByTermPosition.values())) {
            return false;
        }
        if (!Objects.equals(this.matchesByChunkPosition.values(), other.matchesByChunkPosition.values())) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mapping{" + "termId=" + termId + ", term=" + term + ", scores=" + score + '}';
    }

    private class MatchByChunkPositionComparator implements Comparator<Match> {

        @Override
        public int compare(Match match1, Match match2) {
            return Integer.compare(match1.getChunkPosition(), match2.getChunkPosition());
        }
    }

    private class MatchByTermPositionComparator implements Comparator<Match> {

        @Override
        public int compare(Match match1, Match match2) {
            return Integer.compare(match1.getTermPosition(), match2.getTermPosition());
        }
    }

    /**
     * This utility class is used to sort a collection of {@link Mapping}
     * according to its score in decreasing order
     * @param mappings
     * @return
     */
    public static List<Mapping> sortByScore(Collection<Mapping> mappings) {
        List<Mapping> mappingsList = new ArrayList<>(mappings);
        Collections.sort(mappingsList, new MappingScoreComparator());
        return mappingsList;
    }

    private static class MappingScoreComparator implements Comparator<Mapping> {

        @Override
        public int compare(Mapping mapping1, Mapping mapping2) {
            return -Double.compare(mapping1.getScore(), mapping2.getScore());
        }
    }
}
