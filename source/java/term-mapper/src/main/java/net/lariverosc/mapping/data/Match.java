package net.lariverosc.mapping.data;

/**
 * This match represents the match of a token between a term and a chunk.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Match {

    /**
     * Enumeration that represents the type of a match
     */
    public enum Type {

        /**
         * Constant used to represent an exact match
         */
        EXACT(1),
        /**
         * Constant used to represent a match with the help of the spell checker
         */
        SPELLED(2),
        /**
         * Constant used to represent a match with the help of the stemmer
         */
        STEM(3);

        private final byte matchType;

        private Type(int matchType) {
            this.matchType = (byte) matchType;
        }

        /**
         *
         * @return
         */
        public byte getMatchType() {
            return matchType;
        }

        /**
         *
         * @return
         */
        @Override
        public String toString() {
            if (matchType == 1) {
                return "EXACT";
            } else if (matchType == 2) {
                return "SPELLED";
            } else if (matchType == 3) {
                return "STEM";
            } else {
                return "";
            }
        }
    }

    /**
     * Stores the position of the token match within the chunk
     */
    private final int chunkPosition;

    /**
     * Stores the position of the token match within the term
     */
    private final int termPosition;

    /**
     * The match type
     */
    private Type matchType = Type.EXACT;

    /**
     *
     * @param chunkPosition
     * @param termPosition
     */
    public Match(int chunkPosition, int termPosition) {
        this.chunkPosition = chunkPosition;
        this.termPosition = termPosition;
    }

    /**
     *
     * @param chunkPosition
     * @param termPosition
     * @param matchType
     */
    public Match(int chunkPosition, int termPosition, Type matchType) {
        this.chunkPosition = chunkPosition;
        this.termPosition = termPosition;
        this.matchType = matchType;
    }

    /**
     *
     * @return
     */
    public int getChunkPosition() {
        return chunkPosition;
    }

    /**
     *
     * @return
     */
    public int getTermPosition() {
        return termPosition;
    }

    /**
     *
     * @return
     */
    public Type getMatchType() {
        return matchType;
    }

    @Override
    public String toString() {
        return "Match{" + "chunkPosition=" + chunkPosition + ", termPosition=" + termPosition + ", type=" + matchType + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.chunkPosition;
        hash = 53 * hash + this.termPosition;
        hash = 53 * hash + (this.matchType != null ? this.matchType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Match other = (Match) obj;
        if (this.chunkPosition != other.chunkPosition) {
            return false;
        }
        if (this.termPosition != other.termPosition) {
            return false;
        }
        if (this.matchType != other.matchType) {
            return false;
        }
        return true;
    }
}
