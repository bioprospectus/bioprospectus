package net.lariverosc.mapping.filters;

import java.util.List;
import java.util.Set;
import net.lariverosc.mapping.ambiguity.AmbiguityResolver;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AmbiguityFilter implements CandidateFilter {

    private List<AmbiguityResolver> ambiguityResolvers;

    @Override
    public Set<Mapping> doFilter(TextChunk textChunk, Set<Mapping> candidateMappings) {
        for (AmbiguityResolver ambiguityResolver : ambiguityResolvers) {
            candidateMappings = ambiguityResolver.resolve(textChunk, candidateMappings);
        }
        return candidateMappings;
    }

    public void setAmbiguityResolvers(List<AmbiguityResolver> ambiguityResolvers) {
        this.ambiguityResolvers = ambiguityResolvers;
    }

}
