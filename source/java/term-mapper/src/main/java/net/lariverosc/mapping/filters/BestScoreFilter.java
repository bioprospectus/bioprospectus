package net.lariverosc.mapping.filters;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.mapping.score.StructuralMappingScore;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class BestScoreFilter implements CandidateFilter {

    @Override
    public Set<Mapping> doFilter(TextChunk textChunk, Set<Mapping> candidateMappings) {
        Map<String, Set<Mapping>> mappingsByChunkPositionMap = buildMappingsByChunkPositionMap(candidateMappings);

        Set<Mapping> filteredMappings = new HashSet<Mapping>();
        for (Set<Mapping> mappings: mappingsByChunkPositionMap.values()) {
            if (mappings.size() > 1) {
                filteredMappings.addAll(proccessMappings(mappings));
            } else {
                filteredMappings.addAll(mappings);
            }
        }
        return filteredMappings;
    }

    private Map<String, Set<Mapping>> buildMappingsByChunkPositionMap(Set<Mapping> candidateMappings) {
        Map<String, Set<Mapping>> mappingsByChunkPositionMap = new HashMap<String, Set<Mapping>>();
        for (Mapping mapping: candidateMappings) {
            String chunkPositionsString = mapping.getChunkPositionsAsString();
            Set<Mapping> mappings = mappingsByChunkPositionMap.get(chunkPositionsString);
            if (mappings == null) {
                Set<Mapping> tempSet = new HashSet<Mapping>();
                tempSet.add(mapping);
                mappingsByChunkPositionMap.put(chunkPositionsString, tempSet);
            } else {
                mappings.add(mapping);
            }
        }
        return mappingsByChunkPositionMap;
    }

    private Set<Mapping> proccessMappings(Set<Mapping> mappings) {
        double maxScore = findMaxScore(mappings);
        Set<Mapping> tempMappings = new HashSet<Mapping>();
        for (Mapping mapping: mappings) {
            if (mapping.getScore() == maxScore) {
                tempMappings.add(mapping);
            }
        }
        return tempMappings;
    }

    private double findMaxScore(Set<Mapping> mappings) {
        double maxScore = -1;
        for (Mapping mapping: mappings) {
            double mappingScore = mapping.getScore();
            if (mappingScore > maxScore) {
                maxScore = mappingScore;
            }
        }
        return maxScore;
    }
}
