package net.lariverosc.mapping.filters;

import java.util.Set;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface CandidateFilter {

    Set<Mapping> doFilter(TextChunk textChunk, Set<Mapping> candidateMappings);
}
