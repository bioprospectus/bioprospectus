package net.lariverosc.mapping.filters;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DuplicateMappingFilter implements CandidateFilter {
    
    private final Logger log = LoggerFactory.getLogger(DuplicateMappingFilter.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    @Override
    public Set<Mapping> doFilter(TextChunk textChunk, Set<Mapping> candidateMappings) {
        Multimap<String, Mapping> mappingsByConceptId = HashMultimap.create();
        Map<String, Mapping> mappingsByTermId = new HashMap<>();
        for (Mapping mapping : candidateMappings) {
            Term term = entityKnowledgeProvider.getTerm(mapping.getTermId());
            mappingsByConceptId.put(term.getConceptId(), mapping);
            mappingsByTermId.put(term.getId(), mapping);
        }

        Set<Mapping> filteredMappings = new HashSet<>();
        for (String conceptId : mappingsByConceptId.keySet()) {
            Collection<Mapping> conceptMappings = mappingsByConceptId.get(conceptId);
            if (conceptMappings.size() == 1) {
                filteredMappings.addAll(conceptMappings);
            } else {
                List<Mapping> conceptMappingsSorted = Mapping.sortByScore(conceptMappings);
                filteredMappings.add(conceptMappingsSorted.get(0));
            }
        }
        return filteredMappings;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

}
