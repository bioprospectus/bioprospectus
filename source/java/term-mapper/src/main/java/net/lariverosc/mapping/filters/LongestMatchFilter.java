package net.lariverosc.mapping.filters;

import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.Sets;
import com.google.common.collect.TreeRangeSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO this is not a good idea because it does not have a real sustentation it is more like an intuition that is
 * hard to explain. You can use IC instead.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class LongestMatchFilter implements CandidateFilter {

    private final Logger log = LoggerFactory.getLogger(LongestMatchFilter.class);

    @Override
    public Set<Mapping> doFilter(TextChunk textChunk,Set<Mapping> candidateMappings) {
        Map<Range<Integer>, Set<Mapping>> connectedIntervalsMap = buildConnectedIntervalsMap(candidateMappings);

        Set<Mapping> mappingsToRemove = new HashSet<Mapping>();
        //for each connected interval
        for (Set<Mapping> connectedIntervalMappings: connectedIntervalsMap.values()) {
            //if the connected interval contains more than one mapping
            if (connectedIntervalMappings.size() > 1) {
                processMappingsInInterval(connectedIntervalMappings, mappingsToRemove);
            }
        }

        //Remove filtered mappings from 
        Set<Mapping> filteredMappings = new HashSet<Mapping>();
        filteredMappings.addAll(Sets.difference(candidateMappings, mappingsToRemove));
        return filteredMappings;
    }

    private void processMappingsInInterval(Set<Mapping> connectedIntervalMappings, Set<Mapping> mappingsToRemove) {
        Map<Mapping, Range<Integer>> rangeByMappings = buildRangeByMappingMap(connectedIntervalMappings);
        //For all mappings in interval
        for (Mapping mapping: connectedIntervalMappings) {
            //If outer mapping was not already discarded
            if (!mappingsToRemove.contains(mapping)) {
                for (Mapping otherMapping: connectedIntervalMappings) {
                    //If inner mapping was not already discarded, and not is the same as outer
                    if (!mappingsToRemove.contains(otherMapping) && !otherMapping.equals(mapping)) {
                        //Get mappings ranges
                        Range<Integer> mappingRange = rangeByMappings.get(mapping);
                        Range<Integer> otherMappingRange = rangeByMappings.get(otherMapping);
                        //If outer mapping range encloses inner mapping range are not share the same chunk positions
                        if (mappingRange.encloses(otherMappingRange) && !mapping.getChunkPositions().equals(otherMapping.getChunkPositions())) {
                            //Remove inner mapping from valid mappings
                            mappingsToRemove.add(otherMapping);
//                            log.debug("Mapping discarted: " + otherMapping);
                        }
                    }
                }
            }
        }
    }

    private Map<Mapping, Range<Integer>> buildRangeByMappingMap(Set<Mapping> mappings) {
        Map<Mapping, Range<Integer>> rangeByMappingMap = new HashMap<Mapping, Range<Integer>>();
        for (Mapping mapping: mappings) {
            rangeByMappingMap.put(mapping, Range.closed(mapping.getFirstChunkPosition(), mapping.getLastChunkPosition()));
        }
        return rangeByMappingMap;
    }

    private Map<Range<Integer>, Set<Mapping>> buildConnectedIntervalsMap(Set<Mapping> mappings) {
        RangeSet<Integer> connectedIntervals = getConnectedIntervalsSet(mappings);
        Map<Range<Integer>, Set<Mapping>> connectedIntervalsMap = new HashMap<Range<Integer>, Set<Mapping>>();
        for (Range<Integer> range: connectedIntervals.asRanges()) {
            for (Mapping mapping: mappings) {
                if (range.encloses(Range.closed(mapping.getFirstChunkPosition(), mapping.getLastChunkPosition()))) {
                    Set<Mapping> tempSet = connectedIntervalsMap.get(range);
                    if (tempSet == null) {
                        tempSet = new HashSet<Mapping>();
                    }
                    tempSet.add(mapping);
                    connectedIntervalsMap.put(range, tempSet);
                }
            }
        }
        return connectedIntervalsMap;
    }

    private RangeSet<Integer> getConnectedIntervalsSet(Set<Mapping> mappings) {
        RangeSet<Integer> rangeSet = TreeRangeSet.create();
        for (Mapping mapping: mappings) {
            rangeSet.add(Range.closed(mapping.getFirstChunkPosition(), mapping.getLastChunkPosition()));
        }
        return rangeSet;
    }
}
