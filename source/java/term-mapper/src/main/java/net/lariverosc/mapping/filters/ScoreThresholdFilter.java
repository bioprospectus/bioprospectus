package net.lariverosc.mapping.filters;

import java.util.HashSet;
import java.util.Set;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ScoreThresholdFilter implements CandidateFilter {

    private final Logger log = LoggerFactory.getLogger(ScoreThresholdFilter.class);

    private double scoreThreshold;

    public ScoreThresholdFilter(double scoreThreshold) {
        log.info("scoreThreshold {}", scoreThreshold);
        this.scoreThreshold = scoreThreshold;
    }

    @Override
    public Set<Mapping> doFilter(TextChunk textChunk, Set<Mapping> candidateMappings) {
        return filterByScore(candidateMappings);
    }

    private Set<Mapping> filterByScore(Set<Mapping> candidateMappings) {
        Set<Mapping> filteredMappings = new HashSet<Mapping>();
        for (Mapping mapping: candidateMappings) {
            if (mapping.getScore() >= scoreThreshold) {
                filteredMappings.add(mapping);
            } else {
                log.trace("Match discarted: " + mapping);
            }
        }
        return filteredMappings;
    }

    public void setScoreThreshold(double scoreThreshold) {
        this.scoreThreshold = scoreThreshold;
    }
}
