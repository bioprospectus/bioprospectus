package net.lariverosc.mapping.filters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TopMappingsFilter implements CandidateFilter {

    private final int k;

    public TopMappingsFilter(int k) {
        this.k = k;
    }

    @Override
    public Set<Mapping> doFilter(TextChunk textChunk, Set<Mapping> candidateMappings) {
        List<Mapping> sortedMappings = Mapping.sortByScore(candidateMappings);
        Set<Mapping> filteredCandidates;
        if (sortedMappings.size() > k) {
            filteredCandidates = new HashSet<Mapping>(sortedMappings.subList(0, k));
        } else {
            filteredCandidates = new HashSet<Mapping>(sortedMappings);
        }
        return filteredCandidates;
    }

}
