package net.lariverosc.mapping.io;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.mapping.data.ChunkAnnotations;
import net.lariverosc.mapping.data.DocumentMappings;
import net.lariverosc.mapping.data.DocumentMappingsConsumer;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CSVDocumentMappingsConsumer implements DocumentMappingsConsumer {

    private final Logger log = LoggerFactory.getLogger(CSVDocumentMappingsConsumer.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private final Object[] header;

    private final FileWriter csvFile;

    private final CSVPrinter csvPrinter;

    public CSVDocumentMappingsConsumer(String filePath) throws IOException {
        csvFile = new FileWriter(filePath);
        csvPrinter = new CSVPrinter(csvFile, CSVFormat.DEFAULT);
        header = new Object[]{"termId", "conceptId", "category", "isLeave", "term", "textSpans", "numSpans", "maxTextSpan"};
        csvPrinter.printRecord(header);
    }

    @Override
    public void consume(DocumentMappings documentMappings) {
        try {
            for (String field : documentMappings.fieldNames()) {
                List<ChunkAnnotations> fieldMappings = documentMappings.getFieldMappings(field);
                for (ChunkAnnotations chunkAnnotations : fieldMappings) {
                    TextChunk textChunk = chunkAnnotations.getTextChunk();
                    Set<Annotation> annotations = chunkAnnotations.getAnnotations();
                    for (Annotation annotation : annotations) {
                        List record = generateRecord(textChunk.getText(), annotation);
                        if (record.size() == header.length) {
                            csvPrinter.printRecord(record);
                        } else {
                            log.warn("Illegal record length {}", record.size());
                        }
                    }
                }
            }
            log.info("Document sucessfully stored id={}", documentMappings.getDocumentId());
        } catch (IOException ioe) {
            close();
            throw new RuntimeException("Error creating csv file", ioe);
        }
    }

    private List generateRecord(String fieldValue, Annotation annotation) {
        Term term = entityKnowledgeProvider.getTerm(annotation.getId());
        Concept concept = entityKnowledgeProvider.getConceptByTermId(annotation.getId());
        List record = new ArrayList();
        record.add(annotation.getId());
        record.add(concept.getId());
        record.add(concept.getCategory());
        record.add(hierarchyKnowledgeProvider.getChildrens(concept).isEmpty());
        record.add(term.getTerm());
        List<TextSpan> textSpans = new ArrayList<>(annotation.getTextSpans());
        TextSpan.sortByStart(textSpans);
        StringBuilder sb = new StringBuilder();
        for (TextSpan textSpan : textSpans) {
            sb.append(textSpan.getCoveredText(fieldValue)).append(" ");
        }
        record.add(sb.toString().trim());
        record.add(annotation.getTextSpans().size());
        TextSpan maxTextSpan = TextSpan.getMaxTextSpan(textSpans);
        record.add(maxTextSpan.getCoveredText(fieldValue));
        return record;
    }

    public void close() {
        try {
            if (csvFile != null) {
                csvFile.flush();
                csvFile.close();
            }
            if (csvPrinter != null) {
                csvPrinter.close();
            }
        } catch (IOException ex) {
        }
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setHierarchyKnowledgeProvider(HierarchyKnowledgeProvider hierarchyKnowledgeProvider) {
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
    }

}
