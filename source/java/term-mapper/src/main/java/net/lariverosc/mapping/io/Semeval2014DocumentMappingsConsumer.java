package net.lariverosc.mapping.io;

import net.lariverosc.mapping.data.DocumentMappingsConsumer;
import net.lariverosc.mapping.data.DocumentMappings;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.mapping.data.ChunkAnnotations;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Semeval2014DocumentMappingsConsumer implements DocumentMappingsConsumer {

    private final Logger log = LoggerFactory.getLogger(Semeval2014DocumentMappingsConsumer.class);

    private final Map<String, String> sauiToCuiMap;

    private final String outPath;

    private final String termToCuiFilePath;

    public Semeval2014DocumentMappingsConsumer(String outPath, String termToCuiFilePath) {
        this.outPath = IOUtils.fixPath(outPath);
        this.termToCuiFilePath = termToCuiFilePath;
        sauiToCuiMap = loadSauiToCuiMap();
    }

    private Map<String, String> loadSauiToCuiMap() {
        Map<String, String> map = new HashMap<String, String>();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(termToCuiFilePath));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineSplited = line.split(",");
                map.put(lineSplited[0], lineSplited[1]);
            }
        } catch (IOException ioe) {
            log.error("Error while load valid cuis file: " + ioe.getMessage());
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
            }
        }
        return map;
    }

    @Override
    public void consume(DocumentMappings documentMappings) {
        try {
            saveDocumentMappings(documentMappings);
        } catch (IOException ex) {
            log.error("Error while consuming documentMappings", ex);
        }
    }

    private void saveDocumentMappings(DocumentMappings documentMappings) throws IOException {
        log.info("saving mappings for document: " + documentMappings.getDocumentId());
        String[] filePathSplited = documentMappings.getDocumentId().split(File.separator);
        if (filePathSplited.length < 2) {
            throw new RuntimeException();
        }
        String fileName = filePathSplited[filePathSplited.length - 1];
        String parentDir = filePathSplited[filePathSplited.length - 2];
        List<ChunkAnnotations> contentMappings = documentMappings.getFieldMappings("content");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outPath + fileName.replace("text", "pipe")));
        for (ChunkAnnotations chunkAnnotations : contentMappings) {
            TextChunk textChunk = chunkAnnotations.getTextChunk();
            Set<Annotation> annotations = chunkAnnotations.getAnnotations();
            for (Annotation annotation : annotations) {
                String termId = annotation.getId();
                Collection<TextSpan> textSpans = annotation.getTextSpans();
                Collection<TextSpan> textSpansMerged = TextSpan.mergeContinuousSpans(textSpans);
                Collection<TextSpan> offsetedSpans = TextSpan.offsetSpans(textSpansMerged, textChunk.getTextSpan().getStart());
                String mappingSpans = TextSpan.asString(offsetedSpans);
                String mappingsSpanFormated = "||" + mappingSpans.replaceAll("(-|,)", "||");
                if (sauiToCuiMap.containsKey(termId)) {
                    if (parentDir.equalsIgnoreCase("DISCHARGE")) {
                        bufferedWriter.write(fileName.replace(".text", "-" + parentDir.toUpperCase() + "_SUMMARY.txt||Disease_Disorder||") + sauiToCuiMap.get(termId) + mappingsSpanFormated);
                    } else {
                        bufferedWriter.write(fileName.replace(".text", "-" + parentDir.toUpperCase() + "_REPORT.txt||Disease_Disorder||") + sauiToCuiMap.get(termId) + mappingsSpanFormated);
                    }
                } else {
                    log.warn(termId + " not found in sauiToCuiMap");
                }
                bufferedWriter.newLine();
            }
        }
        bufferedWriter.close();
    }

}
