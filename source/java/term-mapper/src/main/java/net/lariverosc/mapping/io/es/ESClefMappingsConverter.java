package net.lariverosc.mapping.io.es;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.lang.NullPointerException;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.hierarchy.HierarchyKnowledgeProvider;
import net.lariverosc.knowledge.KnowledgeSourceException;
import net.lariverosc.mapping.data.ChunkAnnotations;
import net.lariverosc.mapping.data.DocumentMappings;
import net.lariverosc.mapping.data.DocumentMappingsConverter;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.highlight.AnnotationHighlighter;
import net.lariverosc.text.annotation.highlight.DefaultAnnotationHighlighter;
import net.lariverosc.text.document.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ESClefMappingsConverter implements DocumentMappingsConverter<JsonObject> {

    private final AnnotationHighlighter annotationHighlighter;

    private EntityKnowledgeProvider entityKnowledgeProvider;

    private HierarchyKnowledgeProvider hierarchyKnowledgeProvider;

    private static final Logger log = LoggerFactory.getLogger(ESClefMappingsConverter.class);

    public ESClefMappingsConverter() {
        annotationHighlighter = new DefaultAnnotationHighlighter("em", "em");
    }

    @Override
    public JsonObject fromDocumentMappings(DocumentMappings documentMappings) throws Exception {
        Document document = documentMappings.getDocument();
        Multiset<String> allMappings = HashMultiset.create();
        JsonObject jsonArticle = new JsonObject();
        JsonObject jsonArticleHighlighted = new JsonObject();
        JsonArray jsonAuthors = new JsonArray();
        for (String attributeName : document.attributeNames()) {
            if (attributeName.startsWith("author")) {
                jsonAuthors.add(new JsonPrimitive(document.getDocAttribute(attributeName)));
            } else {
                jsonArticle.addProperty(attributeName, document.getDocAttribute(attributeName));
            }
        }
        if (jsonAuthors.size() > 0) {
            jsonArticle.add("authors", jsonAuthors);
        }

        for (String field : document.fieldNames()) {//For each field
            String fieldValue = document.getFieldValue(field);
            processField(jsonArticle, field, fieldValue);
            List<ChunkAnnotations> fieldMappings = documentMappings.getFieldMappings(field);
            if (fieldMappings != null && !fieldMappings.isEmpty()) {
                Set<Annotation> fieldAnnotations = extractAnnotations(allMappings, fieldMappings);
                String fieldValueHighlighted = annotationHighlighter.highlight(fieldValue, fieldAnnotations);
                processField(jsonArticleHighlighted, field, fieldValueHighlighted);
            }
        }

        JsonObject jsonMappings = new JsonObject();
        if (!allMappings.isEmpty()) {
            JsonArray allMappingsJsonArray = new JsonArray();
            for (String conceptId : allMappings.elementSet()) {
                allMappingsJsonArray.add(new JsonPrimitive(conceptId));
            }
            jsonMappings.add("all_mappings", allMappingsJsonArray);
        }

        JsonObject jsonDocument = new JsonObject();
        jsonDocument.add("article", jsonArticle);
        jsonDocument.add("article_highlight", jsonArticleHighlighted);
        jsonDocument.add("mappings", jsonMappings);
        return jsonDocument;
    }

    private List<String> conceptToStringId(List<Concept> listConcepts, String topConceptId) {
        List<String> listStringIds = new ArrayList<String>();
        for (Concept concept : listConcepts) {
            if (!concept.getId().equals(topConceptId)) {
                listStringIds.add(concept.getId());
                log.info("Adding ontology concept: {} ", concept.getId());
            }
        }
        return listStringIds;
    }

    private Set<Annotation> extractAnnotations(final Multiset<String> allMappings, final List<ChunkAnnotations> fieldMappings) {
        Set<Annotation> fieldAnnotations = new HashSet<>();
        String topConceptId = entityKnowledgeProvider.getTopConceptId();
        for (ChunkAnnotations chunkAnnotations : fieldMappings) { //For each chunk mapping
            TextChunk textChunk = chunkAnnotations.getTextChunk();
            Set<Annotation> annotations = chunkAnnotations.getAnnotations();
            for (Annotation annotation : annotations) {
                Concept concept = entityKnowledgeProvider.getConceptByTermId(annotation.getId());
                //If concept is an attribute, continue
                if (concept.getId().equals(topConceptId)) {
                    log.info("Concept {} found to be root", concept.getId());
                    continue;
                }
                //Now we walk the ontology looking for the parents of a concept
                //So we elaborate a copy of the concept.
                log.info("Looking for concept: {} ", concept.getId());
                int sizeAllMappingsBefore = allMappings.size();
                List<Concept> conceptsToRoot = hierarchyKnowledgeProvider.getPathToRoot(concept);
                log.info("Number of concepts to the root: {} (Including attributes) ", conceptsToRoot.size());
                allMappings.addAll(conceptToStringId(conceptsToRoot, topConceptId));
                //This is trickly but is necesary to not iterate twice over all annotations
                allMappings.add(concept.getId());
                int sizeAllMappingsAfter = allMappings.size();
                log.info("New concepts in the path to root added: {} ", sizeAllMappingsAfter - sizeAllMappingsBefore);
                //Processing mappings
                Collection<TextSpan> textSpans = annotation.getTextSpans();
                Collection<TextSpan> textSpansMerged = TextSpan.mergeContinuousSpans(textSpans);
                Collection<TextSpan> offsetedSpans = TextSpan.offsetSpans(textSpansMerged, textChunk.getTextSpan().getStart());
                //Creating new annotations with appropiate offset and attributes
                Annotation newAnnotation = new Annotation(concept.getId(), new HashSet<>(offsetedSpans));
                newAnnotation.appendAttribute("class", "annotation", " ");
                newAnnotation.appendAttribute("class", concept.getId(), " ");
                newAnnotation.addAttribute("conceptId", concept.getId());
                newAnnotation.addAttribute("category", concept.getCategory());
                fieldAnnotations.add(newAnnotation);
            }
        }
        return fieldAnnotations;
    }

    private void processField(JsonObject jsonArticle, String field, String fieldValue) {
        if (field.endsWith("caption")) {
            String iri = field.substring(0, field.lastIndexOf("-"));
            processFigure(jsonArticle, iri, fieldValue);
        } else {
            jsonArticle.addProperty(field, fieldValue);
        }
    }

    private void processFigure(JsonObject jsonArticle, String iri, String caption) {
        JsonArray jsonfiguresArray = jsonArticle.getAsJsonArray("figures");
        if (jsonfiguresArray == null) {
            jsonfiguresArray = new JsonArray();
        }
        JsonObject jsonFigure = new JsonObject();
        jsonFigure.addProperty("iri", iri);
        jsonFigure.addProperty("caption", caption);
        jsonfiguresArray.add(jsonFigure);
        jsonArticle.add("figures", jsonfiguresArray);
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public void setHierarchyKnowledgeProvider(HierarchyKnowledgeProvider hierarchyKnowledgeProvider) {
        this.hierarchyKnowledgeProvider = hierarchyKnowledgeProvider;
    }

}
