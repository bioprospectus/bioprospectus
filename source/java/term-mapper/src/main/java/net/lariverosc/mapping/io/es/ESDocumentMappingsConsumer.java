package net.lariverosc.mapping.io.es;

import com.google.gson.JsonObject;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import net.lariverosc.mapping.data.DocumentMappings;
import net.lariverosc.mapping.data.DocumentMappingsConsumer;
import net.lariverosc.mapping.data.DocumentMappingsConverter;
import net.lariverosc.util.IOUtils;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.indices.IndexMissingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ESDocumentMappingsConsumer implements DocumentMappingsConsumer {

    private final Logger log = LoggerFactory.getLogger(ESDocumentMappingsConsumer.class);

    private DocumentMappingsConverter<JsonObject> jsonDocumentMappingsConverter;

    private final Client client;

    private final String indexName;

    private final String documentType;

    private final String mappingConfPath;

    public ESDocumentMappingsConsumer(Client client, String indexName, String documentType, String mappingConfPath) {
        this.client = client;
        this.indexName = indexName;
        this.documentType = documentType;
        this.mappingConfPath = mappingConfPath;
//        deleteIndex();
//        createESIndex();
    }

    @Override
    public void consume(DocumentMappings documentMappings) {
        try {
            JsonObject jsonMappings = jsonDocumentMappingsConverter.fromDocumentMappings(documentMappings);
            indexDocument(jsonMappings.toString());
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ESDocumentMappingsConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createESIndex() {
        CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);
        createIndexRequestBuilder.setSettings(getIndexSettings());
        createIndexRequestBuilder.addMapping(documentType, IOUtils.readContent(mappingConfPath));
        CreateIndexResponse createIndexResponse = createIndexRequestBuilder.execute().actionGet();
        if (!createIndexResponse.isAcknowledged()) {
            throw new RuntimeException("Exception while creating ES index");
        }
    }

    private void deleteIndex() {
        try {
            DeleteIndexResponse deleteIndexResponse = client.admin().indices()
                    .prepareDelete(indexName)
                    .execute()
                    .actionGet();
            if (deleteIndexResponse.isAcknowledged()) {
                log.info(indexName + " successfully deleted");
            }
        } catch (IndexMissingException ime) {
        }
    }

    private Settings getIndexSettings() {
        ImmutableSettings.Builder indexSettings = ImmutableSettings.settingsBuilder();
        indexSettings.put("number_of_shards", 5);
        indexSettings.put("number_of_replicas", 0);
//        indexSettings.put("index.gateway.type", "none");
//        indexSettings.put("index.mapper.dynamic", "false");
        indexSettings.put(getAnalyzerDefinition());
        return indexSettings.build();
    }

    private Map<String, String> getAnalyzerDefinition() {
        Map<String, String> customAnalizer = new HashMap<>();
        //customAnalyzer-raw
        customAnalizer.put("index.analysis.analyzer.default.type", "custom");
        customAnalizer.put("index.analysis.analyzer.default.tokenizer", "whitespace");
        customAnalizer.put("index.analysis.analyzer.default.filter.0", "lowercase");
        customAnalizer.put("index.analysis.analyzer.default.filter.1", "stopTokenFilter");
        customAnalizer.put("index.analysis.analyzer.default.filter.2", "asciifoldingTokenFilter");
        customAnalizer.put("index.analysis.analyzer.default.filter.3", "stemmerTokenFilter");
        //stopTokenFilter
        customAnalizer.put("index.analysis.filter.stopTokenFilter.type", "stop");
        customAnalizer.put("index.analysis.filter.stopTokenFilter.stopwords", "_english_");
        //stemmerTokenFilter
        customAnalizer.put("index.analysis.filter.stemmerTokenFilter.type", "stemmer");
        customAnalizer.put("index.analysis.filter.stemmerTokenFilter.stopwords_path", "light_english");
        //asciiFoldingTokenFilter
        customAnalizer.put("index.analysis.filter.asciifoldingTokenFilter.type", "asciifolding");
        return customAnalizer;
    }

    private void indexDocument(String jsonDocument) {
        IndexResponse indexResponse = client.prepareIndex(indexName, documentType)
                .setSource(jsonDocument)
                .execute()
                .actionGet();
        String _id = indexResponse.getId();
        log.info("Document sucessfully indexed _id=" + _id + " " + jsonDocument.substring(0, 100));
    }

    public void setDocumentMappingsConverter(DocumentMappingsConverter<JsonObject> jsonDocumentMappingsConverter) {
        this.jsonDocumentMappingsConverter = jsonDocumentMappingsConverter;
    }

}
