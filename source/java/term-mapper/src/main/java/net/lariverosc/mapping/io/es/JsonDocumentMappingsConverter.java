package net.lariverosc.mapping.io.es;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.lang.NullPointerException;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.KnowledgeSourceException;
import net.lariverosc.mapping.data.ChunkAnnotations;
import net.lariverosc.mapping.data.DocumentMappings;
import net.lariverosc.mapping.data.DocumentMappingsConverter;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.data.Match;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.annotation.highlight.AnnotationHighlighter;
import net.lariverosc.text.annotation.highlight.DefaultAnnotationHighlighter;
import net.lariverosc.text.document.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class JsonDocumentMappingsConverter implements DocumentMappingsConverter<JsonObject> {

    private static final Logger log = LoggerFactory.getLogger(JsonDocumentMappingsConverter.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    @Override
    public JsonObject fromDocumentMappings(DocumentMappings documentMappings) throws Exception {
        JsonObject jsonMappings = new JsonObject();
        Document document = documentMappings.getDocument();
        jsonMappings.addProperty("documentId", document.getId());
        for (String field : document.fieldNames()) {
            jsonMappings.addProperty(field, document.getFieldValue(field));
        }

        AnnotationHighlighter annotationHighlighter = new DefaultAnnotationHighlighter("em", "em");
        String topConceptId = entityKnowledgeProvider.getTopConceptId();
        Multiset<String> allMappings = HashMultiset.create();
        for (String field : documentMappings.fieldNames()) {//For each field
            List<ChunkAnnotations> fieldMappings = documentMappings.getFieldMappings(field);
            Set<Annotation> allFieldAnnotations = new HashSet<>();
            for (ChunkAnnotations chunkAnnotations : fieldMappings) { //For each chunk mapping
                TextChunk textChunk = chunkAnnotations.getTextChunk();
                Set<Annotation> annotations = chunkAnnotations.getAnnotations();
                for (Annotation annotation : annotations) {
                    Concept concept = entityKnowledgeProvider.getConceptByTermId(annotation.getId());
                    //Now we don't care where the node is, only we look for its father.
                    //We only check now whether the father concept is the root or not.
                    Concept testConcept = entityKnowledgeProvider.getConceptByTermId(annotation.getId());
                    while (true) {
                        // testConcept can become null if it is returned so from getFatherConcept
                        // If so it will break the loop
                        Concept fatherConcept = getFatherConcept(testConcept, entityKnowledgeProvider, topConceptId);
                        if (fatherConcept != null) {
                            allMappings.add(fatherConcept.getId());
                            testConcept = getFatherConcept(fatherConcept, entityKnowledgeProvider, topConceptId);
                        } else {
                            break;
                        }
                    }
                    //Add the mapping data to
                    allMappings.add(concept.getId());
                    Collection<TextSpan> textSpans = annotation.getTextSpans();
                    Collection<TextSpan> textSpansMerged = TextSpan.mergeContinuousSpans(textSpans);
                    Collection<TextSpan> offsetedSpans = TextSpan.offsetSpans(textSpansMerged, textChunk.getTextSpan().getStart());
                    Annotation newAnnotation = new Annotation(concept.getId(), new HashSet<>(offsetedSpans));
                    newAnnotation.appendAttribute("class", "annotation", " ");
                    newAnnotation.appendAttribute("class", concept.getId(), " ");
                    newAnnotation.addAttribute("conceptId", concept.getId());
                    newAnnotation.addAttribute("category", concept.getCategory());
                    allFieldAnnotations.add(newAnnotation);
                }
            }
            String fieldHighlight = annotationHighlighter.highlight(document.getFieldValue(field), allFieldAnnotations);
            jsonMappings.addProperty(field + "Highlight", fieldHighlight);
        }

        if (!allMappings.isEmpty()) {
            JsonArray allMappingsJsonArray = new JsonArray();
            for (String conceptId : allMappings.elementSet()) {
                allMappingsJsonArray.add(new JsonPrimitive(conceptId));
            }
            jsonMappings.add("all_mappings", allMappingsJsonArray);
        }

        return jsonMappings;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public Concept getFatherConcept(Concept concept, EntityKnowledgeProvider entityKnowledgeProvider, String topConceptId) {
        try {
            Set<Relationship> listRelationships = entityKnowledgeProvider.getRelationsBySource(concept.getId());
            //In case the list of relationships is null
            if (listRelationships.isEmpty()) {
                return null;
            }
            Relationship relationship = listRelationships.iterator().next();
            //In case we reach top concept of the Ontology
            if (relationship.getTargetId().equals(topConceptId)) {
                return null;
            }
            log.info("Looking for concept: {} ", relationship.getTargetId());

            Concept fatherConcept = entityKnowledgeProvider.getByConceptId(relationship.getTargetId());
            return fatherConcept;
        } catch (KnowledgeSourceException e) {
            log.error("Caught KnowledgeSourceException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            log.error("Caught NullPointerException: " + e.getMessage());
            return null;
        }

    }

    public static void main(String[] args) throws Exception {
        JsonDocumentMappingsConverter jdmc = new JsonDocumentMappingsConverter();
        Document document = new Document();
        document.setId("1");
        document.addField("name", "alejandro riveros");
        DocumentMappings documentMappings = new DocumentMappings(document);
        List<Mapping> mappings = new ArrayList<>();
        Mapping mapping = new Mapping("2", "nombre", null);
        mapping.addMatch(new Match(0, 1));
        mapping.addMatch(new Match(1, 1));
        mappings.add(mapping);
        List<ChunkAnnotations> chunkMappings = new ArrayList<>();
        TextChunk textChunk = new TextChunk("alejandro riveros");
        List<TextToken> textTokens = new ArrayList<>();
        textTokens.add(new TextToken("alejandro", new TextSpan(0, 9), TextToken.Type.WORD));
        textTokens.add(new TextToken("riveros", new TextSpan(10, 17), TextToken.Type.WORD));
        textChunk.setTokens(textTokens);
        chunkMappings.add(new ChunkAnnotations(textChunk, mappings));
        documentMappings.addMappings("name", chunkMappings);
        JsonObject fromDocumentMappings = jdmc.fromDocumentMappings(documentMappings);
        System.out.println(fromDocumentMappings.toString());

    }

}
