package net.lariverosc.mapping.io.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import net.lariverosc.mapping.data.DocumentMappings;
import net.lariverosc.mapping.data.DocumentMappingsConsumer;
import net.lariverosc.mapping.data.DocumentMappingsConverter;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MongoDocumentMappingsConsumer implements DocumentMappingsConsumer {

    private final Logger log = LoggerFactory.getLogger(MongoDocumentMappingsConsumer.class);

    private DBCollection dbCollection;

    private DocumentMappingsConverter<DBObject> documentMappingsConverter;

    public MongoDocumentMappingsConsumer() {
        this.documentMappingsConverter = new MongoDocumentMappingsConverter();
    }

    @Override
    public void consume(DocumentMappings documentMappings) {
        try {
            log.debug("Adding mappings to document {}", documentMappings.getDocumentId());
            DBObject mappingsDbObject = documentMappingsConverter.fromDocumentMappings(documentMappings);
            BasicDBObject query = new BasicDBObject("_id", new ObjectId(documentMappings.getDocumentId()));
            BasicDBObject set = new BasicDBObject("$set", mappingsDbObject);

            dbCollection.update(query, set);
            log.info("Document sucessfully stored id={}", documentMappings.getDocumentId());
        } catch (Exception ex) {
            log.error("Error while consume mappings for document " + documentMappings.getDocumentId(), ex);
        }
    }

    public void setDbCollection(DBCollection dbCollection) {
        this.dbCollection = dbCollection;
    }

    public void setDocumentMappingsConverter(DocumentMappingsConverter<DBObject> documentMappingsConverter) {
        this.documentMappingsConverter = documentMappingsConverter;
    }

}
