package net.lariverosc.mapping.io.mongodb;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.lang.NullPointerException;
import net.lariverosc.mapping.data.DocumentMappings;
import net.lariverosc.mapping.data.DocumentMappingsConverter;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.domain.Relationship;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.knowledge.KnowledgeSourceException;
import net.lariverosc.mapping.data.ChunkAnnotations;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import net.lariverosc.text.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MongoDocumentMappingsConverter implements DocumentMappingsConverter<DBObject> {

    private static final Logger log = LoggerFactory.getLogger(MongoDocumentMappingsConverter.class);

    private EntityKnowledgeProvider entityKnowledgeProvider;

    @Override
    public DBObject fromDocumentMappings(DocumentMappings documentMappings) throws Exception {
        DBObject mappingsByFieldDBObject = new BasicDBObject();
        String topConceptId = entityKnowledgeProvider.getTopConceptId();
        Multiset<String> allMappings = HashMultiset.create();
        for (String fieldName : documentMappings.fieldNames()) {//For each field

            List<ChunkAnnotations> fieldMappings = documentMappings.getFieldMappings(fieldName);
            HashMultimap<String, String> fieldMappingsMap = HashMultimap.create();
            for (ChunkAnnotations chunkAnnotations : fieldMappings) { //For each chunk mapping
                //Get textChunk and mappings
                TextChunk textChunk = chunkAnnotations.getTextChunk();
                Set<Annotation> annotations = chunkAnnotations.getAnnotations();
                for (Annotation annotation : annotations) {
                    Concept concept = entityKnowledgeProvider.getConceptByTermId(annotation.getId());
                    //Now we walk the ontology looking for the parents of a concept
                    //So we elaborate a copy of the concept.
                    Concept testConcept = entityKnowledgeProvider.getConceptByTermId(annotation.getId());
                    while (!allMappings.contains(testConcept.getId())) {
                        // testConcept can become null if it is returned so from getParentConcept
                        // Also if a concept is already in the allMappings variable we break too the loop.
                        Concept parentConcept = getParentConcept(testConcept, entityKnowledgeProvider, topConceptId);
                        // parentConcept can be null, so we break the loop
                        if (parentConcept != null) {
                            allMappings.add(parentConcept.getId());
                            testConcept = parentConcept; //testConcept will never be null
                        } else {
                            break;
                        }
                    }


                    //Add the mapping data to
                    allMappings.add(concept.getId());

                    Collection<TextSpan> textSpans = annotation.getTextSpans();
                    Collection<TextSpan> textSpansMerged = TextSpan.mergeContinuousSpans(textSpans);
                    Collection<TextSpan> offsetedSpans = TextSpan.offsetSpans(textSpansMerged, textChunk.getTextSpan().getStart());

                    fieldMappingsMap.put(concept.getId(), TextSpan.asString(offsetedSpans));
                }
            }

            BasicDBList fieldMappingsDBList = new BasicDBList();
            for (String conceptId : fieldMappingsMap.keySet()) {//For each mapping in this field
                //Build the spans list
                BasicDBList mappingSpansDBList = new BasicDBList();
                Set<String> mappingSpans = fieldMappingsMap.get(conceptId);
                for (String mappingSpan : mappingSpans) {
                    mappingSpansDBList.add(mappingSpan);
                }
                //Build the mapping object
                BasicDBObject mappingDBObject = new BasicDBObject();
                mappingDBObject.put("id", conceptId);
                mappingDBObject.put("spans", mappingSpansDBList);
                fieldMappingsDBList.add(mappingDBObject);
            }

            String jsonFieldName = StringUtils.getJsonValidName(fieldName);
            mappingsByFieldDBObject.put(jsonFieldName, fieldMappingsDBList);
        }

        DBObject mappingsDBObject = new BasicDBObject();
        if (!mappingsByFieldDBObject.keySet().isEmpty()) {
            mappingsDBObject.put("mappings_by_field", mappingsByFieldDBObject);
        }

        if (!allMappings.isEmpty()) {

            BasicDBList allMappingsDBList = new BasicDBList();
            allMappingsDBList.addAll(allMappings.elementSet());
            mappingsDBObject.put("all_mappings", allMappingsDBList);

            BasicDBObject allMappingsFreqDBObject = new BasicDBObject();
            for (String conceptId: allMappings.elementSet()) {
                allMappingsFreqDBObject.append(conceptId, allMappings.count(conceptId));
            }
            mappingsDBObject.put("all_mappings_freq", allMappingsFreqDBObject);

            BasicDBObject allMappingsDescriptionDbObject = new BasicDBObject();
            for (String conceptId: allMappings.elementSet()) {
                Concept concept = entityKnowledgeProvider.getByConceptId(conceptId);
                allMappingsDescriptionDbObject.put(conceptId, concept.getDescription());
            }
            mappingsDBObject.put("all_mappings_description", allMappingsDescriptionDbObject);
        }
        DBObject documentDBObject = new BasicDBObject();
        documentDBObject.put("mappings", mappingsDBObject);
        return documentDBObject;
    }

    private Set<Annotation> extractAnnotations(final Multiset<String> allMappings, final List<ChunkAnnotations> fieldMappings) {
        Set<Annotation> fieldAnnotations = new HashSet<>();
        for (ChunkAnnotations chunkAnnotations : fieldMappings) { //For each chunk mapping
            TextChunk textChunk = chunkAnnotations.getTextChunk();
            Set<Annotation> annotations = chunkAnnotations.getAnnotations();
            for (Annotation annotation : annotations) {
                Concept concept = entityKnowledgeProvider.getConceptByTermId(annotation.getId());
                //This is trickly but is necesary to not iterate twice over all annotations
                allMappings.add(concept.getId());
                //Processing mappings
                Collection<TextSpan> textSpans = annotation.getTextSpans();
                Collection<TextSpan> textSpansMerged = TextSpan.mergeContinuousSpans(textSpans);
                Collection<TextSpan> offsetedSpans = TextSpan.offsetSpans(textSpansMerged, textChunk.getTextSpan().getStart());
                //Creating new annotations with appropiate offset and attributes
                Annotation newAnnotation = new Annotation(concept.getId(), new HashSet<>(offsetedSpans));
                newAnnotation.appendAttribute("class", "annotation", " ");
                newAnnotation.appendAttribute("class", concept.getId(), " ");
                newAnnotation.addAttribute("conceptId", concept.getId());
                newAnnotation.addAttribute("category", concept.getCategory());
                fieldAnnotations.add(newAnnotation);
            }
        }
        return fieldAnnotations;
    }

    public void setEntityKnowledgeProvider(EntityKnowledgeProvider entityKnowledgeProvider) {
        this.entityKnowledgeProvider = entityKnowledgeProvider;
    }

    public Concept getParentConcept(Concept concept, EntityKnowledgeProvider entityKnowledgeProvider, String topConceptId) {
        try {
            Set<Relationship> listRelationships = entityKnowledgeProvider.getRelationsBySource(concept.getId());
            //In case the list of relationships is null
            if (listRelationships.isEmpty()) {
                return null;
            }
            Relationship relationship = listRelationships.iterator().next();
            //In case we reach top concept of the Ontology
            if (relationship.getTargetId().equals(topConceptId)) {
                return null;
            }
            log.info("Looking for concept: {} ", relationship.getTargetId());

            Concept parentConcept = entityKnowledgeProvider.getByConceptId(relationship.getTargetId());
            return parentConcept;
        } catch (KnowledgeSourceException e) {
            log.error("Caught KnowledgeSourceException: " + e.getMessage());
            return null;
        } catch (NullPointerException e) {
            log.error("Caught NullPointerException: " + e.getMessage());
            return null;
        }

    }

}
