package net.lariverosc.mapping.io.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import java.util.ArrayList;
import java.util.List;
import net.lariverosc.mongodb.document.MongoDocumentsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TermMapperMongoDocumentsProvider extends MongoDocumentsProvider {

    private final Logger log = LoggerFactory.getLogger(TermMapperMongoDocumentsProvider.class);

    @Override
    public List<String> getAllDocumentIds() {
        BasicDBObject query = new BasicDBObject("mappings", new BasicDBObject("$exists", false));
        BasicDBObject fields = new BasicDBObject("_id", 1);
        DBCursor dbCursor = dbCollection.find(query, fields);
        List<String> documentIds = new ArrayList<>();
        while (dbCursor.hasNext()) {
            documentIds.add(dbCursor.next().get("_id").toString());
        }
        return documentIds;
    }

}
