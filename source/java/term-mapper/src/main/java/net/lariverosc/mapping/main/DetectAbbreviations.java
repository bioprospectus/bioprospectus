package net.lariverosc.mapping.main;

import com.google.common.base.Joiner;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.lariverosc.knowledge.domain.Term;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.text.util.AbbreviationMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DetectAbbreviations {

    private static final Logger log = LoggerFactory.getLogger(DetectAbbreviations.class);

    private static ClassPathXmlApplicationContext initSpringContext() {
        log.info("Starting Spring application context");
        List<String> contextConfigLocations = new ArrayList<String>();
        contextConfigLocations.add("classpath*:snomed-context.xml");
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(contextConfigLocations.toArray(new String[0]));
        applicationContext.registerShutdownHook();
        return applicationContext;
    }

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext applicationContext = initSpringContext();
        EntityKnowledgeProvider entityKnowledgeProvider = applicationContext.getBean("snomedEntityKnowledgeProvider", EntityKnowledgeProvider.class);
        Map<String, Term> allTerms = entityKnowledgeProvider.getAllTerms();

        Pattern pattern = Pattern.compile("^[A-Z]{2,}\\s-\\s.*");
        Matcher matcher = pattern.matcher("");
        AbbreviationMatcher detector = new AbbreviationMatcher();
        int i = 0;
        for (Term term: allTerms.values()) {
            if (matcher.reset(term.getTerm()).matches()) {
                String[] split = term.getTerm().split("\\s-\\s");
                if (split.length == 2) {
                    String findBest = detector.findBest(split[0], split[1]);
                    System.out.println((i++)+" match: " + findBest + " term: " + term);
                } else {
                    String[] splitCopy = Arrays.copyOfRange(split, 1, split.length);
                    String join = Joiner.on(" ").join(splitCopy);
                    String findBest = detector.findBest(split[0], join);
                    System.out.println((i++)+" match: " + findBest + " term: " + term);
                }
            }
        }

    }

}
