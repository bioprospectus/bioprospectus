package net.lariverosc.mapping.main;

import com.beust.jcommander.ParameterException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.mapping.MappingAgent;
import net.lariverosc.mapping.data.DocumentMappingsConsumer;
import net.lariverosc.index.Index;
import net.lariverosc.index.IndexManager;
import net.lariverosc.collection.DictionaryCollectionLoader;
import net.lariverosc.index.FileIndexManager;
import net.lariverosc.index.TextIndexAgent;
import net.lariverosc.mapping.MappingAlgorithm;
import net.lariverosc.text.document.DocumentsProvider;
import net.lariverosc.text.spell.LuceneSpellChecker;
import net.lariverosc.text.spell.SpellChecker;
import net.lariverosc.weirdness.Weirdness;
import net.lariverosc.weirdness.WordFreqListFileLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MapBatchMain extends MapMainBase {

    private static final Logger log = LoggerFactory.getLogger(MapBatchMain.class);

    public static void main(String[] args) {
        try {
            new MapBatchMain(args);
        } catch (Throwable t) {
            log.error("Fatal Error!!! {}", t);
        }
    }

    public MapBatchMain(String[] args) throws Exception {
        super(args);
    }

    @Override
    protected void execute() throws Exception {
        validateArguments();

        if (!indexExist()) {
            log.warn("Index does not exists proceed to create one!!!");
            Index index = buildIndex();
            buildSpellIndex(index);
//                computeWeirdness(arguments, index);
        }
        ClassPathXmlApplicationContext applicationContext = initSpringContext();

        MappingAgent mappingAgent = new MappingAgent();
        mappingAgent.setMappingAlgorithm(applicationContext.getBean("mappingAlgorithm", MappingAlgorithm.class));
        setupTermSelectionStrategy(0, applicationContext);
        setupInputAndOutputs(applicationContext, mappingAgent);
        mappingAgent.processMultiThread(mappingArguments.getNumThreads());
    }

    private void validateArguments() {
        if ((mappingArguments.getInPath() == null && mappingArguments.getOutPath() == null)
            && (mappingArguments.getMongoDbUrl() == null)) {
            throw new ParameterException("You must provide either a in/out path or a mongoDb collection to annotate");
        }
    }

    private boolean indexExist() {
        File indexFile = new File(getTerminologyIndexPath());
        File spellIndexDir = new File(getSpellIndexPath());
        return indexFile.isFile() && indexFile.exists() && spellIndexDir.isDirectory() && spellIndexDir.exists();
    }

    private Index buildIndex() throws IOException {
        TextIndexAgent textIndexAgent = new TextIndexAgent();
        textIndexAgent.setCollectionLoader(new DictionaryCollectionLoader(mappingArguments.getTerminologyPath(), "utf-8", "\t"));
        Index index = textIndexAgent.buildIndex();
        IndexManager indexManager = new FileIndexManager(getIndexPath());
        indexManager.saveIndex(index);
        return index;
    }

    private void buildSpellIndex(Index index) {
        Set<String> indexTerms = index.getTerms();
        Set<String> generalDictWords = null;
        if (mappingArguments.getGeneralVocabularyPath() != null) {
            generalDictWords = new WordFreqListFileLoader(mappingArguments.getGeneralVocabularyPath(), "utf-8").loadWordList().keySet();
        }
        Set<String> spellDictWords = new HashSet<String>();
        if (indexTerms != null) {
            spellDictWords.addAll(indexTerms);
        }
        if (generalDictWords != null) {
            spellDictWords.addAll(generalDictWords);
        }
        SpellChecker spellChecker = new LuceneSpellChecker(getSpellIndexPath());
        spellChecker.buildIndex(spellDictWords);
    }

    /**
     * There are three modes in which term-mapper can work:
     * 1. File input - File output
     * 2. MongoDB input - MongoDB output
     * 3. MongoDB input - MongoDB and ElasticSearch as outputs
     */
    private void setupInputAndOutputs(ClassPathXmlApplicationContext applicationContext, MappingAgent mappingAgent) {
        mappingAgent.addDocumentMappingsConsumer(applicationContext.getBean("csvDocumentMappingsConsumer", DocumentMappingsConsumer.class));
        if (useMongoDB()) {
            log.info("Using MongoDB");
            mappingAgent.setDocumentsProvider(applicationContext.getBean("termMapperMongoDocumentsProvider", DocumentsProvider.class));
            //To avoid output in MongoDB
            //mappingAgent.addDocumentMappingsConsumer(applicationContext.getBean("mongoDocumentMappingsConsumer", DocumentMappingsConsumer.class));
        } else {
            mappingAgent.setDocumentsProvider(applicationContext.getBean("fileSystemDocumentProvider", DocumentsProvider.class));
            //mappingAgent.addDocumentMappingsConsumer(applicationContext.getBean("semeval2014AnnotationsConsumer", DocumentMappingsConsumer.class));
        }
        if (useElasticSearch()) {
            log.info("Using ElasticSearch");
            mappingAgent.addDocumentMappingsConsumer(applicationContext.getBean("esDocumentMappingsConsumer", DocumentMappingsConsumer.class));
        }
    }

    private ClassPathXmlApplicationContext initSpringContext() {
        log.info("Starting Spring application context");
        List<String> contextConfigLocations = new ArrayList<String>();
        contextConfigLocations.add("classpath*:text-pipe-context.xml");
        contextConfigLocations.add("classpath*:snomed-context.xml");
        if (useMongoDB()) {
            contextConfigLocations.add("classpath*:mongodb-context.xml");
            contextConfigLocations.add("classpath:term-mapper-mongodb-context.xml");
        }
        if (useElasticSearch()) {
            contextConfigLocations.add("classpath:term-mapper-es-context.xml");
        }
        contextConfigLocations.add("classpath:term-mapper-context.xml");
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(contextConfigLocations.toArray(new String[0]));
        applicationContext.registerShutdownHook();
        return applicationContext;
    }

    private void computeWeirdness(ApplicationContext context, Index index) {
        if (mappingArguments.getGeneralVocabularyPath() != null) {
            Map<String, Integer> allCollectionFreq = index.getGlobalFrequencies();
            Map<String, Integer> genWordFreq = new WordFreqListFileLoader(mappingArguments.getGeneralVocabularyPath(), "utf-8").loadWordList();
            Weirdness weirdness = new Weirdness();
            weirdness.computeWeirdness(genWordFreq, allCollectionFreq);
        }
    }

}
