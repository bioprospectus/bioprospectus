package net.lariverosc.mapping.main;

import ch.qos.logback.classic.Level;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import net.lariverosc.mapping.MappingAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MapInteractiveMain extends MapMainBase {

    private static final Logger log = LoggerFactory.getLogger(MapInteractiveMain.class);

    public static void main(String[] args) {
        try {
            new MapInteractiveMain(args);
        } catch (Exception e) {
            log.error("Fatal Error!!! {}", e);
        }
    }

    public MapInteractiveMain(String[] args) throws Exception {
        super(args);
    }

    @Override
    protected void execute() throws Exception {
        ClassPathXmlApplicationContext applicationContext = initSpringContext();

        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) org.slf4j.LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.TRACE);

        MappingAlgorithm mappingAlgorithm = applicationContext.getBean("mappingAlgorithm", MappingAlgorithm.class);
        setupTermSelectionStrategy(1, applicationContext);

        BufferedReader ir = new BufferedReader(new InputStreamReader(System.in));
        String inText;
        printMainMenu();
        while ((inText = ir.readLine()) != null && !inText.equalsIgnoreCase("quit!")) {
            try {
                int option = Integer.parseInt(inText);
                switch (option) {
                    case 1:
                        System.out.print("\nType text and press enter->");
                        inText = ir.readLine();
                        mappingAlgorithm.mapText(inText.trim());
                        break;
                    case 2:
                        System.out.println("Choose the term selection strategy");
                        System.out.println("1. exact-spell-stem");
                        System.out.println("2. exact-spell");
                        System.out.println("3. exact-stemming");
                        System.out.println("4. exact");
                        inText = ir.readLine();
                        int strategy = Integer.parseInt(inText);
                        setupTermSelectionStrategy(strategy, applicationContext);
                        break;
                    default:
                        System.out.println("Invalid option!!!");
                        break;
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Invalid option!!!");
            }
            printMainMenu();
        }
    }

    private void printMainMenu() {
        System.out.println("Welcome to Term-Mapper interactive shell");
        System.out.println("Choose an option:");
        System.out.println("1.Map");
        System.out.println("2.Setup term selection strategy");
        System.out.println("Or type quit! to exit.");
    }

    private ClassPathXmlApplicationContext initSpringContext() {
        log.info("Starting Spring application context");
        List<String> contextConfigLocations = new ArrayList<>();
        contextConfigLocations.add("classpath*:text-pipe-context.xml");
        contextConfigLocations.add("classpath*:snomed-context.xml");
        contextConfigLocations.add("classpath:term-mapper-context.xml");
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(contextConfigLocations.toArray(new String[0]));
        applicationContext.registerShutdownHook();
        return applicationContext;
    }

}
