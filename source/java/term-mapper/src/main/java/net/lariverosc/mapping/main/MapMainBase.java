package net.lariverosc.mapping.main;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import java.io.File;
import net.lariverosc.mapping.step.CandidateGenerationStep;
import net.lariverosc.mapping.terms.TermSelector;
import static net.lariverosc.util.EnvironmentUtils.*;
import net.lariverosc.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public abstract class MapMainBase {

    private static final Logger log = LoggerFactory.getLogger(MapMainBase.class);

    protected static final String TERM_MAPPER_HOME_ENV_NAME = "TERM_MAPPER_HOME";

    protected final MappingArguments mappingArguments;

    public MapMainBase(String[] args) throws Exception {
        checkEnv(TERM_MAPPER_HOME_ENV_NAME);
        this.mappingArguments = parseArguments(args);
        if (mappingArguments == null) {
            throw new ParameterException("You must provide valid parameters");
        }
        setUpEnvs(mappingArguments);
        execute();
    }

    private MappingArguments parseArguments(String[] args) {
        MappingArguments tempArguments = new MappingArguments();
        JCommander jCommander = new JCommander(tempArguments, args);
        jCommander.setProgramName("term-mapper");
        if (tempArguments.isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
        return tempArguments;
    }

    private void setUpEnvs(MappingArguments mappingArguments) {
        exportEnv("LANGUAGE", mappingArguments.getLanguage());
        exportEnv("INDEX_DIR", getIndexPath());
        exportEnv("MODELS_PATH", getDataPath());
        exportEnv("TERMINOLOGY_PATH", mappingArguments.getTerminologyPath());
        exportEnv("TERMINOLOGY_NAME", getTerminologyName());
        if (mappingArguments.getInPath() != null) {
            exportEnv("IN_PATH", mappingArguments.getInPath());
        }
        if (mappingArguments.getOutPath() != null) {
            exportEnv("OUT_PATH", mappingArguments.getOutPath());
        }
        if (mappingArguments.getMongoDbUrl() != null) {
            exportEnv("MONGO_URL", mappingArguments.getMongoDbUrl());
        }
        if (mappingArguments.getElasticSearchUrl() != null) {
            exportEnv("ES_URL", mappingArguments.getElasticSearchUrl());
        }
    }

    protected String getIndexPath() {
        return getEnvValue(TERM_MAPPER_HOME_ENV_NAME) + File.separator + "index";
    }

    protected String getDataPath() {
        return getEnvValue(TERM_MAPPER_HOME_ENV_NAME) + File.separator + "data";
    }

    protected String getTerminologyName() {
        return IOUtils.getFileName(mappingArguments.getTerminologyPath(), false);
    }

    protected String getTerminologyIndexPath() {
        return getIndexPath() + File.separator + getTerminologyName() + ".sobj";
    }

    protected String getSpellIndexPath() {
        return getIndexPath() + File.separator + "spell" + File.separator + getTerminologyName();
    }

    protected boolean useMongoDB() {
        return mappingArguments.getMongoDbUrl() != null;
    }

    protected boolean useElasticSearch() {
        return mappingArguments.getElasticSearchUrl() != null;
    }

    protected void setupTermSelectionStrategy(int paramTermSelectionStrategy, ClassPathXmlApplicationContext applicationContext) {
        int termSelectionStrategy = paramTermSelectionStrategy == 0 ? mappingArguments.getTermSelectionStrategy() : paramTermSelectionStrategy;
        CandidateGenerationStep candidateGenerationStep = applicationContext.getBean("candidateGenerationStep", CandidateGenerationStep.class);
        switch (termSelectionStrategy) {
            case 1:
                candidateGenerationStep.setUseSpellCheckSelection(true);
                candidateGenerationStep.setUseStemmingSelection(true);
                log.info("Using 1. exact-spell-stem term selection strategy");
                break;
            case 2:
                candidateGenerationStep.setUseSpellCheckSelection(true);
                candidateGenerationStep.setUseStemmingSelection(false);
                log.info("Using 2. exact-spell term selection strategy");
                break;
            case 3:
                candidateGenerationStep.setUseSpellCheckSelection(false);
                candidateGenerationStep.setUseStemmingSelection(true);
                log.info("Using 3. exact-stemming term selection strategy");
                break;
            case 4:
                candidateGenerationStep.setUseSpellCheckSelection(false);
                candidateGenerationStep.setUseStemmingSelection(false);
                log.info("Using 4. exact term selection strategy");
                break;
            default:
                log.info("Using default term selection strategy exact-spell-stem");
                break;
        }
    }

    protected abstract void execute() throws Exception;
}
