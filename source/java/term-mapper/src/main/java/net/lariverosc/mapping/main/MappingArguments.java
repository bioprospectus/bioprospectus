package net.lariverosc.mapping.main;

import com.beust.jcommander.Parameter;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MappingArguments {

    @Parameter(names = {"-l", "--lang"}, description = "The language", required = true)
    private String language;

    @Parameter(names = {"-t", "--terms"}, description = "The path to the file name will be used to store the index within TERM_MAPPER_HOME/index", required = true)
    private String terminologyPath = null;

    @Parameter(names = {"-g", "--gen-voc"}, description = "Optional: A file with additional vocabulary", required = false)
    private String generalVocabularyPath = null;

    @Parameter(names = {"-i", "--in-path"}, description = "The directory path where documents to map are located", required = false)
    private String inPath = null;

    @Parameter(names = {"-o", "--out-path"}, description = "The directory path where the mapppings will be located", required = false)
    private String outPath = null;

    @Parameter(names = {"-ts", "--term-select"}, description = "Optional: The term selection strategy between: "
        + "1. exact-spell-stem\n "
        + "2. exact-spell\n"
        + "3. exact-stemming\n"
        + "4. exact\n"
        + "default to 2", required = false)
    private int termSelectionStrategy = 1;

    @Parameter(names = "--mongodb-url", description = "If the documents to map are in mongo,the url in the form mongodb://localhost:27017/db.collection", required = false)
    private String mongoDbUrl = null;

    @Parameter(names = "--es-url", description = "If the mappings will be stored in elasticsearch,the url in the form http://localhost:9300", required = false)
    private String elasticSearchUrl = null;

    @Parameter(names = {"-nt", "--numThreads"}, description = "The number of parallel threads to run", required = false)
    private int numThreads = 1;

    @Parameter(names = {"-h", "--help"}, description = "print this message", help = true)
    private Boolean help = false;

    public String getLanguage() {
        return language;
    }

    public String getTerminologyPath() {
        return terminologyPath;
    }

    public String getGeneralVocabularyPath() {
        return generalVocabularyPath;
    }

    public String getInPath() {
        return inPath;
    }

    public String getOutPath() {
        return outPath;
    }

    public int getTermSelectionStrategy() {
        return termSelectionStrategy;
    }

    public String getMongoDbUrl() {
        return mongoDbUrl;
    }

    public String getElasticSearchUrl() {
        return elasticSearchUrl;
    }

    public int getNumThreads() {
        return numThreads;
    }

    public Boolean isHelp() {
        return help;
    }

}
