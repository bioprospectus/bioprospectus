package net.lariverosc.mapping.score;

import java.util.List;
import java.util.SortedSet;
import net.lariverosc.index.Index;
import net.lariverosc.index.weigthing.TFIDF;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class IdfMappingScore implements MappingScore {

    private final Logger log = LoggerFactory.getLogger(IdfMappingScore.class);


    private Index index;

    @Override
    public double getScore(TextChunk textChunk, Mapping mapping) {
        List<String> termTokens = mapping.getTermTokens();
        double idfSum = 0;
        double[] idfValues = new double[termTokens.size()];
        for (int i = 0; i < termTokens.size(); i++) {
            double tokenWeirdnessLog = TFIDF.getWeight(index, mapping.getTermId(), termTokens.get(i));
            idfValues[i] = tokenWeirdnessLog;
            idfSum += tokenWeirdnessLog;
        }
        SortedSet<Integer> termMatchesPositions = mapping.getTermMatchesPositions();
        double idfWeigth = 0;
        for (Integer matchPosition: termMatchesPositions) {
            idfWeigth += idfValues[matchPosition];
        }
        return idfWeigth / (idfSum + 1);
    }

    public void setIndex(Index index) {
        this.index = index;
    }

}
