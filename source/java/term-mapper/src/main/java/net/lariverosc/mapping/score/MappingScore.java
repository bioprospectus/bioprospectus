package net.lariverosc.mapping.score;

import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface MappingScore {

    double getScore(TextChunk textChunk, Mapping mapping);

}
