package net.lariverosc.mapping.score;

import java.util.Collection;
import net.lariverosc.index.Index;
import net.lariverosc.mapping.data.Match;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class StructuralMappingScore implements MappingScore {

    private static final Logger log = LoggerFactory.getLogger(StructuralMappingScore.class);

    private Index index;

    @Override
    public double getScore(TextChunk textChunk, Mapping mapping) {
        double variation = getVariation(mapping);
        double coverage = getCoverage(mapping);
        double dispersion = getDispersion(mapping);
        return 0.7 * coverage + 0.2 * variation + 0.1 * dispersion;
    }

    /**
     * Returns the variation of the given match group.
     *
     * @param mapping
     *
     * @return the match group variation
     */
    private double getVariation(Mapping mapping) {
        Collection<Match> allMatches = mapping.getAllMatches();
        double totalVariation = 0;
        for (Match match: allMatches) {
            switch (match.getMatchType()) {
                case EXACT:
                    break;
                case SPELLED:
                    totalVariation += 1;
                    break;
                case STEM:
                    totalVariation += 2;
                    break;
            }
        }
        if (totalVariation == 0) {
            return 1;
        }
        return 1 / (totalVariation / (double) mapping.getTermMatches() + 1);

    }

    /**
     * Returns the variation of the given match group.
     *
     * @param mapping
     *
     * @return
     */
    public double getCoverage(Mapping mapping) {
        int termNumWords = index.getDocumentNumTerms(mapping.getTermId());
        return (double) mapping.getChunkMatches() / (double) termNumWords;
    }

    /**
     * Returns the dispersion in the given match group, this value is obtained
     * as the inverse of the sum over the distance between the normalized
     * chunk position and the position within the term.
     *
     * The normalized chunk position is the obtained by subtract the first
     * chunk position from the match chunk position.
     *
     * @param mapping
     *
     * @return
     */
    public double getDispersion(Mapping mapping) {
        double sparse = 0;
        int firstChunkPosition = mapping.getFirstChunkPosition();

        Collection<Match> allMatches = mapping.getAllMatches();
        for (Match match: allMatches) {
            int positionInTerm = match.getTermPosition();
            int normPositionInChunk = match.getChunkPosition() - firstChunkPosition;
            sparse += Math.abs(positionInTerm - normPositionInChunk);
        }
        return 1 / (sparse + 1);
    }

    private double getCohesiveness(Mapping mapping) {
        return 0;
    }

    public void setIndex(Index index) {
        this.index = index;
    }

}
