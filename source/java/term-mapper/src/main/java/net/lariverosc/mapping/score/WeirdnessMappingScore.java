package net.lariverosc.mapping.score;

import java.util.List;
import java.util.SortedSet;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.weirdness.data.WeirdnessManager;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class WeirdnessMappingScore implements MappingScore {

    private final Logger log = LoggerFactory.getLogger(WeirdnessMappingScore.class);

    private WeirdnessManager weirdnessManager;

    @Override
    public double getScore(TextChunk textChunk, Mapping mapping) {
        double weirnessWeight = getWeirdnessWeight(textChunk, mapping);
        return weirnessWeight;
    }

    public double getWeirdnessWeight(TextChunk textChunk, Mapping mapping) {
        List<String> termTokens = mapping.getTermTokens();
        double weirdnessSum = 0;
        double[] weirdnessValues = new double[termTokens.size()];

        for (int i = 0; i < termTokens.size(); i++) {

            double tokenWeirdnessLog = Math.log(weirdnessManager.getValue(termTokens.get(i)) + 1);
            weirdnessValues[i] = tokenWeirdnessLog;
            weirdnessSum += tokenWeirdnessLog;
        }

        SortedSet<Integer> termMatchesPositions = mapping.getTermMatchesPositions();
        double weirdnessWeigth = 0;
        for (Integer matchPosition : termMatchesPositions) {
            weirdnessWeigth += weirdnessValues[matchPosition];
        }
        return weirdnessWeigth / (weirdnessSum + 1);
    }

    public void setWeirdnessManager(WeirdnessManager weirdnessManager) {
        this.weirdnessManager = weirdnessManager;
    }

}
