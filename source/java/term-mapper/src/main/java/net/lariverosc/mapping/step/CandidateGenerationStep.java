package net.lariverosc.mapping.step;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.index.Index;
import net.lariverosc.index.TermOccurrence;
import net.lariverosc.mapping.data.Match;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.terms.ExactTermSelector;
import net.lariverosc.mapping.terms.SpellTermSelector;
import net.lariverosc.mapping.terms.StemTermSelector;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CandidateGenerationStep {

    private final Logger log = LoggerFactory.getLogger(CandidateGenerationStep.class);

    private Index index;

    private ExactTermSelector exactTermSelector;

    private SpellTermSelector spellTermSelector;

    private StemTermSelector stemTermSelector;

    private boolean USE_SPELL_CHECKER = true;

    private boolean USE_STEMMER = false;

    public Set<Mapping> getCandidates(TextChunk textChunk) {
        //Build whole term match map using the inverted index
        Map<String, Mapping> wholeMappings = buildWholeMappings(textChunk);
        
        //Build partial chunk matches spliting the whole chunk matches according to term length
        Set<Mapping> candidateMappings = buildPartialMappings(wholeMappings);

        if (log.isTraceEnabled()) {
            logCandidates(wholeMappings.values());
        }
        return candidateMappings;
    }

    protected Map<String, Mapping> buildWholeMappings(TextChunk textChunk) {
        Map<String, Mapping> wholeMappings = new HashMap<>();
        List<TextToken> tokens = textChunk.getTokens();
        for (TextToken token: tokens) {
            switch (token.getType()) {
                case ABBREVIATION:
                    processAbbreviations(wholeMappings, token);
                    break;
                case ACRONYM:
                    break;
                case ALPHANUMERIC:
                    processAlphanum(wholeMappings, token);
                    break;
                case WORD:
                    processWords(wholeMappings, token);
                    break;
                case STOP:
                    break;
                default:
                    break;
            }
        }
        return wholeMappings;
    }

    private void processAbbreviations(final Map<String, Mapping> wholeMappings, final TextToken textToken) {
        String[] split = textToken.getPayload().split(":");
        String termId = split[1];
        String term = split[2];
        Mapping mapping = new Mapping(termId, term, Arrays.asList(split));
        mapping.addMatch(new Match(textToken.getIndex(), 0, Match.Type.EXACT));
        mapping.setScore(1d);
        wholeMappings.put(termId, mapping);
    }

    private void processWords(final Map<String, Mapping> wholeMappings, final TextToken textToken) {
        final String token = textToken.getText();
        Map<String, Set<TermOccurrence>> occurrencesByToken1 = exactTermSelector.selectTerms(token);
        if (!occurrencesByToken1.isEmpty()) {
            addMappings(wholeMappings, textToken.getIndex(), Match.Type.EXACT, occurrencesByToken1);
        } else {
            log.debug("Not exact candidates found for word {}", token);
        }

        if (USE_SPELL_CHECKER) {
            Map<String, Set<TermOccurrence>> occurrencesByToken2 = spellTermSelector.selectTerms(token);
            if (!occurrencesByToken2.isEmpty()) {
                addMappings(wholeMappings, textToken.getIndex(), Match.Type.SPELLED, occurrencesByToken2);
            } else {
                log.debug("Not spelling candidates found for word {}", token);
            }
        }

        if (USE_STEMMER) {
            Map<String, Set<TermOccurrence>> occurrencesByToken3 = stemTermSelector.selectTerms(token);
            if (!occurrencesByToken3.isEmpty()) {
                addMappings(wholeMappings, textToken.getIndex(), Match.Type.STEM, occurrencesByToken3);
            } else {
                log.debug("Not stemming candidates found for word {}", token);
            }
        }
    }

    private void processAlphanum(final Map<String, Mapping> wholeMappings, final TextToken textToken) {
        String token = textToken.getText();
        Match.Type matchType = Match.Type.EXACT;
        Map<String, Set<TermOccurrence>> occurrencesByTerm = exactTermSelector.selectTerms(token);
        if (!occurrencesByTerm.isEmpty()) {
            addMappings(wholeMappings, textToken.getIndex(), matchType, occurrencesByTerm);
        } else {
            log.debug("Not candidates found for Alphanum {}", token);
        }

    }

    private void addMappings(final Map<String, Mapping> wholeMappings, final int candidatePosition, final Match.Type matchType, final Map<String, Set<TermOccurrence>> occurrencesByTerm) {
        for (String candidateTerm: occurrencesByTerm.keySet()) {
            //for each ocurrence
            for (TermOccurrence candidateOccurrence: occurrencesByTerm.get(candidateTerm)) {
                //Create a new match
                Match match = new Match(candidatePosition, candidateOccurrence.getPosition(), matchType);
                //Verify if the term was already matched
                Mapping mapping = wholeMappings.get(candidateOccurrence.getDocumentId());
                //If not was already matched create a new mapping
                if (mapping == null) {
                    mapping = new Mapping(candidateOccurrence.getDocumentId(), index.getDocument(candidateOccurrence.getDocumentId()), index.getDocumentTokens(candidateOccurrence.getDocumentId()));
                }
                //Add the match to the mapping
                mapping.addMatch(match);
                //Update or create the mapping entry within the whole chunk mapping
                wholeMappings.put(mapping.getTermId(), mapping);
            }
        }
    }

    /**
     *
     * @param wholeMappings
     *
     * @return
     */
    protected Set<Mapping> buildPartialMappings(Map<String, Mapping> wholeMappings) {
        Set<Mapping> partialMappings = new HashSet<>();
        for (Map.Entry<String, Mapping> wholeMappingEntry: wholeMappings.entrySet()) {
            String termId = wholeMappingEntry.getKey();
            Mapping wholeMapping = wholeMappingEntry.getValue();
            int termTotalWords = index.getDocumentNumTerms(termId);
            processWholeMapping(partialMappings, wholeMapping, termTotalWords);
        }
        return partialMappings;
    }

    private void processWholeMapping(Set<Mapping> partialMappings, Mapping wholeMapping, int termTotalWords) {

        int lastChunkMatchPosition = -1;

        Mapping tempMapping = new Mapping(wholeMapping);

        for (Iterator<Integer> chunkIterator = wholeMapping.iteratorByChunkPosition(); chunkIterator.hasNext();) {

            int chunkMatchPosition = chunkIterator.next();

            for (Match match: wholeMapping.getMatchesByChunkPosition(chunkMatchPosition)) {

                int chunkGapSize = lastChunkMatchPosition != -1 ? chunkMatchPosition - lastChunkMatchPosition : 0;
                //the chunk gap is great that the term then split the mapping
                if (chunkGapSize > termTotalWords) {
                    partialMappings.add(tempMapping);
                    tempMapping = new Mapping(wholeMapping);
                }

                //the term token was already matched then split the mapping
                if (tempMapping.containsMatchesInTermPosition(match.getTermPosition())) {
                    partialMappings.add(tempMapping);
                    tempMapping = new Mapping(wholeMapping);
                }
                tempMapping.addMatch(match);
            }
            //Update last chunk match position
            lastChunkMatchPosition = chunkMatchPosition;
        }

        partialMappings.add(tempMapping);
    }

    private void logCandidates(Collection<Mapping> candidateMappings) {
        log.trace("Found {} canditate terms in chunk.", candidateMappings.size());
        StringBuilder sb = new StringBuilder();
        for (Mapping mapping: candidateMappings) {
            sb.append(mapping.getTermId()).append(" -- ").append(mapping.getTerm()).append("\n");
        }
        log.trace("\n" + sb.toString());
    }

    public void setIndex(Index index) {
        this.index = index;
    }

    public void setExactTermSelector(ExactTermSelector exactTermSelector) {
        this.exactTermSelector = exactTermSelector;
    }

    public void setSpellTermSelector(SpellTermSelector spellTermSelector) {
        this.spellTermSelector = spellTermSelector;
    }

    public void setStemTermSelector(StemTermSelector stemTermSelector) {
        this.stemTermSelector = stemTermSelector;
    }

    public void setUseSpellCheckSelection(boolean USE_SPELL_CHECKER) {
        this.USE_SPELL_CHECKER = USE_SPELL_CHECKER;
    }

    public void setUseStemmingSelection(boolean USE_STEMMER) {
        this.USE_STEMMER = USE_STEMMER;
    }

}
