package net.lariverosc.mapping.step;

import java.util.Set;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.score.MappingScore;
import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CandidateScoringStep {

    private MappingScore mappingScore;

    /**
     *
     * @param textChunk
     * @param candidateMappings
     *
     */
    public void evaluate(TextChunk textChunk, Set<Mapping> candidateMappings) {
        for (Mapping mapping: candidateMappings) {
            if (mapping.getScore() == -1) {
                mapping.setScore(mappingScore.getScore(textChunk, mapping));
            }
        }
    }

    public void setMappingScore(MappingScore mappingScore) {
        this.mappingScore = mappingScore;
    }

}
