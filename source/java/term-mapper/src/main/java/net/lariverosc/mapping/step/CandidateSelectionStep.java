package net.lariverosc.mapping.step;

import java.util.List;
import java.util.Set;
import net.lariverosc.mapping.filters.CandidateFilter;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.TextChunk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CandidateSelectionStep {

    private final Logger log = LoggerFactory.getLogger(CandidateSelectionStep.class);

    private List<CandidateFilter> filters;

    public Set<Mapping> filterCandidates(TextChunk textChunk, Set<Mapping> candidateMappings) {
        if (log.isTraceEnabled()) {
            log.trace("INITIAL MATCH CANDIDATES");
            logCandidateSet(textChunk.getTokens(), candidateMappings);
        }
        for (CandidateFilter filter : filters) {
            candidateMappings = filter.doFilter(textChunk, candidateMappings);
            if (log.isTraceEnabled()) {
                log.trace("MATCH CANDIDATES AFTER FILTER: {}", filter.getClass().getName());
                logCandidateSet(textChunk.getTokens(), candidateMappings);
            }
        }
        return candidateMappings;
    }

    private void logCandidateSet(List<TextToken> chunkTokens, Set<Mapping> candidateMappings) {
        for (Mapping mapping : candidateMappings) {
            log.trace(mapping.toString());
        }
    }

    public void setFilters(List<CandidateFilter> filters) {
        this.filters = filters;
    }
}
