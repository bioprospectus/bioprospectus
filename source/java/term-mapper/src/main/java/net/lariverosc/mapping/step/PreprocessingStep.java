package net.lariverosc.mapping.step;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.nlp.Chunker;
import net.lariverosc.text.nlp.SentenceDetector;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;
import net.lariverosc.text.spell.SpellChecker;
import net.lariverosc.text.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class PreprocessingStep {

    private final Logger log = LoggerFactory.getLogger(PreprocessingStep.class);

    private SentenceDetector sentenceDetector;

    private Chunker chunker;

    private final TextPipe textPipe;

    public PreprocessingStep() {
        TextPipeConfig textPipeConfig = new TextPipeConfig().withLanguage(TextPipeConfig.Language.ENGLISH)
            .withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD)
            .withTokenMatchers(
                TextPipeConfig.TokenMatcher.ACRONYM,
                TextPipeConfig.TokenMatcher.DECIMAL_NUMBER,
                TextPipeConfig.TokenMatcher.PERCENTAGE,
                TextPipeConfig.TokenMatcher.ROMAN_NUMBER,
                TextPipeConfig.TokenMatcher.GENE,
                TextPipeConfig.TokenMatcher.STOP
            )
            .addTokenTransform(TextPipeConfig.TokenTransform.LOWER_CASE)
            .withTokenFilters(TextPipeConfig.TokenFilter.LENGTH, TextPipeConfig.TokenFilter.STOP_WORD)
            .withMaxTokenLength(25);

//            .addDictionary(getDictionaryConfig())
//            .addAbbreviationsDictionary(getAbbreviationsConfig());
        textPipe = new TextPipe(textPipeConfig);
    }

    public List<TextChunk> doAnalysis(String text) {
        List<TextChunk> textChunks = new ArrayList<TextChunk>();
        int lineOffSet = 0;
        List<String> lines = StringUtils.getLines(text);
        for (String line: lines) {
            TextChunk[] sentences = sentenceDetector.getSentences(line);
            for (TextChunk sentence: sentences) {
                TextChunk textChunk = textPipe.proccess(sentence.getText());
                textChunk.setTextSpan(new TextSpan(sentence.getTextSpan(), lineOffSet));
                log.trace("TextChunk after textPipe:\n{}", textChunk);
                textChunks.add(textChunk);
            }
            lineOffSet += line.length() + 1;
        }
        return textChunks;
    }

    private static TextPipeConfig.DictionaryConfig getDictionaryConfig() {
        Set<String> terms = new HashSet<String>();
        terms.add("dr");
        return new TextPipeConfig.DictionaryConfig("Ignore", terms);
    }

    private static TextPipeConfig.AbbreviationsConfig getAbbreviationsConfig() {
        Map<String, String> abbreviations = new HashMap<String, String>();
        abbreviations.put("MR", "1230641019:Mitral regurgitaion");
        abbreviations.put("OP", "499977011:Osteoporosis");
        abbreviations.put("TR", "2744307018:T regurgitaion");
        return new TextPipeConfig.AbbreviationsConfig("SnomedAbbr", abbreviations, true);
    }

    public void setSentenceDetector(SentenceDetector sentenceDetector) {
        this.sentenceDetector = sentenceDetector;
    }

    public void setChunker(Chunker chunker) {
        this.chunker = chunker;
    }

}
