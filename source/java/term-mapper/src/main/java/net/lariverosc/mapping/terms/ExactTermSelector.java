package net.lariverosc.mapping.terms;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.lariverosc.index.Index;
import net.lariverosc.index.TermOccurrence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ExactTermSelector implements TermSelector {

    private final Logger log = LoggerFactory.getLogger(ExactTermSelector.class);

    private Index index;

    @Override
    public Map<String, Set<TermOccurrence>> selectTerms(String term) {
        Set<TermOccurrence> termOcurrences = index.lookup(term);
        if (!termOcurrences.isEmpty()) {
            log.info("Generating stem in Exact candidates for word: {}, found [{}]", term, termOcurrences.size());
            Map<String, Set<TermOccurrence>> ocurrencesByTerm = new HashMap<>();
            ocurrencesByTerm.put(term, termOcurrences);
            return ocurrencesByTerm;
        }
        return Collections.emptyMap();
    }

    public void setIndex(Index index) {
        this.index = index;
    }

}
