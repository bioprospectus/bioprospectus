package net.lariverosc.mapping.terms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.index.Index;
import net.lariverosc.index.TermOccurrence;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.spell.SpellChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SpellTermSelector implements TermSelector {

    private final Logger log = LoggerFactory.getLogger(SpellTermSelector.class);

    private Index index;

    private SpellChecker spellChecker;

    @Override
    public Map<String, Set<TermOccurrence>> selectTerms(String term) {
        String[] similarWords = spellChecker.similarWords(term, 1);
        Map<String, Set<TermOccurrence>> ocurrencesByToken = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        for (String similarTerm: similarWords) {
            Set<TermOccurrence> termOccurrences = index.lookup(similarTerm);
            if (!termOccurrences.isEmpty()) {
                ocurrencesByToken.put(similarTerm, termOccurrences);
                sb.append("[").append(similarTerm).append(",").append(termOccurrences.size()).append("]");
            }
        }
        log.debug("Generating spelling candidates for word: {}, found {}", term, sb.toString());
        return ocurrencesByToken;
    }

    public void setIndex(Index index) {
        this.index = index;
    }

    public void setSpellChecker(SpellChecker spellChecker) {
        this.spellChecker = spellChecker;
    }

}
