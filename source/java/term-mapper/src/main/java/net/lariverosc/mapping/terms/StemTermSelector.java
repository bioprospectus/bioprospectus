package net.lariverosc.mapping.terms;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.lariverosc.index.Index;
import net.lariverosc.index.TermOccurrence;
import net.lariverosc.text.stemming.Stemmer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class StemTermSelector implements TermSelector {

    private final Logger log = LoggerFactory.getLogger(StemTermSelector.class);

    private Index index;

    private Stemmer stemmer;

    @Override
    public Map<String, Set<TermOccurrence>> selectTerms(String term) {
        String stem = stemmer.stem(term);
        Set<String> commonStemTerms = index.getGroupTerms(stem);
        Map<String, Set<TermOccurrence>> ocurrencesByTerm = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        for (String realTerm: commonStemTerms) {
            Set<TermOccurrence> termOccurrences = index.lookup(realTerm);
            if (!termOccurrences.isEmpty()) {
                ocurrencesByTerm.put(realTerm, termOccurrences);
                sb.append("[").append(realTerm).append(",").append(termOccurrences.size()).append("]");
            }
        }
        log.debug("Generating stem candidates for word: {}, found {}", term, sb.toString());
        return ocurrencesByTerm;
    }

    public void setIndex(Index index) {
        this.index = index;
    }

    public void setStemmer(Stemmer stemmer) {
        this.stemmer = stemmer;
    }

}
