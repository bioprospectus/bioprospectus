package net.lariverosc.mapping.terms;

import java.util.Map;
import java.util.Set;
import net.lariverosc.index.TermOccurrence;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface TermSelector {

    Map<String, Set<TermOccurrence>> selectTerms(String term);
}
