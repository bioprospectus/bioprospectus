package net.lariverosc.weirdness;

import java.util.HashMap;
import java.util.Map;
import net.lariverosc.weirdness.data.WeirdnessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO 
 * Weirdness is weak because it cannot manage not previously viewed vocabulary and requires large collections.
 * Is better to use TD-IDF instead
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Weirdness {

    private final Logger log = LoggerFactory.getLogger(Weirdness.class);

    private WeirdnessManager weirdnessManager;

    private int countAllWords(Map<String, Integer> dictionary) {
        int total = 0;
        for (Integer freq : dictionary.values()) {
            total += freq;
        }
        return total;
    }

    public void computeWeirdness(Map<String, Integer> generalDict, Map<String, Integer> specialistDict) {
        log.info("Computing weirdness");
        long generalTotalWords = countAllWords(generalDict);
        long specialistTotalWords = countAllWords(specialistDict);

        Map<String, Double> weirdness = new HashMap<String, Double>();
        double val, max = 0;
        for (String word : specialistDict.keySet()) {
            long currentSpecialist = specialistDict.containsKey(word) ? specialistDict.get(word) : 0;
            long currentGeneral = generalDict.containsKey(word) ? generalDict.get(word) : 1;
            
            val = ((double) currentSpecialist / (double) specialistTotalWords) / ((double) currentGeneral / (double) generalTotalWords);
            weirdness.put(word, val);
            max = val > max ? val : max;
        }

//        for (String word : specialistDict.keySet()) {
//            weirdness.put(word, weirdness.get(word) / max);
//        }

        weirdnessManager.storeValues(weirdness);
    }

    public void setWeirdnessManager(WeirdnessManager weirdnessManager) {
        this.weirdnessManager = weirdnessManager;
    }
}
