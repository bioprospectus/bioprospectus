package net.lariverosc.weirdness;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import net.lariverosc.collection.CollectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class WordFreqListFileLoader implements WordFreqListLoader {

    private final Logger log = LoggerFactory.getLogger(WordFreqListFileLoader.class);

    private final String filePath;

    private final String encoding;

    public WordFreqListFileLoader(String filePath, String encoding) {
        this.filePath = filePath;
        this.encoding = encoding;
    }

    @Override
    public Map<String, Integer> loadWordList() {
        log.info("Loading file {} ...", filePath);
        BufferedReader bufferedReader = null;
        try {
            Map<String, Integer> temp = new HashMap< String, Integer>();
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), encoding));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineSplit = line.split("\t");
                temp.put(lineSplit[0].trim(), Integer.parseInt(lineSplit[1]));
            }
            bufferedReader.close();
            return temp;
        } catch (IOException ioe) {
            log.error("Error while loading the collection", ioe);
            throw new CollectionException("Error while loading the collection file" + filePath, ioe);
        } catch (NumberFormatException nfe) {
            log.error("Error while loading the collection", nfe);
            throw new CollectionException("Error while loading the collection file" + filePath, nfe);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ioe) {
                }
            }
        }
    }
}
