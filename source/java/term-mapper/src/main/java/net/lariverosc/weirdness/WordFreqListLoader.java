package net.lariverosc.weirdness;

import java.util.Map;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface WordFreqListLoader {

    Map<String, Integer> loadWordList();
}
