package net.lariverosc.weirdness.data;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CachedWeirdnessManager implements WeirdnessManager {

    private final Logger log = LoggerFactory.getLogger(CachedWeirdnessManager.class);

    private WeirdnessManager weirdnessManager;

    private Map<String, Double> weirdnessCache;

    public CachedWeirdnessManager(WeirdnessManager weirdnessManager) {
        this.weirdnessManager = weirdnessManager;
        preFetchValues();
    }

    private void preFetchValues() {
        log.info("Pre-fetching all available weirdness values");
        weirdnessCache = new ConcurrentHashMap<String, Double>(getAllValues());
    }

    @Override
    public void storeValues(Map<String, Double> weirdnessMap) {
        weirdnessManager.storeValues(weirdnessMap);
    }

    @Override
    public double getValue(String word) {
        Double weirdness = weirdnessCache.get(word);
        if (weirdness == null) {
            return 1;
        }
        return weirdness;
    }

    @Override
    public Map<String, Double> getAllValues() {
        return weirdnessManager.getAllValues();
    }
}
