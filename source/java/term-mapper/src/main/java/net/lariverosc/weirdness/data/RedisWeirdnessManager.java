package net.lariverosc.weirdness.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class RedisWeirdnessManager implements WeirdnessManager {

    private final Logger log = LoggerFactory.getLogger(RedisWeirdnessManager.class);

    private final String REDIS_NAME_SPACE = "WEIRDNESS";

    private final JedisPool jedisPool;

    public RedisWeirdnessManager(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    @Override
    public void storeValues(Map<String, Double> weirdnessMap) {
        Jedis jedis = jedisPool.getResource();
        Pipeline pipeline = jedis.pipelined();
        pipeline.multi();
        for (Map.Entry<String, Double> entry : weirdnessMap.entrySet()) {
            pipeline.hset(REDIS_NAME_SPACE, String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
        }
        Response<List<Object>> response = pipeline.exec();
        jedisPool.returnResource(jedis);
    }

    @Override
    public double getValue(String word) {
        Jedis jedis = jedisPool.getResource();
        String weirdnessStr = jedis.hget(REDIS_NAME_SPACE, word);
        double weirdness = 1d;
        if (weirdnessStr == null) {
            log.warn("weirdness not found for conceptId {}", word);
        }else{
            weirdness = Double.parseDouble(weirdnessStr);
        }
        jedisPool.returnResource(jedis);
        return weirdness;
    }

    @Override
    public Map<String, Double> getAllValues() {
        Map<String, Double> allIcValues = new HashMap<String, Double>();
        Jedis jedis = jedisPool.getResource();
        Map<String, String> tempMap = jedis.hgetAll(REDIS_NAME_SPACE);
        for (Map.Entry<String, String> entry : tempMap.entrySet()) {
            allIcValues.put(entry.getKey(), Double.parseDouble(entry.getValue()));
        }
        jedisPool.returnResource(jedis);
        return allIcValues;
    }
}
