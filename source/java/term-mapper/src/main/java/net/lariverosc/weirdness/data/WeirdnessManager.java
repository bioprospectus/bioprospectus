package net.lariverosc.weirdness.data;

import java.util.Map;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface WeirdnessManager {

    void storeValues(Map<String, Double> weirdnessMap);

    double getValue(String word);

    Map<String, Double> getAllValues();
}
