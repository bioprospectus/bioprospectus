package net.lariverosc.mapping;

import net.lariverosc.text.nlp.SentenceDetector;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 *
 * All tests in this class take as base the following mock collection, which is
 * initialized within {@link MockCollectionLoader}.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MappingAlgorithmTest {

    private SentenceDetector getMockSentenceDetector() {
        SentenceDetector sentenceDetector = mock(SentenceDetector.class);
        when(sentenceDetector.getSentencesAsString(anyString())).thenAnswer(new Answer<String[]>() {
            @Override
            public String[] answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return new String[]{(String) args[0]};
            }
        });
        return sentenceDetector;
    }
}
