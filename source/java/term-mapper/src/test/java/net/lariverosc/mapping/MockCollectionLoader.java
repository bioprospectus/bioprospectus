package net.lariverosc.mapping;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import net.lariverosc.collection.CollectionLoader;

/**
 * Mock collection loader
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 *
 * term 1 -> "11word 12word 13word 14word"
 *
 * term 2 -> "21word 22word 23word 24word 25word 26word"
 *
 * term 3 -> "31word 32word 33word 34word 31word 32word 33word 34word"
 *
 * term 4 -> "41word 42word 41word 42word 41word 42word 41word 42word"
 *
 * term 5 -> "58word 57word 56word 55word 54word 53word 52word 51word"
 *
 * term 6 -> "61word 63word 65word 67word 69word"
 *
 * term 7 -> "71word 71word 71word 72word 72word"
 *
 * term 8 -> "81word 82word 83word"
 *
 * term 9 -> "91word 92word 91word"
 *
 * term 10 -> "101word 102word 101word 103word"
 */
public class MockCollectionLoader implements CollectionLoader {

    private Map<String, String> collection;

    public MockCollectionLoader() {
        collection = new HashMap<String, String>();
        collection.put("1", "11word 12word 13word 14word");
        collection.put("2", "21word 22word 23word 24word 25word 26word");
        collection.put("3", "31word 32word 33word 34word 31word 32word 33word 34word");
        collection.put("4", "41word 42word 41word 42word 41word 42word 41word 42word");
        collection.put("5", "58word 57word 56word 55word 54word 53word 52word 51word");
        collection.put("6", "61word 63word 65word 67word 69word");
        collection.put("7", "71word 71word 71word 72word 72word");
        collection.put("8", "81word 82word 83word");
        collection.put("9", "91word 92word 91word");
        collection.put("10", "101word 102word 101word 103word");
    }

    @Override
    public String getCollectionName() {
        return "mock-collection";
    }

    @Override
    public Map<String, String> getCollection() {
        return collection;
    }

}
