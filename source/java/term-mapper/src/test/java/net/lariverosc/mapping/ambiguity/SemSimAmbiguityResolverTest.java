package net.lariverosc.mapping.ambiguity;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.knowledge.entity.EntityKnowledgeProvider;
import net.lariverosc.mapping.score.StructuralMappingScore;
import net.lariverosc.mapping.data.Match;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.step.TestUtil;
import net.lariverosc.semsim.data.SemanticSimilarityManager;
import org.testng.annotations.Test;
import org.testng.Assert;
import static org.mockito.Mockito.*;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SemSimAmbiguityResolverTest {

    @Test
    public void shouldNotDetectAmbiguityBecasuePosition() {
        //Setup match data
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.5);
        Mapping mapping2 = new Mapping("2", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(2, 2)}).withScore(0.5);
        Collections.addAll(mappings, mapping1, mapping2);

        SemSimAmbiguityResolver ambiguityResolver = new SemSimAmbiguityResolver();

        Set<Mapping> unAmbiguousMappings = ambiguityResolver.resolve(null, mappings);

        Assert.assertEquals(unAmbiguousMappings.size(), 2);
        Assert.assertTrue(unAmbiguousMappings.contains(mapping1));
        Assert.assertTrue(unAmbiguousMappings.contains(mapping2));
    }

    @Test
    public void shouldNotDetectAmbiguityBecauseScore() {
        //Setup match data
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.5);
        Mapping mapping2 = new Mapping("2", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.7);
        Collections.addAll(mappings, mapping1, mapping2);

        SemSimAmbiguityResolver ambiguityResolver = new SemSimAmbiguityResolver();

        Set<Mapping> unAmbiguousMappings = ambiguityResolver.resolve(null, mappings);

        Assert.assertEquals(unAmbiguousMappings.size(), 2);
        Assert.assertTrue(unAmbiguousMappings.contains(mapping1));
        Assert.assertTrue(unAmbiguousMappings.contains(mapping2));
    }

    @Test
    public void shouldSolveAmbiguityWithAnyBecauseSameConcept() {
        //Setup knowledsource data
        Concept concept1 = new Concept().withId("1");
        EntityKnowledgeProvider mockEntityKnowledgeProvider = mock(EntityKnowledgeProvider.class);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("1"))).thenReturn(concept1);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("11"))).thenReturn(concept1);

        //Setup match data
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.7);
        Mapping mapping2 = new Mapping("11", "term11", TestUtil.getTokens("term11")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.7);
        Collections.addAll(mappings, mapping1, mapping2);

        SemSimAmbiguityResolver ambiguityResolver = new SemSimAmbiguityResolver();
        ambiguityResolver.setEntityKnowledgeProvider(mockEntityKnowledgeProvider);

        Set<Mapping> unAmbiguousMappings = ambiguityResolver.resolve(null, mappings);

        Assert.assertEquals(unAmbiguousMappings.size(), 1);
        Assert.assertTrue(unAmbiguousMappings.contains(mapping1) || unAmbiguousMappings.contains(mapping2));
    }

    @Test
    public void shouldResolveAmbiguity() {
        //Setup knowledsource data
        Concept concept1 = new Concept().withId("1");
        Concept concept2 = new Concept().withId("2");
        Concept concept3 = new Concept().withId("3");

        EntityKnowledgeProvider mockEntityKnowledgeProvider = mock(EntityKnowledgeProvider.class);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("1"))).thenReturn(concept1);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("2"))).thenReturn(concept2);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("3"))).thenReturn(concept3);

        SemanticSimilarityManager mockSemanticSimilarityManager = mock(SemanticSimilarityManager.class);
        when(mockSemanticSimilarityManager.getValue(eq(concept1), eq(concept3))).thenReturn(0.2d);
        when(mockSemanticSimilarityManager.getValue(eq(concept2), eq(concept3))).thenReturn(0.8d);
        when(mockSemanticSimilarityManager.getValue(eq(concept1), eq(concept2))).thenReturn(0d);

        //Setup match data
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.5);
        Mapping mapping2 = new Mapping("2", "term2", TestUtil.getTokens("term2")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.5);
        Mapping mapping3 = new Mapping("3", "term3", TestUtil.getTokens("term3")).withMatches(new Match[]{new Match(2, 2)}).withScore(0.5);
        Collections.addAll(mappings, mapping1, mapping2, mapping3);

        SemSimAmbiguityResolver ambiguityResolver = new SemSimAmbiguityResolver();
        ambiguityResolver.setEntityKnowledgeProvider(mockEntityKnowledgeProvider);
        ambiguityResolver.setSemanticSimilarityManager(mockSemanticSimilarityManager);

        Set<Mapping> unAmbiguousMappings = ambiguityResolver.resolve(null, mappings);

        Assert.assertEquals(unAmbiguousMappings.size(), 2);
        Assert.assertFalse(unAmbiguousMappings.contains(mapping1));
        Assert.assertTrue(unAmbiguousMappings.contains(mapping2));
        Assert.assertTrue(unAmbiguousMappings.contains(mapping3));
    }

    @Test
    public void shouldSolveAmbiguityWithAnyBecauseConceptsAreEquallySimilarToContext() {
        //Setup knowledsource data
        Concept concept1 = new Concept().withId("1");
        Concept concept2 = new Concept().withId("2");
        Concept concept3 = new Concept().withId("3");

        EntityKnowledgeProvider mockEntityKnowledgeProvider = mock(EntityKnowledgeProvider.class);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("1"))).thenReturn(concept1);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("2"))).thenReturn(concept2);
        when(mockEntityKnowledgeProvider.getConceptByTermId(eq("3"))).thenReturn(concept3);

        SemanticSimilarityManager mockSemanticSimilarityManager = mock(SemanticSimilarityManager.class);
        when(mockSemanticSimilarityManager.getValue(eq(concept1), eq(concept3))).thenReturn(0.3d);
        when(mockSemanticSimilarityManager.getValue(eq(concept2), eq(concept3))).thenReturn(0.3d);
        when(mockSemanticSimilarityManager.getValue(eq(concept1), eq(concept2))).thenReturn(0d);

        //Setup match data
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.7);
        Mapping mapping2 = new Mapping("2", "term2", TestUtil.getTokens("term2")).withMatches(new Match[]{new Match(1, 1)}).withScore(0.7);
        Mapping mapping3 = new Mapping("3", "term3", TestUtil.getTokens("term3")).withMatches(new Match[]{new Match(2, 2)}).withScore(0.5);
        Collections.addAll(mappings, mapping1, mapping2, mapping3);

        SemSimAmbiguityResolver ambiguityResolver = new SemSimAmbiguityResolver();
        ambiguityResolver.setEntityKnowledgeProvider(mockEntityKnowledgeProvider);
        ambiguityResolver.setSemanticSimilarityManager(mockSemanticSimilarityManager);

        Set<Mapping> unAmbiguousMappings = ambiguityResolver.resolve(null, mappings);

        Assert.assertEquals(unAmbiguousMappings.size(), 2);
        Assert.assertTrue(unAmbiguousMappings.contains(mapping1) || unAmbiguousMappings.contains(mapping2));
        Assert.assertTrue(unAmbiguousMappings.contains(mapping3));
    }
}
