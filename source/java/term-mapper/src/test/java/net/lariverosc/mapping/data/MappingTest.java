package net.lariverosc.mapping.data;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import net.lariverosc.mapping.step.TestUtil;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MappingTest {

    @Test
    public void shouldAddAndRetriveMatches() throws Exception {
        Mapping mapping = new Mapping("1", "term1", TestUtil.getTokens("term1"));
        mapping.addMatch(new Match(1, 1));
        mapping.addMatch(new Match(1, 2));
        mapping.addMatch(new Match(4, 3));
        mapping.addMatch(new Match(5, 3));

        Assert.assertEquals(mapping.getMatchesByChunkPosition(1).size(), 2);
        Assert.assertEquals(mapping.getMatchesByChunkPosition(4).size(), 1);
        Assert.assertEquals(mapping.getMatchesByChunkPosition(5).size(), 1);
        Assert.assertEquals(mapping.getMatchesByTermPosition(1).size(), 1);
        Assert.assertEquals(mapping.getMatchesByTermPosition(2).size(), 1);
        Assert.assertEquals(mapping.getMatchesByTermPosition(3).size(), 2);
    }

    @Test
    public void shouldAddAndRetriveTheRightNumberOfMatches() throws Exception {
        Mapping mapping = new Mapping("1", "term1", TestUtil.getTokens("term1"));
        mapping.addMatch(new Match(1, 1));
        mapping.addMatch(new Match(1, 2));
        mapping.addMatch(new Match(2, 3));
        mapping.addMatch(new Match(2, 4));

        Assert.assertEquals(mapping.getChunkMatches(), 2);
        Assert.assertEquals(mapping.getTermMatches(), 4);
    }

    @Test
    public void shouldAddAndRetriveOnlyOneMatch() throws Exception {
        Mapping mapping = new Mapping("1", "term1", TestUtil.getTokens("term1"));
        mapping.addMatch(new Match(2, 2));
        mapping.addMatch(new Match(2, 2));

        Assert.assertEquals(mapping.getChunkMatches(), 1);
        Assert.assertEquals(mapping.getTermMatches(), 1);
        Assert.assertEquals(mapping.getMatchesByChunkPosition(2).size(), 1);
        Assert.assertEquals(mapping.getMatchesByTermPosition(2).size(), 1);

    }

    @Test
    public void shouldIterateOverChunkPositionThenTermPosition() throws Exception {
        Mapping mapping = new Mapping("1", "term1", TestUtil.getTokens("term1"));
        mapping.addMatch(new Match(1, 1));
        mapping.addMatch(new Match(1, 2));
        mapping.addMatch(new Match(2, 3));
        mapping.addMatch(new Match(2, 4));

        Iterator<Integer> iteratorByChunkPosition = mapping.iteratorByChunkPosition();
        Assert.assertEquals(iteratorByChunkPosition.next().intValue(), 1);
        Assert.assertEquals(iteratorByChunkPosition.next().intValue(), 2);
        Assert.assertFalse(iteratorByChunkPosition.hasNext());

        Iterator<Match> iterator = mapping.getMatchesByChunkPosition(1).iterator();
        Assert.assertEquals(iterator.next().getTermPosition(), 1);
        Assert.assertEquals(iterator.next().getTermPosition(), 2);
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void shouldIterateOverTermPositionThenChunkPosition() throws Exception {
        Mapping mapping = new Mapping("1", "term1", TestUtil.getTokens("term1"));
        mapping.addMatch(new Match(1, 1));
        mapping.addMatch(new Match(2, 1));
        mapping.addMatch(new Match(3, 2));
        mapping.addMatch(new Match(4, 2));

        Iterator<Integer> iteratorByTermPosition = mapping.iteratorByTermPosition();
        Assert.assertEquals(iteratorByTermPosition.next().intValue(), 1);
        Assert.assertEquals(iteratorByTermPosition.next().intValue(), 2);
        Assert.assertFalse(iteratorByTermPosition.hasNext());

        Iterator<Match> iterator = mapping.getMatchesByTermPosition(1).iterator();
        Assert.assertEquals(iterator.next().getChunkPosition(), 1);
        Assert.assertEquals(iterator.next().getChunkPosition(), 2);
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void chunkPositionsStringShouldBeUsableAsHash() throws Exception {
        Mapping mapping1 = createSampleMapping();
        Mapping mapping2 = createSampleMapping();
        String mapping1ChunkPositionsString = mapping1.getChunkPositionsAsString();
        String mapping2ChunkPositionsString = mapping2.getChunkPositionsAsString();
        Assert.assertEquals(mapping1ChunkPositionsString.hashCode(), mapping2ChunkPositionsString.hashCode());

        mapping2.addMatch(new Match(5, 3));
        mapping2ChunkPositionsString = mapping2.getChunkPositionsAsString();
        Assert.assertNotEquals(mapping1ChunkPositionsString.hashCode(), mapping2ChunkPositionsString.hashCode());

        //For sentecen positions which sum is equal
        Mapping mapping3 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(13, 2)});
        Mapping mapping4 = new Mapping("2", "term2", TestUtil.getTokens("term2")).withMatches(new Match[]{new Match(6, 1), new Match(7, 1)});
        String mapping3ChunkPositionsString = mapping3.getChunkPositionsAsString();
        String mapping4ChunkPositionsString = mapping4.getChunkPositionsAsString();
        Assert.assertNotEquals(mapping3ChunkPositionsString.hashCode(), mapping4ChunkPositionsString.hashCode());
    }

    @Test
    public void MappingHashTest() throws Exception {
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1, Match.Type.EXACT)});
        Mapping mapping2 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1, Match.Type.SPELLED)});
        Assert.assertNotEquals(mapping1.hashCode(), mapping2.hashCode());
        Assert.assertNotEquals(mapping1, mapping2);
        Set<Mapping> temp = new HashSet<>();
        temp.add(mapping1);
        temp.add(mapping2);
        temp.add(mapping2);
        Assert.assertEquals(temp.size(),2);
    }

    private Mapping createSampleMapping() {
        Mapping mapping = new Mapping("1", "term1", TestUtil.getTokens("term1"));
        mapping.addMatch(new Match(1, 1));
        mapping.addMatch(new Match(2, 1));
        mapping.addMatch(new Match(3, 2));
        mapping.addMatch(new Match(4, 2));
        return mapping;
    }
}
