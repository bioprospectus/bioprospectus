package net.lariverosc.mapping.filters;

import java.util.HashSet;
import java.util.Set;
import net.lariverosc.mapping.data.Match;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.step.TestUtil;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class LongestMatchFilterTest {

    @Test
    public void mustChooseTheLongestMappingInIntervalWithTwoMappings() {
        LongestMatchFilter longestMatchFilter = new LongestMatchFilter();
        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(3, 3), new Match(4, 4)}));
        candidateSet.add(new Mapping("2", "term2", TestUtil.getTokens("term2")).withMatches(new Match[]{new Match(4, 4)}));
        Set<Mapping> filteredSet = longestMatchFilter.doFilter(null, candidateSet);
        assertEquals(filteredSet.size(), 1);
        assertEquals(filteredSet.iterator().next().getTermId(), "1");
    }

    @Test
    public void mustChooseTheLongestMappingInIntervalWithThreeMappings() {
        LongestMatchFilter longestMatchFilter = new LongestMatchFilter();
        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(2, 2), new Match(3, 3), new Match(4, 4)}));
        candidateSet.add(new Mapping("2", "term2", TestUtil.getTokens("term2")).withMatches(new Match[]{new Match(2, 2), new Match(3, 3)}));
        candidateSet.add(new Mapping("3", "term3", TestUtil.getTokens("term3")).withMatches(new Match[]{new Match(2, 2)}));
        Set<Mapping> filteredSet = longestMatchFilter.doFilter(null, candidateSet);
        assertEquals(filteredSet.size(), 1);
        assertEquals(filteredSet.iterator().next().getTermId(), "1");
    }

    @Test
    public void mustChooseTheLongestMappingInIntervalWithGap() {
        LongestMatchFilter longestMatchFilter = new LongestMatchFilter();
        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(2, 2), new Match(4, 4)}));
        candidateSet.add(new Mapping("2", "term2", TestUtil.getTokens("term2")).withMatches(new Match[]{new Match(2, 2)}));
        Set<Mapping> filteredSet = longestMatchFilter.doFilter(null, candidateSet);
        assertEquals(filteredSet.size(), 1);
        assertEquals(filteredSet.iterator().next().getTermId(), "1");
    }

    @Test
    public void mustNotFilterBecauseNotOverlapping() {
        LongestMatchFilter longestMatchFilter = new LongestMatchFilter();
        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1), new Match(2, 2)}));
        candidateSet.add(new Mapping("2", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(5, 5), new Match(6, 6)}));
        Set<Mapping> filteredSet = longestMatchFilter.doFilter(null, candidateSet);
        assertEquals(filteredSet.size(), 2);
    }

    @Test
    public void mustNotFilterBecausePartialOverlapping() {
        LongestMatchFilter longestMatchFilter = new LongestMatchFilter();
        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1), new Match(3, 3)}));
        candidateSet.add(new Mapping("2", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(5, 5), new Match(6, 6)}));
        candidateSet.add(new Mapping("3", "term3", TestUtil.getTokens("term3")).withMatches(new Match[]{new Match(2, 2), new Match(4, 4), new Match(5, 5)}));
        Set<Mapping> filteredSet = longestMatchFilter.doFilter(null, candidateSet);
        assertEquals(filteredSet.size(), 3);
    }

    @Test
    public void mustNotFilterOnOverlappingIfMappingAreEqual() {
        LongestMatchFilter longestMatchFilter = new LongestMatchFilter();
        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1), new Match(2, 2)}));
        candidateSet.add(new Mapping("2", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1), new Match(2, 2)}));
        Set<Mapping> filteredSet = longestMatchFilter.doFilter(null, candidateSet);
        assertEquals(filteredSet.size(), 2);
    }

    @Test
    public void mustNotFilterOnNotRealOverlapping() {
        LongestMatchFilter longestMatchFilter = new LongestMatchFilter();
        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(1, 1), new Match(3, 3)}));
        candidateSet.add(new Mapping("2", "term1", TestUtil.getTokens("term1")).withMatches(new Match[]{new Match(2, 2), new Match(4, 4)}));
        Set<Mapping> filteredSet = longestMatchFilter.doFilter(null, candidateSet);
        assertEquals(filteredSet.size(), 2);
    }
}
