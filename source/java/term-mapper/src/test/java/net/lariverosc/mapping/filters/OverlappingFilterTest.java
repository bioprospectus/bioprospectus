package net.lariverosc.mapping.filters;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import net.lariverosc.mapping.score.StructuralMappingScore;
import net.lariverosc.mapping.data.Match;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.step.TestUtil;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class OverlappingFilterTest {

    @Test
    public void shouldFilterOverlappingDistinctScore() {
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withScore(0.7).withMatches(new Match[]{new Match(2, 2), new Match(3, 3)});
        Mapping mapping2 = new Mapping("2", "term2", TestUtil.getTokens("term2")).withScore(0.8).withMatches(new Match[]{new Match(2, 2), new Match(3, 3)});
        Collections.addAll(mappings, mapping1, mapping2);

        BestScoreFilter bestScoreFilter = new BestScoreFilter();
        Set<Mapping> filteredSet = bestScoreFilter.doFilter(null, mappings);

        assertEquals(filteredSet.size(), 1);
        assertTrue(filteredSet.contains(mapping2));
    }

    @Test
    public void shouldNotFilterOverlappingBecauseSameScore() {
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withScore(0.8).withMatches(new Match[]{new Match(2, 2), new Match(3, 3)});
        Mapping mapping2 = new Mapping("2", "term2", TestUtil.getTokens("term2")).withScore(0.8).withMatches(new Match[]{new Match(2, 2), new Match(3, 3)});
        Collections.addAll(mappings, mapping1, mapping2);

        BestScoreFilter overlappingFilter = new BestScoreFilter();
        Set<Mapping> filteredSet = overlappingFilter.doFilter(null, mappings);

        assertEquals(filteredSet.size(), 2);
        assertTrue(filteredSet.contains(mapping1));
        assertTrue(filteredSet.contains(mapping2));
    }

    @Test
    public void shouldNotFilterBecauseTheOverlappingIsPartial() {
        Set<Mapping> mappings = new HashSet<Mapping>();
        Mapping mapping1 = new Mapping("1", "term1", TestUtil.getTokens("term1")).withScore(0.8).withMatches(new Match[]{new Match(1, 1), new Match(2, 2)});
        Mapping mapping2 = new Mapping("2", "term2", TestUtil.getTokens("term2")).withScore(0.8).withMatches(new Match[]{new Match(2, 2), new Match(3, 3)});
        Mapping mapping3 = new Mapping("3", "term3", TestUtil.getTokens("term3")).withScore(0.8).withMatches(new Match[]{new Match(1, 1), new Match(2, 2), new Match(3, 3)});
        Collections.addAll(mappings, mapping1, mapping2, mapping3);

        BestScoreFilter bestScoreFilter = new BestScoreFilter();
        Set<Mapping> filteredSet = bestScoreFilter.doFilter(null, mappings);

        assertEquals(filteredSet.size(), 3);
        assertTrue(filteredSet.contains(mapping1));
        assertTrue(filteredSet.contains(mapping2));
        assertTrue(filteredSet.contains(mapping3));
    }
}
