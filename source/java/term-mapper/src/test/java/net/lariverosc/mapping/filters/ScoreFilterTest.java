package net.lariverosc.mapping.filters;

import java.util.HashSet;
import java.util.Set;
import net.lariverosc.mapping.score.StructuralMappingScore;
import net.lariverosc.mapping.score.WeirdnessMappingScore;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.step.TestUtil;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ScoreFilterTest {

    @Test
    public void shouldFilterAll() {

        ScoreThresholdFilter filterByScore = new ScoreThresholdFilter(0.5);

        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withScore(0.1));
        candidateSet.add(new Mapping("2", "term2", TestUtil.getTokens("term2")).withScore(0.2));
        Set<Mapping> filteredSet = filterByScore.doFilter(null, candidateSet);

        assertTrue(filteredSet.isEmpty());
    }

    @Test
    public void shouldFilterNothing() {

        ScoreThresholdFilter filterByScore = new ScoreThresholdFilter(0.5);

        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withScore(0.5));
        candidateSet.add(new Mapping("2", "term2", TestUtil.getTokens("term2")).withScore(0.6));
        Set<Mapping> filteredSet = filterByScore.doFilter(null, candidateSet);

        assertEquals(filteredSet.size(), 2);
    }

    @Test
    public void shouldFilterSome() {

        ScoreThresholdFilter filterByScore = new ScoreThresholdFilter(0.5);

        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")).withScore(0.4));
        candidateSet.add(new Mapping("2", "term2", TestUtil.getTokens("term2")).withScore(0.5));
        Set<Mapping> filteredSet = filterByScore.doFilter(null, candidateSet);

        assertEquals(filteredSet.size(), 1);
    }

    @Test
    public void shouldFilterBecauseNotScore() {

        ScoreThresholdFilter filterByScore = new ScoreThresholdFilter(0.5);

        HashSet<Mapping> candidateSet = new HashSet<Mapping>();
        candidateSet.add(new Mapping("1", "term1", TestUtil.getTokens("term1")));
        candidateSet.add(new Mapping("2", "term2", TestUtil.getTokens("term2")));
        Set<Mapping> filteredSet = filterByScore.doFilter(null, candidateSet);

        assertTrue(filteredSet.isEmpty());
    }
}
