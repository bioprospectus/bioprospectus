package net.lariverosc.mapping.step;

import static org.testng.Assert.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import net.lariverosc.index.Index;
import net.lariverosc.index.TermOccurrence;
import net.lariverosc.index.TextIndexAgent;
import net.lariverosc.mapping.MockCollectionLoader;
import net.lariverosc.mapping.data.Mapping;
import net.lariverosc.mapping.terms.ExactTermSelector;
import net.lariverosc.mapping.terms.SpellTermSelector;
import net.lariverosc.mapping.terms.StemTermSelector;
import net.lariverosc.mapping.terms.TermSelector;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;
import net.lariverosc.text.spell.SpellChecker;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CandidateGenerationStepTest {

    @Test
    public void shouldNotMatchAnything() throws Exception {
        String chunk = "invalid1 invalid2";
        Map<String, Mapping> wholeChunkMappings = buildWholeMapping(chunk);

        assertEquals(wholeChunkMappings.keySet().size(), 0);
    }

    @Test
    public void shouldMatchOneDocumentWithTwoTermTokensAndOneChunkTokens() throws Exception {
        // term 3 -> "31word 32word 33word 34word 31word 32word 33word 34word"
        String chunk = "31word invalid";
        Map<String, Mapping> wholeChunkMappings = buildWholeMapping(chunk);

        assertEquals(wholeChunkMappings.keySet().size(), 1);

        String termId = wholeChunkMappings.keySet().iterator().next();
        assertEquals(wholeChunkMappings.get(termId).getChunkMatches(), 1);
        assertEquals(wholeChunkMappings.get(termId).getTermMatches(), 2);
    }

    @Test
    public void shouldMatchOneDocumentWithTwoTermTokensAndTwoChunkTokens() throws Exception {
        // term 1 -> "11word 12word 13word 14word"
        String chunk = "11word invalid 12word";
        Map<String, Mapping> wholeChunkMatches = buildWholeMapping(chunk);

        assertEquals(wholeChunkMatches.keySet().size(), 1);

        String termId = wholeChunkMatches.keySet().iterator().next();
        assertEquals(wholeChunkMatches.get(termId).getChunkMatches(), 2);
        assertEquals(wholeChunkMatches.get(termId).getTermMatches(), 2);
    }

    @Test
    public void shouldMatchTwoDocumentsEachWithOneTermTokensAndOneChunkTokens() throws Exception {
        // term 1 -> "11word 12word 13word 14word"
        String chunk = "11word invalid 21word";

        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.keySet().size(), 2);

        Iterator<String> iterator = partialMappings.keySet().iterator();
        String termId1 = iterator.next();
        Mapping mapping1 = partialMappings.get(termId1).iterator().next();

        assertEquals(mapping1.getChunkMatches(), 1);
        assertEquals(mapping1.getTermMatches(), 1);

        String termId2 = iterator.next();
        Mapping mapping2 = partialMappings.get(termId2).iterator().next();
        assertEquals(mapping2.getChunkMatches(), 1);
        assertEquals(mapping2.getTermMatches(), 1);
    }

    @Test
    public void shouldMatchOneDocumentWithTwoTermTokensAndFourChunkTokens() throws Exception {
        String chunk = "11word 12word invalid 11word 12word";
        Map<String, Mapping> wholeChunkMappings = buildWholeMapping(chunk);

        assertEquals(wholeChunkMappings.keySet().size(), 1);

        String termId = wholeChunkMappings.keySet().iterator().next();
        assertEquals(wholeChunkMappings.get(termId).getChunkMatches(), 4);
        assertEquals(wholeChunkMappings.get(termId).getTermMatches(), 2);
    }

    @Test
    public void shouldGenerateTwoPartialMappingsForASenteceWithDuplicates() throws Exception {
        // term 8 -> "81word 82word 83word"
        String chunk = "81word invalid 81word";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);
        assertEquals(partialMappings.get("8").size(), 2);
    }

    @Test
    public void shouldGenerateOnePartialMatchForATermWithDuplicates() throws Exception {
        //term 9 -> "91word 92word 91word"
        String chunk = "91word invalid invalid";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.get("9").size(), 1);
        Mapping mapping = partialMappings.get("9").iterator().next();
        assertEquals(mapping.getChunkMatches(), 1);
        assertEquals(mapping.getTermMatches(), 2);
    }

    @Test
    public void shouldGenerateOnePartialMatch() throws Exception {
        // term 8 -> "81word 82word 83word"
        String chunk = "81word 82word invalid";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.get("8").size(), 1);
        Mapping mapping = partialMappings.get("8").iterator().next();
        assertEquals(mapping.getChunkMatches(), 2);
        assertEquals(mapping.getTermMatches(), 2);

    }

    @Test
    public void shouldGenerateOnePartialMatchDespiteChunkGap() throws Exception {
        //term 1 -> "11word 12word 13word 14word"
        String chunk = "11word 12word invalid 13word";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.get("1").size(), 1);
        Mapping mapping = partialMappings.get("1").iterator().next();
        assertEquals(mapping.getChunkMatches(), 3);
        assertEquals(mapping.getTermMatches(), 3);

    }

    @Test
    public void shouldGenerateOnePartialMatchDespiteTermGap() throws Exception {
        //term 1 -> "11word 12word 13word 14word"
        String chunk = "11word 12word 14word invalid";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.get("1").size(), 1);
        Mapping mapping = partialMappings.get("1").iterator().next();
        assertEquals(mapping.getChunkMatches(), 3);
        assertEquals(mapping.getTermMatches(), 3);
    }

    @Test
    public void shouldGenerateTwoPartialMappingsBecauseBigChunkGap() throws Exception {
        // term 8 -> "81word 82word 83word"
        String chunk = "81word invalid invalid invalid 82word 83word";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.get("8").size(), 2);
    }

    @Test
    public void shouldGenerateOnePartialMatchBecauseSmallChunkGap() throws Exception {
        // term 8 -> "81word 82word 83word"
        String chunk = "81word invalid 83word";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.get("8").size(), 1);
        Mapping mapping = partialMappings.get("8").iterator().next();
        assertEquals(mapping.getChunkMatches(), 2);
        assertEquals(mapping.getTermMatches(), 2);
    }

    @Test
    public void shouldGenerateOnePartialMatchAndIgnoreTermGap() throws Exception {
        //term 2 -> "21word 22word 23word 24word 25word 26word"
        String chunk = "21word 25word 26word";
        Map<String, Set<Mapping>> partialMappings = buildPartialMappings(chunk);

        assertEquals(partialMappings.get("2").size(), 1);
        Mapping mapping = partialMappings.get("2").iterator().next();
        assertEquals(mapping.getChunkMatches(), 3);
        assertEquals(mapping.getTermMatches(), 3);
    }

    private Map<String, Mapping> buildWholeMapping(String chunk) {
        TextChunk textChunk = buildTextChunk(chunk);
        CandidateGenerationStep candidateBuilder = getCandidateBuilder();
        Map<String, Mapping> wholeChunkMappings = candidateBuilder.buildWholeMappings(textChunk);
        return wholeChunkMappings;
    }

    private Map<String, Set<Mapping>> buildPartialMappings(String chunk) {
        CandidateGenerationStep candidateBuilder = getCandidateBuilder();
        TextChunk textChunk = buildTextChunk(chunk);
        Map<String, Mapping> wholeChunkMappings = candidateBuilder.buildWholeMappings(textChunk);
        Set<Mapping> partialMappings = candidateBuilder.buildPartialMappings(wholeChunkMappings);
        return buildMappingsByTermMap(partialMappings);
    }

    private Map<String, Set<Mapping>> buildMappingsByTermMap(Set<Mapping> mappings) {
        Map<String, Set<Mapping>> mappingsByTerm = new HashMap<String, Set<Mapping>>();
        for (Mapping mapping: mappings) {
            Set<Mapping> mappingsSet = mappingsByTerm.get(mapping.getTermId());
            if (mappingsSet == null) {
                mappingsSet = new HashSet<Mapping>();
            }
            mappingsSet.add(mapping);
            mappingsByTerm.put(mapping.getTermId(), mappingsSet);
        }
        return mappingsByTerm;
    }

    private CandidateGenerationStep getCandidateBuilder() {
        CandidateGenerationStep candidateBuilder = new CandidateGenerationStep();
        Index mockIndex = buildMockIndex();
        candidateBuilder.setIndex(buildMockIndex());
        candidateBuilder.setStemTermSelector(buildMockTermSelector(mockIndex));
        candidateBuilder.setSpellTermSelector(buildSpellTermSelector(mockIndex));
        candidateBuilder.setExactTermSelector(buildExactTermSelector(mockIndex));
        return candidateBuilder;
    }

    private Index buildMockIndex() {
        TextPipeConfig textPipeConfig = new TextPipeConfig().withLanguage(TextPipeConfig.Language.ENGLISH)
            .withTokenizer(TextPipeConfig.Tokenizer.WHITE_SPACE)
            .withTokenTransforms(TextPipeConfig.TokenTransform.LOWER_CASE);

        TextIndexAgent textIndexAgent = new TextIndexAgent(textPipeConfig);
        textIndexAgent.setCollectionLoader(new MockCollectionLoader());
        return textIndexAgent.buildIndex();
    }

    private TextChunk buildTextChunk(String chunk) {
        TextPipe textPipe = new TextPipe(new TextPipeConfig().withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD).addTokenTransform(TextPipeConfig.TokenTransform.LOWER_CASE));
        return textPipe.proccess(chunk);
    }

    private StemTermSelector buildMockTermSelector(final Index index) {
        StemTermSelector termSelector = mock(StemTermSelector.class);
        when(termSelector.selectTerms(any(String.class))).thenAnswer(new Answer<Map<String, Set<TermOccurrence>>>() {
            @Override
            public Map<String, Set<TermOccurrence>> answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                String term = args[0].toString();
                Map<String, Set<TermOccurrence>> tempMap = new HashMap<String, Set<TermOccurrence>>();
                tempMap.put(term, index.lookup(term));
                return tempMap;
            }
        });
        return termSelector;
    }

    private ExactTermSelector buildExactTermSelector(final Index index) {
        ExactTermSelector termSelector = mock(ExactTermSelector.class);
        when(termSelector.selectTerms(any(String.class))).thenAnswer(new Answer<Map<String, Set<TermOccurrence>>>() {
            @Override
            public Map<String, Set<TermOccurrence>> answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                String term = args[0].toString();
                Map<String, Set<TermOccurrence>> tempMap = new HashMap<String, Set<TermOccurrence>>();
                tempMap.put(term, index.lookup(term));
                return tempMap;
            }
        });
        return termSelector;
    }

    private SpellTermSelector buildSpellTermSelector(final Index index) {
        SpellTermSelector termSelector = mock(SpellTermSelector.class);
        when(termSelector.selectTerms(any(String.class))).thenAnswer(new Answer<Map<String, Set<TermOccurrence>>>() {
            @Override
            public Map<String, Set<TermOccurrence>> answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                String term = args[0].toString();
                Map<String, Set<TermOccurrence>> tempMap = new HashMap<String, Set<TermOccurrence>>();
                tempMap.put(term, index.lookup(term));
                return tempMap;
            }
        });
        return termSelector;
    }

    private SpellChecker buildMockSpellChecker() {
        SpellChecker spellChecker = mock(SpellChecker.class);
        when(spellChecker.spell(any(String.class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        });
        when(spellChecker.similarWords(any(String.class), anyInt())).thenAnswer(new Answer<String[]>() {
            @Override
            public String[] answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return new String[]{(String) args[0]};
            }
        });
        return spellChecker;
    }
}
