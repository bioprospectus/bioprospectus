package net.lariverosc.mapping.step;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TestUtil {

    public static List<String> getTokens(String s) {
        String[] split = s.split(" ");
        return Arrays.asList(split);
    }

}
