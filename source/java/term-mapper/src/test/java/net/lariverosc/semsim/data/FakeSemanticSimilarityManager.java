package net.lariverosc.semsim.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.lariverosc.knowledge.domain.Concept;
import net.lariverosc.semsim.SimilarConcept;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class FakeSemanticSimilarityManager implements SemanticSimilarityManager {

    private Map<String, Double> similarityValues;

    public FakeSemanticSimilarityManager() {
        similarityValues = new HashMap<String, Double>();
        init();
    }

    private void init() {
        similarityValues.put("1-2", 0.4d);
        similarityValues.put("1-3", 0.8d);
        similarityValues.put("1-4", 0d);
        similarityValues.put("2-3", 0.9d);
        similarityValues.put("2-4", 0.2d);
        similarityValues.put("3-4", 0.4d);
    }

    @Override
    public void saveValues(String conceptId, Collection<SimilarConcept> semanticSimilarityValues) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getValue(Concept concept1, Concept concept2) {
        Double similarity = similarityValues.get(getKey(concept1.getId(), concept2.getId()));
        if (similarity != null) {
            return similarity;
        }
        return 0;
    }

    @Override
    public List<SimilarConcept> getTopKValues(Concept concept, int k) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private String getKey(String conceptId1, String conceptId2) {
        return conceptId1 + "-" + conceptId2;
    }
}
