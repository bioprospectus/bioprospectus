package net.lariverosc.text;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.text.filter.TokenCharAcceptor;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    private static TextPipeConfig.DictionaryConfig getDictionaryConfig() {
        Set<String> terms = new HashSet<String>();
        terms.add("java");
        return new TextPipeConfig.DictionaryConfig("testDict", terms);
    }

    private static TextPipeConfig.AbbreviationsConfig getAbbreviationsConfig() {
        Map<String, String> abbreviations = new HashMap<String, String>();
        abbreviations.put("Mr", "123:Mister");
        abbreviations.put("USD", "1234:US dollar");
        return new TextPipeConfig.AbbreviationsConfig("testAbbr", abbreviations, true);
    }

    public static void main(String[] args) throws IOException {
        String s = "TEXT: Mr. Alejandro Riveros Is a Java programmer, its email is lariverosc@gmail.com 100% $5.5 Millions 200 alpha-2";

        TextPipeConfig textPipeConfig = new TextPipeConfig()
            .withTokenizer(TextPipeConfig.Tokenizer.CUSTOM)
            .withLanguage(TextPipeConfig.Language.ENGLISH)
            .withTokenTransforms(TextPipeConfig.TokenTransform.LOWER_CASE)
            .withTokenMatchers(
                TextPipeConfig.TokenMatcher.ACRONYM,
                TextPipeConfig.TokenMatcher.DECIMAL_NUMBER,
                TextPipeConfig.TokenMatcher.PERCENTAGE,
                TextPipeConfig.TokenMatcher.MONEY,
                TextPipeConfig.TokenMatcher.EMAIL,
                TextPipeConfig.TokenMatcher.GENE,
                TextPipeConfig.TokenMatcher.STOP,
                TextPipeConfig.TokenMatcher.ALPHANUMERIC
            )
            .withTokenCharAcceptor(new TokenCharAcceptor() {

                @Override
                public boolean isTokenChar(int c) {
                    return (!Character.isWhitespace(c) || c == 64) && c != 44 && c != 47 && c != 58 && c != 59;
                }
            })
            .withTokenFilters(TextPipeConfig.TokenFilter.STOP_WORD)
            .addDictionary(getDictionaryConfig())
            .addAbbreviationsDictionary(getAbbreviationsConfig());

        TextPipe textPipe = new TextPipe(textPipeConfig);
        for (int i = 0; i < 1; i++) {
            System.out.println(s);
            List<TextToken> tokens = textPipe.getTokens(s);
            for (TextToken token: tokens) {
                System.out.println(token);
            }
        }
    }
}
