package net.lariverosc.text;

import java.util.List;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextChunk {

    private final String text;

    private TextSpan textSpan;

    private List<TextToken> tokens;

    private String payload;

    public TextChunk(String text) {
        this.text = text;
    }

    public TextChunk(String text, TextSpan textSpan) {
        this.text = text;
        this.textSpan = textSpan;
    }

    public String getText() {
        return text;
    }

    public TextSpan getTextSpan() {
        return textSpan;
    }

    public void setTextSpan(TextSpan textSpan) {
        this.textSpan = textSpan;
    }

    public List<TextToken> getTokens() {
        return tokens;
    }

    public void setTokens(List<TextToken> tokens) {
        this.tokens = tokens;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getTokensAsString() {
        StringBuilder sb = new StringBuilder();
        for (TextToken textToken: tokens) {
            sb.append(textToken.getText()).append(" ");
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TextChunk{" + "text=").append(text).append(", textSpan=").append(textSpan).append(", payload=").append(payload).append('}').append("\n");
        if (tokens != null) {
            for (TextToken textToken: tokens) {
                sb.append(textToken).append("\n");
            }
        }
        return sb.toString();
    }

}
