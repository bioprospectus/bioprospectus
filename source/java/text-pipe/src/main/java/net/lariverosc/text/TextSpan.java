package net.lariverosc.text;

import com.google.common.collect.ComparisonChain;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;

/**
 * Represents a text span with an start inclusive and end exclusive index.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextSpan {

    private final int start;

    private final int end;

    public TextSpan(String textSpanStr) {
        this(Integer.parseInt(textSpanStr.split("-")[0]), Integer.parseInt(textSpanStr.split("-")[1]));
    }

    public TextSpan(int start, int end) {
        if (start < 0 || start > end) {
            throw new IllegalArgumentException("start can't be greater than end start:" + start + " end:" + end);
        }
        this.start = start;
        this.end = end;
    }

    public TextSpan(TextSpan textSpan, int offset) {
        this(textSpan.getStart() + offset, textSpan.getEnd() + offset);
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getLength() {
        return end - start;
    }

    public boolean contains(TextSpan span) {
        return start <= span.getStart() && span.getEnd() < end;
    }

    public boolean contains(int index) {
        return start <= index && index < end;
    }

    public boolean intersects(TextSpan textSpan) {
        if (this.contains(textSpan) || textSpan.contains(this)) {
            return true;
        }
        int maxStart = textSpan.getStart() > this.getStart() ? textSpan.getStart() : this.getStart();
        int minEnd = textSpan.getEnd() < this.getEnd() ? textSpan.getEnd() : this.getEnd();
        return maxStart - minEnd < 1;
    }

    public TextSpan merge(TextSpan textSpan) {
        return TextSpan.merge(this, textSpan);
    }

    public String getCoveredText(String s) {
        if (this.end > s.length()) {
            throw new IllegalArgumentException("The span " + toString() + " is outside the given text which has length " + s.length());
        }

        return s.substring(this.start, this.end);
    }

    public String getUncoveredText(String s) {
        return getTextBefore(s) + getTextAfter(s);
    }

    public String getTextBefore(String s) {
        return s.substring(0, this.start);
    }

    public String getTextAfter(String s) {
        if (this.end > s.length()) {
            throw new IllegalArgumentException("The span " + toString() + " is outside the given text which has length " + s.length());
        }
        return s.substring(this.end);
    }

    public static List<TextSpan> sortByStart(List<TextSpan> textSpans) {
        Collections.sort(textSpans, new StartIndexComparator());
        return textSpans;
    }

    public static List<TextSpan> sortByEnd(List<TextSpan> textSpans) {
        Collections.sort(textSpans, new EndIndexComparator());
        return textSpans;
    }

    private static class StartIndexComparator implements Comparator<TextSpan> {

        @Override
        public int compare(TextSpan ts1, TextSpan ts2) {
            return ComparisonChain.start()
                    .compare(ts1.getStart(), ts2.getStart())
                    .compare(ts1.getEnd(), ts2.getEnd()).result();
        }
    }

    private static class EndIndexComparator implements Comparator<TextSpan> {

        @Override
        public int compare(TextSpan ts1, TextSpan ts2) {
            return ComparisonChain.start()
                    .compare(ts1.getEnd(), ts2.getEnd())
                    .compare(ts1.getStart(), ts2.getStart()).result();
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + this.start;
        hash = 79 * hash + this.end;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TextSpan other = (TextSpan) obj;
        if (this.start != other.start) {
            return false;
        }
        if (this.end != other.end) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getStart() + "-" + getEnd();
    }

    public static TextSpan merge(TextSpan textSpan1, TextSpan textSpan2) {
        int mergeStart = textSpan1.getStart() < textSpan2.getStart() ? textSpan1.getStart() : textSpan2.getStart();
        int mergeEnd = textSpan1.getEnd() > textSpan2.getEnd() ? textSpan1.getEnd() : textSpan2.getEnd();
        return new TextSpan(mergeStart, mergeEnd);
    }

    public static Collection<TextSpan> mergeIntersectedSpans(Collection<TextSpan> textSpans) {
        List<TextSpan> textSpansList = new ArrayList<>(textSpans);
        TextSpan.sortByStart(textSpansList);

        Deque<TextSpan> stack = new ArrayDeque<>();
        for (TextSpan textSpan : textSpansList) {
            if (!stack.isEmpty()) {
                TextSpan first = stack.peekFirst();
                if (first.intersects(textSpan)) {
                    stack.removeFirst();
                    stack.addFirst(first.merge(textSpan));
                } else {
                    stack.addFirst(textSpan);
                }
            } else {
                stack.addFirst(textSpan);
            }
        }
        return stack;
    }

    public static Collection<TextSpan> mergeContinuousSpans(Collection<TextSpan> textSpans) {
        ArrayList<TextSpan> textSpansList = new ArrayList<>(textSpans);
        if (textSpans.size() < 2) {
            return textSpansList;
        }
        TextSpan.sortByStart(textSpansList);
        List<TextSpan> temp = new ArrayList<>();
        TextSpan currentTextSpan = textSpansList.get(0);
        for (int i = 1; i < textSpans.size(); i++) {
            TextSpan nextTextSpan = textSpansList.get(i);
            if (currentTextSpan.getEnd() + 1 >= nextTextSpan.getStart()) {
                int newEnd = nextTextSpan.getEnd() > currentTextSpan.getEnd() ? nextTextSpan.getEnd() : currentTextSpan.getEnd();
                currentTextSpan = new TextSpan(currentTextSpan.getStart(), newEnd);
            } else {
                temp.add(currentTextSpan);
                currentTextSpan = nextTextSpan;
            }
        }
        temp.add(currentTextSpan);
        return temp;
    }

    public static Collection<TextSpan> offsetSpans(Collection<TextSpan> textSpans, int offset) {
        Collection<TextSpan> offsetedSpans = new ArrayList<>();
        for (TextSpan textSpan : textSpans) {
            offsetedSpans.add(new TextSpan(textSpan, offset));
        }
        return offsetedSpans;
    }

    public static String asString(Collection<TextSpan> textSpans) {
        List<TextSpan> sortedTextSpans = TextSpan.sortByStart(new ArrayList<>(textSpans));
        StringBuilder sb = new StringBuilder();
        for (TextSpan textSpan : sortedTextSpans) {
            int start = textSpan.getStart();
            int end = textSpan.getEnd();
            sb.append(start).append("-").append(end).append(",");
        }
        return sb.substring(0, sb.length() - 1);
    }

    public static int getMinStart(Collection<TextSpan> textSpans) {
        int min = Integer.MAX_VALUE;
        for (TextSpan textSpan : textSpans) {
            if (textSpan.getStart() < min) {
                min = textSpan.getStart();
            }
        }
        return min;
    }

    public static int getMaxEnd(Collection<TextSpan> textSpans) {
        int max = Integer.MIN_VALUE;
        for (TextSpan textSpan : textSpans) {
            if (textSpan.getEnd() > max) {
                max = textSpan.getEnd();
            }
        }
        return max;
    }

    public static TextSpan getMaxTextSpan(Collection<TextSpan> textSpans) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (TextSpan textSpan : textSpans) {
            if (textSpan.getStart() < min) {
                min = textSpan.getStart();
            }
            if (textSpan.getEnd() > max) {
                max = textSpan.getEnd();
            }
        }
        return new TextSpan(min, max);
    }

}
