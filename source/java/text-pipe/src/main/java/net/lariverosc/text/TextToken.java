package net.lariverosc.text;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import net.lariverosc.text.util.TokenFeatures;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextToken {

    public static enum Type {

        ABBREVIATION("ABBR"),
        ACRONYM("ACRONYM"),
        ALPHANUMERIC("ALPHANUM"),
        DATETIME("DATETIME"),
        DICT_TERM("DICT_TERM"),
        EMAIL("EMAIL"),
        PUNCTUACTION("PUNCT"),
        GENE_SEQ("GENE_SEQ"),
        MONEY("MONEY"),
        NUMBER("NUMBER"),
        PERCENTAGE("PERCENTAGE"),
        ROMAN("ROMAN"),
        STOP("STOP"),
        UNKNOW("UNKNOW"),
        WORD("WORD");

        private final String name;

        private Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "Type{" + name + "}";
        }

        public static TextToken.Type getByName(String name) {
            if (name.equals(ABBREVIATION.getName())) {
                return ABBREVIATION;
            }
            if (name.equals(ACRONYM.getName())) {
                return ACRONYM;
            }
            if (name.equals(ALPHANUMERIC.getName())) {
                return ALPHANUMERIC;
            }
            if (name.equals(DATETIME.getName())) {
                return DATETIME;
            }
            if (name.equals(DICT_TERM.getName())) {
                return DICT_TERM;
            }
            if (name.equals(EMAIL.getName())) {
                return EMAIL;
            }
            if (name.equals(GENE_SEQ.getName())) {
                return GENE_SEQ;
            }
            if (name.equals(MONEY.getName())) {
                return MONEY;
            }
            if (name.equals(NUMBER.getName())) {
                return NUMBER;
            }
            if (name.equals(PERCENTAGE.getName())) {
                return PERCENTAGE;
            }
            if (name.equals(PUNCTUACTION.getName())) {
                return PUNCTUACTION;
            }
            if (name.equals(ROMAN.getName())) {
                return ROMAN;
            }
            if (name.equals(STOP.getName())) {
                return STOP;
            }
            if (name.equals(WORD.getName())) {
                return WORD;
            }
            return UNKNOW;
        }

    }

    private final String text;

    private final TextToken.Type type;

    private final TextSpan textSpan;

    private TokenFeatures tokenFeatures;

    private String payload;

    private final int position;

    private final int index;

    private final int posIncrement;

    private final int posLength;

    public TextToken(String text, TextSpan textSpan, TextToken.Type type) {
        this(text, textSpan, type, null, null, 0, 0, 0, 0);
    }

    public TextToken(String text, TextSpan textSpan, Type type, TokenFeatures tokenFeatures, String payload, int position, int index, int posIncrement, int posLength) {
        this.text = text;
        this.textSpan = textSpan;
        this.type = type;
        this.tokenFeatures = tokenFeatures;
        this.payload = payload;
        this.position = position;
        this.index = index;
        this.posIncrement = posIncrement;
        this.posLength = posLength;
    }

    public String getText() {
        return text;
    }

    public TextToken.Type getType() {
        return type;
    }

    public TokenFeatures getTokenFeatures() {
        return tokenFeatures;
    }

    public void setTokenFeatures(TokenFeatures tokenFeatures) {
        this.tokenFeatures = tokenFeatures;
    }

    public TextSpan getTextSpan() {
        return textSpan;
    }

    public int getPosition() {
        return position;
    }

    public int getIndex() {
        return index;
    }

    public int getPosIncrement() {
        return posIncrement;
    }

    public int getPosLength() {
        return posLength;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        return "TextToken{" + "text=" + text + ", type=" + type + ", textSpan=" + textSpan + ", payload=" + payload + ", position=" + position+", index=" + index + ", posIncrement=" + posIncrement + ", posLength=" + posLength + ", tokenFeatures=" + tokenFeatures + "}";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + (this.text != null ? this.text.hashCode() : 0);
        hash = 47 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 47 * hash + (this.textSpan != null ? this.textSpan.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TextToken other = (TextToken) obj;
        if ((this.text == null) ? (other.text != null) : !this.text.equals(other.text)) {
            return false;
        }
        if ((this.type == null) ? (other.type != null) : !this.type.equals(other.type)) {
            return false;
        }
        if (this.textSpan != other.textSpan && (this.textSpan == null || !this.textSpan.equals(other.textSpan))) {
            return false;
        }
        return true;
    }

    public static String joinTokens(List<TextToken> textTokens) {
        StringBuilder sb = new StringBuilder();
        for (TextToken textToken: textTokens) {
            sb.append(textToken.getText()).append(" ");
        }
        return sb.toString().trim();
    }

    public static List<TextToken> sortByStart(List<TextToken> textTokens) {
        Collections.sort(textTokens, new TextToken.StartIndexComparator());
        return textTokens;
    }

    public static List<TextToken> sortByEnd(List<TextToken> textTokens) {
        Collections.sort(textTokens, new TextToken.EndIndexComparator());
        return textTokens;
    }

    public static List<TextToken> sortAlphabetically(List<TextToken> textTokens) {
        Collections.sort(textTokens, new TextToken.AlphabeticalComparator());
        return textTokens;
    }

    private static class StartIndexComparator implements Comparator<TextToken> {

        @Override
        public int compare(TextToken tt1, TextToken tt2) {
            return Integer.compare(tt1.getTextSpan().getStart(), tt2.getTextSpan().getStart());
        }
    }

    private static class EndIndexComparator implements Comparator<TextToken> {

        @Override
        public int compare(TextToken tt1, TextToken tt2) {
            return Integer.compare(tt1.getTextSpan().getEnd(), tt2.getTextSpan().getEnd());
        }
    }

    private static class AlphabeticalComparator implements Comparator<TextToken> {

        @Override
        public int compare(TextToken tt1, TextToken tt2) {
            return tt1.getText().compareTo(tt2.getText());
        }
    }
}
