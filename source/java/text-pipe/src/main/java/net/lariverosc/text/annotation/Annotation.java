package net.lariverosc.text.annotation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import net.lariverosc.text.TextSpan;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Annotation {

    private final String id;

    private final Set<TextSpan> textSpans;

    private final Map<String, String> attributes;

    public Annotation(String id) {
        this(id, new HashSet<TextSpan>());
    }

    public Annotation(String id, Set<TextSpan> textSpans) {
        this.id = id;
        this.textSpans = textSpans;
        this.attributes = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public Set<TextSpan> getTextSpans() {
        return textSpans;
    }

    public void addTextSpan(TextSpan textSpan) {
        textSpans.add(textSpan);
    }

    public void addAllTextSpans(Set<TextSpan> textSpans) {
        this.textSpans.addAll(textSpans);
    }

    public String getAttribute(String key) {
        return attributes.get(key);
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void addAllAttributes(Map<String, String> attributes) {
        for (String key : attributes.keySet()) {
            addAttribute(key, attributes.get(key));
        }
    }

    /**
     * Add attributes to the annotation
     *
     * @param key the attribute name
     * @param value the attribute value
     * @param attributeSeparator the attribute separator
     */
    public void appendAttribute(String key, String value, String attributeSeparator) {
        String currentValue = attributes.get(key);
        if (currentValue != null && !currentValue.isEmpty()) {
            attributes.put(key, currentValue + attributeSeparator + value);
        } else {
            attributes.put(key, value);
        }
    }

    public void addAttribute(String key, String value) {
        attributes.put(key, value);
    }

    public Annotation withTextSpan(TextSpan textSpan) {
        addTextSpan(textSpan);
        return this;
    }

    public Annotation withAttribute(String key, String value) {
        addAttribute(key, value);
        return this;
    }

    public String getAttributesAsString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> attribute: attributes.entrySet()) {
            sb.append(attribute.getKey()).append("=\"").append(attribute.getValue()).append("\"").append(" ");
        }
        return sb.toString().trim();
    }

    public String getCoveredText(String textChunk) {
        StringBuilder sb = new StringBuilder();
        List<TextSpan> textSpanList = new ArrayList<TextSpan>(textSpans);
        TextSpan.sortByStart(textSpanList);
        for (TextSpan textSpan : textSpanList) {
            sb.append(textSpan.getCoveredText(textChunk)).append(" ");
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.textSpans);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Annotation other = (Annotation) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.textSpans, other.textSpans)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Annotation{" + "id=" + id + ", textSpans=" + textSpans + ", attributes=" + attributes + '}';
    }

}
