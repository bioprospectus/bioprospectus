package net.lariverosc.text.annotation;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 * @param <T>
 */
public interface AnnotationConverter<T> {

    Annotation toAnnotation(T annotationData);
}
