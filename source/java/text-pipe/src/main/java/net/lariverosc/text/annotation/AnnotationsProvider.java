package net.lariverosc.text.annotation;

import java.util.Set;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public abstract class AnnotationsProvider {

    protected AnnotationConverter annotationConverter;

    public abstract Set<Annotation> getAnnotations(String docId);

    public void setAnnotationConverter(AnnotationConverter annotationConverter) {
        this.annotationConverter = annotationConverter;
    }
}
