package net.lariverosc.text.annotation;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.lariverosc.util.IOUtils;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class FileSystemAnnotationProvider extends AnnotationsProvider {

    private final String basePath;

    public FileSystemAnnotationProvider(String basePath) {
        this.basePath = IOUtils.fixPath(basePath);
    }

    @Override
    public Set<Annotation> getAnnotations(String docId) {
        File file = new File(basePath + docId);
        if (file.exists() && file.isFile()) {
            List<String> lines = IOUtils.readLines(file);
            Set<Annotation> annotations = new HashSet<Annotation>();
            for (String line: lines) {
                annotations.add(annotationConverter.toAnnotation(line));
            }
            return annotations;
        } else {
            return null;
        }
    }
}
