package net.lariverosc.text.annotation;

import net.lariverosc.text.TextSpan;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class PipeAnnotationConverter implements AnnotationConverter<String> {

    private final String FIELD_SEPARATOR;

    public PipeAnnotationConverter() {
        this("\\|\\|");
    }

    public PipeAnnotationConverter(String separator) {
        this.FIELD_SEPARATOR = separator;
    }

    @Override
    public Annotation toAnnotation(String annotationData) {
        String[] fields = annotationData.split(FIELD_SEPARATOR);
        Annotation annotation = new Annotation(fields[2]);
        annotation.addAttribute("field", "content");
        annotation.addAttribute("category", fields[1]);
        annotation.addAttribute("conceptId", fields[2]);
        for (int i = 3; i < fields.length; i += 2) {
            annotation.addTextSpan(new TextSpan(Integer.parseInt(fields[i]), Integer.parseInt(fields[i + 1])));
        }
        return annotation;
    }
}
