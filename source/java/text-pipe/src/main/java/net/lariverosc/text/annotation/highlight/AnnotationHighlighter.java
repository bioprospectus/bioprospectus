package net.lariverosc.text.annotation.highlight;

import java.util.Set;
import net.lariverosc.text.annotation.Annotation;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface AnnotationHighlighter {

    public String highlight(String textChunk, Set<Annotation> annotations);
}
