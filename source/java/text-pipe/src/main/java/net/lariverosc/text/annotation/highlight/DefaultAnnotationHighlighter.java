package net.lariverosc.text.annotation.highlight;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DefaultAnnotationHighlighter implements AnnotationHighlighter {

    private final String START_MARK;

    private final String END_MARK;

    public DefaultAnnotationHighlighter() {
        this("START", "END");
    }

    public DefaultAnnotationHighlighter(String startMark, String endMark) {
        this.START_MARK = startMark;
        this.END_MARK = endMark;
    }

    @Override
    public String highlight(String textChunk, Set<Annotation> originalAnnotations) {
        Set<ExpandedAnnotation> annotations = expandAnnotations(originalAnnotations);
        Multimap<Integer, ExpandedAnnotation> startIndexMap = buildStartIndexMap(annotations);
        Multimap<Integer, ExpandedAnnotation> endIndexMap = buildEndIndexMap(annotations);
        Deque<ExpandedAnnotation> openStack = new LinkedList<>();//Stack that stores the open tags
        Deque<ExpandedAnnotation> tempStack = new LinkedList<>();//Temporal stack used to store pending tags

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < textChunk.length(); i++) {//Iterate over characters
            if (startIndexMap.containsKey(i)) { //if exist any annotation starting here
                List<ExpandedAnnotation> currentStartingAnnotations = sortAnnotationsByLength(startIndexMap.get(i));
                for (ExpandedAnnotation annotation: currentStartingAnnotations) {
                    if (openStack.isEmpty()) {
                        openStack.push(annotation);
                        sb.append(buildStartMark(annotation));
                    } else {
                        if (openStack.peek().getTextSpan().getStart() == annotation.getTextSpan().getStart()) {
                            openStack.push(annotation);
                            sb.append(buildStartMark(annotation));
                        } else {
                            int currentEnd = annotation.getTextSpan().getEnd();
                            if (openStack.peek().getTextSpan().getEnd() >= currentEnd) {
                                openStack.push(annotation);
                                sb.append(buildStartMark(annotation));
                            } else {
                                while (!openStack.isEmpty() && openStack.peek().getTextSpan().getEnd() < currentEnd) {
                                    tempStack.push(openStack.pop());
                                    sb.append(buildEndMark());
                                }
                                openStack.push(annotation);
                                sb.append(buildStartMark(annotation));
                                while (!tempStack.isEmpty()) {
                                    ExpandedAnnotation tempAnnotation = tempStack.pop();
                                    openStack.push(tempAnnotation);
                                    sb.append(buildStartMark(tempAnnotation));
                                }
                            }
                        }
                    }
                }
            }
            if (endIndexMap.containsKey(i)) { //if exist any annotation starting here
                Collection<ExpandedAnnotation> currentEndingAnnotations = endIndexMap.get(i);
                for (ExpandedAnnotation annotation: currentEndingAnnotations) {
                    openStack.pop();
                    sb.append(buildEndMark());
                }
            }
            sb.append(StringEscapeUtils.escapeHtml4(textChunk.substring(i, i+1)));//TODO abstract this in a html highlighther
        }

        while (!openStack.isEmpty()) {
            tempStack.push(openStack.pop());
            sb.append(buildEndMark());
        }
        return sb.toString();
    }

    private Set<ExpandedAnnotation> expandAnnotations(Set<Annotation> annotations) {
        Set<ExpandedAnnotation> expandedAnnotations = new HashSet<>();
        for (Annotation annotation: annotations) {
            Set<TextSpan> textSpans = annotation.getTextSpans();
            for (TextSpan textSpan: textSpans) {
                ExpandedAnnotation temp = new ExpandedAnnotation(annotation.getId(), textSpan, annotation.getAttributesAsString());
                expandedAnnotations.add(temp);
            }
        }
        return expandedAnnotations;
    }

    private String buildStartMark(ExpandedAnnotation annotation) {
        StringBuilder sb = new StringBuilder();
        if (annotation.getAttributes().isEmpty()) {

            return sb.append("<")
                    .append(this.START_MARK)
                    .append(" id=\"")
                    .append(annotation.getId())
                    .append("\" >").toString();
        } else {
            return sb.append("<")
                    .append(this.START_MARK)
                    .append(" id=\"")
                    .append(annotation.getId())
                    .append("\" ")
                    .append(annotation.getAttributes())
                    .append(" >").toString();
        }
    }

    private String buildEndMark() {
        return "</" + this.END_MARK + ">";
    }

    private Multimap<Integer, ExpandedAnnotation> buildStartIndexMap(Set<ExpandedAnnotation> annotations) {
        Multimap<Integer, ExpandedAnnotation> startIndexMap = HashMultimap.create();
        for (ExpandedAnnotation annotation : annotations) {
            TextSpan textSpan = annotation.getTextSpan();
            startIndexMap.put(textSpan.getStart(), annotation);
        }
        return startIndexMap;
    }

    private Multimap<Integer, ExpandedAnnotation> buildEndIndexMap(Set<ExpandedAnnotation> annotations) {
        Multimap<Integer, ExpandedAnnotation> endIndexMap = HashMultimap.create();
        for (ExpandedAnnotation annotation : annotations) {
            TextSpan textSpan = annotation.getTextSpan();
            endIndexMap.put(textSpan.getEnd(), annotation);
        }
        return endIndexMap;
    }

    private List<ExpandedAnnotation> sortAnnotationsByLength(Collection<ExpandedAnnotation> annotations) {
        List<ExpandedAnnotation> annotationsList = new ArrayList<>();
        for (ExpandedAnnotation annotation : annotations) {
            annotationsList.add(annotation);
        }
        Collections.sort(annotationsList, new Comparator<ExpandedAnnotation>() {

            @Override
            public int compare(ExpandedAnnotation annotation1, ExpandedAnnotation annotation2) {
                TextSpan textSpan1 = annotation1.getTextSpan();
                TextSpan textSpan2 = annotation2.getTextSpan();
                return -Integer.compare(textSpan1.getLength(), textSpan2.getLength());
            }
        });
        return annotationsList;
    }

    private class ExpandedAnnotation {

        private final String id;

        private final TextSpan textSpan;

        private final String attributes;

        public ExpandedAnnotation(String id, TextSpan textSpan, String attributes) {
            this.id = id;
            this.textSpan = textSpan;
            this.attributes = attributes;
        }

        public String getId() {
            return id;
        }

        public TextSpan getTextSpan() {
            return textSpan;
        }

        public String getAttributes() {
            return attributes;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 17 * hash + Objects.hashCode(this.id);
            hash = 17 * hash + Objects.hashCode(this.textSpan);
            hash = 17 * hash + Objects.hashCode(this.attributes);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ExpandedAnnotation other = (ExpandedAnnotation) obj;
            if (!Objects.equals(this.id, other.id)) {
                return false;
            }
            if (!Objects.equals(this.textSpan, other.textSpan)) {
                return false;
            }
            if (!Objects.equals(this.attributes, other.attributes)) {
                return false;
            }
            return true;
        }

    }
}
