package net.lariverosc.text.document;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Document {

    private String id;

    private final Map<String, String> fields;

    private Map<String, String> attributes;

    public Document() {
        fields = new HashMap<>();
        attributes = new HashMap<>();
    }

    public void addField(String fieldName, String value) {
        if ((fieldName != null && !fieldName.isEmpty()) && (value != null && !value.isEmpty())) {
            fields.put(fieldName, value);
        }
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getFieldValue(String fieldName) {
        return fields.get(fieldName);
    }

    public Set<String> fieldNames() {
        return fields.keySet();
    }

    public Set<String> attributeNames() {
        return attributes.keySet();
    }

    public void addDocAttribute(String key, String value) {
        attributes.put(key, value);
    }

    public String getDocAttribute(String key) {
        return attributes.get(key);
    }

    public void addFieldAttribute(String field, String key, String value) {
        attributes.put(field + "-" + key, value);
    }

    public String getFieldAttribute(String field, String key) {
        return attributes.get(field + "-" + key);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id: ").append(id).append("\n");
        for (Map.Entry<String, String> field : fields.entrySet()) {
            sb.append("\t").append(field.getKey()).append(":").append(field.getValue()).append("\n");
        }
        return sb.toString();
    }
}
