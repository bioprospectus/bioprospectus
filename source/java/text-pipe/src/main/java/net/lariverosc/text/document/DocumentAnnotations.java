package net.lariverosc.text.document;

import java.util.Map;
import java.util.Set;
import net.lariverosc.text.annotation.Annotation;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DocumentAnnotations {

    private String docId;

    private Map<String, Set<Annotation>> annotationsByField;

}
