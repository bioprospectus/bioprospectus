package net.lariverosc.text.document;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 * @param <T>
 */
public interface DocumentConverter<T> {

    Document toDocument(T inObject);
}
