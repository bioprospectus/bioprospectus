package net.lariverosc.text.document;

import java.util.List;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public abstract class DocumentsProvider {

    protected DocumentConverter documentConverter;

    public abstract List<String> getAllDocumentIds();

    public abstract Document getDocument(String docId);

    public void setDocumentConverter(DocumentConverter documentConverter) {
        this.documentConverter = documentConverter;
    }

}
