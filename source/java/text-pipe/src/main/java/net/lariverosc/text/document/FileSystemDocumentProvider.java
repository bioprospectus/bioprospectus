package net.lariverosc.text.document;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.lariverosc.util.IOUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class FileSystemDocumentProvider extends DocumentsProvider {

    private final Logger log = LoggerFactory.getLogger(FileSystemDocumentProvider.class);
    
    private final String basePath;

    private final String[] extensions;

    private final boolean recursive;

    private final boolean isDirectory;

    public FileSystemDocumentProvider(String basePath, boolean recursive, String[] extensions) {
        this.basePath = IOUtils.fixPath(basePath);
        this.recursive = recursive;
        this.extensions = extensions;
        this.isDirectory = new File(basePath).isDirectory();
    }

    @Override
    public List<String> getAllDocumentIds() {
        List<String> fileNameList = new ArrayList<>();
        if (isDirectory) {
            Collection<File> listFiles = FileUtils.listFiles(new File(basePath), extensions, recursive);
            for (File file : listFiles) {
                fileNameList.add(file.getAbsolutePath().substring(basePath.length()));
                
            }
        } else {
            fileNameList.add(new File(basePath).getAbsolutePath());
        }
        return fileNameList;
    }

    @Override
    public Document getDocument(String docId) {
        File file = new File(basePath + docId);
        if (file.exists() && file.isFile()) {
            Document document = documentConverter.toDocument(file);
            document.setId(docId);
            return document;
        } else {
            return null;
        }
    }

}
