package net.lariverosc.text.document;

import java.io.File;
import net.lariverosc.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class PlainTextDocumentConverter implements DocumentConverter<File> {

    private final Logger log = LoggerFactory.getLogger(PlainTextDocumentConverter.class);

    @Override
    public Document toDocument(File file) {
        Document document = new Document();
        document.addField("content", IOUtils.readContent(file));
        return document;
    }
}
