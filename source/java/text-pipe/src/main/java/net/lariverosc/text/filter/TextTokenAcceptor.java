package net.lariverosc.text.filter;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface TextTokenAcceptor {

    boolean accept(TextToken textToken);
}
