package net.lariverosc.text.filter;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface TokenAcceptor {

    boolean accept(String token);
}
