package net.lariverosc.text.filter;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface TokenCharAcceptor {

    boolean isTokenChar(int c);
}
