package net.lariverosc.text.filter;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TypeAcceptor implements TextTokenAcceptor {

    private final TextToken.Type type;

    public TypeAcceptor(TextToken.Type type) {
        this.type = type;
    }

    @Override
    public boolean accept(TextToken textToken) {
        return type.equals(textToken.getType());
    }

}
