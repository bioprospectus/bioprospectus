package net.lariverosc.text.index;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 * @param <v>
 */
public class Context<v extends Number> {

    private final Map<String, Map<String, v>> contextMap;

    public Context() {
        this.contextMap = new HashMap<>();
    }

    public void addEntry(String term, String contextTerm, v value) {
        Map<String, v> tempMap = contextMap.get(term);
        if (tempMap == null) {
            tempMap = new HashMap<>();
        }
        tempMap.put(contextTerm, value);
        contextMap.put(term, tempMap);
    }

    public Set<String> getContext(String term) {
        Map<String, v> tempMap = contextMap.get(term);
        if (tempMap != null) {
            return tempMap.keySet();
        }
        return Collections.EMPTY_SET;
    }

    public v getValue(String term, String contextTerm) {
        Map<String, v> tempMap = contextMap.get(term);
        if (tempMap != null) {
            return tempMap.get(contextTerm);
        }
        return (v) new Double(0);
    }

}
