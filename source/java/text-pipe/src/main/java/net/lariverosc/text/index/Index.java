package net.lariverosc.text.index;

import java.util.List;
import java.util.Set;
import net.lariverosc.text.TextSpan;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 * @param <k>
 */
public abstract class Index<k> {

    /**
     *
     */
    protected final String name;

    /**
     *
     * @param name
     */
    public Index(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Computes and returns the TF-IDF weight for the given document and term
     * @param documentId
     * @param term
     * @return
     */
    public double getTFIDF(k documentId, String term) {
        double tf = Math.sqrt(getInDocumentFrequency(documentId, term));
        double idf = Math.log(getTotalDocuments() / getDocumentFrequency(term) + 1) + 1;
        return tf * idf;
    }

    /**
     * Add a term occurrence.
     * @param documentId
     * @param term
     * @param position Can be null
     */
    public abstract void addTermOccurrence(k documentId, String term, TextSpan position);

    /**
     *
     * @return
     */
    public abstract Set<String> getTerms();

    /**
     *
     * @param term
     * @return
     */
    public abstract Set<k> getDocuments(String term);

    /**
     *
     * @param term
     * @return
     */
    public abstract Set<TermOccurrence> getOccurrences(String term);

    /**
     *
     * @param term
     * @return
     */
    public abstract int getGlobalFrequency(String term);

    /**
     * Returns the number of documents that contains the given term.
     * @param term
     * @return
     */
    public abstract int getDocumentFrequency(String term);

    /**
     *Returns the frequency of the term in the given document.
     * @param documentId
     * @param term
     * @return
     */
    public abstract int getInDocumentFrequency(k documentId, String term);

    /**
     * Returns the total number of documents in the index.
     * @return
     */
    public abstract int getTotalDocuments();

    /**
     *
     * @param documentId
     * @return
     */
    public abstract List<String> getDocumentTerms(k documentId);

    /**
     *
     * @param documentId
     * @return
     */
    public abstract int getDocumentLength(k documentId);

}
