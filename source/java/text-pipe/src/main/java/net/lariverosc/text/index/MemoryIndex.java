package net.lariverosc.text.index;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.SetMultimap;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.text.TextSpan;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 * @param <k>
 */
public class MemoryIndex<k> extends Index<k> {

    private final Multiset<String> terms;

    private final Map<String, Multiset<k>> invertedIndex;

    private final SetMultimap<String, TermOccurrence> occurrencesIndex;

    private final ListMultimap<k, String> documentTerms;

    public MemoryIndex(String name) {
        super(name);
        terms = HashMultiset.create();
        invertedIndex = new HashMap<>();
        occurrencesIndex = HashMultimap.create();
        documentTerms = ArrayListMultimap.create();
    }

    @Override
    public void addTermOccurrence(k documentId, String term, TextSpan position) {
        //Add term to index terms set
        terms.add(term);

        //Add document to inverted index
        Multiset<k> documents = invertedIndex.get(term);
        if (documents == null) {
            documents = HashMultiset.create();
        }
        documents.add(documentId);
        invertedIndex.put(term, documents);

        if (position != null) {
            //Add term to occurrences index i.e. with position
            occurrencesIndex.put(term, new TermOccurrence(documentId, position));
        }

        //Add term to document terms list
        documentTerms.put(documentId, term);
    }

    @Override
    public Set<String> getTerms() {
        return terms.elementSet();
    }

    @Override
    public Set<k> getDocuments(String term) {
        Multiset<k> multiset = invertedIndex.get(term);
        if (multiset != null) {
            return multiset.elementSet();
        }
        return Collections.EMPTY_SET;
    }

    @Override
    public Set<TermOccurrence> getOccurrences(String term) {
        return occurrencesIndex.get(term);
    }

    @Override
    public int getGlobalFrequency(String term) {
        return terms.count(term);
    }

    @Override
    public int getDocumentFrequency(String term) {
        return getDocuments(term).size();
    }

    @Override
    public int getInDocumentFrequency(k documentId, String term) {
        return invertedIndex.get(term).count(documentId);
    }

    @Override
    public int getTotalDocuments() {
        return documentTerms.keySet().size();
    }

    @Override
    public List<String> getDocumentTerms(k documentId) {
        return documentTerms.get(documentId);
    }

    @Override
    public int getDocumentLength(k documentId) {
        return getDocumentTerms(documentId).size();
    }

}
