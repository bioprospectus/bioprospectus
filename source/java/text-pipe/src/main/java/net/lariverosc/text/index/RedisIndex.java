package net.lariverosc.text.index;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.lariverosc.text.TextSpan;
import redis.clients.jedis.Jedis;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class RedisIndex extends Index<String> {

    private static final String SEP = ":";

    private final String INDEX_NAME;

    private final String TERMS_KEY;

    private final String DOCUMENTS_KEY;

    private final String INVERTED_INDEX_PREFIX;

    private final String OCCURRENCES_INDEX_PREFIX;

    private final String DOCUMENT_TERMS_PREFIX;

    private final boolean readOnly;

    private Jedis jedis;

    public RedisIndex(String name) {
        this(name, false);
    }

    public RedisIndex(String name, boolean readOnly) {
        super(name);
        this.INDEX_NAME = getBasePrefix(name);
        this.readOnly = readOnly;
        /*This key corresponds to a hash which stores the global frequency of the terms
         {term1: 20, term2: 57 }*/
        this.TERMS_KEY = INDEX_NAME + SEP + "TERMS";
        /*This key corresponds to a set which stores all the documents ids in the index {doc1, doc2}*/
        this.DOCUMENTS_KEY = INDEX_NAME + SEP + "DOCS";//SET
        /*This prefix is followed by the term and corresponds to a hash which stores the documents in
         which the corresponding term appears along with its frequency within the corresponding document
         INVERTED_INDEX_PREFIX:term-> {doc1:1, doc2:3}*/
        this.INVERTED_INDEX_PREFIX = INDEX_NAME + SEP + "I_IDX" + SEP;//HASH
        /*This prefix is followed by the term and corresponds to a set which stores the documents in
         which the corresponding term appears along with its position
         OCCURRENCES_INDEX_PREFIX:term-> {doc1=0-1, doc1=5-7, doc2=3-5}*/
        this.OCCURRENCES_INDEX_PREFIX = INDEX_NAME + SEP + "O_IDX" + SEP;//SET
        /*This prefix is followed by the documentId and corresponds to a list which stores the terms
         in the document
         DOCUMENT_TERMS_PREFIX:documentId-> [term1,term2,term1]*/
        this.DOCUMENT_TERMS_PREFIX = INDEX_NAME + SEP + "D_TER" + SEP;//LIST
    }

    private String getBasePrefix(String indexName) {
        final int basePrefixSize = 10;
        return indexName.length() > basePrefixSize ? indexName.toUpperCase().substring(0, basePrefixSize) : indexName.toUpperCase();
    }

    @Override
    public String getName() {
        return INDEX_NAME;
    }

    @Override
    public void addTermOccurrence(String documentId, String term, TextSpan position) {
        if (!readOnly) {
            jedis.hincrBy(TERMS_KEY, term, 1);

            jedis.sadd(DOCUMENTS_KEY, documentId);

            jedis.hincrBy(INVERTED_INDEX_PREFIX + term, documentId, 1);

            if (position != null) {
                jedis.sadd(OCCURRENCES_INDEX_PREFIX + term, documentId + "=" + position.toString());
            }

            jedis.rpush(DOCUMENT_TERMS_PREFIX + documentId, term);
        }
    }

    @Override
    public Set<String> getTerms() {
        return jedis.hkeys(TERMS_KEY);
    }

    @Override
    public Set<String> getDocuments(String term) {
        return jedis.hkeys(INVERTED_INDEX_PREFIX + term);
    }

    @Override
    public Set<TermOccurrence> getOccurrences(String term) {
        Set<String> occurences = jedis.smembers(OCCURRENCES_INDEX_PREFIX + term);
        Set<TermOccurrence> temp = new HashSet<>();
        for (String occurenceStr : occurences) {
            String[] docAndTextSpan = occurenceStr.split("=");
            String documentId = docAndTextSpan[0];
            TextSpan textSpan = new TextSpan(docAndTextSpan[1]);
            temp.add(new TermOccurrence<>(documentId, textSpan));
        }
        return temp;
    }

    @Override
    public int getGlobalFrequency(String term) {
        return parseInt(jedis.hget(TERMS_KEY, term));
    }

    @Override
    public int getDocumentFrequency(String term) {
        return getDocuments(term).size();
    }

    @Override
    public int getInDocumentFrequency(String documentId, String term) {
        return parseInt(jedis.hget(INVERTED_INDEX_PREFIX + term, documentId));
    }

    @Override
    public int getTotalDocuments() {
        return jedis.scard(DOCUMENTS_KEY).intValue();
    }

    @Override
    public List<String> getDocumentTerms(String documentId) {
        return jedis.lrange(DOCUMENT_TERMS_PREFIX + documentId, 0, -1);
    }

    @Override
    public int getDocumentLength(String documentId) {
        return parseInt(jedis.hget(DOCUMENTS_KEY, documentId));
    }

    private int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }

    public void setJedis(Jedis jedis) {
        this.jedis = jedis;
    }

    public static void main(String[] args) {
        RedisIndex redisIndex = new RedisIndex("testIdx");
        redisIndex.setJedis(new Jedis("10.0.2.15"));
        redisIndex.addTermOccurrence("doc1", "w1", new TextSpan(0, 2));
        redisIndex.addTermOccurrence("doc1", "w2", new TextSpan(0, 2));
        redisIndex.addTermOccurrence("doc2", "w1", new TextSpan(0, 2));
        redisIndex.addTermOccurrence("doc2", "w1", new TextSpan(3, 5));
        redisIndex.addTermOccurrence("doc3", "w1", new TextSpan(3, 5));
        redisIndex.addTermOccurrence("doc3", "w3", new TextSpan(3, 5));
    }
}
