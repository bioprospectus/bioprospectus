package net.lariverosc.text.index;

import java.util.Objects;
import net.lariverosc.text.TextSpan;

/**
 * This class represents a term occurrence within a document as a pair {documentId, position}.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 * @param <k>
 */
public class TermOccurrence<k> {

    /*
     * The document id
     */
    private final k documentId;

    /*
     * The position of the term ocurrence within this document.
     */
    private final TextSpan position;

    public TermOccurrence(k documentId, TextSpan position) {
        this.documentId = documentId;
        this.position = position;
    }

    public k getDocumentId() {
        return documentId;
    }

    public TextSpan getPosition() {
        return position;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.documentId);
        hash = 47 * hash + Objects.hashCode(this.position);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TermOccurrence<?> other = (TermOccurrence<?>) obj;
        if (!Objects.equals(this.documentId, other.documentId)) {
            return false;
        }
        if (!Objects.equals(this.position, other.position)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return documentId.toString() + "=" + position.toString();
    }

}
