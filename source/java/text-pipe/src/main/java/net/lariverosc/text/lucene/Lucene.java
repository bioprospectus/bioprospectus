package net.lariverosc.text.lucene;

import org.apache.lucene.util.Version;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class Lucene {

    public static final Version VERSION = Version.LATEST;

}
