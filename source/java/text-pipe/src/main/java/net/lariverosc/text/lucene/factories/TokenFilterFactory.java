package net.lariverosc.text.lucene.factories;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import net.lariverosc.text.filter.TokenAcceptor;
import net.lariverosc.text.lucene.Lucene;
import net.lariverosc.text.lucene.filter.StopFilter;
import net.lariverosc.text.pipe.TextPipeConfig;
import org.apache.commons.codec.language.DoubleMetaphone;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.UpperCaseFilter;
import org.apache.lucene.analysis.en.EnglishMinimalStemFilter;
import org.apache.lucene.analysis.es.SpanishLightStemFilter;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterFilterFactory;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.apache.lucene.analysis.pattern.PatternReplaceFilter;
import org.apache.lucene.analysis.phonetic.PhoneticFilter;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.sinks.TeeSinkTokenFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.AbstractAnalysisFactory;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.FilteringTokenFilter;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TokenFilterFactory {

    private TokenFilterFactory() {
    }

    /**
     *
     * @param tokenStream
     * @param min
     * @param max
     * @param enablePositionsIncrements
     * @return
     */
    public static TokenFilter buildLengthFilter(TokenStream tokenStream, int min, int max, boolean enablePositionsIncrements) {
        if (enablePositionsIncrements) {
            LengthFilter lengthFilter = new LengthFilter(tokenStream, min, max);
            return lengthFilter;
        }
        return new LengthFilter(tokenStream, min, max);
    }

    /**
     *
     * @param tokenStream
     * @return
     */
    public static TokenFilter buildLowerCaseFilter(TokenStream tokenStream) {
        return new LowerCaseFilter(tokenStream);
    }

    /**
     *
     * @param tokenStream
     * @return
     */
    public static TokenFilter buildUpperCaseFilter(TokenStream tokenStream) {

        return new UpperCaseFilter(tokenStream);
    }

    public static TokenFilter buildStopFilter(TokenStream tokenStream, CharArraySet stopWords, boolean enablePositionsIncrements) {
        if (enablePositionsIncrements) {
            StopFilter stopFilter = new StopFilter(tokenStream, stopWords);
            return stopFilter;
        }
        return new StopFilter(tokenStream, stopWords);
    }

    /**
     *
     * @param tokenStream
     * @param preserveOriginal
     * @return
     */
    public static TokenFilter buildASCIIFoldingFilter(TokenStream tokenStream, boolean preserveOriginal) {
        return new ASCIIFoldingFilter(tokenStream, preserveOriginal);
    }

    /**
     *
     * @param tokenStream
     * @param min
     * @param max
     * @return
     */
    public static TokenFilter buildNGramTokenFilter(TokenStream tokenStream, int min, int max) {
        return new NGramTokenFilter(tokenStream, min, max);
    }

    /**
     *
     * @param tokenStream
     * @param min
     * @param max
     * @param outputUnigrams
     * @return
     */
    public static TokenFilter buildShingleFilter(TokenStream tokenStream, int min, int max, boolean outputUnigrams) {
        ShingleFilter shingleFilter = new ShingleFilter(tokenStream, min, max);
        shingleFilter.setOutputUnigrams(outputUnigrams);
        return shingleFilter;
    }

    /**
     *
     * @param tokenStream
     * @param tokenAcceptor
     * @return
     */
    public static TokenFilter buildFilteringTokenFilter(TokenStream tokenStream, final TokenAcceptor tokenAcceptor) {
        return new FilteringTokenFilter(Lucene.VERSION, tokenStream) {
            private final CharTermAttribute charTermAttribute = addAttribute(CharTermAttribute.class);

            @Override
            protected boolean accept() throws IOException {
                String token = charTermAttribute.toString();
                return tokenAcceptor.accept(token);
            }
        };
    }

    /**
     *
     * @param tokenStream
     * @param pattern
     * @param replacement
     * @return
     */
    public static TokenFilter buildPatternReplaceFilter(TokenStream tokenStream, String pattern, String replacement) {
        return new PatternReplaceFilter(tokenStream, Pattern.compile(pattern), replacement, true);
    }

    /**
     * Use codec's from org.apache.commons.codec.language package
     *
     * @param tokenStream
     * @param replace
     * @return
     */
    public static TokenFilter buildPhoneticFilter(TokenStream tokenStream, boolean replace) {
        return new PhoneticFilter(tokenStream, new DoubleMetaphone(), replace);
    }

    public static TokenFilter buildStemFilter(TokenStream tokenStream, TextPipeConfig.Language language) {
        if (language.equals(TextPipeConfig.Language.ENGLISH)) {
            return new EnglishMinimalStemFilter(tokenStream);
        }
        if (language.equals(TextPipeConfig.Language.SPANISH)) {
            return new SpanishLightStemFilter(tokenStream);
        }
        return null;
    }

    public static TokenFilter buildWordDelimiterFilter(TokenStream tokenStream, TextPipeConfig.WordDelimiterConfig wdc) {
        return buildWordDelimiterFilter(tokenStream,
                wdc.isPreserveOriginal(),
                wdc.isSplitOnCaseChange(),
                wdc.isSplitOnNumerics(),
                wdc.isGenerateWordParts(),
                wdc.isGenerateNumberParts(),
                wdc.isCatenateWords(),
                wdc.isCatenateNumbers(),
                wdc.isCatenateAll());
    }

    public static TokenFilter buildWordDelimiterFilter(TokenStream tokenStream,
            boolean preserveOriginal,
            boolean splitOnCaseChange,
            boolean splitOnNumerics,
            boolean generateWordParts,
            boolean generateNumberParts,
            boolean catenateWords,
            boolean catenateNumbers,
            boolean catenateAll) {
        Map<String, String> args = new HashMap<>();
        args.put(AbstractAnalysisFactory.LUCENE_MATCH_VERSION_PARAM, "LATEST");
        args.put("preserveOriginal", preserveOriginal ? "1" : "0");
        args.put("splitOnCaseChange", splitOnCaseChange ? "1" : "0");
        args.put("splitOnNumerics", splitOnNumerics ? "1" : "0");
        args.put("generateWordParts", generateWordParts ? "1" : "0");
        args.put("generateNumberParts", generateNumberParts ? "1" : "0");
        args.put("catenateWords", catenateWords ? "1" : "0");
        args.put("catenateNumbers", catenateNumbers ? "1" : "0");
        args.put("catenateAll", catenateAll ? "1" : "0");
        args.put("stemEnglishPossessive", "0");
        return new WordDelimiterFilterFactory(args).create(tokenStream);
    }

    public static TokenFilter buildTeeSinkTokenFilter(TokenStream tokenStream) {
        return new TeeSinkTokenFilter(tokenStream);
    }
}
