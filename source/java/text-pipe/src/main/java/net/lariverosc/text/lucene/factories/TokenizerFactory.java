package net.lariverosc.text.lucene.factories;

import java.io.Reader;
import java.util.regex.Pattern;
import net.lariverosc.text.filter.TokenCharAcceptor;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LetterTokenizer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.pattern.PatternTokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharTokenizer;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TokenizerFactory {

    public static Tokenizer buildStandardTokenizer(Reader reader, int maxTokenLength) {
        StandardTokenizer standardTokenizer = new StandardTokenizer(reader);
        standardTokenizer.setMaxTokenLength(maxTokenLength);
        return standardTokenizer;
    }

    public static Tokenizer buildWhitespaceTokenizer(Reader reader) {
        return new WhitespaceTokenizer(reader);
    }

    public static Tokenizer buildLetterTokenizer(Reader reader) {
        return new LetterTokenizer(reader);
    }

    public static Tokenizer buildPatternTokenizer(Reader reader, String pattern) {
        return new PatternTokenizer(reader, Pattern.compile(pattern), -1);
    }

    public static Tokenizer customTokenizer(Reader reader, TokenCharAcceptor tokenCharAcceptor) {
        return new CustomTokenizer(reader, tokenCharAcceptor);
    }
}

class CustomTokenizer extends CharTokenizer {

    private final TokenCharAcceptor tokenCharAcceptor;

    public CustomTokenizer(Reader in, TokenCharAcceptor tokenCharAcceptor) {
        super(in);
        this.tokenCharAcceptor = tokenCharAcceptor;
    }

    @Override
    protected boolean isTokenChar(int c) {
        return tokenCharAcceptor.isTokenChar(c);
    }
}
