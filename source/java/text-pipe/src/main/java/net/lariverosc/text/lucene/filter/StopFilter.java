package net.lariverosc.text.lucene.filter;

import java.io.IOException;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.FilteringTokenFilter;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class StopFilter extends FilteringTokenFilter {

    private final CharArraySet stopWords;
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);

    public StopFilter(TokenStream in, CharArraySet stopWords) {
        super(in);
        this.stopWords = stopWords;
    }

    @Override
    protected boolean accept() throws IOException {
        return !stopWords.contains(termAtt.buffer(), 0, termAtt.length());
    }
}
