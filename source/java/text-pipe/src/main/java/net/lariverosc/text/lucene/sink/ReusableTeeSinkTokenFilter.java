package net.lariverosc.text.lucene.sink;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.util.AttributeImpl;
import org.apache.lucene.util.AttributeSource;

/**
 * Same as org.apache.lucene.analysis.sinks.TeeSinkTokenFilter but without weakReferences
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public final class ReusableTeeSinkTokenFilter extends TokenFilter {

    private final List<SinkTokenStream> sinks = new LinkedList<>();

    /**
     * Instantiates a new TeeSinkTokenFilter.
     */
    public ReusableTeeSinkTokenFilter(TokenStream input) {
        super(input);
    }

    /**
     * Returns a new {@link SinkTokenStream} that receives all tokens consumed by this stream.
     */
    public SinkTokenStream newSinkTokenStream() {
        return newSinkTokenStream(ACCEPT_ALL_FILTER);
    }

    /**
     * Returns a new {@link SinkTokenStream} that receives all tokens consumed by this stream that pass the supplied
     * filter.
     *
     * @param filter
     * @return
     */
    public SinkTokenStream newSinkTokenStream(SinkFilter filter) {
        SinkTokenStream sink = new SinkTokenStream(this.cloneAttributes(), filter);
        this.sinks.add(sink);
        return sink;
    }

    /**
     * Adds a {@link SinkTokenStream} created by another <code>TeeSinkTokenFilter</code> to this one. The supplied
     * stream will also receive all consumed tokens. This method can be used to pass tokens from two different tees to
     * one sink.
     * @param sink
     */
    public void addSinkTokenStream(final SinkTokenStream sink) {
        // check that sink has correct factory
        if (!this.getAttributeFactory().equals(sink.getAttributeFactory())) {
            throw new IllegalArgumentException("The supplied sink is not compatible to this tee");
        }
        // add eventually missing attribute impls to the existing sink
        for (Iterator<AttributeImpl> it = this.cloneAttributes().getAttributeImplsIterator(); it.hasNext();) {
            sink.addAttributeImpl(it.next());
        }
        this.sinks.add(sink);
    }

    /**
     * <code>TeeSinkTokenFilter</code> passes all tokens to the added sinks when itself is consumed. To be sure, that
     * all tokens from the input stream are passed to the sinks, you can call this methods. This instance is exhausted
     * after this, but all sinks are instant available.
     */
    public void consumeAllTokens() throws IOException {
        while (incrementToken()) {
        }
    }

    @Override
    public boolean incrementToken() throws IOException {
        if (input.incrementToken()) {
            // capture state lazily - maybe no SinkFilter accepts this state
            AttributeSource.State state = null;
            for (SinkTokenStream sink: sinks) {
                if (sink.accept(this)) {
                    if (state == null) {
                        state = this.captureState();
                    }
                    sink.addState(state);
                }
            }
            return true;
        }

        return false;
    }

    @Override
    public final void end() throws IOException {
        super.end();
        AttributeSource.State finalState = captureState();
        for (SinkTokenStream sink: sinks) {
            sink.setFinalState(finalState);
        }
    }

    @Override
    public void close() throws IOException {
        super.close();
        for (SinkTokenStream sink: sinks) {
            sink.reset();
        }
    }

    /**
     * A filter that decides which {@link AttributeSource} states to store in the sink.
     */
    public static abstract class SinkFilter {

        /**
         * Returns true, iff the current state of the passed-in {@link AttributeSource} shall be stored in the sink.
         */
        public abstract boolean accept(AttributeSource source);

        /**
         * Called by {@link SinkTokenStream#reset()}. This method does nothing by default and can optionally be
         * overridden.
         */
        public void reset() throws IOException {
            // nothing to do; can be overridden
        }
    }

    /**
     * TokenStream output from a tee with optional filtering.
     */
    public static final class SinkTokenStream extends TokenStream {

        private final List<AttributeSource.State> cachedStates = new LinkedList<>();
        private AttributeSource.State finalState;
        private Iterator<AttributeSource.State> it;
        private final SinkFilter filter;

        private SinkTokenStream(AttributeSource source, SinkFilter filter) {
            super(source);
            this.filter = filter;
        }

        private boolean accept(AttributeSource source) {
            return filter.accept(source);
        }

        private void addState(AttributeSource.State state) {
            if (it != null) {
                throw new IllegalStateException("The tee must be consumed before sinks are consumed.");
            }
            cachedStates.add(state);
        }

        private void setFinalState(AttributeSource.State finalState) {
            this.finalState = finalState;
        }

        @Override
        public final boolean incrementToken() {
            // lazy init the iterator
            if (it == null) {
                it = cachedStates.iterator();
            }

            if (!it.hasNext()) {
                return false;
            }

            AttributeSource.State state = it.next();
            restoreState(state);
            return true;
        }

        @Override
        public final void end() {
            if (finalState != null) {
                restoreState(finalState);
            }
        }

        @Override
        public final void reset() {
            cachedStates.clear();
            finalState = null;
            it = null;
        }
    }

    private static final SinkFilter ACCEPT_ALL_FILTER = new SinkFilter() {
        @Override
        public boolean accept(AttributeSource source) {
            return true;
        }
    };

}
