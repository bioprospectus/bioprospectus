package net.lariverosc.text.lucene.sink.matcher;

import com.google.common.base.CharMatcher;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.lucene.sink.ReusableTeeSinkTokenFilter;
import net.lariverosc.text.util.TokenFeatures;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PayloadAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.util.AttributeSource;
import org.apache.lucene.util.BytesRef;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AbbreviationDictMatcher extends ReusableTeeSinkTokenFilter.SinkFilter {

    private final TextToken.Type type = TextToken.Type.ABBREVIATION;

    private final String dictionaryName;

    private final Map<String, String> abbreviations;

    public AbbreviationDictMatcher(String dictionaryName, Map<String, String> abbreviations, boolean generateVariants) {
        this.dictionaryName = dictionaryName;
        if (generateVariants) {
            this.abbreviations = generateDictVariants(abbreviations);
        } else {
            this.abbreviations = new HashMap<String, String>(abbreviations);
        }
    }

    private Map<String, String> generateDictVariants(Map<String, String> abbreviations) {
        Map<String, String> temp = new HashMap<String, String>();
        for (String abbreviation: abbreviations.keySet()) {
            String payload = abbreviations.get(abbreviation);
            Set<String> generateAbbreviationVariants = generateAbbreviationVariants(abbreviation);
            for (String abbreviationVariant: generateAbbreviationVariants) {
                temp.put(abbreviationVariant, payload);
            }
        }

        return temp;
    }

    private Set<String> generateAbbreviationVariants(String abbreviation) {
        Set<String> temp = new HashSet<String>();
        //As is
        temp.add(abbreviation);
        //Ending with point
        if (!abbreviation.endsWith(".")) {
            temp.add(abbreviation + ".");
        }
        TokenFeatures tokenShape = TokenFeatures.extract(abbreviation);
        if (tokenShape.isAllUpperCaseLetter()) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < abbreviation.length(); i++) {
                sb.append(abbreviation.charAt(i)).append(".");
            }
            temp.add(sb.toString());
        } else if (tokenShape.containsPunctuationMarks()) {
            temp.add(CharMatcher.JAVA_LETTER.retainFrom(abbreviation));
        }

        return temp;
    }

    @Override
    public boolean accept(AttributeSource source) {
        String charTermAttributeString = source.getAttribute(CharTermAttribute.class).toString();
        if (abbreviations.containsKey(charTermAttributeString)) {
            source.getAttribute(TypeAttribute.class).setType(type.getName());
            source.getAttribute(PayloadAttribute.class).setPayload(new BytesRef(dictionaryName + ":" + abbreviations.get(charTermAttributeString)));
            return true;
        }
        return false;
    }

}
