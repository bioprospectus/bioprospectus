package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AcronymMatcher extends RegexSinkMatcher {

    public AcronymMatcher() {
        super(TextToken.Type.ACRONYM, "^\\(?([A-Z](\\.)?){2,6}\\)?$");
    }
}
