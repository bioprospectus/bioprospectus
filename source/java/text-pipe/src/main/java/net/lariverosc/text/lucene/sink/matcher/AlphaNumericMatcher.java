package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AlphaNumericMatcher extends RegexSinkMatcher{

    public AlphaNumericMatcher() {
        super(TextToken.Type.ALPHANUMERIC, "^(?=.*[0-9])(?=.*[A-Za-z])[A-Za-z0-9_-]{2,}$");
    }
}
