package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DateTimeMatcher extends RegexSinkMatcher {

    public DateTimeMatcher() {
        super(TextToken.Type.DATETIME, "^(?=\\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\\s|$))|(?:2[0-8]|1\\d|0?[1-9]))([-./])(?:1[012]|0?[1-9])\\1(?:1[6-9]|[2-9]\\d)?\\d\\d(?:(?=\\s\\d)\\s|$))?(((0?[1-9]|1[012])(:[0-5]\\d){0,2}(\\s[AP]M))|([01]\\d|2[0-3])(:[0-5]\\d){1,2})?$");
    }
}
