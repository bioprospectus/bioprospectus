package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DecimalNumberMatcher extends RegexSinkMatcher {

    public DecimalNumberMatcher() {
        super(TextToken.Type.NUMBER, "^(\\+|-)?\\d+((\\.|,)\\d+)?");
    }

}
