
package net.lariverosc.text.lucene.sink.matcher;

import java.util.HashSet;
import java.util.Set;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.lucene.sink.ReusableTeeSinkTokenFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PayloadAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.util.AttributeSource;
import org.apache.lucene.util.BytesRef;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DictionaryMatcher extends ReusableTeeSinkTokenFilter.SinkFilter {

    private final TextToken.Type type = TextToken.Type.DICT_TERM;

    private final String dictionaryName;

    private final Set<String> dictionary;

    public DictionaryMatcher(String dictionaryName, Set<String> dictionary) {
        this.dictionaryName = dictionaryName;
        this.dictionary = new HashSet<String>();
        for (String term: dictionary) {
            this.dictionary.add(term.toLowerCase());
        }

    }

    @Override
    public boolean accept(AttributeSource source) {
        String charTermAttrbuteString = source.getAttribute(CharTermAttribute.class).toString().toLowerCase();
        if (dictionary.contains(charTermAttrbuteString)) {
            source.getAttribute(TypeAttribute.class).setType(type.getName());
            source.getAttribute(PayloadAttribute.class).setPayload(new BytesRef(dictionaryName));
            return true;
        }
        return false;
    }
}
