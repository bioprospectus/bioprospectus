package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.AttributeSource;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class EmailMatcher extends RegexSinkMatcher {

    public EmailMatcher() {
        super(TextToken.Type.EMAIL, "[A-Za-z0-9](([_\\.\\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.\\-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,})");
    }

    @Override
    public boolean accept(AttributeSource source) {
        String token = source.getAttribute(CharTermAttribute.class).toString();
        int lastIndexOf = token.lastIndexOf("@");
        if (lastIndexOf > 0) {
            return super.accept(source);
        }
        return false;
    }

}
