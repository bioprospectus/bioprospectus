package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class GeneSequenceMatcher extends RegexSinkMatcher {

    public GeneSequenceMatcher() {
        super(TextToken.Type.GENE_SEQ, "[ACGT]{4,}","[acgt]{4,}");
    }

}
