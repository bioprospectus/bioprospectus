package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MoneyMatcher extends RegexSinkMatcher {

    public MoneyMatcher() {
        super(TextToken.Type.MONEY, "^(\\$(\\d+|\\d{1,3}(,\\d{3})*)(\\.\\d+)?)$");
    }
}
