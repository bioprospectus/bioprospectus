package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class PercentageMatcher extends RegexSinkMatcher {

    public PercentageMatcher() {
        super(TextToken.Type.PERCENTAGE, "^(\\+|-)?(\\d{1,3})(\\.\\d+)?%$");
    }
}
