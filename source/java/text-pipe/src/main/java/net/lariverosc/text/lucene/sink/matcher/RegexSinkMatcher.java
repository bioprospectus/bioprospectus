package net.lariverosc.text.lucene.sink.matcher;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.lucene.sink.ReusableTeeSinkTokenFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.util.AttributeSource;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class RegexSinkMatcher extends ReusableTeeSinkTokenFilter.SinkFilter {

    private final TextToken.Type type;

    private final Set<Matcher> matchers;

    public RegexSinkMatcher(TextToken.Type type, String... regex) {
        this.type = type;
        this.matchers = new HashSet<>();
        for (String currentRegex: regex) {
            this.matchers.add(Pattern.compile(currentRegex).matcher(""));
        }
    }

    @Override
    public boolean accept(AttributeSource source) {
        CharTermAttribute charTermAtt = source.getAttribute(CharTermAttribute.class);
        for (Matcher matcher : matchers) {
            if (matcher.reset(charTermAtt).matches()) {
                TypeAttribute typeAtt = source.getAttribute(TypeAttribute.class);
                typeAtt.setType(type.getName());
                return true;
            }
        }
        return false;
    }
}
