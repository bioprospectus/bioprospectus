package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;

/**
 * Match Roman numbers up to 4999
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class RomanNumberMatcher extends RegexSinkMatcher {

    public RomanNumberMatcher() {
        super(TextToken.Type.ROMAN, "^M{0,5}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$");
    }

}
