package net.lariverosc.text.lucene.sink.matcher;

import net.lariverosc.text.TextToken;
import net.lariverosc.text.lucene.sink.ReusableTeeSinkTokenFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.CharacterUtils;
import org.apache.lucene.util.ArrayUtil;
import org.apache.lucene.util.AttributeSource;
import org.apache.lucene.util.RamUsageEstimator;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class StopMatcher extends ReusableTeeSinkTokenFilter.SinkFilter {

    private final CharArraySet stopWords;

    private final CharacterUtils charUtils = CharacterUtils.getInstance();

    private final TextToken.Type type = TextToken.Type.STOP;

    public StopMatcher(CharArraySet stopWords) {
        this.stopWords = stopWords;
    }

    @Override
    public boolean accept(AttributeSource source) {
        CharTermAttribute termAtt = source.getAttribute(CharTermAttribute.class);
        char[] copyBuffer = new char[ArrayUtil.oversize(termAtt.length(), RamUsageEstimator.NUM_BYTES_CHAR)];
        System.arraycopy(termAtt.buffer(), 0, copyBuffer, 0, termAtt.length());
        charUtils.toLowerCase(copyBuffer, 0, termAtt.length());
        boolean isStop = stopWords.contains(copyBuffer, 0, termAtt.length());
        if (isStop) {
            TypeAttribute typeAtt = source.getAttribute(TypeAttribute.class);
            typeAtt.setType(type.getName());
            return true;
        }
        return false;
    }

}
