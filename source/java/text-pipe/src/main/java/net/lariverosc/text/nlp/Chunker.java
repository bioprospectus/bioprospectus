package net.lariverosc.text.nlp;

import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface Chunker {

    TextChunk[] getChunks(String sentence);
}
