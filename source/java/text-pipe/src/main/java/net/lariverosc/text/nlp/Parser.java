package net.lariverosc.text.nlp;

import net.lariverosc.text.util.tree.TreeNode;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface Parser {

    TreeNode<ParserPayload> parse(String chunk);
}
