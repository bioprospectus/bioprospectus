package net.lariverosc.text.nlp;

import net.lariverosc.text.TextChunk;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class ParserPayload {

    private TextChunk textChunk;

    private String type;

    private String label;

    private double score;

    public TextChunk getTextChunk() {
        return textChunk;
    }

    public void setTextChunk(TextChunk textChunk) {
        this.textChunk = textChunk;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "ParserPayload{" + "textChunk=" + textChunk + ", type=" + type + ", label=" + label + ", score=" + score + '}';
    }

}
