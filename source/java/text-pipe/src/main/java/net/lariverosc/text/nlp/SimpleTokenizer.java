package net.lariverosc.text.nlp;

import java.util.List;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;

/**
 *
 * @author alejandro
 */
public class SimpleTokenizer implements Tokenizer {

    @Override
    public TextToken[] getTokens(String s) {
        TextPipe textPipe = new TextPipe(new TextPipeConfig().withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD));
        List<TextToken> tokens = textPipe.getTokens(s);
        return tokens.toArray(new TextToken[0]);
    }

    @Override
    public String[] getTokensAsString(String s) {
        TextPipe textPipe = new TextPipe(new TextPipeConfig().withTokenizer(TextPipeConfig.Tokenizer.LUCENE_STANDARD));
        List<TextToken> tokens = textPipe.getTokens(s);
        String[] stringTokens = new String[tokens.size()];
        for (int i = 0; i < tokens.size(); i++) {
            stringTokens[i] = tokens.get(i).getText();
        }
        return stringTokens;
    }
}
