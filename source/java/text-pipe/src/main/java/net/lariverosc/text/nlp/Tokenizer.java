package net.lariverosc.text.nlp;

import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>

 */
public interface Tokenizer {

    TextToken[] getTokens(String s);

    String[] getTokensAsString(String s);

}
