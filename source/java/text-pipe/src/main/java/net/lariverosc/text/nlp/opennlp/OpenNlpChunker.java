package net.lariverosc.text.nlp.opennlp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.nlp.Chunker;
import net.lariverosc.text.nlp.PosTagger;
import net.lariverosc.text.nlp.SimpleTokenizer;
import net.lariverosc.text.nlp.Tokenizer;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.annotation.highlight.AnnotationHighlighter;
import net.lariverosc.text.annotation.highlight.DefaultAnnotationHighlighter;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.util.Span;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class OpenNlpChunker implements Chunker {

    private final Logger log = LoggerFactory.getLogger(OpenNlpChunker.class);

    private final ChunkerME chunker;

    private Tokenizer tokenizer;

    private PosTagger posTagger;

    public OpenNlpChunker(String modelPath) throws IOException {
        InputStream input = null;
        try {
            input = new FileInputStream(modelPath);
            this.chunker = new ChunkerME(new ChunkerModel(input));
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }

    }

    @Override
    public TextChunk[] getChunks(String sentence) {
        TextToken[] textTokens = tokenizer.getTokens(sentence);
        String[] tokensAsString = tokenizer.getTokensAsString(sentence);
        String[] posTags = posTagger.getPosTags(sentence);
        Span[] chunkerSpans = chunker.chunkAsSpans(tokensAsString, posTags);
        TextChunk[] chunks = new TextChunk[chunkerSpans.length];
        for (int i = 0; i < chunkerSpans.length; i++) {
            Span span = chunkerSpans[i];
            int start = textTokens[span.getStart()].getTextSpan().getStart();
            int end = textTokens[span.getEnd() - 1].getTextSpan().getEnd();
            TextSpan textSpan = new TextSpan(start, end);
            TextChunk textChunk = new TextChunk(textSpan.getCoveredText(sentence), textSpan);
            textChunk.setTokens(Arrays.asList(Arrays.copyOfRange(textTokens, span.getStart(), span.getEnd())));
            textChunk.setPayload(span.getType());
            chunks[i] = textChunk;

        }
        return chunks;
    }

    public void setTokenizer(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    public void setPosTagger(PosTagger posTagger) {
        this.posTagger = posTagger;
    }

    public static void main(String[] args) throws IOException {
        String modelsPath = "/home/alejandro/data/term-mapper/data/";
        OpenNlpPosTagger posTagger = new OpenNlpPosTagger(modelsPath + "en-pos.bin");
        posTagger.setTokenizer(new SimpleTokenizer());
        OpenNlpChunker nlpc = new OpenNlpChunker(modelsPath + "en-chunker.bin");
        nlpc.setTokenizer(new SimpleTokenizer());
        nlpc.setPosTagger(posTagger);
        TextChunk[] chunks = nlpc.getChunks("Information presented here largely gleaned from that source given non-verbal status of patient");
        for (TextChunk textChunk : chunks) {
            System.out.println(textChunk.toString());
        }
    }
}
