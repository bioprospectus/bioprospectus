package net.lariverosc.text.nlp.opennlp;

import net.lariverosc.text.nlp.ParserPayload;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.nlp.Parser;
import net.lariverosc.text.TextChunk;
import net.lariverosc.text.util.tree.TreeNode;
import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class OpenNlpParser implements Parser {

    private final Logger log = LoggerFactory.getLogger(OpenNlpParser.class);

    private final opennlp.tools.parser.Parser parser;

    public OpenNlpParser(String modelPath) throws IOException {
        InputStream input = null;
        try {
            input = new FileInputStream(modelPath);
            this.parser = ParserFactory.create(new ParserModel(input));
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public TreeNode<ParserPayload> parse(String chunk) {
        Parse[] parseLine = ParserTool.parseLine(chunk, parser, 1);
        return buildTree(parseLine[0]);
    }

    private TreeNode<ParserPayload> buildTree(Parse parse) {
        ParserPayload rootNodePayload = buildNodePayload(parse);
        TreeNode<ParserPayload> rootNode = new TreeNode(rootNodePayload);
        buildTreeRecursive(rootNode, parse);
        return rootNode;
    }

    private void buildTreeRecursive(TreeNode treeNode, Parse parse) {
        Parse[] childrens = parse.getChildren();
        for (Parse childParse: childrens) {
            if (!"TK".equals(childParse.getType())) {
                ParserPayload childNodePayload = buildNodePayload(childParse);
                TreeNode<ParserPayload> childNode = new TreeNode(childNodePayload);
                treeNode.add(childNode);
                buildTreeRecursive(childNode, childParse);
            }
        }
    }

    private ParserPayload buildNodePayload(Parse parse) {
        ParserPayload payload = new ParserPayload();
        TextChunk textChunk = new TextChunk(parse.getCoveredText());
        textChunk.setTextSpan(new TextSpan(parse.getSpan().getStart(), parse.getSpan().getEnd()));
        payload.setTextChunk(textChunk);
        payload.setType(parse.getType());
        payload.setLabel(parse.getLabel());
        payload.setScore(parse.getProb());
        return payload;
    }

    public static void main(String[] args) throws IOException {
        OpenNlpParser onp = new OpenNlpParser("/home/bio/bioprospectus/source/java/term-mapper/data/en-chunker.bin");
        TreeNode<ParserPayload> parse = onp.parse("Cultures from her abdominal wound returned Staph species sensitive to vancomycin");
        System.out.println(parse);

    }
}
