package net.lariverosc.text.nlp.opennlp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.nlp.PosTagger;
import net.lariverosc.text.nlp.Tokenizer;
import net.lariverosc.text.TextChunk;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class OpenNlpPosTagger implements PosTagger {

    private final Logger log = LoggerFactory.getLogger(OpenNlpPosTagger.class);

    private Tokenizer tokenizer;

    private final POSTaggerME tagger;

    public OpenNlpPosTagger(String modelPath) throws IOException {
        InputStream input = null;
        try {
            input = new FileInputStream(modelPath);
            this.tagger = new POSTaggerME(new POSModel(input));
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public String[] getPosTags(String text) {
        String[] textTokens = tokenizer.getTokensAsString(text);
        String[] tags = tagger.tag(textTokens);
        return tags;
    }

    @Override
    public String[] getPosTags(TextChunk textChunk) {
        List<TextToken> tokens = textChunk.getTokens();
        if (tokens == null) {
            throw new IllegalArgumentException("textChunk must contain tokens");
        }
        String[] textTokens = new String[tokens.size()];
        for (int i = 0; i < tokens.size(); i++) {
            textTokens[i] = tokens.get(i).getText();
        }
        String[] tags = tagger.tag(textTokens);
        return tags;
    }

    public void setTokenizer(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

}
