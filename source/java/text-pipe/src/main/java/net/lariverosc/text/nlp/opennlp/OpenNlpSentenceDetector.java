package net.lariverosc.text.nlp.opennlp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.nlp.SentenceDetector;
import net.lariverosc.text.TextChunk;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.Span;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class OpenNlpSentenceDetector implements SentenceDetector {

    private final Logger log = LoggerFactory.getLogger(OpenNlpSentenceDetector.class);

    private final SentenceDetectorME sentenceDetectorME;

    public OpenNlpSentenceDetector(String modelPath) throws IOException {
        InputStream input = null;
        try {
            input = new FileInputStream(modelPath);
            this.sentenceDetectorME = new SentenceDetectorME(new SentenceModel(input));

        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public TextChunk[] getSentences(String text) {
        Span[] tokenizePos = sentenceDetectorME.sentPosDetect(text);
        TextChunk[] textChunks = new TextChunk[tokenizePos.length];
        for (int i = 0; i < tokenizePos.length; i++) {
            Span span = tokenizePos[i];
            textChunks[i] = new TextChunk(span.getCoveredText(text).toString(), new TextSpan(span.getStart(), span.getEnd()));
        }
        return textChunks;
    }

    @Override
    public String[] getSentencesAsString(String text) {
        return sentenceDetectorME.sentDetect(text);
    }
}
