package net.lariverosc.text.nlp.opennlp;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.nlp.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class OpenNlpTokenizer implements Tokenizer {

    private final Logger log = LoggerFactory.getLogger(OpenNlpTokenizer.class);

    private final TokenizerME tokenizer;

    public OpenNlpTokenizer(String modelPath) throws IOException {
        InputStream input = null;
        try {
            input = new FileInputStream(modelPath);
            this.tokenizer = new TokenizerME(new TokenizerModel(input));
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
    }

    @Override
    public TextToken[] getTokens(String s) {
        Span[] tokenSpans = tokenizer.tokenizePos(s);
        TextToken[] textSpans = new TextToken[tokenSpans.length];
        for (int i = 0; i < tokenSpans.length; i++) {
            Span span = tokenSpans[i];

            textSpans[i] = new TextToken(span.getCoveredText(s).toString(), new TextSpan(span.getStart(), span.getEnd()), TextToken.Type.ALPHANUMERIC);
        }
        return textSpans;
    }

    @Override
    public String[] getTokensAsString(String s) {
        return tokenizer.tokenize(s);
    }
}
