package net.lariverosc.text.parsing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TikaParserMain {

    private final static Logger log = LoggerFactory.getLogger(TikaParserMain.class);

    public static void main(String[] args) throws FileNotFoundException {


        InputStream stream = new FileInputStream("/Users/desarrollo/workspace/messages-flow/000gQ58ot4ju9FpL.1.xml.html");
        try {
            AutoDetectParser parser = new AutoDetectParser();
            ContentHandler handler = new BodyContentHandler(System.out);
            Metadata metadata = new Metadata();
            ParseContext context = new ParseContext();
            parser.parse(stream, handler, metadata, context);

        } catch (IOException ioe) {
            log.error("Error while parsing", ioe);
        } catch (SAXException saxe) {
            log.error("Error while parsing", saxe);
        } catch (TikaException te) {
            log.error("Error while parsing", te);
        } finally {

            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException ioe) {
                }
            }


        }

    }
}
