package net.lariverosc.text.pipe;

import net.lariverosc.text.TextChunk;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.lariverosc.text.TextToken;
import net.lariverosc.text.filter.TextTokenAcceptor;
import net.lariverosc.text.util.TokenFeatures;
import org.apache.lucene.analysis.Tokenizer;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextPipe {

    private final TextPipeConfig textPipeConfig;

    private final ThreadLocal<TextTokenStream> mainTextTokenStream = new ThreadLocal<>();

    private final ThreadLocal<TextTokenStream> extractionTextTokenStream = new ThreadLocal<>();

    private final TextTokenAcceptor textTokenAcceptor;

    public TextPipe(TextPipeConfig textPipeConfig) {
        this(textPipeConfig, null);
    }

    public TextPipe(TextPipeConfig textPipeConfig, TextTokenAcceptor textTokenAcceptor) {
        this.textPipeConfig = textPipeConfig;
        this.textTokenAcceptor = textTokenAcceptor;
    }

    public List<TextToken> getTokens(String text) {
        try {
            initStreams();
            TextTokenStream mainStream = mainTextTokenStream.get();
            mainStream.reuse(text);
            List<TextToken> mainTokens = collectInternal(mainStream);

            TextTokenStream extractionStream = extractionTextTokenStream.get();
            if (extractionStream != null) {
                extractionStream.reuse(text);
                List<TextToken> extractionTokens = collectInternal(extractionStream);
                List<TextToken> temp = new ArrayList<>();
                for (TextToken mainTextToken: mainTokens) {
                    TextToken extractionTextToken = extractionTokens.get(mainTextToken.getPosition());
                    if (mainTextToken.getType().equals(extractionTextToken.getType())) {
                        temp.add(mainTextToken);
                    } else {
                        TextToken textToken = new TextToken(
                            extractionTextToken.getText(),
                            extractionTextToken.getTextSpan(),
                            extractionTextToken.getType(),
                            extractionTextToken.getTokenFeatures(),
                            extractionTextToken.getPayload(),
                            extractionTextToken.getPosition(),
                            mainTextToken.getIndex(), mainTextToken.getPosIncrement(), mainTextToken.getPosLength());
                        temp.add(textToken);
                    }
                }
                return temp;
            }
            return mainTokens;
        } catch (IOException ioe) {
            throw new TextPipeException("Exception while processing text ", ioe);
        }
    }

    private void initStreams() throws IOException {
        if (mainTextTokenStream.get() == null) {
            Tokenizer tokenizer = TextTokenStreamFactory.getTokenizer(textPipeConfig);

            mainTextTokenStream.set(TextTokenStreamFactory.getSimpleTextTokenStream(tokenizer, textPipeConfig));

            if (textPipeConfig.getTokenMatchers() != null && !textPipeConfig.getTokenMatchers().isEmpty()
                || textPipeConfig.getDictionaries() != null && !textPipeConfig.getDictionaries().isEmpty()
                || textPipeConfig.getAbbreviationDictionaries() != null && !textPipeConfig.getAbbreviationDictionaries().isEmpty()) {

                extractionTextTokenStream.set(TextTokenStreamFactory.getExtractTextTokenStream(tokenizer, textPipeConfig));
            }
        }
    }

    public TextChunk proccess(String text) {
        TextChunk textChunk = new TextChunk(text);
        textChunk.setTokens(getTokens(text));
        return textChunk;
    }

    public List<String> getTokensAsString(String text) {
        List<TextToken> tokens = getTokens(text);
        List<String> stringTokens = new ArrayList<>();
        for (TextToken textToken: tokens) {
            stringTokens.add(textToken.getText());
        }
        return stringTokens;
    }

    private List<TextToken> collectInternal(TextTokenStream textStream) throws IOException {
        List<TextToken> tokens = new ArrayList<>();
        while (textStream.nextToken()) {
            tokens.add(buildTextToken(textStream));
        }
        textStream.close();
        return tokens;
    }

    private TextToken buildTextToken(TextTokenStream textTokenStream) {
        String token = textTokenStream.getToken();
        TokenFeatures tokenFeatures = TokenFeatures.extract(token);
        TextToken textToken = new TextToken(
            token,
            textTokenStream.getSpan(),
            getTokenType(token, textTokenStream.getType(), tokenFeatures),
            tokenFeatures,
            textTokenStream.getPayload(),
            textTokenStream.getPosition(),
            textTokenStream.getIndex(),
            textTokenStream.getPositionIncrement(),
            textTokenStream.getPositionLength()
        );
        return textToken;
    }

    private TextToken.Type getTokenType(String token, String type, TokenFeatures tokenFeatures) {
        if (type != null) {
            if ("word".equals(type)) {//Lucene string for words
                return TextToken.Type.WORD;
            }
            if ("<ALPHANUM>".equals(type)) {//Lucene string for alphanumerics
                if (tokenFeatures.isAllLetter()) {
                    return TextToken.Type.WORD;
                }
                return TextToken.Type.ALPHANUMERIC;
            }
            return TextToken.Type.getByName(type);
        }
        return TextToken.Type.UNKNOW;
    }

    public TextPipeConfig getTextPipeConfig() {
        return textPipeConfig;
    }

}
