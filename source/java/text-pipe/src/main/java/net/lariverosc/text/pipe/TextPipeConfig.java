package net.lariverosc.text.pipe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.lariverosc.text.filter.TokenCharAcceptor;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextPipeConfig {

    public enum Language {

        ENGLISH("en"),
        SPANISH("sp");

        private final String lang;

        private Language(String lang) {
            this.lang = lang;
        }

        public String getShortStr() {
            return lang;
        }

    };

    public enum Tokenizer {

        WHITE_SPACE,
        LETTER,
        LUCENE_STANDARD,
        PATTERN,
        CUSTOM;

    }

    public enum TokenMatcher {
        DECIMAL_NUMBER,
        ROMAN_NUMBER,
        PERCENTAGE,
        ACRONYM,
        MONEY,
        EMAIL,
        URL,
        GENE,
        STOP,
        ALPHANUMERIC

    }

    public enum TokenTransform {

        LOWER_CASE,
        UPPER_CASE,
        STEM, //Improve this, select stemmer algorithm i.e. porter, snowball, light etc.
        ASCII_FOLDING,
        NGRAM,
        SHINGLE,
        WORD_DELIMITER;

    }

    public enum TokenFilter {

        LENGTH,
        STOP_WORD;

    }

    private Language language;

    private TextTokenStream textStream;

    private Tokenizer tokenizer;

    private String tokenizerPattern;

    private int minTokenLength = 2;

    private int maxTokenLength = 30;

    private TokenCharAcceptor tokenCharAcceptor;

    private List<TokenMatcher> tokenMatchers;

    private List<TokenTransform> tokenTransforms;

    private List<TokenFilter> tokenFilters;

    private int ngramMin = 2;

    private int ngramMax = 5;

    private int shingleMin = 2;

    private int shingleMax = 5;

    private Set<DictionaryConfig> dictionaryConfigs;

    private Set<AbbreviationsConfig> abbreviationsConfigs;

    private WordDelimiterConfig wordDelimiterConfig;

    public TextPipeConfig() {
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public TextPipeConfig withLanguage(Language language) {
        this.language = language;
        return this;
    }

    public TextTokenStream getTextStream() {
        return textStream;
    }

    public void setTextStream(TextTokenStream textStream) {
        this.textStream = textStream;
    }

    public TextPipeConfig withTextStream(TextTokenStream textStream) {
        this.textStream = textStream;
        return this;
    }

    public Tokenizer getTokenizer() {
        return tokenizer;
    }

    public void setTokenizer(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    public TextPipeConfig withTokenizer(Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
        return this;
    }

    public String getTokenizerPattern() {
        return tokenizerPattern;
    }

    public void setTokenizerPattern(String tokenizerPattern) {
        this.tokenizerPattern = tokenizerPattern;
    }

    public TextPipeConfig withTokenizerPattern(String tokenizerPattern) {
        this.tokenizerPattern = tokenizerPattern;
        return this;
    }

    public int getMinTokenLength() {
        return minTokenLength;
    }

    public void setMinTokenLength(int minTokenLength) {
        this.minTokenLength = minTokenLength;
    }

    public TextPipeConfig withMinTokenLength(int minTokenLength) {
        this.minTokenLength = minTokenLength;
        return this;
    }

    public int getMaxTokenLength() {
        return maxTokenLength;
    }

    public void setMaxTokenLength(int maxTokenLength) {
        this.maxTokenLength = maxTokenLength;
    }

    public TextPipeConfig withMaxTokenLength(int maxTokenLength) {
        this.maxTokenLength = maxTokenLength;
        return this;
    }

    public TokenCharAcceptor getTokenCharAcceptor() {
        return tokenCharAcceptor;
    }

    public void setTokenCharAcceptor(TokenCharAcceptor tokenCharAcceptor) {
        this.tokenCharAcceptor = tokenCharAcceptor;
    }

    public TextPipeConfig withTokenCharAcceptor(TokenCharAcceptor tokenCharAcceptor) {
        this.tokenCharAcceptor = tokenCharAcceptor;
        return this;
    }

    public List<TokenMatcher> getTokenMatchers() {
        return tokenMatchers;
    }

    public void setTokenMatchers(List<TokenMatcher> tokenMatchers) {
        this.tokenMatchers = tokenMatchers;
    }

    public TextPipeConfig withTokenMatchers(List<TokenMatcher> tokenMatchers) {
        this.tokenMatchers = tokenMatchers;
        return this;
    }

    public TextPipeConfig withTokenMatchers(TokenMatcher... tokenMatchers) {
        for (TokenMatcher tokenMatcher: tokenMatchers) {
            addTokenMatcher(tokenMatcher);
        }
        return this;
    }

    public TextPipeConfig addTokenMatcher(TokenMatcher tokenMatcher) {
        if (tokenMatchers == null) {
            tokenMatchers = new ArrayList<TokenMatcher>();
        }
        tokenMatchers.add(tokenMatcher);
        return this;
    }

    public List<TokenTransform> getTokenTransforms() {
        return tokenTransforms;
    }

    public void setTokenTransforms(List<TokenTransform> tokenTransforms) {
        this.tokenTransforms = tokenTransforms;
    }

    public TextPipeConfig withTokenTransforms(List<TokenTransform> tokenTransforms) {
        this.tokenTransforms = tokenTransforms;
        return this;
    }

    public TextPipeConfig withTokenTransforms(TokenTransform... tokenTransforms) {
        for (TokenTransform tokenTransform: tokenTransforms) {
            addTokenTransform(tokenTransform);
        }
        return this;
    }

    public TextPipeConfig addTokenTransform(TokenTransform tokenTransform) {
        if (tokenTransforms == null) {
            tokenTransforms = new ArrayList<TokenTransform>();
        }
        tokenTransforms.add(tokenTransform);
        return this;
    }

    public List<TokenFilter> getTokenFilters() {
        return tokenFilters;
    }

    public void setTokenFilters(List<TokenFilter> tokenFilters) {
        this.tokenFilters = tokenFilters;
    }

    public TextPipeConfig withTokenFilters(List<TokenFilter> tokenFilters) {
        this.tokenFilters = tokenFilters;
        return this;
    }

    public TextPipeConfig withTokenFilters(TokenFilter... tokenFilters) {
        for (TokenFilter tokenFilter: tokenFilters) {
            addTokenFilter(tokenFilter);
        }
        return this;
    }

    public TextPipeConfig addTokenFilter(TokenFilter tokenFilter) {
        if (tokenFilters == null) {
            tokenFilters = new ArrayList<TokenFilter>();
        }
        tokenFilters.add(tokenFilter);
        return this;
    }

    public int getNgramMin() {
        return ngramMin;
    }

    public void setNgramMin(int ngramMin) {
        this.ngramMin = ngramMin;
    }

    public TextPipeConfig withNgramMin(int ngramMin) {
        this.ngramMin = ngramMin;
        return this;
    }

    public int getNgramMax() {
        return ngramMax;
    }

    public void setNgramMax(int ngramMax) {
        this.ngramMax = ngramMax;
    }

    public TextPipeConfig withNgramMax(int ngramMax) {
        this.ngramMax = ngramMax;
        return this;
    }

    public int getShingleMin() {
        return shingleMin;
    }

    public void setShingleMin(int shingleMin) {
        this.shingleMin = shingleMin;
    }

    public TextPipeConfig withShingleMin(int shingleMin) {
        this.shingleMin = shingleMin;
        return this;
    }

    public int getShingleMax() {
        return shingleMax;
    }

    public void setShingleMax(int shingleMax) {
        this.shingleMax = shingleMax;
    }

    public TextPipeConfig withShingleMax(int shingleMax) {
        this.shingleMax = shingleMax;
        return this;
    }

    public Set<DictionaryConfig> getDictionaries() {
        return dictionaryConfigs;
    }

    public TextPipeConfig addDictionary(DictionaryConfig dictionaryConfig) {
        if (dictionaryConfigs == null) {
            dictionaryConfigs = new HashSet<DictionaryConfig>();
        }
        dictionaryConfigs.add(dictionaryConfig);
        return this;
    }

    public Set<AbbreviationsConfig> getAbbreviationDictionaries() {
        return abbreviationsConfigs;
    }

    public TextPipeConfig addAbbreviationsDictionary(AbbreviationsConfig abbreviationsConfig) {
        if (abbreviationsConfigs == null) {
            abbreviationsConfigs = new HashSet<AbbreviationsConfig>();
        }
        abbreviationsConfigs.add(abbreviationsConfig);
        return this;
    }

    public WordDelimiterConfig getWordDelimiterConfig() {
        return wordDelimiterConfig;
    }

    public void setWordDelimiterConfig(WordDelimiterConfig wordDelimiterConfig) {
        this.wordDelimiterConfig = wordDelimiterConfig;
    }

    public TextPipeConfig withWordDelimiterConfig(WordDelimiterConfig wordDelimiterConfig) {
        this.wordDelimiterConfig = wordDelimiterConfig;
        return this;
    }

    public static class DictionaryConfig {

        private final String dictionaryName;

        private final Set<String> dictionary;

        public DictionaryConfig(String dictionaryName, Set<String> dictionary) {
            this.dictionaryName = dictionaryName;
            this.dictionary = dictionary;
        }

        public String getDictionaryName() {
            return dictionaryName;
        }

        public Set<String> getDictionary() {
            return dictionary;
        }
    }

    public static class AbbreviationsConfig {

        private final String dictionaryName;
        private final Map<String, String> abbreviations;

        private final boolean generateVariants;

        public AbbreviationsConfig(String dictionaryName, Map<String, String> abbreviations, boolean generateVariants) {
            this.dictionaryName = dictionaryName;
            this.abbreviations = abbreviations;
            this.generateVariants = generateVariants;
        }

        public String getDictionaryName() {
            return dictionaryName;
        }

        public Map<String, String> getAbbreviations() {
            return abbreviations;
        }

        public boolean isGenerateVariants() {
            return generateVariants;
        }

    }

    public static class WordDelimiterConfig {

        private final boolean preserveOriginal;

        private final boolean splitOnCaseChange;

        private final boolean splitOnNumerics;

        private final boolean generateWordParts;

        private final boolean generateNumberParts;

        private final boolean catenateWords;

        private final boolean catenateNumbers;

        private final boolean catenateAll;

        public WordDelimiterConfig(boolean preserveOriginal, boolean splitOnCaseChange, boolean splitOnNumerics, boolean generateWordParts, boolean generateNumberParts, boolean catenateWords, boolean catenateNumbers, boolean catenateAll) {
            this.preserveOriginal = preserveOriginal;
            this.splitOnCaseChange = splitOnCaseChange;
            this.splitOnNumerics = splitOnNumerics;
            this.generateWordParts = generateWordParts;
            this.generateNumberParts = generateNumberParts;
            this.catenateWords = catenateWords;
            this.catenateNumbers = catenateNumbers;
            this.catenateAll = catenateAll;
        }

        public boolean isPreserveOriginal() {
            return preserveOriginal;
        }

        public boolean isSplitOnCaseChange() {
            return splitOnCaseChange;
        }

        public boolean isSplitOnNumerics() {
            return splitOnNumerics;
        }

        public boolean isGenerateWordParts() {
            return generateWordParts;
        }

        public boolean isGenerateNumberParts() {
            return generateNumberParts;
        }

        public boolean isCatenateWords() {
            return catenateWords;
        }

        public boolean isCatenateNumbers() {
            return catenateNumbers;
        }

        public boolean isCatenateAll() {
            return catenateAll;
        }

    }

}
