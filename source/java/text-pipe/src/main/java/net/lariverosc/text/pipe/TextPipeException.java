package net.lariverosc.text.pipe;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextPipeException extends RuntimeException {

    public TextPipeException() {
    }

    public TextPipeException(String message) {
        super(message);
    }

    public TextPipeException(String message, Throwable cause) {
        super(message, cause);
    }
}
