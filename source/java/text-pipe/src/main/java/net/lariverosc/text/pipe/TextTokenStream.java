package net.lariverosc.text.pipe;

import java.io.IOException;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.util.ReusableStringReader;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PayloadAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionLengthAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
class TextTokenStream {

    private final Tokenizer source;

    private final TokenStream sink;

    private ReusableStringReader reusableStringReader;

    private int position = -1;

    private int index = -1;

    private CharTermAttribute charTermAttribute;

    private PositionIncrementAttribute positionIncrementAttribute;

    private OffsetAttribute offsetAttribute;

    private TypeAttribute typeAttribute;

    private PositionLengthAttribute positionLengthAttribute;

    private PayloadAttribute payloadAttribute;

    public TextTokenStream(Tokenizer source, TokenStream sink) throws IOException {
        this.source = source;
        this.sink = sink;
        initStream();
        initAttributes();
    }

    private void initStream() throws IOException {
        reusableStringReader = new ReusableStringReader();
        sink.reset();
    }

    private void initAttributes() {
        charTermAttribute = sink.addAttribute(CharTermAttribute.class);
        positionIncrementAttribute = sink.addAttribute(PositionIncrementAttribute.class);
        offsetAttribute = sink.addAttribute(OffsetAttribute.class);
        typeAttribute = sink.addAttribute(TypeAttribute.class);
        positionLengthAttribute = sink.addAttribute(PositionLengthAttribute.class);
        payloadAttribute = sink.addAttribute(PayloadAttribute.class);
    }

    protected boolean nextToken() throws IOException {
        boolean incrementToken = sink.incrementToken();
        int increment = getPositionIncrement();
        if (increment > 0) {
            position += increment;
        }
        position = getPosition();
        index++;
        return incrementToken;
    }

    protected void reuse(String newText) throws IOException {
        positionIncrementAttribute.setPositionIncrement(0);
        position = -1;
        index = -1;
        sink.close();
        setNewText(newText);
        sink.reset();
    }

    private void setNewText(String text) throws IOException {
        reusableStringReader.setValue(text);
        source.setReader(reusableStringReader);
    }

    protected void close() throws IOException {
        sink.end();
        sink.close();
    }

    protected String getToken() {
        return charTermAttribute.toString();
    }

    protected int getPosition() {
        return position;
    }

    public int getIndex() {
        return index;
    }

    protected int getPositionIncrement() {
        return positionIncrementAttribute.getPositionIncrement();
    }

    protected int getPositionLength() {
        return positionLengthAttribute.getPositionLength();
    }

    protected TextSpan getSpan() {
        return new TextSpan(offsetAttribute.startOffset(), offsetAttribute.endOffset());
    }

    protected String getType() {
        return typeAttribute.type();
    }

    protected String getPayload() {
        if (payloadAttribute != null && payloadAttribute.getPayload() != null) {
            return payloadAttribute.getPayload().utf8ToString();
        }
        return null;
    }

}
