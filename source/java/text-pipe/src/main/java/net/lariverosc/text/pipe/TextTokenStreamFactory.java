package net.lariverosc.text.pipe;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.lariverosc.text.lucene.factories.TokenFilterFactory;
import net.lariverosc.text.lucene.factories.TokenizerFactory;
import net.lariverosc.text.lucene.sink.ReusableTeeSinkTokenFilter;
import net.lariverosc.text.lucene.sink.matcher.AbbreviationDictMatcher;
import net.lariverosc.text.lucene.sink.matcher.AcronymMatcher;
import net.lariverosc.text.lucene.sink.matcher.AlphaNumericMatcher;
import net.lariverosc.text.lucene.sink.matcher.DecimalNumberMatcher;
import net.lariverosc.text.lucene.sink.matcher.DictionaryMatcher;
import net.lariverosc.text.lucene.sink.matcher.EmailMatcher;
import net.lariverosc.text.lucene.sink.matcher.GeneSequenceMatcher;
import net.lariverosc.text.lucene.sink.matcher.MoneyMatcher;
import net.lariverosc.text.lucene.sink.matcher.PercentageMatcher;
import net.lariverosc.text.lucene.sink.matcher.RomanNumberMatcher;
import net.lariverosc.text.lucene.sink.matcher.StopMatcher;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.es.SpanishAnalyzer;
import org.apache.lucene.analysis.sinks.TeeSinkTokenFilter;
import org.apache.lucene.analysis.util.CharArraySet;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
class TextTokenStreamFactory {

    private TextTokenStreamFactory() {
    }

    protected static TextTokenStream getSimpleTextTokenStream(Tokenizer tokenizer, TextPipeConfig textPipeConfig) throws IOException {
        TokenStream tokenStreamRef = tokenizer;

        List<TextPipeConfig.TokenFilter> tokenFilters = textPipeConfig.getTokenFilters();
        if (tokenFilters != null) {
            for (TextPipeConfig.TokenFilter tokenFilter: tokenFilters) {
                tokenStreamRef = getTokenFilter(textPipeConfig, tokenStreamRef, tokenFilter);
            }
        }
        List<TextPipeConfig.TokenTransform> tokenTransforms = textPipeConfig.getTokenTransforms();
        if (tokenTransforms != null) {
            for (TextPipeConfig.TokenTransform tokenTransform: tokenTransforms) {
                tokenStreamRef = getTokenTransform(textPipeConfig, tokenStreamRef, tokenTransform);
            }
        }

        return new TextTokenStream(tokenizer, tokenStreamRef);
    }

    protected static TextTokenStream getExtractTextTokenStream(Tokenizer tokenizer, TextPipeConfig textPipeConfig) throws IOException {
        ReusableTeeSinkTokenFilter teeSinkTokenFilter = new ReusableTeeSinkTokenFilter(tokenizer);
        List<ReusableTeeSinkTokenFilter.SinkFilter> sinkFilters = buildSinkFiltersList(textPipeConfig);
        for (ReusableTeeSinkTokenFilter.SinkFilter sinkFilter: sinkFilters) {
            teeSinkTokenFilter.newSinkTokenStream(sinkFilter);
        }
        return new TextTokenStream(tokenizer, teeSinkTokenFilter);
    }

    protected static Tokenizer getTokenizer(TextPipeConfig textPipeConfig) {
        StringReader empty = new StringReader("");
        switch (textPipeConfig.getTokenizer()) {
            case WHITE_SPACE:
                return TokenizerFactory.buildWhitespaceTokenizer(empty);
            case LETTER:
                return TokenizerFactory.buildLetterTokenizer(empty);
            case PATTERN:
                return TokenizerFactory.buildPatternTokenizer(empty, textPipeConfig.getTokenizerPattern());
            case LUCENE_STANDARD:
                return TokenizerFactory.buildStandardTokenizer(empty, textPipeConfig.getMaxTokenLength());
            case CUSTOM:
                return TokenizerFactory.customTokenizer(empty, textPipeConfig.getTokenCharAcceptor());
            default:
                return TokenizerFactory.buildWhitespaceTokenizer(empty);
        }
    }

    private static TokenFilter getTokenFilter(TextPipeConfig textPipeConfig, TokenStream tokenStream, TextPipeConfig.TokenFilter tokenFilter) {
        switch (tokenFilter) {
            case LENGTH:
                return TokenFilterFactory.buildLengthFilter(tokenStream, textPipeConfig.getMinTokenLength(), textPipeConfig.getMaxTokenLength(), true);
            case STOP_WORD:
                return TokenFilterFactory.buildStopFilter(tokenStream, getStopSet(textPipeConfig.getLanguage()), false);
        }
        return null;
    }

    private static TokenFilter getTokenTransform(TextPipeConfig textPipeConfig, TokenStream tokenStream, TextPipeConfig.TokenTransform tokenTransform) {
        switch (tokenTransform) {
            case LOWER_CASE:
                return TokenFilterFactory.buildLowerCaseFilter(tokenStream);
            case UPPER_CASE:
                return TokenFilterFactory.buildUpperCaseFilter(tokenStream);
            case ASCII_FOLDING:
                return TokenFilterFactory.buildASCIIFoldingFilter(tokenStream, true);
            case STEM:
                return TokenFilterFactory.buildStemFilter(tokenStream, textPipeConfig.getLanguage());
            case NGRAM:
                return TokenFilterFactory.buildNGramTokenFilter(tokenStream, textPipeConfig.getNgramMin(), textPipeConfig.getNgramMax());
            case SHINGLE:
                return TokenFilterFactory.buildShingleFilter(tokenStream, textPipeConfig.getShingleMin(), textPipeConfig.getShingleMax(), false);
            case WORD_DELIMITER:
                return TokenFilterFactory.buildWordDelimiterFilter(tokenStream, textPipeConfig.getWordDelimiterConfig());
        }
        return null;
    }

    private static ReusableTeeSinkTokenFilter.SinkFilter getTokenMatchers(TextPipeConfig textPipeConfig, TextPipeConfig.TokenMatcher tokenMatcher) {
        switch (tokenMatcher) {
            case DECIMAL_NUMBER:
                return new DecimalNumberMatcher();
            case EMAIL:
                return new EmailMatcher();
            case GENE:
                return new GeneSequenceMatcher();
            case MONEY:
                return new MoneyMatcher();
            case ACRONYM:
                return new AcronymMatcher();
            case PERCENTAGE:
                return new PercentageMatcher();
            case ROMAN_NUMBER:
                return new RomanNumberMatcher();
            case STOP:
                return new StopMatcher(getStopSet(textPipeConfig.getLanguage()));
            case ALPHANUMERIC:
                return new AlphaNumericMatcher();
        }
        return null;
    }

    private static List<ReusableTeeSinkTokenFilter.SinkFilter> buildSinkFiltersList(TextPipeConfig textPipeConfig) {
        List<ReusableTeeSinkTokenFilter.SinkFilter> sinkFilters = new ArrayList<>();
        for (TextPipeConfig.TokenMatcher tokenMatcher: textPipeConfig.getTokenMatchers()) {
            ReusableTeeSinkTokenFilter.SinkFilter sinkFilter = getTokenMatchers(textPipeConfig, tokenMatcher);
            if (sinkFilter != null) {
                sinkFilters.add(sinkFilter);
            }
        }

        Set<TextPipeConfig.DictionaryConfig> dictionaries = textPipeConfig.getDictionaries();
        if (dictionaries != null) {
            for (TextPipeConfig.DictionaryConfig dictionary: dictionaries) {
                sinkFilters.add(new DictionaryMatcher(dictionary.getDictionaryName(), dictionary.getDictionary()));
            }
        }

        Set<TextPipeConfig.AbbreviationsConfig> abbreviationDictionaries = textPipeConfig.getAbbreviationDictionaries();
        if (abbreviationDictionaries != null) {
            for (TextPipeConfig.AbbreviationsConfig abbreviationDictionary: abbreviationDictionaries) {
                sinkFilters.add(new AbbreviationDictMatcher(abbreviationDictionary.getDictionaryName(), abbreviationDictionary.getAbbreviations(), abbreviationDictionary.isGenerateVariants()));
            }
        }
        return sinkFilters;
    }

    private static TokenStream initTeeSinkTokenFilter(TokenStream tokenStreamRef, List<TeeSinkTokenFilter.SinkFilter> sinkFilters) throws IOException {
        TeeSinkTokenFilter teeSinkTokenFilter = new TeeSinkTokenFilter(tokenStreamRef);
        for (TeeSinkTokenFilter.SinkFilter sinkFilter: sinkFilters) {
            teeSinkTokenFilter.newSinkTokenStream(sinkFilter);
        }
        return teeSinkTokenFilter;
    }

    private static CharArraySet getStopSet(TextPipeConfig.Language language) {
        if (language.equals(TextPipeConfig.Language.SPANISH)) {
            CharArraySet defaultStopSet = new CharArraySet(SpanishAnalyzer.getDefaultStopSet(), true);
            return defaultStopSet;
        }
        if (language.equals(TextPipeConfig.Language.ENGLISH)) {
            CharArraySet defaultStopSet = new CharArraySet(EnglishAnalyzer.getDefaultStopSet(), true);
            defaultStopSet.add("all");
            return defaultStopSet;
        }
        return CharArraySet.EMPTY_SET;
    }

}
