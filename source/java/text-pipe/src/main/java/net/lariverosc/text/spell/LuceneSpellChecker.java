package net.lariverosc.text.spell;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.spell.DirectSpellChecker;
import org.apache.lucene.search.spell.SuggestMode;
import org.apache.lucene.search.spell.SuggestWord;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.IOContext;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class LuceneSpellChecker implements SpellChecker {

    private final Logger log = LoggerFactory.getLogger(LuceneSpellChecker.class);

    private final DirectSpellChecker directSpellChecker = new DirectSpellChecker();

    private final String spellIndexPath;

    private final float SUGGEST_ACCURACCY = 0.85f;

    private IndexReader indexReader;

    public LuceneSpellChecker(String spellIndexPath) {
        this.spellIndexPath = spellIndexPath;
    }

    @Override
    public String spell(String word) {
        if (indexReader == null) {
            indexReader = openIndex();
        }
        SuggestWord[] suggestedWords;
        try {
            suggestedWords = directSpellChecker.suggestSimilar(new Term("words", word), 1, indexReader, SuggestMode.SUGGEST_WHEN_NOT_IN_INDEX, SUGGEST_ACCURACCY);
            if (suggestedWords.length > 0) {
                return suggestedWords[0].string;
            }
        } catch (IOException ioe) {
            log.error("Error while spelling", ioe);
        }
        return word;
    }

    @Override
    public String[] similarWords(String word, int num) {
        if (indexReader == null) {
            indexReader = openIndex();
        }
        SuggestWord[] suggestedWords;
        try {
            suggestedWords = directSpellChecker.suggestSimilar(new Term("words", word), num, indexReader, SuggestMode.SUGGEST_ALWAYS, SUGGEST_ACCURACCY);
            String[] suggestWordsStr = new String[suggestedWords.length];
            for (int i = 0; i < suggestWordsStr.length; i++) {
                suggestWordsStr[i] = suggestedWords[i].string;
            }
            return suggestWordsStr;
        } catch (IOException ioe) {
            log.error("Error while spelling", ioe);
        }
        return new String[]{word};
    }

    private IndexReader openIndex() {
        try {
            Directory ramDirectory = new RAMDirectory(FSDirectory.open(new File(spellIndexPath)), IOContext.READ);
            indexReader = DirectoryReader.open(ramDirectory);
            return indexReader;
        } catch (IOException ioe) {
            log.error("Error while open spell index", ioe);
            throw new RuntimeException(ioe);
        }
    }

    @Override
    public void buildIndex(Set<String> words) {
        try {
            File file = validateIndexDirectory();
            log.info("Creating spell index for {} words", words.size());
            Directory directory = FSDirectory.open(file);
            IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LATEST, new StandardAnalyzer(Version.LATEST));
            IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);
            for (String word : words) {
                log.info(word);
                Document document = new Document();
                document.add(new TextField("words", word.toLowerCase().trim(), Field.Store.NO));
                indexWriter.addDocument(document);
            }
            indexWriter.commit();
            indexWriter.close();
        } catch (IOException ioe) {
            log.error("Error while build spell index", ioe);
            throw new RuntimeException(ioe);
        }
    }

    private File validateIndexDirectory() {
        File file = new File(spellIndexPath);
        if (!file.exists()) {
            log.info("creating new spell index directory {}", spellIndexPath);         
            file.mkdir();
        } else {
            cleanIndexDirectory(file);
        }
        return file;
    }

    private void cleanIndexDirectory(File indexDirectory) {
        log.info("clean old index data");
        File[] list = indexDirectory.listFiles();
        for (File file : list) {
            if (file != null) {
                file.delete();
            }
        }
    }
}
