package net.lariverosc.text.spell;

import java.util.Set;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface SpellChecker {

    /**
     *
     * @param word
     * @return
     */
    String spell(String word);

    String[] similarWords(String word, int num);

    /**
     *
     * @param words
     */
    void buildIndex(Set<String> words);
}
