package net.lariverosc.text.stemming;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class FakeStemmer implements Stemmer {

    @Override
    public String stem(String word) {
        return word;
    }

}
