package net.lariverosc.text.stemming;

import net.lariverosc.text.pipe.TextPipeConfig;
import org.tartarus.snowball.SnowballProgram;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class SnowballStemmer implements Stemmer {

    private SnowballProgram stemmer;

    public SnowballStemmer(String language) {
        switch (language) {
            case "en":
                initSnowballStemmerInstance(TextPipeConfig.Language.ENGLISH);
                break;
            case "es":
                initSnowballStemmerInstance(TextPipeConfig.Language.SPANISH);
                break;
            default:
                throw new IllegalArgumentException("Not snowlball stemmer for the given language:" + language);
        }
    }

    public SnowballStemmer(TextPipeConfig.Language language) {
        initSnowballStemmerInstance(language);
    }

    private void initSnowballStemmerInstance(TextPipeConfig.Language language) {
        String stemmerClassName;
        switch (language) {
            case ENGLISH:
                stemmerClassName = "EnglishStemmer";
                break;
            case SPANISH:
                stemmerClassName = "SpanishStemmer";
                break;
            default:
                throw new IllegalArgumentException("Not snowlball stemmer for the given language:" + language);
        }
        try {
            Class<? extends SnowballProgram> stemClass = Class.forName("org.tartarus.snowball.ext." + stemmerClassName).asSubclass(SnowballProgram.class);
            stemmer = stemClass.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException("Invalid stemmer class specified: " + language, e);
        }
    }

    @Override
    public String stem(String word) {
        stemmer.setCurrent(word);
        stemmer.stem();
        return stemmer.getCurrent();
    }

}
