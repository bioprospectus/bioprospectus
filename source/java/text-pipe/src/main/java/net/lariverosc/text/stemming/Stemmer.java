package net.lariverosc.text.stemming;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public interface  Stemmer {

    String stem(String word);

}
