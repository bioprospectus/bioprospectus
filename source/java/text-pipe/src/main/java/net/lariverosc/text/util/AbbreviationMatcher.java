package net.lariverosc.text.util;

/**
 * Based on paper code improved matching of last word
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class AbbreviationMatcher {

    /**
     * Method findBestLongForm takes as input a short-form and a long-
     * form candidate (a list of words) and returns the best long-form
     * that matches the short-form, or null if no match is found.
     * Code from "A SIMPLE ALGORITHM FOR IDENTIFYING ABBREVIATION DEFINITIONS IN BIOMEDICAL TEXT"
     * @param shortForm
     * @param longForm
     * @return 
     */
    public String findBest(String shortForm, String longForm) {
        final String innerLongForm = longForm.trim();
        int shortIndex; // The index on the short form 
        int longIndex; // The index on the long form 
        int lastIndex;
        char currChar; // The current character to match 
        shortIndex = shortForm.length() - 1; // Set sIndex at the end of the short form 
        longIndex = innerLongForm.length() - 1; // Set lIndex at the end of the long form 
        lastIndex = innerLongForm.length() - 1; // Set lastIndex at the end of the long form 
        for (; shortIndex >= 0; shortIndex--) { // Scan the short form starting from end to start 
            // Store the next character to match. Ignore case 
            currChar = Character.toLowerCase(shortForm.charAt(shortIndex));
            // ignore non alphanumeric characters 
            if (!Character.isLetterOrDigit(currChar)) {
                continue;
            }
            // Decrease lIndex while current character in the long form 
            // does not match the current character in the short form. 
            // If the current character is the first character in the 
            // short form, decrement lIndex until a matching character 
            // is found at the beginning of a word in the long form. 
            while (((longIndex >= 0)
                && (Character.toLowerCase(innerLongForm.charAt(longIndex)) != currChar))
                || ((shortIndex == 0) && (longIndex > 0)
                && (Character.isLetterOrDigit(innerLongForm.charAt(longIndex - 1))))) {
                longIndex--;
            }
            // If no match was found in the long form for the current 
            // character, return null (no match). 
            if (longIndex < 0) {
                return null;
            }
            if (shortIndex == shortForm.length() - 1) {

                lastIndex = innerLongForm.substring(longIndex).indexOf(" ");
                if (lastIndex == -1) {
                    lastIndex = innerLongForm.length() - 1;
                } else {
                    lastIndex += longIndex;
                }
            }
            // A match was found for the current character. Move to the 
            // next character in the long form. 
            longIndex--;
        }
        // Find the beginning of the first word (in case the first character matches the beginning of a hyphenated word). 
        longIndex = innerLongForm.lastIndexOf(" ", longIndex) + 1;
        // Return the best long form, the substring of the original 
        // long form, starting from lIndex up to the end of the original long form. 
        return innerLongForm.substring(longIndex, lastIndex + 1).trim();
    }

    public static void main(String[] args) {
        // "ID" "Inferior dental block"

        AbbreviationMatcher am = new AbbreviationMatcher();
        System.out.println(am.findBest("ID", "Inferior dental "));
    }
}
