package net.lariverosc.text.util;

import java.util.ArrayList;
import java.util.List;
import net.lariverosc.text.pipe.TextPipe;
import net.lariverosc.text.pipe.TextPipeConfig;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class CollocationUtils {

    private final int leftWindowSize;

    private final int rightWindowSize;

    public CollocationUtils(int leftWindowSize, int rightWindowSize) {
        this.leftWindowSize = leftWindowSize;
        this.rightWindowSize = rightWindowSize;
    }

    public List<List<String>> getWindowList(List<String> tokens) {
        List<List<String>> windowList = new ArrayList< List<String>>();
        for (int i = 0; i < tokens.size(); i++) {

            int leftIndex = i - leftWindowSize >= 0 ? i - leftWindowSize : 0;
            int rightIndex = i + rightWindowSize < tokens.size() ? i + rightWindowSize : tokens.size() - 1;

            List<String> windowTokens = getWindowTokens(tokens, i, leftIndex, rightIndex);
            windowList.add(windowTokens);
        }
        return windowList;
    }

    private List<String> getWindowTokens(List<String> tokens, int currentIndex, int leftIndex, int rightIndex) {
        List<String> windowTokens = new ArrayList<String>();
        for (int i = leftIndex; i <= rightIndex; i++) {
            if (i == currentIndex) {
//                list.add(window.get(i).toUpperCase());
            } else {
                windowTokens.add(tokens.get(i));
            }
        }
        return windowTokens;
    }

    public static void main(String[] args) {
        String s = "this is a simple text taken from my imagination in order to generate a large sequence of tokens";
        TextPipe textPipe = new TextPipe(new TextPipeConfig());
        List<String> tokens = textPipe.getTokensAsString(s);
        for (String textToken: tokens) {
            System.out.print(textToken + ", ");
        }
        System.out.println("");
        CollocationUtils wf = new CollocationUtils(2, 2);
        List< List<String>> coocurrences = wf.getWindowList(tokens);
        for (int i = 0; i < tokens.size(); i++) {
            System.out.print(tokens.get(i).toUpperCase() + ": ");
            List<String> list = coocurrences.get(i);
//            Collections.sort(list);
            for (String word: list) {
                System.out.print(word + ", ");
            }
            System.out.println("");
        }
    }
}
