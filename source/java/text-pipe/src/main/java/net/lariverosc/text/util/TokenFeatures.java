package net.lariverosc.text.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Recognizes predefined patterns in strings.
 * Based on OpenNLP String pattern class.
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TokenFeatures {

    private static final int INITAL_CAPITAL_LETTER = 0x1;

    private static final int ALL_CAPITAL_LETTER = 0x1 << 1;

    private static final int ALL_LOWERCASE_LETTER = 0x1 << 2;

    private static final int ALL_LETTERS = 0x1 << 3;

    private static final int ALL_DIGIT = 0x1 << 4;

    private static final int CONTAINS_PERIOD = 0x1 << 5;

    private static final int CONTAINS_COMMA = 0x1 << 6;

    private static final int CONTAINS_SLASH = 0x1 << 7;

    private static final int CONTAINS_BACKSLASH = 0x1 << 8;

    private static final int CONTAINS_DIGIT = 0x1 << 9;

    private static final int CONTAINS_HYPHEN = 0x1 << 10;

    private static final int CONTAINS_LETTERS = 0x1 << 11;

    private static final int CONTAINS_UPPERCASE = 0x1 << 12;

    private static final int CONTAINS_COLON = 0x1 << 13;

    private static final int CONTAINS_SEMICOLON = 0x1 << 14;

    private final int pattern;

    private final String shape;

    private final int digits;

    private TokenFeatures(int pattern, int digits, String shape) {
        this.pattern = pattern;
        this.digits = digits;
        this.shape = shape;
    }

    /**
     * @return true if all characters are letters.
     */
    public boolean isAllLetter() {
        return (pattern & ALL_LETTERS) > 0;
    }

    /**
     * @return true if first letter is capital.
     */
    public boolean isInitialCapitalLetter() {
        return (pattern & INITAL_CAPITAL_LETTER) > 0;
    }

    /**
     * @return true if all letters are capital.
     */
    public boolean isAllUpperCaseLetter() {
        return (pattern & ALL_CAPITAL_LETTER) > 0;
    }

    /**
     * @return true if all letters are lower case.
     */
    public boolean isAllLowerCaseLetter() {
        return (pattern & ALL_LOWERCASE_LETTER) > 0;
    }

    /**
     * @return true if all chars are digits.
     */
    public boolean isAllDigit() {
        return (pattern & ALL_DIGIT) > 0;
    }

    /**
     * Retrieves the number of digits.
     *
     * @return
     */
    public int digits() {
        return digits;
    }

    public int getPattern() {
        return pattern;
    }

    public String getShape() {
        return shape;
    }

    public boolean containsPeriod() {
        return (pattern & CONTAINS_PERIOD) > 0;
    }

    public boolean containsComma() {
        return (pattern & CONTAINS_COMMA) > 0;
    }

    public boolean containsSlash() {
        return (pattern & CONTAINS_SLASH) > 0;
    }

    public boolean containsBackSlash() {
        return (pattern & CONTAINS_BACKSLASH) > 0;
    }

    public boolean containsDigit() {
        return (pattern & CONTAINS_DIGIT) > 0;
    }

    public boolean containsHyphen() {
        return (pattern & CONTAINS_HYPHEN) > 0;
    }

    public boolean containsLetters() {
        return (pattern & CONTAINS_LETTERS) > 0;
    }

    public boolean containsColon() {
        return (pattern & CONTAINS_COLON) > 0;
    }

    public boolean containsSemicolon() {
        return (pattern & CONTAINS_SEMICOLON) > 0;
    }

    public boolean containsPunctuationMarks() {
        return containsColon() || containsComma() || containsSemicolon() || containsPeriod();
    }

    private static final char UPPER_SHAPE = 'X';

    private static final char LOWER_SHAPE = 'x';

    private static final char NUMBER_SHAPE = '#';

    private static final char PUNCTUACTION_SHAPE = '.';

    private static final char SEPARATOR_SHAPE = '-';

    private static final char SPACE_SHAPE = ' ';

    public static TokenFeatures extract(String token) {

        int pattern = ALL_CAPITAL_LETTER | ALL_LOWERCASE_LETTER | ALL_DIGIT | ALL_LETTERS;
        int digits = 0;
        StringBuilder shape = new StringBuilder();
        char last = ' ';
        for (int i = 0; i < token.length(); i++) {
            final char currentChar = token.charAt(i);
            
            //Extract shape
            if (Character.isLetter(currentChar) && Character.isUpperCase(currentChar)) {
                if (last != UPPER_SHAPE) {
                    shape.append(UPPER_SHAPE);
                }
            } else if (Character.isLetter(currentChar) && Character.isLowerCase(currentChar)) {
                if (last != LOWER_SHAPE) {
                    shape.append(LOWER_SHAPE);
                }
            } else if (Character.isDigit(currentChar)) {
                if (last != NUMBER_SHAPE) {
                    shape.append(NUMBER_SHAPE);
                }
            } else if (Character.isWhitespace(currentChar)) {
                if (last != SPACE_SHAPE) {
                    shape.append(SPACE_SHAPE);
                }
            } else if (currentChar == '.' || currentChar == ',' || currentChar == ':' || currentChar == ';') {
                if (last != PUNCTUACTION_SHAPE) {
                    shape.append(PUNCTUACTION_SHAPE);
                }
            } else if (currentChar == '/' || currentChar == '\\' || currentChar == '-' || currentChar == '_') {
                if (last != SEPARATOR_SHAPE) {
                    shape.append(SEPARATOR_SHAPE);
                }
            } else {
                shape.append(currentChar);
            }
            last = shape.charAt(shape.length() - 1);

            
            final int letterType = Character.getType(currentChar);

            boolean isLetter = letterType == Character.UPPERCASE_LETTER
                    || letterType == Character.LOWERCASE_LETTER
                    || letterType == Character.TITLECASE_LETTER
                    || letterType == Character.MODIFIER_LETTER
                    || letterType == Character.OTHER_LETTER;

            if (isLetter) {
                pattern |= CONTAINS_LETTERS;
                pattern &= ~ALL_DIGIT;

                if (letterType == Character.UPPERCASE_LETTER) {
                    if (i == 0) {
                        pattern |= INITAL_CAPITAL_LETTER;
                    }

                    pattern |= CONTAINS_UPPERCASE;

                    pattern &= ~ALL_LOWERCASE_LETTER;
                } else {
                    pattern &= ~ALL_CAPITAL_LETTER;
                }
            } else {
                // contains chars other than letter, this means
                // it can not be one of these:
                pattern &= ~ALL_LETTERS;
                pattern &= ~ALL_CAPITAL_LETTER;
                pattern &= ~ALL_LOWERCASE_LETTER;

                if (letterType == Character.DECIMAL_DIGIT_NUMBER) {
                    pattern |= CONTAINS_DIGIT;
                    digits++;
                } else {
                    pattern &= ~ALL_DIGIT;
                }

                switch (currentChar) {
                    case ',':
                        pattern |= CONTAINS_COMMA;
                        break;

                    case '.':
                        pattern |= CONTAINS_PERIOD;
                        break;

                    case ':':
                        pattern |= CONTAINS_COLON;
                        break;
                    case ';':
                        pattern |= CONTAINS_SEMICOLON;
                        break;
                    case '/':
                        pattern |= CONTAINS_SLASH;
                        break;

                    case '\\':
                        pattern |= CONTAINS_BACKSLASH;
                        break;

                    case '-':
                        pattern |= CONTAINS_HYPHEN;
                        break;

                    default:
                        break;
                }
            }
        }

        return new TokenFeatures(pattern, digits, shape.toString());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("shape:").append(getShape()).append(";");
        sb.append("isAllLetter:").append(isAllLetter()).append(";");
        sb.append("isAllLowerCaseLetter:").append(isAllLowerCaseLetter()).append(";");
        sb.append("isAllUpperCaseLetter:").append(isAllUpperCaseLetter()).append(";");
        sb.append("isInitialCapitalLetter:").append(isInitialCapitalLetter()).append(";");
        if (digits > 0) {
            sb.append("numDigits:").append(digits).append(";");
            sb.append("isAllDigit:").append(isAllDigit()).append(";");
        }
        sb.append("containsPunctuationMarks:").append(containsPunctuationMarks()).append(";");
        if (containsPunctuationMarks()) {
            sb.append("containsPeriod:").append(containsPeriod()).append(";");
            sb.append("containsComma:").append(containsComma()).append(";");
            sb.append("containsSemicolon:").append(containsSemicolon()).append(";");
            sb.append("containsHyphen:").append(containsHyphen()).append(";");
            sb.append("containsSlash:").append(containsSlash()).append(";");
            sb.append("containsBackSlash:").append(containsBackSlash()).append(";");
        }
        return sb.toString();
    }

    public static void printPattern(String string) {
        TokenFeatures tokenFeatures = TokenFeatures.extract(string);
        System.out.println("############\nString: " + string);
        Method[] methods = tokenFeatures.getClass().getMethods();
        for (Method method : methods) {
            String methodName = method.getName();
            if (methodName.startsWith("is") || methodName.startsWith("contains") || methodName.equals("getShape") || methodName.equals("digits")) {
                try {
                    System.out.println(methodName + ": " + method.invoke(tokenFeatures, new Object[0]));
                } catch (IllegalAccessException iae) {
                } catch (IllegalArgumentException iae) {
                } catch (InvocationTargetException ite) {
                }
            }
        }
    }

    public static void main(String[] args) {
        printPattern("Ing.");
        printPattern("USA");
        printPattern("U.S.A");
        printPattern("Respect Sr. in your hands.");
    }
}
