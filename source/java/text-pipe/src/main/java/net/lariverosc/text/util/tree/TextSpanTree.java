package net.lariverosc.text.util.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import net.lariverosc.text.TextSpan;

/**
 * Based on https://github.com/apache/cassandra/blob/trunk/src/java/org/apache/cassandra/utils/IntervalTree.java
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextSpanTree {

    private final TextSpanNode head;

    public TextSpanTree() {
        head = null;
    }

    public TextSpanTree(List<TextSpan> textSpans) {
        head = new TextSpanNode(textSpans);
    }

    public int max() {
        return head.v_max;
    }

    public int min() {
        return head.v_min;
    }

    public List<TextSpan> search(TextSpan searchSpan) {
        List<TextSpan> results = new ArrayList<TextSpan>();
        searchInternal(head, searchSpan, results);
        return results;
    }

    private void searchInternal(TextSpanNode node, TextSpan searchSpan, List<TextSpan> results) {
        if (null == head) {
            return;
        }
        if (null == node || node.v_pt == -1) {
            return;
        }
        //if searchInterval.contains(node.v_pt)
        //then add every interval contained in this node to the result set then search left and right for further
        //overlapping intervals
        if (searchSpan.contains(node.v_pt)) {
            for (TextSpan textSpan: node.intersects_left) {
                results.add(textSpan);
            }

            searchInternal(node.left, searchSpan, results);
            searchInternal(node.right, searchSpan, results);
            return;
        }

        //if v.pt < searchInterval.left
        //add intervals in v with v[i].right >= searchInterval.left
        //L contains no overlaps
        //R May
        if (node.v_pt < searchSpan.getStart()) {
            for (TextSpan textSpan: node.intersects_right) {
                if (textSpan.getEnd() >= searchSpan.getStart()) {
                    results.add(textSpan);
                } else {
                    break;
                }
            }
            searchInternal(node.right, searchSpan, results);
            return;
        }

        //if v.pt > searchInterval.right
        //add intervals in v with [i].left <= searchInterval.right
        //R contains no overlaps
        //L May
        if (node.v_pt > searchSpan.getEnd()) {
            for (TextSpan textSpan: node.intersects_left) {
                if (textSpan.getStart() <= searchSpan.getEnd()) {
                    results.add(textSpan);
                } else {
                    break;
                }
            }
            searchInternal(node.left, searchSpan, results);
            return;
        }
    }

    @Override
    public String toString() {
        return "TextSpanTree{" + "head=" + head + '}';
    }

    private void printRec() {
    }

    public static void main(String[] args) {
        List<TextSpan> textSpans = new ArrayList<TextSpan>();
        textSpans.add(new TextSpan(0, 1));
        textSpans.add(new TextSpan(2, 3));
        textSpans.add(new TextSpan(4, 5));
        textSpans.add(new TextSpan(3, 6));

        TextSpanTree spanTree = new TextSpanTree(textSpans);
        List<TextSpan> search = spanTree.search(new TextSpan(1, 2));
        for (TextSpan textSpan: search) {
            System.out.println(textSpan);
        }
    }
}

class TextSpanNode {

    int v_pt = -1;

    int v_min;

    int v_max;

    List<TextSpan> intersects_left;

    List<TextSpan> intersects_right;

    TextSpanNode left = null;

    TextSpanNode right = null;

    public TextSpanNode(List<TextSpan> toBisect) {
        if (toBisect.size() > 0) {
            findMinMedianMax(toBisect);
            List<TextSpan> intersects = getIntersectingIntervals(toBisect);
            intersects_left = new ArrayList<TextSpan>(intersects);
            intersects_right = new ArrayList<TextSpan>(intersects);
            TextSpan.sortByStart(intersects_left);
            TextSpan.sortByEnd(intersects_right);
            Collections.reverse(intersects_right);
            //if i.max < v_pt then it goes to the left subtree
            List<TextSpan> leftSegment = getLeftIntervals(toBisect);
            List<TextSpan> rightSegment = getRightIntervals(toBisect);
//            assert (intersects.size() + leftSegment.size() + rightSegment.size()) == toBisect.size():
//                    "intersects (" + String.valueOf(intersects.size())
//                    + ") + leftSegment (" + String.valueOf(leftSegment.size())
//                    + ") + rightSegment (" + String.valueOf(rightSegment.size())
//                    + ") != toBisect (" + String.valueOf(toBisect.size()) + ")";
            if (leftSegment.size() > 0) {
                this.left = new TextSpanNode(leftSegment);
            }
            if (rightSegment.size() > 0) {
                this.right = new TextSpanNode(rightSegment);
            }
        }
    }

    private List<TextSpan> getLeftIntervals(List<TextSpan> candidates) {
        List<TextSpan> retval = new ArrayList<TextSpan>();
        for (TextSpan candidate: candidates) {
            if (candidate.getEnd() < v_pt) {
                retval.add(candidate);
            }
        }
        return retval;
    }

    private List<TextSpan> getRightIntervals(List<TextSpan> candidates) {
        List<TextSpan> retval = new ArrayList<TextSpan>();
        for (TextSpan candidate: candidates) {
            if (candidate.getStart() > v_pt) {
                retval.add(candidate);
            }
        }
        return retval;
    }

    private List<TextSpan> getIntersectingIntervals(List<TextSpan> candidates) {
        List<TextSpan> retval = new ArrayList<TextSpan>();
        for (TextSpan candidate: candidates) {
            if (candidate.getStart() <= v_pt && candidate.getEnd() >= v_pt) {
                retval.add(candidate);
            }
        }
        return retval;
    }

    private void findMinMedianMax(List<TextSpan> textSpans) {
        if (textSpans.size() > 0) {
            int[] allPoints = new int[textSpans.size() * 2];
            int i = 0;
            for (TextSpan textSpan: textSpans) {
                allPoints[i] = textSpan.getStart();
                allPoints[i + 1] = textSpan.getEnd();
                i += 2;
            }
            Arrays.sort(allPoints);
            v_pt = allPoints[textSpans.size()];
            v_min = allPoints[0];
            v_max = allPoints[allPoints.length - 1];
        }
    }

}
