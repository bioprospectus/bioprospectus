package net.lariverosc.text.util.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.TextToken;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextTokenTree {

    private final TextTokenNode head;

    public TextTokenTree() {
        head = null;
    }

    public TextTokenTree(List<TextToken> textTokens) {
        head = new TextTokenNode(textTokens);
    }

    public int max() {
        return head.v_max;
    }

    public int min() {
        return head.v_min;
    }

    public List<TextToken> search(TextToken searchToken) {
        List<TextToken> results = new ArrayList<TextToken>();
        searchInternal(head, searchToken, results);
        return results;
    }

    private void searchInternal(TextTokenNode node, TextToken searchToken, List<TextToken> results) {
        if (null == head) {
            return;
        }
        if (null == node || node.v_pt == -1) {
            return;
        }
        //if searchInterval.contains(node.v_pt)
        //then add every interval contained in this node to the result set then search left and right for further
        //overlapping intervals
        if (searchToken.getTextSpan().contains(node.v_pt)) {
            for (TextToken textToken: node.intersects_left) {
                results.add(textToken);
            }

            searchInternal(node.left, searchToken, results);
            searchInternal(node.right, searchToken, results);
            return;
        }

        //if v.pt < searchInterval.left
        //add intervals in v with v[i].right >= searchInterval.left
        //L contains no overlaps
        //R May
        if (node.v_pt < searchToken.getTextSpan().getStart()) {
            for (TextToken textToken: node.intersects_right) {
                if (textToken.getTextSpan().getEnd() >= searchToken.getTextSpan().getStart()) {
                    results.add(textToken);
                } else {
                    break;
                }
            }
            searchInternal(node.right, searchToken, results);
            return;
        }

        //if v.pt > searchInterval.right
        //add intervals in v with [i].left <= searchInterval.right
        //R contains no overlaps
        //L May
        if (node.v_pt > searchToken.getTextSpan().getEnd()) {
            for (TextToken textToken: node.intersects_left) {
                if (textToken.getTextSpan().getStart() <= searchToken.getTextSpan().getEnd()) {
                    results.add(textToken);
                } else {
                    break;
                }
            }
            searchInternal(node.left, searchToken, results);
            return;
        }
    }

    @Override
    public String toString() {
        System.out.println("head");
        System.out.println(head);
        return "";
    }

    public static void main(String[] args) {
        List<TextToken> textSpans = new ArrayList<TextToken>();
        textSpans.add(new TextToken("word1", new TextSpan(0, 1), TextToken.Type.WORD));
        textSpans.add(new TextToken("word2", new TextSpan(2, 3), TextToken.Type.WORD));
        textSpans.add(new TextToken("word3", new TextSpan(4, 5), TextToken.Type.WORD));
        textSpans.add(new TextToken("word4", new TextSpan(3, 6), TextToken.Type.WORD));
        textSpans.add(new TextToken("word5", new TextSpan(4, 8), TextToken.Type.WORD));
        textSpans.add(new TextToken("word6", new TextSpan(6, 7), TextToken.Type.WORD));

        TextTokenTree spanTree = new TextTokenTree(textSpans);
        List<TextToken> search = spanTree.search(new TextToken("", new TextSpan(1, 5), TextToken.Type.WORD));
        for (TextToken textSpan: search) {
            System.out.println(textSpan);
        }
        System.out.println("SpanTree");
        System.out.println(spanTree);
    }
}

class TextTokenNode {

    int v_pt = -1;

    int v_min;

    int v_max;

    List<TextToken> intersects_left;

    List<TextToken> intersects_right;

    TextTokenNode left = null;

    TextTokenNode right = null;

    public TextTokenNode(List<TextToken> toBisect) {
        if (toBisect.size() > 0) {
            findMinMedianMax(toBisect);
            List<TextToken> intersects = getIntersectingIntervals(toBisect);
            intersects_left = new ArrayList<TextToken>(intersects);
            intersects_right = new ArrayList<TextToken>(intersects);
            TextToken.sortByStart(intersects_left);
            TextToken.sortByEnd(intersects_right);
            Collections.reverse(intersects_right);
            //if i.max < v_pt then it goes to the left subtree
            List<TextToken> leftSegment = getLeftIntervals(toBisect);
            List<TextToken> rightSegment = getRightIntervals(toBisect);
//            assert (intersects.size() + leftSegment.size() + rightSegment.size()) == toBisect.size():
//                    "intersects (" + String.valueOf(intersects.size())
//                    + ") + leftSegment (" + String.valueOf(leftSegment.size())
//                    + ") + rightSegment (" + String.valueOf(rightSegment.size())
//                    + ") != toBisect (" + String.valueOf(toBisect.size()) + ")";
            if (leftSegment.size() > 0) {
                this.left = new TextTokenNode(leftSegment);
            }
            if (rightSegment.size() > 0) {
                this.right = new TextTokenNode(rightSegment);
            }
        }
    }

    private List<TextToken> getLeftIntervals(List<TextToken> candidates) {
        List<TextToken> retval = new ArrayList<TextToken>();
        for (TextToken candidate: candidates) {
            if (candidate.getTextSpan().getEnd() < v_pt) {
                retval.add(candidate);
            }
        }
        return retval;
    }

    private List<TextToken> getRightIntervals(List<TextToken> candidates) {
        List<TextToken> retval = new ArrayList<TextToken>();
        for (TextToken candidate: candidates) {
            if (candidate.getTextSpan().getStart() > v_pt) {
                retval.add(candidate);
            }
        }
        return retval;
    }

    private List<TextToken> getIntersectingIntervals(List<TextToken> candidates) {
        List<TextToken> retval = new ArrayList<TextToken>();
        for (TextToken candidate: candidates) {
            if (candidate.getTextSpan().getStart() <= v_pt && candidate.getTextSpan().getEnd() >= v_pt) {
                retval.add(candidate);
            }
        }
        return retval;
    }

    private void findMinMedianMax(List<TextToken> textTokens) {
        if (textTokens.size() > 0) {
            int[] allPoints = new int[textTokens.size() * 2];
            int i = 0;
            for (TextToken textSpan: textTokens) {
                allPoints[i] = textSpan.getTextSpan().getStart();
                allPoints[i + 1] = textSpan.getTextSpan().getEnd();
                i += 2;
            }
            Arrays.sort(allPoints);
            v_pt = allPoints[textTokens.size()];
            v_min = allPoints[0];
            v_max = allPoints[allPoints.length - 1];
        }
    }

    
    public String toString2() {
        return "TextTokenNode{" + "v_pt=" + v_pt + ", v_min=" + v_min + ", v_max=" + v_max + '}';
    }

    @Override
    public String toString() {
        System.out.println("v_min" + v_min);
        System.out.println("v_pt" + v_pt);
        System.out.println("v_max" + v_max);
        System.out.println("intersects_left:\n" + getAsString(intersects_left));
        System.out.println("intersects_right:\n" + getAsString(intersects_right));
        if (left != null) {
            System.out.println("nodeLeft:");
            System.out.println(left);
        }
        if (right != null) {
            System.out.println("nodeRight:");
            System.out.println(right);
        }

        return "";
    }

    String getAsString(List<TextToken> textTokens) {
        StringBuilder sb = new StringBuilder();
        for (TextToken textToken: textTokens) {
            sb.append(textToken).append("\n");
        }
        return sb.toString();
    }

}
