package net.lariverosc.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class TextSpanTest {

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldFailConstructor() throws Exception {
        new TextSpan(2, 1);
    }

    @Test
    public void shouldHashcodeAndEquals() throws Exception {
        TextSpan textSpan1 = new TextSpan(2, 5);
        TextSpan textSpan2 = new TextSpan(2, 5);
        TextSpan textSpan3 = new TextSpan(1, 5);

        assertEquals(textSpan1.equals(textSpan2), true);
        assertEquals(textSpan1.hashCode(), textSpan2.hashCode());

        assertEquals(textSpan1.equals(textSpan3), false);
        assertNotEquals(textSpan1.hashCode(), textSpan3.hashCode());
    }

    @Test
    public void shouldSortByStart() throws Exception {
        List<TextSpan> textSpanList = new ArrayList<TextSpan>();
        textSpanList.add(new TextSpan(2, 10));
        textSpanList.add(new TextSpan(1, 10));
        TextSpan.sortByStart(textSpanList);
        assertEquals(textSpanList.get(0).getStart(), 1);
        assertEquals(textSpanList.get(1).getStart(), 2);
    }

    @Test
    public void shouldSortByStartThenByEnd() throws Exception {
        List<TextSpan> textSpanList = new ArrayList<TextSpan>();
        textSpanList.add(new TextSpan(2, 8));
        textSpanList.add(new TextSpan(1, 9));
        textSpanList.add(new TextSpan(1, 6));
        TextSpan.sortByStart(textSpanList);
        assertEquals(textSpanList.get(0).getStart(), 1);
        assertEquals(textSpanList.get(0).getEnd(), 6);
        assertEquals(textSpanList.get(1).getStart(), 1);
        assertEquals(textSpanList.get(1).getEnd(), 9);
        assertEquals(textSpanList.get(2).getStart(), 2);
        assertEquals(textSpanList.get(2).getEnd(), 8);
    }

    @Test
    public void shouldSortByEnd() throws Exception {
        List<TextSpan> textSpanList = new ArrayList<TextSpan>();
        textSpanList.add(new TextSpan(0, 2));
        textSpanList.add(new TextSpan(0, 1));
        TextSpan.sortByEnd(textSpanList);
        assertEquals(textSpanList.get(0).getEnd(), 1);
        assertEquals(textSpanList.get(1).getEnd(), 2);
    }

    @Test
    public void shouldSortByEndThenByStart() throws Exception {
        List<TextSpan> textSpanList = new ArrayList<TextSpan>();
        textSpanList.add(new TextSpan(0, 2));
        textSpanList.add(new TextSpan(2, 6));
        textSpanList.add(new TextSpan(0, 6));
        TextSpan.sortByEnd(textSpanList);
        assertEquals(textSpanList.get(0).getEnd(), 2);
        assertEquals(textSpanList.get(0).getStart(), 0);
        assertEquals(textSpanList.get(1).getEnd(), 6);
        assertEquals(textSpanList.get(1).getStart(), 0);
        assertEquals(textSpanList.get(2).getEnd(), 6);
        assertEquals(textSpanList.get(2).getStart(), 2);
    }

    @Test
    public void shouldReturnCoveredAndUncoveredText() throws Exception {
        TextSpan textSpan = new TextSpan(2, 5);
        String coveredText = textSpan.getCoveredText("0123456");
        assertEquals(coveredText, "234");
        String uncoveredText = textSpan.getUncoveredText("0123456");
        assertEquals(uncoveredText, "0156");
    }

    @Test
    public void shouldReturnLength() throws Exception {
        TextSpan textSpan = new TextSpan(2, 5);
        assertEquals(textSpan.getLength(), 3);
    }

    @Test
    public void shouldValidateIfAnIndexIsContained() throws Exception {
        TextSpan textSpan = new TextSpan(2, 5);
        assertEquals(textSpan.contains(3), true);
        assertEquals(textSpan.contains(0), false);
        assertEquals(textSpan.contains(5), false);
    }

    @Test
    public void shouldValidateIfAnTextSpanIsContained() throws Exception {
        TextSpan textSpan = new TextSpan(2, 5);
        TextSpan containedTextSpan = new TextSpan(2, 4);
        TextSpan notContainedTextSpan = new TextSpan(3, 6);
        assertEquals(textSpan.contains(containedTextSpan), true);
        assertEquals(textSpan.contains(notContainedTextSpan), false);
    }

    @Test
    public void shouldValidateIfAnTextSpanIsConnected() throws Exception {
        TextSpan textSpan1 = new TextSpan(1, 5);
        TextSpan textSpan2 = new TextSpan(5, 6);
        assertEquals(textSpan1.intersects(textSpan2), true);

        TextSpan textSpan3 = new TextSpan(1, 2);
        TextSpan textSpan4 = new TextSpan(1, 3);
        assertEquals(textSpan3.intersects(textSpan4), true);

        TextSpan textSpan5 = new TextSpan(6, 6);
        TextSpan textSpan6 = new TextSpan(1, 5);
        assertEquals(textSpan5.intersects(textSpan6), false);
    }

    @Test
    public void shouldMergeTextSpans() throws Exception {
        TextSpan merge1 = TextSpan.merge(new TextSpan(1, 5), new TextSpan(3, 6));
        assertEquals(merge1.getStart(), 1);
        assertEquals(merge1.getEnd(), 6);

        TextSpan merge2 = TextSpan.merge(new TextSpan(1, 3), new TextSpan(5, 6));
        assertEquals(merge2.getStart(), 1);
        assertEquals(merge2.getEnd(), 6);
    }

    @Test
    public void shouldComputeMinTextSpansForOneConsecutiveBlock() throws Exception {
        Set<TextSpan> textSpans = new HashSet<>();
        textSpans.add(new TextSpan(0, 2));
        textSpans.add(new TextSpan(1, 3));
        textSpans.add(new TextSpan(3, 5));
        Collection<TextSpan> mergedSpans = TextSpan.mergeIntersectedSpans(textSpans);
        assertEquals(mergedSpans.size(), 1);
        TextSpan textSpan = mergedSpans.iterator().next();
        assertEquals(textSpan.getStart(), 0);
        assertEquals(textSpan.getEnd(), 5);
    }

    @Test
    public void shouldComputeMinTextSpansForTwoConsecutiveBlocks() throws Exception {
        Set<TextSpan> textSpans = new HashSet<TextSpan>();
        textSpans.add(new TextSpan(0, 2));
        textSpans.add(new TextSpan(1, 3));
        textSpans.add(new TextSpan(3, 5));
        textSpans.add(new TextSpan(6, 8));
        textSpans.add(new TextSpan(8, 10));

        Collection<TextSpan> mergedSpans = TextSpan.mergeIntersectedSpans(textSpans);
        List<TextSpan> mergedSpansSorted = TextSpan.sortByStart(new ArrayList<TextSpan>(mergedSpans));
        assertEquals(mergedSpans.size(), 2);
        assertEquals(mergedSpansSorted.get(0).getStart(), 0);
        assertEquals(mergedSpansSorted.get(0).getEnd(), 5);

        assertEquals(mergedSpansSorted.get(1).getStart(), 6);
        assertEquals(mergedSpansSorted.get(1).getEnd(), 10);
    }

    @Test
    public void shouldMergeContinousSpans() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(3, 4));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 1);
        TextSpan textSpan = mergedSpans.iterator().next();
        assertEquals(textSpan.getStart(), 0);
        assertEquals(textSpan.getEnd(), 4);
    }

    @Test
    public void shouldMergeConnectedSpans() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(2, 4));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 1);
        TextSpan textSpan = mergedSpans.iterator().next();
        assertEquals(textSpan.getStart(), 0);
        assertEquals(textSpan.getEnd(), 4);
    }

    @Test
    public void shouldMergeMultipleConnedtedSpans() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(2, 4));

        textSpanCollection.add(new TextSpan(6, 8));
        textSpanCollection.add(new TextSpan(8, 9));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 2);
        List<TextSpan> mergedSpansSorted = TextSpan.sortByStart(new ArrayList<TextSpan>(mergedSpans));
        assertEquals(mergedSpansSorted.get(0).getStart(), 0);
        assertEquals(mergedSpansSorted.get(0).getEnd(), 4);
        assertEquals(mergedSpansSorted.get(1).getStart(), 6);
        assertEquals(mergedSpansSorted.get(1).getEnd(), 9);
    }

    @Test
    public void shouldMergeOverlappedSpansSameStart() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(0, 5));
        textSpanCollection.add(new TextSpan(0, 3));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 1);
        TextSpan textSpan = mergedSpans.iterator().next();
        assertEquals(textSpan.getStart(), 0);
        assertEquals(textSpan.getEnd(), 5);
    }

    @Test
    public void shouldMergeOverlappedSpansSameEnd() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 5));
        textSpanCollection.add(new TextSpan(1, 5));
        textSpanCollection.add(new TextSpan(3, 5));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 1);
        TextSpan textSpan = mergedSpans.iterator().next();
        assertEquals(textSpan.getStart(), 0);
        assertEquals(textSpan.getEnd(), 5);
    }

    @Test
    public void shouldMergeOverlappedSpans() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 5));
        textSpanCollection.add(new TextSpan(1, 3));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 1);
        TextSpan textSpan = mergedSpans.iterator().next();
        assertEquals(textSpan.getStart(), 0);
        assertEquals(textSpan.getEnd(), 5);
    }

    @Test
    public void shouldReturnOnlyOneSpan() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(0, 2));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 1);
        TextSpan textSpan = mergedSpans.iterator().next();
        assertEquals(textSpan.getStart(), 0);
        assertEquals(textSpan.getEnd(), 2);
    }

    @Test
    public void shouldMergeContinousSpansAndIgnoreNonContinuous() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(3, 4));
        textSpanCollection.add(new TextSpan(6, 7));

        Collection<TextSpan> mergedSpans = TextSpan.mergeContinuousSpans(textSpanCollection);
        assertEquals(mergedSpans.size(), 2);
        List<TextSpan> mergedSpansSorted = TextSpan.sortByStart(new ArrayList<TextSpan>(mergedSpans));
        assertEquals(mergedSpansSorted.get(0).getStart(), 0);
        assertEquals(mergedSpansSorted.get(0).getEnd(), 4);
        assertEquals(mergedSpansSorted.get(1).getStart(), 6);
        assertEquals(mergedSpansSorted.get(1).getEnd(), 7);
    }

    @Test
    public void shouldOffsetSpans() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(3, 4));
        textSpanCollection.add(new TextSpan(6, 7));

        Collection<TextSpan> offsetedSpans = TextSpan.offsetSpans(textSpanCollection, 10);
        assertEquals(offsetedSpans.size(), 3);
        List<TextSpan> offsetedSpansSorted = TextSpan.sortByStart(new ArrayList<TextSpan>(offsetedSpans));
        assertEquals(offsetedSpansSorted.get(0).getStart(), 10);
        assertEquals(offsetedSpansSorted.get(0).getEnd(), 12);
        assertEquals(offsetedSpansSorted.get(1).getStart(), 13);
        assertEquals(offsetedSpansSorted.get(1).getEnd(), 14);
        assertEquals(offsetedSpansSorted.get(2).getStart(), 16);
        assertEquals(offsetedSpansSorted.get(2).getEnd(), 17);
    }

    @Test
    public void shouldConvertSpansToStrings() throws Exception {
        Collection<TextSpan> textSpanCollection = new ArrayList<TextSpan>();
        textSpanCollection.add(new TextSpan(0, 2));
        textSpanCollection.add(new TextSpan(3, 4));
        textSpanCollection.add(new TextSpan(6, 7));

        String spansString = TextSpan.asString(textSpanCollection);
        assertEquals(spansString, "0-2,3-4,6-7");
    }

}
