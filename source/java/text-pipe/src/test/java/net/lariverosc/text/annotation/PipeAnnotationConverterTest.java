package net.lariverosc.text.annotation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.lariverosc.text.TextSpan;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class PipeAnnotationConverterTest {

    @Test
    public void shouldParseAnnotation() throws Exception {
        String annotationStr = "REPORT.txt||Disease_Disorder||C00123||120||140";
        PipeAnnotationConverter pac = new PipeAnnotationConverter();
        Annotation annotation = pac.toAnnotation(annotationStr);
        Assert.assertEquals(annotation.getAttribute("category"), "Disease_Disorder");
        Assert.assertEquals(annotation.getAttribute("conceptId"), "C00123");
        TextSpan textSpan = annotation.getTextSpans().iterator().next();
        Assert.assertEquals(textSpan.getStart(), 120);
        Assert.assertEquals(textSpan.getEnd(), 140);
    }

    @Test
    public void shouldParseAnnotationWithGap() throws Exception {
        String annotationStr = "REPORT.txt||Disease_Disorder||C00123||120||140||150||160";
        PipeAnnotationConverter pac = new PipeAnnotationConverter();
        Annotation annotation = pac.toAnnotation(annotationStr);
        Assert.assertEquals(annotation.getAttribute("category"), "Disease_Disorder");
        Assert.assertEquals(annotation.getAttribute("conceptId"), "C00123");
        Assert.assertEquals(annotation.getTextSpans().size(), 2);
        Set<TextSpan> textSpans = annotation.getTextSpans();
        List<TextSpan> textSpansList = new ArrayList<TextSpan>(textSpans);
        TextSpan.sortByStart(textSpansList);
        TextSpan textSpan1 = textSpansList.get(0);
        Assert.assertEquals(textSpan1.getStart(), 120);
        Assert.assertEquals(textSpan1.getEnd(), 140);
        TextSpan textSpan2 = textSpansList.get(1);
        Assert.assertEquals(textSpan2.getStart(), 150);
        Assert.assertEquals(textSpan2.getEnd(), 160);
    }
}
