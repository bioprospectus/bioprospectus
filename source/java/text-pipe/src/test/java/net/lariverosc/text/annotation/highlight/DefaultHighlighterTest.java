package net.lariverosc.text.annotation.highlight;

import java.util.HashSet;
import java.util.Set;
import net.lariverosc.text.TextSpan;
import net.lariverosc.text.annotation.Annotation;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class DefaultHighlighterTest {

    @Test
    public void shouldHighlightOneBlockAnnotation() throws Exception {
        AnnotationHighlighter highlighter = new DefaultAnnotationHighlighter("ini", "end");
        Set<Annotation> annotations = new HashSet<Annotation>();
        annotations.add(new Annotation("a1").withTextSpan(new TextSpan(1, 3)));
        String highlightedStr = highlighter.highlight("0123456789", annotations);
        assertEquals(highlightedStr, "0<ini id=\"a1\" >12</end>3456789");
    }

    @Test
    public void shouldHighlightTwoBlocksAnnotation() throws Exception {
        AnnotationHighlighter highlighter = new DefaultAnnotationHighlighter("ini", "end");
        Set<Annotation> annotations = new HashSet<Annotation>();
        annotations.add(new Annotation("a1").withTextSpan(new TextSpan(1, 3)).withTextSpan(new TextSpan(5, 6)));
        String highlightedStr = highlighter.highlight("0123456789", annotations);
        assertEquals(highlightedStr, "0<ini id=\"a1\" >12</end>34<ini id=\"a1\" >5</end>6789");
    }

    @Test
    public void shouldHighlightTwoAnnotationsEqualStart() throws Exception {
        AnnotationHighlighter highlighter = new DefaultAnnotationHighlighter("ini", "end");
        Set<Annotation> annotations = new HashSet<Annotation>();
        annotations.add(new Annotation("a1").withTextSpan(new TextSpan(1, 3)));
        annotations.add(new Annotation("a2").withTextSpan(new TextSpan(1, 4)));
        String highlightedStr = highlighter.highlight("0123456789", annotations);
        assertEquals(highlightedStr, "0<ini id=\"a2\" ><ini id=\"a1\" >12</end>3</end>456789");
    }
    @Test
    public void shouldHighlightAnnotationsAtEnd() throws Exception {
        AnnotationHighlighter highlighter = new DefaultAnnotationHighlighter("ini", "end");
        Set<Annotation> annotations = new HashSet<Annotation>();
        annotations.add(new Annotation("a1").withTextSpan(new TextSpan(1, 10)));
        String highlightedStr = highlighter.highlight("0123456789", annotations);
        assertEquals(highlightedStr, "0<ini id=\"a1\" >123456789</end>");
    }

    @Test
    public void shouldHighlightTwoAnnotationsEqualTextSpan() throws Exception {
        AnnotationHighlighter highlighter = new DefaultAnnotationHighlighter("ini", "end");
        Set<Annotation> annotations = new HashSet<Annotation>();
        annotations.add(new Annotation("a1").withTextSpan(new TextSpan(1, 4)));
        annotations.add(new Annotation("a2").withTextSpan(new TextSpan(1, 4)));
        String highlightedStr = highlighter.highlight("0123456789", annotations);
        assertTrue("0<ini id=\"a1\" ><ini id=\"a2\" >123</end></end>456789".equals(highlightedStr)
                || "0<ini id=\"a2\" ><ini id=\"a1\" >123</end></end>456789".equals(highlightedStr));
    }

    @Test
    public void shouldHighlightTwoNestedBlocksDifferentAnnotation() throws Exception {
        AnnotationHighlighter highlighter = new DefaultAnnotationHighlighter("ini", "end");
        Set<Annotation> annotations = new HashSet<Annotation>();
        annotations.add(new Annotation("a1").withTextSpan(new TextSpan(1, 4)));
        annotations.add(new Annotation("a2").withTextSpan(new TextSpan(2, 6)));
        String highlightedStr = highlighter.highlight("0123456789", annotations);
        assertEquals(highlightedStr, "0<ini id=\"a1\" >1</end><ini id=\"a2\" ><ini id=\"a1\" >23</end>45</end>6789");
    }

    @Test
    public void shouldHighlightTwoAnnotationsSameBlock() throws Exception {
        AnnotationHighlighter highlighter = new DefaultAnnotationHighlighter("ini", "end");
        Set<Annotation> annotations = new HashSet<Annotation>();
        annotations.add(new Annotation("a1").withTextSpan(new TextSpan(1, 4)));
        annotations.add(new Annotation("a2").withTextSpan(new TextSpan(1, 4)));
        String highlightedStr = highlighter.highlight("0123456789", annotations);
        assertTrue("0<ini id=\"a1\" ><ini id=\"a2\" >123</end></end>456789".equals(highlightedStr)
                || "0<ini id=\"a2\" ><ini id=\"a1\" >123</end></end>456789".equals(highlightedStr));
    }
}
