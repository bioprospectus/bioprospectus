package net.lariverosc.text.index;

import net.lariverosc.text.TextSpan;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MemoryIndexTest {

    @Test
    public void shouldIndexOneTerm() {
        Index<String> index = new MemoryIndex<>("test");
        index.addTermOccurrence("doc1", "word1", new TextSpan(0, 4));

        assertEquals(index.getName(), "test");
        assertEquals(index.getTerms().size(), 1);
        assertEquals(index.getTotalDocuments(), 1);
    }

}
