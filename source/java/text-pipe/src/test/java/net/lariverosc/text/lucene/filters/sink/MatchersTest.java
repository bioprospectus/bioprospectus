package net.lariverosc.text.lucene.filters.sink;

import net.lariverosc.text.lucene.sink.matcher.MoneyMatcher;
import net.lariverosc.text.lucene.sink.matcher.EmailMatcher;
import net.lariverosc.text.lucene.sink.matcher.GeneSequenceMatcher;
import net.lariverosc.text.lucene.sink.matcher.DecimalNumberMatcher;
import net.lariverosc.text.lucene.sink.matcher.DictionaryMatcher;
import net.lariverosc.text.lucene.sink.matcher.PercentageMatcher;
import net.lariverosc.text.lucene.sink.matcher.RomanNumberMatcher;
import java.util.HashSet;
import java.util.Set;
import net.lariverosc.text.lucene.sink.matcher.AcronymMatcher;
import net.lariverosc.text.lucene.sink.matcher.AlphaNumericMatcher;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PayloadAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.apache.lucene.util.AttributeSource;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Alejandro Riveros Cruz <lariverosc@gmail.com>
 */
public class MatchersTest {

    @Test
    public void shouldMatchAlphaNumerics() throws Exception {
        AlphaNumericMatcher matcher = new AlphaNumericMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("alpha-2")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("alpha_2")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("123")));
        Assert.assertFalse(matcher.accept(buildAttributeSource("money")));
    }

    @Test
    public void shouldMatchDecimalNumbers() throws Exception {
        DecimalNumberMatcher matcher = new DecimalNumberMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("3.1415")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("1070")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("0.1")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("0,9")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("-1.2")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("+3")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("1000000")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("1O")));//1 and O letter
        Assert.assertFalse(matcher.accept(buildAttributeSource("1e34")));
    }

    @Test
    public void shouldMatchMoney() throws Exception {
        MoneyMatcher matcher = new MoneyMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("$1550.2")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("$0.2")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("$1,000,000.02")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("s123")));
        Assert.assertFalse(matcher.accept(buildAttributeSource("money")));
    }

    @Test
    public void shouldMatchPercentages() throws Exception {
        PercentageMatcher matcher = new PercentageMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("0.1%")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("100%")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("100")));
        Assert.assertFalse(matcher.accept(buildAttributeSource("1p")));
    }

    @Test
    public void shouldMatchGeneSequences() throws Exception {
        GeneSequenceMatcher matcher = new GeneSequenceMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("tgctgctgct")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("CTGCAGGAACTTCTTCTGGAAGACCTTCTCCTCCTGCAAATAAAACCTCACCCATGAATGCTCACGCAAG")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("CATS")));
        Assert.assertFalse(matcher.accept(buildAttributeSource("1CAT")));
    }

    @Test
    public void shouldMatchAcronymForm() throws Exception {
        AcronymMatcher matcher = new AcronymMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("USA")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("U.S.A")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("U.S.A.")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("(USA)")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("Cats")));
        Assert.assertFalse(matcher.accept(buildAttributeSource("USd")));
    }

    @Test
    public void shouldMatchDictionaryTerms() throws Exception {
        Set<String> dictionary = new HashSet<String>();
        dictionary.add("alejandro");
        dictionary.add("lucene");
        DictionaryMatcher matcher = new DictionaryMatcher("test", dictionary);

        Assert.assertTrue(matcher.accept(buildAttributeSource("alejandro")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("lucene")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("other")));
    }

    @Test
    public void shouldMatchEmail() throws Exception {
        EmailMatcher matcher = new EmailMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("lariveros@gmail.com")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("lariveros@unal.edu.co")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("lariveros_at_gmail")));//1 and O letter
        Assert.assertFalse(matcher.accept(buildAttributeSource("notemail@")));
    }

    @Test
    public void shouldMatchRomanNumbers() throws Exception {
        RomanNumberMatcher matcher = new RomanNumberMatcher();

        Assert.assertTrue(matcher.accept(buildAttributeSource("III")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("IV")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("XIV")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("V")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("MM")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("MCM")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("CD")));
        Assert.assertTrue(matcher.accept(buildAttributeSource("MLXVI")));

        Assert.assertFalse(matcher.accept(buildAttributeSource("ABC")));
        Assert.assertFalse(matcher.accept(buildAttributeSource("123")));
        Assert.assertFalse(matcher.accept(buildAttributeSource("ILLINOIS")));

    }

    private static AttributeSource buildAttributeSource(String string) {
        AttributeSource attributeSource = new AttributeSource();
        CharTermAttribute charTermAttribute = attributeSource.addAttribute(CharTermAttribute.class);
        charTermAttribute.append(string);
        attributeSource.addAttribute(TypeAttribute.class);
        attributeSource.addAttribute(PayloadAttribute.class);
        return attributeSource;
    }

}
