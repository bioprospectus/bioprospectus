var fs = require('fs');
var xml2js = require('xml2js');
var http = require('http');
var _ = require('underscore');

/**
* This experiment corresponds to a FTS over the clef2013 documents where each document has been divided to contain onbly one image.
*/

var buildQuery = function(queryStr){
	return {
		"from": 0,
		"size": 1000, 
		"query": {
			"query_string": {
				"fields": [
               		"article.figure.caption",
               		"article.title",
               		"article.fulltext"
            	], 
				"query": queryStr,
				"use_dis_max": true
			}
		},
		"fields": [      
			"article.figure.@iri"
		]
	};
};

var esQuery = function(topicId, queryObj){
	var indexName = 'clef-by-fig-snomed';
	var options = {
		host : 'optimus',
		port : 9200,
		path : indexName + '/_search',
		method : 'POST'
	};
	var req = http.request(options, function(res) {
		var data = '';
		res.on('data', function (chunk) {
			data += chunk;
		});
		res.on('end', function () {
			if (res.statusCode === 200){
				var resData = JSON.parse(data)
				var count = 0;
				var hashes = {};
				resData.hits.hits.forEach(function(hit){
					var iri = hit.fields['article.figure.@iri'];
					if (iri){
						var hash = topicId+iri;
						if (!(hash in hashes)){
							console.log(topicId+' 1 '+iri+' '+count+' '+hit._score+' test');
							count++;
						}
						hashes[hash] = true;
					}
				});
			}else{
				console.log(queryObj);
				throw 'ES response error'+res.statusCode;
			}
		});
	});
	req.on('error', function(err) {
		console.log(err);
	});
	req.write(JSON.stringify(queryObj));	
	req.end();
};

var parser = new xml2js.Parser();

parser.on('end', function(result) {
	result.TOPICS.TOPIC.forEach(function(topic){
		esQuery(topic.ID[0], buildQuery( topic['EN-DESCRIPTION'][0].replace('/',' ') ) );
	});
});

fs.readFile('../../../data/image-based-topics.xml', function(err, data) {
	parser.parseString(data);
});

