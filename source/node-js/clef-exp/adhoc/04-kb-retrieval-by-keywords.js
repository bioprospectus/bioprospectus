var fs = require('fs');
var xml2js = require('xml2js');
var http = require('http');
var _ = require('underscore');
var MongoClient = require('mongodb').MongoClient;

/**
* This experiment corresponds to a KB retrieval over the clef2013 documents such as they come
*/

var KBQuery = function(topicId, keywords){
	var queryObj = function(keywords,concepts){
		return 
	};

	var options = {
		host : 'localhost',
		port : 8184,
		path : '/knowledge/snomeden/kbretrieval/',
		method : 'POST',
		headers : {
			'Content-Type':'application/json'
		}
	};

	var req = http.request(options, function(res) {

		var data = '';
		res.on('data', function (chunk) {
			data += chunk;
		});

		res.on('end', function () {
			if (res.statusCode === 200){
				var resData = JSON.parse(data)
				MongoClient.connect("mongodb://localhost:27017/clef", function(err, db) {
					if(!err) {
					  //console.log("Connected to MongoDB success");
					}
					var collection = db.collection('articles_raw');
					var count = 0;
					resData.forEach(function(hit){
							var stream = collection.find({"article.@pmid":hit.key},{"article.figures":1}).stream();
							stream.on("data", function(mongoDoc) {
								if(count <= 1000){
									var figures = mongoDoc.article.figures.figure;
									if (_ .isArray(figures)){
										figures = _.uniq(figures); 
										figures.forEach(function(figure){
								  	       console.log(topicId+' 1 '+figure['@iri']+' '+count+' '+hit.value+' test');	
										   count++;
								  	    });
									}else{
		 					  	        console.log(topicId+' 1 '+figures['@iri']+' '+count+' '+hit.value+' test');	
									    count++;
									}
							    }
							});
					});
				});
			}else{
				console.log(query);
				throw 'response error'+res.statusCode;
			}
		});
	});
	req.on('error', function(err) {
		console.log(err);
	});
	var query  = {"query":{ "keywords":[keywords],"concepts":[]}};
	req.write(JSON.stringify(query));	
	req.end();
};

var parser = new xml2js.Parser();
parser.on('end', function(result) {
	result.TOPICS.TOPIC.forEach(function(topic){
		KBQuery(topic.ID[0],topic['EN-DESCRIPTION'][0]);
	});
});

fs.readFile('../../../data/image-based-topics.xml', function(err, data) {
	parser.parseString(data);
});
