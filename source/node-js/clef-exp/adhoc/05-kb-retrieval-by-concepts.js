var fs = require('fs');
var readline = require('readline');
var http = require('http');
var _ = require('underscore');
var MongoClient = require('mongodb').MongoClient;

/**
* This experiment corresponds to a KB retrieval over the clef2013 documents such as they come
*/

var KBQuery = function(topicId, query){
	var options = {
		host : 'localhost',
		port : 8184,
		path : '/knowledge/snomeden/kbretrieval/',
		method : 'POST',
		headers : {
			'Content-Type':'application/json'
		}
	};

	var req = http.request(options, function(res) {

		var data = '';
		res.on('data', function (chunk) {
			data += chunk;
		});

		res.on('end', function () {
			if (res.statusCode === 200){
				var resData = JSON.parse(data)
				MongoClient.connect("mongodb://localhost:27017/clef", function(err, db) {
					if(!err) {
					  //console.log("Connected to MongoDB success");
					}
					var collection = db.collection('articles_raw');
					var count = 0;
					resData.forEach(function(hit){
							var stream = collection.find({"article.@pmid":hit.key},{"article.figures":1}).stream();
							stream.on("data", function(mongoDoc) {
								if(count <= 1000){
									var figures = mongoDoc.article.figures.figure;
									if (_ .isArray(figures)){
										figures = _.uniq(figures); 
										figures.forEach(function(figure){
								  	       console.log(topicId+' 1 '+figure['@iri']+' '+count+' '+hit.value+' test');	
										   count++;
								  	    });
									}else{
		 					  	        console.log(topicId+' 1 '+figures['@iri']+' '+count+' '+hit.value+' test');	
									    count++;
									}
							    }
							});
					});
				});
			}else{
				console.log(query);
				throw 'response error'+res.statusCode;
			}
		});
	});
	req.on('error', function(err) {
		console.log(err);
	});
	req.write(JSON.stringify(query));	
	req.end();
};

var rd = readline.createInterface({
    input: fs.createReadStream('../../../data/image-based-topics-pro.txt'),
    output: process.stdout,
    terminal: false
});

rd.on('line', function(line) {
    var jsonQuery = JSON.parse(line);
    jsonQuery.query.keywords=[];
    KBQuery(jsonQuery.topic,jsonQuery);
});