var fs = require('fs');
var readline = require('readline');
var http = require('http');
var _ = require('underscore');
var MongoClient = require('mongodb').MongoClient;

/**
* This experiment corresponds to a KB retrieval over the clef2013 documents such as they come
*/


var ESQuery = function(topicId, query){
	var queryObj = {
		"from": 0,
		"size": 1000, 
		"query": {
			"query_string": {
				"fields": [
					"article.figures.caption",
					"article.abstract",
					"article.title"
				], 
				"query": query,
				"use_dis_max": true
			}
		},
		"fields": [
			"article.pmid",        
			"article.figures.iri"
		]
	};

	var options = {
		host : 'optimus',
		port : 9200,
		path : 'clef-snomeden/_search',
		method : 'POST'
	};

	var req = http.request(options, function(res) {
		var data = '';
		res.on('data', function (chunk) {
			data += chunk;
		});
		res.on('end', function () {
			if (res.statusCode === 200){
				var resData = JSON.parse(data)
				var count = 0;
				resData.hits.hits.forEach(function(hit){
					if(count <= 1000){
						var iris = hit.fields['article.figures.iri'];
						if (_ .isArray(iris)){
							iris = _.uniq(iris); 
							iris.forEach(function(iri){
								console.log(topicId+' 1 '+iri+' '+count+' '+hit._score+' test');	
								count++;
							});
						}else{
							console.log(topicId+' 1 '+iris+' '+count+' '+hit._score+' test');	
							count++;
						}
					}
					
				});
			}else{
				console.log(query);
				throw 'ES response error'+res.statusCode;
			}
		});
	});
	req.on('error', function(err) {
		console.log(err);
	});
	req.write(JSON.stringify(queryObj));	
	req.end();
};

var rd = readline.createInterface({
	input: fs.createReadStream('../../../data/image-based-topics-pro.txt'),
	output: process.stdout,
	terminal: false
});


var conceptTerms = {
	113091000:["MR","NMR","MRI","Magnetic resonance imaging"],
	118189007:["prenatal finding"],
	118600007:["lymphoma"],
	13025001:["pseudopolyposis of colon","inflammatory polyps of colon"],
	1305003:["twin","replicate","duplicate","double"],
	13595002:["torsion of ovary","ovarian torsion"],
	14537002:["Hodgkin lymphoma","Hodgkins disease"],
	15842009:["RVT Renal vein thrombosis"],
	16310003:["ultrasound procedure","ultrasound","diagnostic sonar","USS","Ultrasonogram","ultrasonography"],
	19063003:["knee arthroplasty","knee replacement","repair of knee joint"],
	19943007:["hepatic cirrhosis","cirrhosis of liver"],
	86217007:["Avascular necrosis","Ischaemic necrosis","Ischemic necrosis"],
	257444003:["photograph"],
	26929004:["Alzheimer disease","AD"],
	281255004:["SBO"],
	301766008:["lesion of brain"],
	302858007:["osteoma"],
	36118008:["pneumothorax"],
	363358000:["malignant tumor of lung"],
	363406005:["malignant tumor of colon","cancer of colon"],
	363680008:["roentgenography","xray","radiographic imaging procedure","diagnostic radiography"],
	397932003:["congenital talipes equinovarus","clubfoot congenital","TEV Talipes equinovarus","clubfoot"],
	400006008:["hamartoma"],
	40689003:["male gonad","testicle","testis"],
	41040004:["Down syndrome","t21 Trisomy 21"],
	41607009:["renal cell carcinoma","Grawitz tumour","Grawitz tumor","hypernephroma","renal cell adenocarcinoma"],
	423827005:["inspection using endoscope","endoscopy"],
	48638002:["nephrocalcinosis"],
	51398009:["hamartoma"],
	58008004:["telangiectasis of liver","hepatic peliosis"],
	64144002:["prurigo","pruritic rash","pruritic rash","itchy skin eruption"],
	64859006:["osteoporosis","OP"],
	69748006:["thyroid","thyroid structure","thyroid gland"],
	69896004:["proliferative arthritis","atrophic arthritis","rheumatoid disease","chronic rheumatic arthritis","rheumatoid arthritis","rheumatic gout","RA","RhA"],
	70405009:["aneurysm of splenic artery"],
	70921007:["intestinal polyposis","polyposis coli","adenomatosis","adenomatous"],
	77386006:["pregnancy","pregnant","gestation"],
	77477000:["computerised tomograph","computed tomography","CAT","Computerized tomography"],
	82918005:["PET Positron emission tomography","PET scan"],
	88092000:["muscle wasting","muscle thinning","amyotrophia","muscular atrophy","muscle atrophy","amyotrophy"],
	88518009:["neurohepatic degeneration","WD","hepatocerebral degeneration","Wilson disease","copper storage disease","Westphal Strumpell syndrome","westphal Strümpell syndrome","Wilsons disease","hepatolenticular degeneration","Wilsons disease","hepatolenticular degeneration syndrome","progressive lenticular degeneration","cerebral pseudosclerosis","Kinnier Wilson disease"],
	129113006:["intra-aortic balloon pump device"]
};

var conceptTermsOri = {
	113091000:["MR Magnetic resonance","NMR Nuclear magnetic resonance","magnetic resonance study","magnetic resonance imaging","MRI"],
	118189007:["prenatal finding"],
	118600007:["lymphoma","malignant lymphoma"],
	13025001:["pseudopolyposis of colon","inflammatory polyps of colon"],
	1305003:["twin","replicate","duplicate","double"],
	13595002:["torsion of ovary","ovarian torsion"],
	14537002:["Hodgkin lymphoma","malignant lymphoma Hodgkins","Hodgkins disease"],
	15842009:["RVT Renal vein thrombosis","renal vein thrombosis"],
	16310003:["ultrasound procedure","ultrasound","ultrasound scan","diagnostic sonar","USS Ultrasound scan","Ultrasonogram","ultrasonography","US Ultrasound","diagnostic ultrasonography"],
	19063003:["knee arthroplasty","arthroplasty of knee","knee replacement","repair of knee joint"],
	19943007:["hepatic cirrhosis","cirrhosis of liver","CL Cirrhosis of liver","cirrhosis of liver"],
	86217007:["Avascular necrosis","Ischaemic necrosis","Ischemic necrosis"],
	257444003:["photograph"],
	26929004:["Alzheimer disease","AD   Alzheimers disease"],
	281255004:["SBO"],
	301766008:["lesion of brain"],
	302858007:["osteoma"],
	36118008:["pneumothorax"],
	363358000:["malignant tumor of lung","CA Lung cancer"],
	363406005:["malignant tumor of colon","cancer of colon","malignant tumour of colon","CA Cancer of colon"],
	363680008:["roentgenography","xray","radiographic imaging procedure","radiographic imaging procedure","diagnostic radiography","diagnostic imaging procedure using X rays"],
	397932003:["congenital talipes equinovarus","clubfoot congenital","TEV Talipes equinovarus","congenital clubfoot","clubfoot"],
	400006008:["hamartoma"],
	40689003:["male gonad","testis structure","testicle","testis structure","testis"],
	41040004:["Down syndrome","t21 Trisomy 21","complete trisomy 21 syndrome"],
	41607009:["renal cell carcinoma","Grawitz tumour","Grawitz tumor","hypernephroma","renal cell adenocarcinoma"],
	423827005:["inspection using endoscope","endoscopy"],
	48638002:["nephrocalcinosis"],
	51398009:["hamartoma"],
	58008004:["telangiectasis of liver","hepatic peliosis"],
	64144002:["prurigo","pruritic rash","pruritic rash","itchy skin eruption"],
	64859006:["osteoporosis","OP Osteoporosis"],
	69748006:["thyroid","thyroid structure","thyroid gland"],
	69896004:["proliferative arthritis","atrophic arthritis","rheumatoid disease","chronic rheumatic arthritis","rheumatoid arthritis","rheumatic gout","RA","RhA"],
	70405009:["aneurysm of splenic artery"],
	70921007:["intestinal polyposis","polyposis coli","adenomatosis","adenomatous"],
	77386006:["pregnancy","pregnant","gestation"],
	77477000:["computerised tomograph","computed tomography","CAT","Computerized tomography"],
	82918005:["PET Positron emission tomography","PET scan"],
	88092000:["muscle wasting","muscle thinning","amyotrophia","muscular atrophy","muscle atrophy","muscle wasting disorder","amyotrophy","muscle atrophy"],
	88518009:["neurohepatic degeneration","WD Wilson's disease","hepatocerebral degeneration","Wilson disease","copper storage disease","Westphal Strumpell syndrome","westphal Strümpell syndrome","Wilsons disease","hepatolenticular degeneration","Wilsons disease","hepatolenticular degeneration syndrome","progressive lenticular degeneration","cerebral pseudosclerosis","Kinnier Wilson disease"],
	129113006:["intra-aortic balloon pump, device","intra-aortic balloon pump"]
};
var stopWords = ["in","of","the","on","a"];
rd.on('line', function(line) {
	var jsonQuery = JSON.parse(line);
	var newKeywords = [];
	jsonQuery.query.keywords.forEach(function(keyword){
		var keywordSplited = keyword.split(" ");
		keywordSplited.forEach(function(originalKeyword){
			if (!_.contains(stopWords, originalKeyword.toLowerCase().trim())){
				newKeywords.push(originalKeyword + "^3")	
			}else{
				newKeywords.push(originalKeyword)	
			}
			
		});
	});
	var expandedKeywords=[];
	jsonQuery.query.concepts.forEach(function(conceptId){
		if (conceptId in conceptTerms){
			conceptTerms[conceptId].forEach(function(term) {
				expandedKeywords.push(term);
			});	
		}
	});
	newKeywords = newKeywords.concat(expandedKeywords)
	var queryStr = _.uniq(newKeywords).join(" ");
	ESQuery(jsonQuery.topic,queryStr);
});