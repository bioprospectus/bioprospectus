var fs = require('fs');
var readline = require('readline');
var http = require('http');
var _ = require('underscore');
var MongoClient = require('mongodb').MongoClient;

/**
* This experiment corresponds to a KB retrieval over the clef2013 documents such as they come
*/


var ESQuery = function(topicId, query){
	var queryObj = {
		"from": 0,
		"size": 1000, 
		"query": {
			"query_string": {
				"fields": [
					"article.figures.caption",
					"article.abstract",
					"article.title"
				], 
				"query": query,
				"use_dis_max": true
			}
		},
		"fields": [
			"article.pmid",        
			"article.figures.iri"
		]
	};

	var options = {
		host : 'optimus',
		port : 9200,
		path : 'clef-snomeden/_search',
		method : 'POST'
	};

	var req = http.request(options, function(res) {
		var data = '';
		res.on('data', function (chunk) {
			data += chunk;
		});
		res.on('end', function () {
			if (res.statusCode === 200){
				var resData = JSON.parse(data)
				var count = 0;
				resData.hits.hits.forEach(function(hit){
					if(count <= 1000){
						var iris = hit.fields['article.figures.iri'];
						if (_ .isArray(iris)){
							iris = _.uniq(iris); 
							iris.forEach(function(iri){
								console.log(topicId+' 1 '+iri+' '+count+' '+hit._score+' test');	
								count++;
							});
						}else{
							console.log(topicId+' 1 '+iris+' '+count+' '+hit._score+' test');	
							count++;
						}
					}
					
				});
			}else{
				console.log(query);
				throw 'ES response error'+res.statusCode;
			}
		});
	});
	req.on('error', function(err) {
		console.log(err);
	});
	req.write(JSON.stringify(queryObj));	
	req.end();
};


var rd = readline.createInterface({
	input: fs.createReadStream('../../../data/image-based-topics-pro.txt'),
	output: process.stdout,
	terminal: false
});

rd.on('line', function(line) {
	var jsonQuery = JSON.parse(line);
	var queryStr = jsonQuery.query.keywords.join(" ");
	ESQuery(jsonQuery.topic,queryStr);
});