#!/bin/bash
trec_eval -q ~/data/data/clef/qrel_2013_ad_hoc.txt $1 |grep "^map"  | awk '
BEGIN { 
	print "MAP values"
	queries[1]="mixed"
	queries[2]="mixed"      
	queries[3]="visual"       
	queries[4]="mixed"     
	queries[5]="mixed"      
	queries[6]="mixed"       
	queries[7]="semantic"       
	queries[8]="semantic"       
	queries[9]="mixed"       
	queries[10]="mixed"       
	queries[11]="mixed"       
	queries[12]="mixed"       
	queries[13]="visual"       
	queries[14]="visual"       
	queries[15]="mixed"       
	queries[16]="semantic"       
	queries[17]="semantic"       
	queries[18]="mixed"       
	queries[19]="mixed"       
	queries[20]="semantic"       
	queries[21]="mixed"       
	queries[22]="mixed"       
	queries[23]="mixed"       
	queries[24]="visual"       
	queries[25]="semantic"       
	queries[26]="mixed"       
	queries[27]="visual"       
	queries[28]="visual"       
	queries[29]="semantic"       
	queries[30]="semantic"       
	queries[31]="mixed"       
	queries[32]="visual"       
	queries[33]="visual"       
	queries[34]="semantic"       
	queries[35]="semantic"  

	total["semantic"] = 0
	total["visual"] = 0
	total["mixed"] = 0
}
{ 
	print $2, queries[$2], $3;
	total[queries[$2]]+= $3;
}
END{
	print "semantic_total", total["semantic"]
	print "visual_total", total["visual"]
	print "mixed_total", total["mixed"]
}
'