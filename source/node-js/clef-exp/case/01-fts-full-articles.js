var fs = require('fs');
var xml2js = require('xml2js');
var http = require('http');
var _ = require('underscore');

/**
* This experiment corresponds to a FTS over the clef2013 documents such as they come
*/

var esQuery = function(topicId, query){
	var queryObj = {
		"from": 0,
		"size": 1000, 
		"query": {
			"multi_match": {
				"fields": [
               		"article.figures.figure.caption",
               		"article.title",
               		"article.fulltext"
            	], 
				"query": query.replace('/',' '),
				"use_dis_max": true
			}
		},
		"fields": [
			"article.@doi",        
		]
	};

	var options = {
		host : 'optimus',
		port : 9200,
		path : 'clef2/_search',
		method : 'POST'
	};

	var req = http.request(options, function(res) {
		var data = '';
		res.on('data', function (chunk) {
			data += chunk;
		});
		res.on('end', function () {
			if (res.statusCode === 200){
				var resData = JSON.parse(data)
				var count = 0;
				resData.hits.hits.forEach(function(hit){
					if(count <= 1000){
						var doi = hit.fields['article.@doi'];
						if (doi){
							console.log(topicId+' 1 '+doi+' '+count+' '+hit._score+' test');	
							count++;
						}
					}
					
				});
			}else{
				console.log(query);
				throw 'ES response error'+res.statusCode;
			}
		});
	});
	req.on('error', function(err) {
		console.log(err);
	});
	req.write(JSON.stringify(queryObj));	
	req.end();
};

var parser = new xml2js.Parser();
parser.on('end', function(result) {
	result.TOPICS.TOPIC.forEach(function(topic){
		esQuery(topic.ID[0],topic['EN-DESCRIPTION'][0]);
	});
});

fs.readFile('../../../data/case-based-topics.xml', function(err, data) {
	parser.parseString(data);
});


