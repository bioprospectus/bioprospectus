var fs = require('fs');
var xml2js = require('xml2js');
var http = require('http');
var _ = require('underscore');
var ejs = require('elastic.js');
var nc = require('elastic.js/elastic-node-client');
var redis = require("redis");
var mysql = require("mysql");


/**
* This experiment corresponds to a semantic enhanced FTS that is performed in the following steps:
*  (Documents are preprocessed to have only one image and are annotated with concept tags that include frequency information)
*  1. FTS over documents to retrieve most relevant results
*  2. Most relevant concepts are extracted using TF-IDF weigthing criteria
*  3. Original query is expanded using the text in the most relevant concepts
*  4. Query run again using the extended query.
*       
*/


var buildTextQuery = function(queryStr, size){
	return {
	    "from": 0,
		"size": size,
		"query": {
	        "query_string": {
	            "fields": [
	               "article.figure.caption",
	               "article.title",
	               "article.fulltext"
	            ], 
	           "query": queryStr,
	           "use_dis_max": true
	        }  
		},
		"fields": [
	       "all_concepts_weigthed"
	    ]
	}
};


var buildConceptQuery = function(queryStr){
	return {
	    "from": 0,
		"size": 1000,
		"query": {
	        "query_string": {
	            "fields": [
	               "article.figure.caption",
	               "article.title",
	               "article.fulltext"
	            ], 
	           "query": queryStr,
	           "use_dis_max": true
	        }  
		},
		"fields": [
	       "article.@doi"
	    ]
	}
}

var esQuery = function(queryObj,callback){
	var indexName = 'clef-snomed';
	var options = {
		host : 'optimus',
		port : 9200,
		path : indexName + '/_search',
		method : 'POST'
	};
	var req = http.request(options, function(res) {
		var data = '';
		res.on('data', function (chunk) {
			data += chunk;
		});
		res.on('end', function(){
			if (res.statusCode === 200){
				var resData = JSON.parse(data)
				callback(resData);
			}else{
				throw 'ES response error '+res;
			}
		});
	});
	req.on('error', function(err) {
		console.log(err);
	});
	req.write(JSON.stringify(queryObj));	
	req.end();

};

sort_concepts_by_weigth = function(concepts_weighted_merged){
	var concepts_sorted = [];
	for (conceptId in concepts_weighted_merged){
		concepts_sorted.push([conceptId, concepts_weighted_merged[conceptId]])
	}
	concepts_sorted.sort(function(a, b) {return b[1] - a[1]})
	return concepts_sorted;
};

getdata = function(callback){
	var mySqlOptions = {
		host: 'localhost',
		user: 'root',
		password: 'elibom',
		database: 'snomeden'
	};
	mysqlCon = mysql.createConnection(mySqlOptions);
    mysqlCon.connect();
    var conceptToTermsMap = {};
	mysqlCon.query('SELECT descriptorId,conceptId,term FROM descriptors', function(err, rows) {
        if (err) {
            throw err;
        }
        rows.forEach(function(row) {
        	var conceptId = row['conceptId'];
        	if (conceptId in conceptToTermsMap){
        		conceptToTermsMap[conceptId] = conceptToTermsMap[conceptId] + ' ' + row['term'];
        	}else{
        		conceptToTermsMap[conceptId] = row['term'];
        	}
        });
        callback(conceptToTermsMap);
    });
    mysqlCon.end();
    
};


getdata2 = function(callback){
	var mySqlOptions = {
		host: 'localhost',
		user: 'root',
		password: 'elibom',
		database: 'snomeden'
	};
	mysqlCon = mysql.createConnection(mySqlOptions);
    mysqlCon.connect();
    var conceptToTermsMap = {};
	mysqlCon.query('SELECT conceptId,fullySpecifiedName FROM concepts', function(err, rows) {
        if (err) {
            throw err;
        }
        rows.forEach(function(row) {
        	conceptToTermsMap[row['conceptId']] = row['fullySpecifiedName'];
        });
        callback(conceptToTermsMap);
    });
    mysqlCon.end();
    
};

doTopicQuery = function(conceptToTermsMap,topicId, topicStr){
	esQuery(buildTextQuery(topicStr,5), function(resData) {
		concepts_weighted_merged = {};
		resData.hits.hits.forEach(function(hit){
			var concepts_weighted = hit.fields['all_concepts_weigthed'];//all_concepts_weighted
			for (conceptId  in concepts_weighted){
				if (concepts_weighted_merged.hasOwnProperty(conceptId)){
					concepts_weighted_merged[conceptId] += concepts_weighted[conceptId];
				}else{
					concepts_weighted_merged[conceptId] = concepts_weighted[conceptId];
				}	
			}
		});
		var topKConcepts = sort_concepts_by_weigth(concepts_weighted_merged).slice(0,8);
		var newStr = '';
		for (var i in topKConcepts){
			newStr += ' ' + conceptToTermsMap[topKConcepts[i][0]];	
		}
		newStr = newStr
		.replace(/\uFFFD/g, '')
		.replace(/\-/g,' ')
		.replace(/\+/g,' ')
		.replace(/\^/g,' ')
		.replace(/:/g,'')
		.replace(/"/g,'')
		.replace(/'/g,'')
		.replace(/\//g,' ')
		.replace(/\\/g,' ')
		.replace(/</g,'')
		.replace(/>/g,'')
		.replace(/\(/g,'')
		.replace(/\)/g,'')
		.replace(/\[/g,'')
		.replace(/\ AND\ /g,' and ')
		.replace(/\ OR\ /g,' or ')
		.replace(/\]/g,'');
		esQuery(buildConceptQuery(topicStr.replace(/\ /g,'^5 ')+ ' ' +newStr), function(resData2) {
			var count = 0;
			resData2.hits.hits.forEach(function(hit){
				var doi = hit.fields['article.@doi'];
				if (doi){
					console.log(topicId+' 1 '+doi+' '+count+' '+hit._score+' test');		
					count++;
				}
			});	
		});
		
	});
};

var parser = new xml2js.Parser();

parser.on('end', function(result) {
	getdata(function(conceptToTermsMap){
		result.TOPICS.TOPIC.forEach(function(topic){
			var topicId = topic.ID[0];
			var topicStr = topic['EN-DESCRIPTION'][0].replace('/',' ');
			doTopicQuery(conceptToTermsMap,topicId, topicStr);	
		});
	});
});


fs.readFile('../../../data/case-based-topics.xml', function(err, data) {
	parser.parseString(data);
});

