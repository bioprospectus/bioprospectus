var ejs = require('elastic.js');
var nc = require('elastic.js/elastic-node-client');
var Crawler = require("crawler").Crawler;
var inspect = require('eyes').inspector({maxLength: 50000});

var c = new Crawler({
	"maxConnections": 10,
	"callback":function(error,result,$) {
		var pmid = result.uri.split('/').slice(-1)[0];
		if (!error){
			$('div .morecit ul li').filter(function(index){ 
				return $('a',this).attr('alsec') === 'mesh';
			}).each(function(i,item){
				console.log(pmid+'\t'+$(item).text());
			});
		}else{
			console.log('error  on'+pmid+' '+error);			
		}
	},
	"onDrain": function(){
		process.exit(1);
	}
});

ejs.client = nc.NodeClient('localhost', '9200');

var esCallback = function(results) {
	if (results.hits) {
		var hits = results.hits.hits;
		hits.forEach(function(hit){
			if (hit.fields['article.@pmid']){
				console.log('pmid: '+hit.fields['article.@pmid']);
				c.queue('http://www.ncbi.nlm.nih.gov/pubmed/'+hit.fields['article.@pmid']);
			}else{
				console.log('pmcd: '+hit.fields['article.@pmcid']);
			}
		});
	}
};

var esReq = ejs.Request()
	.indices('clef2')
	.types('articles')
	.query(ejs.MatchAllQuery())
	.fields(['article.@pmid','article.@pmcid'])
	.size(80000);

esReq.doSearch(esCallback);