var esProxyFactory = require('./esproxy/elasticsearch-proxy');
var restify = require('restify');
var redis = require("redis");
var _ = require('underscore')._;
var mysql      = require('mysql');

var conceptsMap = {};

var kbmedEsProxy = function() {

    var redisCli;

    var knowledgeCli = restify.createJsonClient({url: 'http://10.0.2.15:8184'});

    var connectRedis = function() {
        var redisOptions = {
            host: 'localhost',
            port: 6379
        };
        console.log('Connecting to Redis ' + JSON.stringify(redisOptions));
        redisCli = redis.createClient(redisOptions.port, redisOptions.host);
        redisCli.on('error', function(err) {
            console.log('Redis connection error: ' + err);
            throw  err;
        });
    };


    var generateKey = function() {
        var date = new Date();
        var key = date.getUTCFullYear() + '.' + date.getUTCMonth() + '.' +
                date.toLocaleTimeString().replace(/:/g, '.') + '.' +
                Math.floor(Math.random() * 10000);
        return key;
    };

    var extractConcepts = function(esResponse) {
        var allConcepts = [];
        var conceptsByDoc = {};
        if (esResponse.hits) {
            esResponse.hits.hits.forEach(function(item) {
                conceptsByDoc[item._source.id] = item._source.all_concepts_list;
                allConcepts = _.union(allConcepts, item._source.all_concepts_list);
            });
        }
        return {
            all: allConcepts,
            byDoc: conceptsByDoc
        };
    };

    var labelFacets = function(esResponse) {
        if (esResponse.facets) {
          var facetTerms = esResponse.facets.concepts.terms;
            if (facetTerms.length > 0){
                facetTerms.forEach(function(item, index) {
                    item['prefTerm'] = conceptsMap[item.term].prefTerm;
                    item['category'] = conceptsMap[item.term].category;
                    console.log('%j',item);
                });
            }
        }
    };

    var getConceptSuggestions = function(conceptsArray) {
        var suggestedConcepts = [];
        return suggestedConcepts;
    };

    var preRequest = function(request, requestData) {
        var requestObj = JSON.parse(requestData);
        if (request.url.match('_search*')) {
            console.log('Search pre-request');
        }
        return JSON.stringify(requestObj);
    };

    var postRequest = function(request, response, responseData) {
        var esResponse = JSON.parse(responseData);
        if (request.url.match('_search*') && response.statusCode === 200) {
            console.log('Search post-request');
            if (esResponse.hits.total > 0 ){
                labelFacets(esResponse);
            }else{
                console.log('not results found');
            }
        }
        return JSON.stringify(esResponse);
    };

    var esProxy = esProxyFactory.getProxy(preRequest, postRequest);
    esProxy.start();
    connectRedis();
};

(function init() {
    var connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : 'elibom',
      database : 'snomeden'
    });

    connection.connect();
    console.log('Loading concepts from DB');
    connection.query('SELECT conceptId,fullySpecifiedName,category FROM concepts;', function(err, rows, fields) {
      if (err) throw err;
      rows.forEach(function(row, index){
          var concept = {};
          concept['id'] = row.conceptId;
          concept['prefTerm'] = row.fullySpecifiedName;
          concept['category'] = row.category;
          conceptsMap[row.conceptId] = concept;
      });
      new kbmedEsProxy()
    });
    connection.end();
})();
