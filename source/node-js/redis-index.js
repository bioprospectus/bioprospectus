var restify = require('restify');
var redis = require("redis");
var _ = require('underscore')._;


var index = function (indexName) {
    var SEP = ':';
    var getBasePrefix = function (indexName) {
        var basePrefixSize = 10;
        return indexName.length > basePrefixSize ? indexName.toUpperCase().substring(0, basePrefixSize) : indexName.toUpperCase();
    };
    var _INDEX_NAME = getBasePrefix(indexName);
    var _TERMS_KEY = _INDEX_NAME + SEP + 'TERMS';
    var _DOCUMENTS_KEY = _INDEX_NAME + SEP + 'DOCS';
    var _INVERTED_INDEX_PREFIX = _INDEX_NAME + SEP + 'I_IDX' + SEP;
    var _OCCURRENCES_INDEX_PREFIX = _INDEX_NAME + SEP + 'O_IDX' + SEP;
    var _DOCUMENT_TERMS_PREFIX = _INDEX_NAME + SEP + 'D_TER' + SEP;
    console.log("Init name space for index " + indexName + "ok");
    return function () {
        return {
            NAME: _INDEX_NAME,
            TERMS_KEY: _TERMS_KEY,
            DOCUMENTS_KEY: _DOCUMENTS_KEY,
            INVERTED_INDEX_PREFIX: _INVERTED_INDEX_PREFIX,
            OCCURRENCES_INDEX_PREFIX: _OCCURRENCES_INDEX_PREFIX,
            DOCUMENT_TERMS_PREFIX: _DOCUMENT_TERMS_PREFIX
        };
    }();
};

var redisCli;


var connectRedis = function() {
    var redisOptions = {
        host: 'localhost',
        port: 6379
    };
    console.log('Connecting to Redis ' + JSON.stringify(redisOptions));
    redisCli = redis.createClient(redisOptions.port, redisOptions.host);
    redisCli.on('error', function(err) {
        console.log('Redis connection error: ' + err);
        throw  err;
    });
};



var getTerms= function(){
	return redisCli.hkeys(TERMS_KEY);
};

var getDocuments = function(term){
	return redisCli.hkeys(INVERTED_INDEX_PREFIX + term);
};

var getOccurrences  = function(term){
    return redisCli.smembers(OCCURRENCES_INDEX_PREFIX + term);
};

var getGlobalFrequency = function(term) {
	return redisCli.hget(TERMS_KEY, term);
};

var getDocumentFrequency = function(term) {
    return redisCli.hkeys(INVERTED_INDEX_PREFIX + term).length;
};

 var getInDocumentFrequency = function(documentId,term) {
    return redisCli.hget(INVERTED_INDEX_PREFIX + term, documentId);
};

var getTotalDocuments = function() {
    return redisCli.scard(DOCUMENTS_KEY);
};

var getDocumentTerms = function(documentId) {
    return redisCli.lrange(DOCUMENT_TERMS_PREFIX + documentId, 0, -1);
};


var getDocumentLength = function(documentId) {
    return jedis.hget(DOCUMENTS_KEY, documentId);
}

