'''

@author: Alejandro Riveros Cruz
'''
import json
import copy
import sys
from pyes import *
from pymongo import MongoClient

def print_doc_as_json(doc):
    print json.dumps(doc, sort_keys=True,indent=4, separators=(',', ': '))

def load_json(path):
    json_data=open(path)
    return json.load(json_data)

def delete_caption_annotations(document):
    for k in document['mappings_by_field'].keys():
        if k.endswith('-caption'):
            del document['mappings_by_field'][k]

def process_figure(document, figure):
    """Moves annotations to figure element within mappings_by_field"""
    figIri = figure['@iri']
    document['article']['figure'] = figure
    if 'mappings_by_field' in document:
        if figIri+'-caption'  in document['mappings_by_field']:
            figAnnotations = document['mappings_by_field'][figIri + '-caption']
            del document['mappings_by_field'][figIri + '-caption']
            document['mappings_by_field']['figure'] = {}
            document['mappings_by_field']['figure']['@iri'] = figIri
            document['mappings_by_field']['figure']['caption'] = figAnnotations

def process_document_figures(es_connection, es_opt, document):
    figures = document['article']['figures']['figure']
    del document['article']['figures']
    document_copy = copy.deepcopy(document)
    if type(figures) is list: #The document contains a list of figures
        for figure in figures:
            process_figure(document_copy, figure)
            delete_caption_annotations(document_copy)
            index_document(es_connection, es_opt, document_copy)
    elif type(figures) is tuple: #The document contains only one figure
        process_figure(document, figure)
        index_document(es_connection, es_opt, document)

def index_mongo_collection(mongo_opt, es_opt, split_figures):
    es_connection = ES(es_opt['host']+':'+str(es_opt['port']), timeout=30.0, bulk_size=100)
    mongo_client = MongoClient(mongo_opt['host']+':'+str(mongo_opt['port']))
    db = mongo_client[mongo_opt['db']]
    collection = db[mongo_opt['collection']]
    total_documents = collection.count()
    progress_count = 0
    for document in collection.find():
        document_id = document['_id']
        del document['_id']
        if split_figures:
            process_document_figures(es_connection, es_opt, document)
        else:
            index_document(es_connection, es_opt, document)
        progress_count += 1
        print 'Processing ' + str(progress_count) + '/' + str(total_documents) + ' - ' + str(document_id)
    es_connection.force_bulk()


def setup_es(es_opt, split_figures):
    es_connection = ES(es_opt['host']+':'+str(es_opt['port']))
    try:
        es_connection.indices.delete_index(es_opt['index'])
        print "Index successfully deleted"
    except:
        pass
    es_connection.indices.create_index(es_opt['index'])
    if split_figures:
        mapping = load_json('clef2013-by-fig-esmapping.json')
    else:
        mapping = load_json('clef2013-esmapping.json')
    es_connection.indices.put_mapping(es_opt['type'], {'properties':mapping}, [es_opt['index']])

def index_document(es_connection, es_opt, document):
    es_connection.index(document, es_opt['index'], es_opt['type'], bulk=True)

if __name__ == '__main__':
    split_figures = False
    mongo_opt={
       'host':'localhost',
       'port':27017,
       'db':'clef',
       'collection':'sample100'
    }
    es_opt={
       'host':'192.168.1.4',
       'port':9200,
       'index':'clef-snomed',
       'type':'articles'
    }
    setup_es(es_opt, split_figures)
    index_mongo_collection(mongo_opt, es_opt, split_figures)

