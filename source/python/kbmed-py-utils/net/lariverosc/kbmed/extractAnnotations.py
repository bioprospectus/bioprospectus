#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymongo import MongoClient
import MySQLdb
import redis


mysqldb = MySQLdb.connect(host="localhost", user="root", passwd="elibom", db="snomeden", charset='utf8')
r = redis.StrictRedis(host='localhost', port=6379, db=0)

def load_concepts():
    print 'loading all_concepts'
    cursor = mysqldb.cursor()
    cursor.execute('SELECT conceptId,category,fullySpecifiedName from concepts;')
    all_concepts = {}
    for i in range(0,int(cursor.rowcount)):
        row = cursor.fetchone()
        concept = {}
        concept['conceptId'] = row[0].strip()
        concept['category'] = row[1].strip()
        concept['fullySpecifiedName'] = row[2].strip()
        all_concepts[concept['conceptId']] = concept
    return all_concepts 

def load_icvals():
    print 'loading all_icvals'
    return r.hgetall('ICVALS')


if __name__ == '__main__':
    mongo_client = MongoClient('localhost:27017')
    db = mongo_client['tmapper']
    collection = db['articles-annotated-disamb']
    all_concepts = load_concepts()
    all_icvals = load_icvals()
    print 'article_id,conceptId,weigth,icval,category,fullySpecifiedName'
    for document in collection.find({},):
        article = document['article']
        all_concepts_weigthed = document['all_concepts_weigthed']
        if (article['@pmid']):
            article_id = 'pmid-' + article['@pmid']
        else:
            article_id = 'pmcid-' + article['@pmcid']
        if (all_concepts_weigthed):
            for conceptId in all_concepts_weigthed:
                concept = all_concepts[conceptId]
                icval = 'NA'
                if conceptId in all_icvals:
                    icval = all_icvals[conceptId]
                try:
                    print article_id + ',' + conceptId + ',' + str(all_concepts_weigthed[conceptId]) + ',' + str(icval) + ',"' + concept['category'] + '","' + concept['fullySpecifiedName'].encode('ascii', 'ignore') + '"'
                except UnicodeEncodeError:
                    print 'ERROR-' +  conceptId
                    
    mysqldb.close();                
        
