# -*- coding: iso-8859-15 -*-
'''
Created on Mar 30, 2013

@author: Alejandro Riveros Cruz <lariverosc@gmail.com>
'''

import os
import codecs
import json
import re

from textUtil import remove_accents
from pyes import ES
from pymongo import MongoClient


esConnection = ES('127.0.0.1:9200')
mongoClient = MongoClient()
saruroDB = mongoClient.saruro

HEADINGS = set(['EXAMEN_FÍSICO',
                'SIGNOS_VITALES',
                'ANTECEDENTES',
                'REVISION_POR_SISTEMAS',
                'PARACLINICOS',
                'CUELLO_UTERINO',
                'DATOS_ESPECIALIDAD',
                'CARACTERÍSTICAS_DERMATOLÓGICAS'])

def indexClinicalRecordDict(clinicalRecord):
    esConnection.index(clinicalRecord, 'saruro33', 'cr')
    
def storeClinicalRecordDict(clinicalRecord):
    crCollection = saruroDB.cr
    crCollection.insert(clinicalRecord)

def buildClinicalRecordDict(infile):
    txtfile = codecs.open(infile, "r",'utf-8')
    clinicalRecordDict = {}
    clinicalRecordDict['RespuestasTemp'] = []
    clinicalRecordDict['ObservacionesTemp'] = []
    current = clinicalRecordDict
    last = current
    isObservation = False
    for line in txtfile:  
        line = line.strip()
        line = remove_accents(line)
        if line in HEADINGS: # If the current text is a heading add the record reset observations and continue
            heading = line.lower().replace(" ","_")
            clinicalRecordDict[heading] = {}
            current = clinicalRecordDict[heading]
            isObservation = False
            continue
        if len(line) != 0: #if the line is not empty
            breakIndex = line.find(" : ")
            if breakIndex == -1:
                title = line.upper().strip().replace("\n", "")
                matchRespuesta = re.match('^RESPUESTA-\d*', title)
                if matchRespuesta:
                    current = {}
                    clinicalRecordDict['RespuestasTemp'].append(current)
                    isObservation = False
                elif re.match('^OBSERVATION', title):
                    current = {}
                    clinicalRecordDict['ObservacionesTemp'].append(current)
                    isObservation = True    
                else: 
                    if current.has_key(last) :
                        current[last] = current[last] + " " + title.capitalize()
                    else:
                        current[last] = title.capitalize()    
            else:
                tag = line[:breakIndex].lower().replace(" ","_")
                value = line[breakIndex+3:].strip().capitalize().replace("\n", "")
                if tag=='id':
                    value = value.upper()
                current[tag] = value
                last = tag
                if isObservation and tag=='valor':
                    current = {}
                    clinicalRecordDict['ObservacionesTemp'].append(current)
    fixObservations(clinicalRecordDict)
    fixRespuestas(clinicalRecordDict)
    fixCie(clinicalRecordDict)
    return clinicalRecordDict

def fixObservations(clinicalRecordDict):
    if len(clinicalRecordDict['ObservacionesTemp']) > 0:
        clinicalRecordDict['observaciones'] = {}
        for observation in clinicalRecordDict['ObservacionesTemp']:
            if len(observation) > 0:
                if 'texto' in observation:
                    clinicalRecordDict['observaciones'][observation['titulo'].lower().replace(" ","_")] = observation['valor'] + " " +  observation['texto']
                elif 'valor' in observation:
                    clinicalRecordDict['observaciones'][observation['titulo'].lower().replace(" ","_")] = observation['valor']
                else:    
                    clinicalRecordDict['observaciones']['texto'] = observation['titulo']
    del clinicalRecordDict['ObservacionesTemp']
                
def fixRespuestas(clinicalRecordDict):
    if len(clinicalRecordDict['RespuestasTemp']) >0:
        clinicalRecordDict['respuestas'] = {}
        for respuesta in clinicalRecordDict['RespuestasTemp']:
            if len(respuesta) > 0:
                clinicalRecordDict['respuestas'].update(respuesta)
    del clinicalRecordDict['RespuestasTemp']
    
def fixCie(clinicalRecordDict):
    print 'todo'    
            
def printClinicalRecordAsJson(clinicalRecordDict):          
    print json.dumps(clinicalRecordDict, sort_keys=True,indent=4, separators=(',', ': '))
    
def listFilesRecursivelly(rootdir):
    fileList = []
    for root, files in os.walk(rootdir):
        for crFile in files:
            fileList.append(os.path.join(root,crFile))
    return fileList
                
if __name__ == '__main__':
    #rootdir = sys.argv[1]
    rootdir = "/Users/desarrollo/workspace/personal/data/sarhis"    
    fileList = listFilesRecursivelly(rootdir)
    count = 0;
    for infile in fileList:
        print '{} Processing file '.format(count+1) + infile
        clinicalRecordDict = buildClinicalRecordDict(infile)
        printClinicalRecordAsJson(clinicalRecordDict)
        storeClinicalRecordDict(clinicalRecordDict)
        count += 1
    print 'OK {} documents parsed'.format(count)
    
    