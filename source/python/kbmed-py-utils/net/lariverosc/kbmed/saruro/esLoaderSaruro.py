'''

@author: Alejandro Riveros Cruz
'''
import json
from pyes import ES
from pymongo import MongoClient

def printDocAsJson(doc):          
    print json.dumps(doc, sort_keys=True,indent=4, separators=(',', ': '))
    
    
def indexMongoCollection(mongoOpt,esOpt):
    esConnection = ES(esOpt['host']+':'+str(esOpt['port']))
    mongoClient = MongoClient(mongoOpt['host']+':'+str(mongoOpt['port']))
    db = mongoClient[mongoOpt['dbName']]
    coll = db[mongoOpt['collName']]
    for item in coll.find():
        del item['_id']
        printDocAsJson(item) 
        esConnection.index(item, esOpt['index'],esOpt['type'])
        
            
if __name__ == '__main__':
    mongoOpt={
       'host':'localhost',
       'port':27017,
       'dbName':'tmapper',
       'collName':'cr'       
    }
    esOpt={
       'host':'localhost',
       'port':9200,
       'index':'saruro',
       'type':'cr'    
    }
    indexMongoCollection(mongoOpt, esOpt)
    