# -*- coding: iso-8859-15 -*-
'''
Created on Apr 9, 2013

@author: desarrollo
'''

import codecs
import re
import MySQLdb
import sys



db = MySQLdb.connect(host="localhost", user="root", passwd="elibom", db="snomedes")

def parseConceptsFile(conceptsFile):
    txtfile = codecs.open(conceptsFile, "r",'utf-8')
    cursor = db.cursor()
    count = 0
    for line in txtfile:  
        line = line.strip()
        conceptFields = line.split('\t');
        concept = {}
        if (conceptFields[1]=='0'): #Concept status is active
            concept['id'] = conceptFields[0]
            m = re.match('(.*)(\([\w/+\s]*\))$', conceptFields[2])
            concept['fsn'] = m.group(1).strip()
            concept['category'] = m.group(2)[1:-1]
            concept['primitive'] = conceptFields[5]
            try:
                cursor.execute("""INSERT INTO concepts(conceptId,fullySpecifiedName,category,isPrimitive) VALUES (%s,%s,%s,%s)""",(concept['id'], concept['fsn'],concept['category'],concept['primitive']))
                db.commit()
            except:
                print "Unexpected error:", sys.exc_info()[0]
                db.rollback()
            
        count += 1
        print 'Concepts file - Processing line ' + str(count)       
    cursor.close()

def parseDescriptorsFile(descriptorsFile):
    txtfile = codecs.open(descriptorsFile, "r",'utf-8')
    cursor = db.cursor()
    count = 0
    for line in txtfile:  
        line = line.strip()
        descriptorFields = line.split('\t');
        descriptor = {}
        if (descriptorFields[1]=='0'): #Descriptor status is active
            descriptor['id'] = descriptorFields[0]
            descriptor['conceptId'] = descriptorFields[2]
            descriptor['initialCapitalStatus'] = descriptorFields[4]
            descriptor['descriptionType'] = descriptorFields[5]
            descriptor['languageCode'] = descriptorFields[6]
            
            m = re.match('(.*)(\([\w/+\sēźīņ]*\))$', descriptorFields[3])
            if (m):
                descriptor['term'] = m.group(1).strip()
            else:
                descriptor['term'] = descriptorFields[3].strip()
            try:
                cursor.execute("""INSERT INTO descriptors(descriptorId,conceptId,term,descriptionType,initialCapitalStatus,languageCode) VALUES (%s,%s,%s,%s,%s,%s)""", (descriptor['id'],descriptor['conceptId'],descriptor['term'],descriptor['descriptionType'],descriptor['initialCapitalStatus'],descriptor['languageCode']))
                db.commit()
            except:
                print "Unexpected error:", sys.exc_info()[0]
                db.rollback()
        count += 1
        print 'Descriptors file - Processing line ' + str(count)
    cursor.close()
    
def parseRelationshipsFile(relationshipsFile):
    txtfile = codecs.open(relationshipsFile, "r",'utf-8')
    cursor = db.cursor()
    count = 0
    for line in txtfile:  
        line = line.strip()
        relationshipsFields = line.split('\t');
        relationship = {}
        relationship['id'] = relationshipsFields[0]
        relationship['conceptId1'] = relationshipsFields[1]
        relationship['type'] = relationshipsFields[2]
        relationship['conceptId2'] = relationshipsFields[3]
        relationship['characteristicType'] = relationshipsFields[4]
        relationship['refinability'] = relationshipsFields[5]
        relationship['relationshipGroup'] = relationshipsFields[6]
        try:
            cursor.execute("""INSERT INTO relationships(relationshipId,conceptId1,relationshipType,conceptId2,characteristicType,refinability,relationshipGroup) VALUES (%s,%s,%s,%s,%s,%s,%s)""",(relationship['id'],relationship['conceptId1'],relationship['type'],relationship['conceptId2'],relationship['characteristicType'],relationship['refinability'],relationship['relationshipGroup']))
            db.commit()
        except:
            print "Unexpected error:", sys.exc_info()[0]
            db.rollback()
        count += 1
        print 'Relationships file - Processing line ' + str(count)    
    cursor.close()    
    

if __name__ == '__main__':
    snomedPath = "/Users/desarrollo/workspace/personal/SnomedCT_Release-es_INT_20121031/RF1Release/Terminology/Content/"
    conceptsFile = "sct1_Concepts_Core_INT_20120731.txt"
    descriptorsFile = "sct1_Descriptions_es_INT_20121031.txt"
    relationshipsFile = "sct1_Relationships_Core_INT_20120731.txt"
    parseConceptsFile(snomedPath+conceptsFile)
    parseDescriptorsFile(snomedPath+descriptorsFile)
    parseRelationshipsFile(snomedPath+relationshipsFile)
    db.close();
    