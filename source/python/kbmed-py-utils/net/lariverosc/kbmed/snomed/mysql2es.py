'''

@author: Alejandro Riveros Cruz
'''
import json
import sys
import codecs
import unicodedata
import MySQLdb
import re
from pyes import *

db = MySQLdb.connect(host="localhost", user="root", passwd="root", db="snomeden", charset='utf8')

def remove_accents(text):
    nkfd_form = unicodedata.normalize('NFKD', unicode(text))
    return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

def load_json(path):
    json_data=open(path)
    return json.load(json_data)

def print_doc_as_json(doc):
    print json.dumps(doc, sort_keys=True,indent=4, separators=(',', ': '))

def setup_es(es_opt,mapping_file_path):
    es_connection = ES(es_opt['host']+':'+str(es_opt['port']))
    try:
        es_connection.indices.delete_index(es_opt['index'])
        print "Index successfully deleted"
    except:
        pass
    es_connection.indices.create_index(es_opt['index'])
    mapping = load_json(mapping_file_path)
    es_connection.indices.put_mapping(es_opt['type'], {'properties':mapping}, [es_opt['index']])

def process(es_opt, db_query):
    es_connection = ES(es_opt['host']+':'+str(es_opt['port']), timeout=30.0, bulk_size=100)
    cursor=db.cursor()
    cursor.execute(db_query)
    for i in range(0,int(cursor.rowcount)):
      row = cursor.fetchone()
      document = {}
      document['category'] = row[0].strip()
      document['conceptId'] = row[1].strip()
      document['descriptorId'] = row[2].strip()
      document['term'] = row[3].strip()
      document['term_len'] = len(document['term'])
      document['term_suggest'] = {}
      document['term_suggest']['input'] = document['term']
      document['term_suggest']['weight'] = 1

      output = {}
      output['category'] = document['category']
      output['conceptId'] = document['conceptId']
      output['descriptorId'] = document['descriptorId']
      match = re.match('(.*)(\([\w/+\s]*\))$', document['term'])
      if (match):
        output['term'] = match.group(1).strip()
      else:
        output['term'] = document['term']

      document['term_suggest']['output'] = json.dumps(output)

      print_doc_as_json(document)
      index_document(es_connection, es_opt, document)
    es_connection.force_bulk()            

def index_document(es_connection, es_opt, document):
    es_connection.index(document, es_opt['index'], es_opt['type'], bulk=True)            

if __name__ == '__main__':
    mapping_file_path = sys.argv[1]
    mysql_opt={
       'host':'localhost',
       'port':3306,
       'user':'root',
       'pass':'root',
       'database':'snomeden',
    }
    es_opt={
       'host':'10.0.2.15',
       'port':9200,
       'index':'snomeden-terms',
       'type':'terms'
    }
    setup_es(es_opt,mapping_file_path)
    process(es_opt,"""SELECT c.category,c.conceptID,d.descriptorID,d.term FROM concepts c, descriptors d WHERE c.conceptID=d.conceptID;""")
    db.close();

