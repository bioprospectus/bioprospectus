'''
Created on Jul 24, 2013

@author: desarrollo
'''
import re
from pyes import ES
from nltk.corpus import stopwords

def remove_accents(text):
    return unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore')

def getRelevantTokens(text):
    text = text.lower() 
    words = re.findall(r'\w+', text,flags = re.UNICODE | re.LOCALE) 
    return filter(lambda x: x not in stopwords.words('english'), words)

def loadDictionary(infile,esOpt):
    esConnection = ES(esOpt['host']+':'+str(esOpt['port']), timeout=30.0, bulk_size=10)
    txtfile = open(infile, "r",'utf-8')
    for line in txtfile:  
        line = line.strip()    
        print 'processing term:  '+line
        termFields = line.split('\t')
        term = {}    
        term['id'] = termFields[0]
        term['term'] = termFields[1]
        term['nterm'] = termFields[0] +' '+termFields[1]
        term['length'] = len(termFields[1])
        relevant_words = getRelevantTokens(termFields[1]);
        rlength = 0
        for word in relevant_words:
            rlength += len(word)
        term['rlength'] = rlength    
        esConnection.index(term, esOpt['index'],esOpt['type'])


if __name__ == '__main__':

    esOpt={
       'host':'localhost',
       'port':9200,
       'index':'snomeden',
       'type':'terms'    
    }
    loadDictionary('/home/alejandro/data/data/snomed/snomedenTermsFiltered.txt', esOpt)