'''

@author: Alejandro Riveros Cruz
'''
from pymongo import MongoClient
from math import log

def compute_idf(mongo_opt):
    print 'Computing IDF values'
    mongo_client = MongoClient(mongo_opt['host'] + ':' + str(mongo_opt['port']))
    db = mongo_client[mongo_opt['db']]
    collection = db[mongo_opt['collection']]
    total_documents = collection.count()
    doc_counts = {}
    for document in collection.find():
        mappings = document['mappings']
        try:
            all_mappings = mappings['all_mappings']
            for mapping_id in all_mappings:
                if mapping_id in doc_counts:
                    doc_counts[mapping_id] += 1
                else:
                    doc_counts[mapping_id] = 1
        except KeyError:
            pass
    idf = {}
    for mapping_id in doc_counts:
        idf[mapping_id] = log(total_documents / doc_counts[mapping_id], 2)
    return idf

def get_mappings_weigths(idf, mappings):
    all_mappings_freq = mappings['all_mappings_freq']
    total_mappings = 0
    for mapping_id in all_mappings_freq:
        total_mappings += all_mappings_freq[mapping_id]
    weigths = {}
    for mapping_id in all_mappings_freq:
        mapping_norm_freq = all_mappings_freq[mapping_id] / float(total_mappings)
        tf_idf = mapping_norm_freq * idf[mapping_id]
        if (tf_idf > 0.1):
            weigths[mapping_id] = tf_idf
    return weigths

def weigth_mappings(mongo_opt):
    idf = compute_idf(mongo_opt)
    mongo_client = MongoClient(mongo_opt['host'] + ':' + str(mongo_opt['port']))
    db = mongo_client[mongo_opt['db']]
    collection = db[mongo_opt['collection']]
    total_documents = collection.count()
    progress_count = 0
    for document in collection.find():
        document_id = document['_id']
        del document['_id']

        mappings = document['mappings']
        mappings['mappings_weighted'] = get_mappings_weigths(idf, mappings)
        collection.update({'_id': document_id}, {"$set": document}, upsert=False)

        progress_count += 1
        print 'Processing ' + str(progress_count) + '/' + str(total_documents) + ' - ' + str(document_id)

if __name__ == '__main__':
    mongo_opt = {
       'host':'localhost',
       'port':27017,
       'db':'clef',
       'collection':'sample100'
    }
    weigth_mappings(mongo_opt)
