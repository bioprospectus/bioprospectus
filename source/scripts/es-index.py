 #!/usr/bin/python
 # -*- coding: iso-8859-15 -*-
import os
import sys
from pyes import *

def isNotNull(value):
    return value is not None and len(value) > 0


fileList = []
#rootdir = sys.argv[1]
fileList.append(sys.argv[1]) 
#for root, subFolders, files in os.walk(rootdir):
 #   for file in files:
  #     	fileList.append(os.path.join(root,file))

conn = ES('127.0.0.1:9200')       	
#file = "/Users/desarrollo/workspace/personal/data/sarurotxt/DERMATOLOGIA/RC1_152138.txt"
for file in fileList:
	respuestas = False
	caseId = file.split("/")[-1]
	htmlfile = open(caseId+".html","w")
	txtfile = open(file, "r").readlines()
	texto = titulo = valor = None
	for line in txtfile:
		# line=line.encode("utf-8")
		# line=uline.encode("latin-1","ignore")
		if " : " in line: 		
	   		if line.startswith("texto : "):
	   			texto = line.split(" : ")[1].replace("\n", "")
	   			if respuestas: #Texto para respuestas
					htmlfile.write("""<spam class="entry">"""+texto.capitalize().replace("\n"," ")+"""</spam><br/>""")
	   		elif line.startswith("titulo : "):
	   			titulo = line.split(" : ")[1].replace("\n", "")
	   			if respuestas: #Titulo para respuestas
					htmlfile.write("""<spam class="entry">"""+titulo.capitalize().replace("\n"," ")+"""</spam><br/>""")
	   		elif line.startswith("valor : "):
	   			valor = line.split(" : ")[1].replace("\n", "")
	   			if isNotNull(texto) and isNotNull(titulo):#Titulo-Valor-Texto
	   				htmlfile.write("""<spam class="entry_t">"""+titulo+""": </spam>""")
	   				htmlfile.write("""<spam class="entry_v">"""+valor+" "+texto+"""</spam><br/>""")
	   				texto = titulo = valor = None
	   			elif isNotNull(titulo):#Titulo-Valor
	   				htmlfile.write("""<spam class="entry_t">"""+titulo+""": </spam>""")
	   				htmlfile.write("""<spam class="entry_v">"""+valor+"""</spam><br/>""")
	   				titulo = valor = None
	   		elif isNotNull(texto) and isNotNull(titulo) and not respuestas:
				htmlfile.write("""<spam class="entry">"""+titulo.capitalize().replace("\n"," ")+"""</spam><br/>""")
				htmlfile.write("""<spam class="entry">"""+texto.capitalize().replace("\n"," ")+"""</spam><br/>""")
				texto = titulo = None
	   		else:
	   			title = line.split(" : ")[0]
	   			value = line.split(" : ")[1]
	   			if title.startswith("entrada"):
	   				htmlfile.write("""<spam class="entry_t"></spam>""")
	   			else:
	   				htmlfile.write("""<spam class="entry_t">"""+title.capitalize().replace("\n", "")+""": </spam>""")	
	   			htmlfile.write("""<spam class="entry_v">"""+value.capitalize().replace("\n", "")+"""</spam><br/>""")
	   			title = value = None
		elif line.startswith("RESPUESTA") and not respuestas:
	   		htmlfile.write("""<spam class="subtitle">RESPUESTAS</spam><br/>""")
	   		respuestas = True
		else:
			if isNotNull(texto) and not respuestas:
				htmlfile.write("""<spam class="entry_t">Descripción: </spam>""")
				htmlfile.write("""<spam class="entry">"""+texto.capitalize().replace("\n"," ")+"""</spam><br/>""")
				texto = None
			if  isNotNull(titulo) and not respuestas:
				htmlfile.write("""<spam class="entry">"""+titulo.capitalize().replace("\n"," ")+"""</spam><br/>""")
				titulo = None	
			if not respuestas:	
	   			htmlfile.write("""<spam class="subtitle">"""+line.upper().replace("_", " ").replace("\n", "")+"""</spam><br/>""")
	htmlfile.close()   		
