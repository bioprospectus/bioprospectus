import MySQLdb
import csv

db = MySQLdb.connect(host="localhost",user="root",passwd="root",db="snomeden")

cur = db.cursor()

cur.execute("SELECT descriptorID, term FROM descriptors")

output = cur.fetchall()

with open("terminos.txt","w") as f:
    csv.register_dialect("custom", delimiter="\t", skipinitialspace=True)
    writer = csv.writer(f, dialect="custom")
    for tup in output:
        writer.writerow(tup)
