#!/bin/bash
mvn clean install -f ../java/
cd ../java/term-mapper/
mvn clean install -Pappassembler
#mongo clef --eval 'db.articles_raw.update({},{$unset: {mappings:""}},{multi: true })'
./target/appassembler/bin/map -l en -t $DATA_DIR/data/snomed/snomedenTermsFiltered.txt -g $DATA_DIR/data/ANCwords.txt --mongodb-url mongodb://localhost:27017/clef.articles_raw --es-url http://localhost:9300 -nt 10
