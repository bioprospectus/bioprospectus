from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk.collocations import TrigramCollocationFinder
from nltk.metrics import TrigramAssocMeasures
from nltk import word_tokenize
from nltk.corpus import stopwords
import MySQLdb
import sys
import codecs

OUT_PATH='/home/alejandro/'

def snomedAllConcepts(mergeDescriptors=False):
	snomedDb = MySQLdb.connect(host="localhost", user="root", passwd="larc182010", db="snomedes")
	cursor = snomedDb.cursor()
	cursor.execute("SELECT conceptid,term FROM descriptors order by conceptid")
	allConcepts = ''
	if mergeDescriptors:
		allDescriptors = ''
		changeConcept = False
		currentConceptId = 0
		for row in cursor.fetchall() :
			if (currentConceptId != int(row[0])):
				allConcepts += allDescriptors.strip().lower()+'\n'
				changeConcept = True
				allDescriptors = ''
			allDescriptors +=row[1].decode('iso-8859-1') + ' '  
			currentConceptId = int(row[0])
		allConcepts += allDescriptors.strip().lower()+'\n'
	else:
		for row in cursor.fetchall() :
			allConcepts += row[1].decode('iso-8859-1').lower()+'\n'
	cursor.close()
	snomedDb.close()
	return allConcepts

def snomedBigrams(num2grams,allConcepts):
	allWords = word_tokenize(allConcepts)
	bigram_finder = BigramCollocationFinder.from_words(allWords,window_size=2)
	stopset = set(stopwords.words('spanish'))
	filter_stops = lambda w: len(w) < 3 or w in stopset
	bigram_finder.apply_word_filter(filter_stops)
	bigram_finder.apply_freq_filter(3)
	bigrams= bigram_finder.nbest(BigramAssocMeasures.likelihood_ratio, num2grams)
	f = codecs.open(OUT_PATH+'snomed-bigrams.txt','w', "utf-8")
	for b in bigrams:
		f.write(b[0] +' '+ b[1]+'\n')
		print b[0] +' '+ b[1]
	f.close()

def snomedTrigrams(num3grams,allConcepts):
	stopset = set(stopwords.words('spanish'))
	filter_stops = lambda w: len(w) < 3 or w in stopset
	allWords = word_tokenize(allConcepts)
	trigram_finder = TrigramCollocationFinder.from_words(allWords)
	trigram_finder.apply_word_filter(filter_stops)
	trigram_finder.apply_freq_filter(3)
	trigrams= trigram_finder.nbest(TrigramAssocMeasures.likelihood_ratio, 100)
	f = codecs.open(OUT_PATH+'snomed-trigrams.txt','w', "utf-8")
	for t in trigrams:
		f.write(t[0] +' '+ t[1]+' '+ t[2]+'\n')
		print t[0] +' '+ t[1]+' '+ t[2]
	f.close()

if __name__ == "__main__":
	mergeDescriptors = True
	allConcepts = snomedAllConcepts(mergeDescriptors)
	snomedBigrams(5000,allConcepts)
	snomedTrigrams(2000,allConcepts)