from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk.collocations import TrigramCollocationFinder
from nltk.metrics import TrigramAssocMeasures
from nltk import word_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords
import MySQLdb
import sys
import codecs


def snomedPOSTags():
	snomedDb = MySQLdb.connect(host="localhost", user="root", passwd="root", db="snomeden")
	cursor = snomedDb.cursor()
	cursor.execute("SELECT conceptid,fullySpecifiedName FROM concepts limit 10")
	for row in cursor.fetchall() :
		term = row[1].decode('iso-8859-1').encode('utf8').lower()
		tokens = word_tokenize(term)
		postags = pos_tag(tokens)
		print term 
		print postags
	cursor.close()
	snomedDb.close()

if __name__ == "__main__":
	snomedPOSTags()