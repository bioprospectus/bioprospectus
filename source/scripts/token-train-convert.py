 #!/usr/bin/python
 # -*- coding: iso-8859-15 -*-
import fileinput
sentence = ''
newSentence = False
for line in fileinput.input('esp.testb'):
	tokens = line.decode('iso-8859-1').split() 
	if newSentence:
		if sentence != '- ':
	   		if sentence.endswith('.'):	
			  print sentence.encode('UTF-8')[:-1] + '<SPLIT>.'
		sentence = ''
		newSentence = False
	if len(tokens) > 0:
		if tokens[0] == '.':
			sentence = sentence.strip() + tokens[0]
		elif tokens[0] == ',':
			sentence = sentence.strip() + '<SPLIT>' + tokens[0] + ' '
		else:
			sentence += tokens[0] + ' '
	else:
		if line == '\n':
			newSentence =True

