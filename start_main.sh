#!/bin/bash

PATH=/opt/java/bin:/opt/java/bin:/usr/local/apache-maven-3.3.9/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games


MY_HOME=/home/bioprospectus

#REST KNOWLEDGE SOURCE
RK_NAME="rest-ks"
RK_MAIN_CLASS="net.lariverosc.jetty.JettyServerMain"
RK_HOME=$MY_HOME/bioprospectus/source/java/rest-knowledge-source
RK_PIDFILE="$MY_HOME/$RK_NAME.pid"

#KBMED_WEB
KB_NAME="kbmed"
KB_MAIN_CLASS="net.lariverosc.kbmed.Main"
KB_HOME="$MY_HOME/bioprospectus/source/java/kbmed-web"
KB_PIDFILE="$MY_HOME/$KB_NAME.pid"

echo "Running rest-knowledge-source..."
cd $RK_HOME
/usr/local/apache-maven-3.3.9/bin/mvn exec:java -Dexec.mainClass=$RK_MAIN_CLASS & echo $!>$RK_PIDFILE
cd $MY_HOME/bioprospectus
  
sleep 10

echo "Running kbmed-web..."
cd $KB_HOME
/usr/local/apache-maven-3.3.9/bin/mvn exec:java -Dexec.mainClass=$KB_MAIN_CLASS & echo $!>$KB_PIDFILE
cd $MY_HOME/bioprospectus
