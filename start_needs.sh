#!/bin/bash
# Elasticsearch parameters
PATH=/opt/java/bin:/opt/java/bin:/home/bioprospectus/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games:/usr/local/apache-maven-3.3.9/bin:/usr/local/apache-maven-3.3.9/bin


ES_HOME=/home/bioprospectus/requirements/elasticsearch-1.7.3
ES_MIN_MEM=256m
ES_MAX_MEM=2g
DAEMON=$ES_HOME/bin/elasticsearch
NAME=elasticsearch
DESC=elasticsearch
PID_FILE=/home/bioprospectus/$NAME.pid
DAEMON_OPTS="-p $PID_FILE -Des.path.home=$ES_HOME"

echo "Running redis-server..."
`/usr/local/bin/redis-server` &
echo "Running webdis..."
`/home/bioprospectus/requirements/webdis/webdis` &
echo "Running elasticsearch"
`/usr/local/bin/start-stop-daemon --start --pidfile $PID_FILE --startas $DAEMON -- $DAEMON_OPTS` &

